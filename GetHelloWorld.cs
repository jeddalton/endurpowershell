using System.Management.Automation;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsCommon.Get, "HelloWorld")]
    public class GetHelloWorld : EndurCmdletBase
    {
        protected override void ProcessRecord()
        {    
            WriteObject("Hello World");
        }
    }
}