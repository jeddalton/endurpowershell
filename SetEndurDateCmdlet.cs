using System;
using System.Management.Automation;

namespace Gazprom.MT.PowerShell.Endur
{
    using System.Linq;

    using Gazprom.MT.PowerShell.Endur.Code;

    using Olf.NetToolkit;
    using Olf.Openrisk.Staticdata;

    [Cmdlet(VerbsCommon.Set, "EndurDate")]
    public class SetEndurDateCmdlet : EndurCmdletBase
    {
        [Parameter(Mandatory = true, Position = 0, ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        public DateTime Date { get; set; }

        [Parameter(Position = 1, ValueFromPipelineByPropertyName = true)]
        public string[] Locations { get; set; }

        [Parameter]
        public SwitchParameter ExcludeMarketDate { get; set; }

        [Parameter]
        public SwitchParameter MarketDateOnly { get; set; }

        protected override void ProcessRecord()
        {
            if (!this.ExcludeMarketDate.IsPresent)
            {
                this.WriteVerbose(string.Format("Setting market date to: {0}", this.Date.ToShortDateString()));
                this.Session.Market.CurrentDate = this.Date;
            }

            if (!this.MarketDateOnly.IsPresent)
            {
                if (this.Locations == null || this.Locations.Length == 0)
                {
                    this.Locations =
                        this.Session.StaticDataFactory.GetReferenceChoices(EnumReferenceTable.TradingLocation)
                            .Select(
                                l =>
                                {
                                    var name = l.Name;
                                    l.Dispose();

                                    return name;
                                }).Union(new[] { "Global" }).ToArray();
                }

                var julianDate = this.Session.CalendarFactory.GetJulianDate(this.Date);
                this.Locations.ForEach(
                    l =>
                    {
                        var locationId = l.Equals("Global", StringComparison.InvariantCultureIgnoreCase)
                                             ? 0
                                             : this.Session.StaticDataFactory.GetId(
                                                 EnumReferenceTable.TradingLocation,
                                                 l);

                        this.WriteVerbose(
                            string.Format(
                                "Setting date for location {0} to {1}",
                                this.GetLocationName(locationId),
                                this.Date.ToShortDateString()));

                        var result = EndOfDay.RollToDates(
                            julianDate,
                            julianDate,
                            julianDate,
                            1,
                            julianDate,
                            locationId);
                        if (result != 1)
                        {
                            this.WriteWarning(
                                string.Format(
                                    "Failed to set date for location {0} to {1}",
                                    l,
                                    this.Date.ToShortDateString()));
                        }
                    });
            }
        }

        private string GetLocationName(int locationId)
        {
            return locationId == 0
                       ? "Global"
                       : this.Session.StaticDataFactory.GetName(
                           EnumReferenceTable.TradingLocation,
                           locationId);
        }
    }
}