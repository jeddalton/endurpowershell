﻿namespace Gazprom.MT.PowerShell.Endur
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Management.Automation;

    using Olf.Openrisk.Table;

    using TradeVolume.ServiceContracts;

    [Cmdlet(VerbsCommon.Set, "EndurTradeVolume")]
    public class SetEndurTradeVolumeCmdlet : EndurCmdletBase
    {
        [Alias("Vols")]
        [Parameter(Mandatory = false, ParameterSetName = "Volumes", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true, Position = 0)]
        public List<VolumeEntry> VolumeEntries { get; set;}

        [Alias("Url")]
        [Parameter(Mandatory = true,  Position = 1)]
        public string EnvironmentUrl { get; set; }

        [Alias("U")]
        [Parameter(Mandatory = true, Position = 2)]
        public string User { get; set; }
        
        [Parameter(Mandatory = false, Position = 3)]
        public VolumeTypeOptions VolumeType { get; set; }


        protected override void ProcessRecord()
        {
            if (VolumeEntries != null)
            {
                UploadResult result = null;
                var client = new TradeVolumeClient(new Uri(EnvironmentUrl), User);
                try
                {
                    DealVolumeType dvt = DealVolumeType.NotAllowed;
                    switch (this.VolumeType)
                    {
                        case VolumeTypeOptions.PerTradeSetup: dvt = DealVolumeType.NotAllowed; break;
                        case VolumeTypeOptions.Hourly: dvt = DealVolumeType.Hourly; break;
                        case VolumeTypeOptions.Daily: dvt = DealVolumeType.Daily; break;
                    }
                    result = client.SetTradeVolumesByVolumeType(this.VolumeEntries, dvt);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                WriteObject(result);
            }
            else
            {
                Console.WriteLine("Set-EndurVolumeEntry Error: Delivered 'VolumeEntries' object is null");
                WriteObject(null);
            }
        }
    }
}