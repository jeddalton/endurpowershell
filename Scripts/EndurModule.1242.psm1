﻿function Invoke-EndurSqlCmd
{
	[CmdletBinding(DefaultParameterSetName="QuerySet")]
	Param
	(
		[Parameter(Mandatory=$true,ParameterSetName="QuerySet",ValueFromPipeline=$true)]
		[string] $Query,

		[Parameter(Mandatory=$true,ParameterSetName="FileSet",ValueFromPipeline=$true)]
		[string] $InputFile
	)
	Process
	{
		if ($EndurSession -eq $null)
		{
			throw "There is no valid Endur session"
		}

		$currentLocation = (Get-Location -PSProvider FileSystem).ProviderPath

		switch ($PsCmdlet.ParameterSetName) 
		{ 
			"QuerySet"  { Invoke-Sqlcmd -ServerInstance $EndurSession.ServerName -Database $EndurSession.DatabaseName -Query $Query; break} 
			"FileSet"  
			{ 
				if (-not (Test-Path $InputFile))
				{
					$InputFile = [System.IO.Path]::Combine($currentLocation, $InputFile)
				}
				Invoke-Sqlcmd -ServerInstance $EndurSession.ServerName -Database $EndurSession.DatabaseName -InputFile $InputFile
				break
			} 
		} 		

	}
}

function EndurDeleteHoldingInstrumentByContractCode
(
	[string[]] $ContractCodes
)
{
	# First clear the transactions
	Get-EndurQueryResult -Fields @{"Ins Bond/Fut.Contract Code"=$contractCodes} | Clear-EndurTransaction

	# Now clear the actual holding instruments
	Get-EndurQueryResult -Fields @{"Ins Bond/Fut.Contract Code"=$contractCodes;"Transaction.Tran Type"="Holding"} | Clear-EndurTransaction
}

function EndurAssemblyList
{
	Get-ChildItem -Filter "*.dll" | Where {!$_.Name.StartsWith("Olf.Openrisk.Net") -and !$_.Name.Contains("Test")}
}

function EndurConnectionString
{
	[String]::Format("Data Source={0};Initial Catalog={1};Integrated Security=SSPI", $EndurServerName, $EndurDatabaseName)
}

function CompareXlsDataToSqlQueryResult
{
	[CmdletBinding()]
	PARAM
	(
		[Parameter(Mandatory=$true,ValueFromPipeline=$true)]
		[string]$SqlFile,
		
		[Parameter(Mandatory=$true,ValueFromPipeline=$true)]
		[string]$XlsFile,
		
		[Parameter(Mandatory=$true,ValueFromPipeline=$true)]
		[string]$XlsSheet,
		
		[Parameter(Mandatory=$false,ValueFromPipeline=$true)]
		[string[]]$Keys,
		
		[Parameter(Mandatory=$false,ValueFromPipeline=$true)]
		[string[]]$Exclude,
		
		[Parameter(Mandatory=$true,ValueFromPipeline=$true)]
		[string]$Comment
	)
	PROCESS
	{
		if( ([System.IO.File]::Exists($SqlFile) -eq $false) -or
			([System.IO.File]::Exists($XlsFile) -eq $false)
		){ throw ("File does not exists" ) }
		else{
			$sql = [System.IO.File]::ReadAllText($SqlFile)
			$endurConn = $endurConn = EndurConnectionString 
			$result = @(Compare-Data -LeftConnectionString $endurConn -RightConnectionString $XlsFile -LeftQuery $sql -RightQuery $XlsSheet -EmptyStringIsNull -Keys $Keys -Excludes $Exclude)
			$testresult = Test-AreEqual -Expected 0 -Actual $result.Count -Comment $Comment

			return $testresult
		}
	}
}

function CompareXlsDataFiles
{
	[CmdletBinding()]
	PARAM
	(
		[Parameter(Mandatory=$true,ValueFromPipeline=$true)]
		[string]$leftFile,
		
		[Parameter(Mandatory=$true,ValueFromPipeline=$true)]
		[string]$lefSheet,
		
		[Parameter(Mandatory=$true,ValueFromPipeline=$true)]
		[string]$rightFile,
		
		[Parameter(Mandatory=$true,ValueFromPipeline=$true)]
		[string]$rightSheet,

		[Parameter(Mandatory=$true,ValueFromPipeline=$true)]
		[string[]]$Keys,
		
		[Parameter(Mandatory=$false,ValueFromPipeline=$true)]
		[string[]]$Exclude,
		
		[Parameter(Mandatory=$true,ValueFromPipeline=$true)]
		[string]$Comment
	)
	PROCESS
	{
		if( ([System.IO.File]::Exists($leftFile) -eq $false) -or
			([System.IO.File]::Exists($rightFile) -eq $false)
		){ throw ("File does not exists" ) }
		else{
			$result = @(Compare-Data -LeftConnectionString $leftFile -RightConnectionString $rightFile -LeftQuery $lefSheet -RightQuery $rightSheet -EmptyStringIsNull -Keys $Keys  -Excludes $Exclude)
			$testresult = Test-AreEqual -Expected 0 -Actual $result.Count -Comment $Comment

			return $testresult
		}
	}
}