using System;
using System.Management.Automation;
using Gazprom.MT.PowerShell.Endur.Code;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsCommon.Get, "EndurFieldValue")]
    public class GetEndurFieldValueCmdlet : EndurFieldValueCmdletBase
    {
        protected override void ProcessRecord()
        {
            using (var dc = new DisposableCollection())
            {
                if (this.ParameterSetName == "TransactionId")
                {
                    this.Transaction = dc.Add(this.Session.TradingFactory.RetrieveTransactionById(this.TransactionId));
                }

                var tranfFieldId = this.GetTranfFieldId();
                if (tranfFieldId == 0)
                {
                    throw new InvalidOperationException(string.Format("Could not find field named/with ID {0}", this.Field));
                }

                var field = dc.Add(GetField(tranfFieldId));

                this.WriteObject(FieldSetter.GetFieldValue(field));
            }
        }
    }
}