﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management.Automation;
using System.Text;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsLifecycle.Invoke, "EndurBrokerFeeProcess")]
    public class InvokeEndurBrokerFeeProcess : EndurCmdletBase
    {
        #region avs
        private const string AvsScript = @"{{   
	                                                brokerFeesTbl = TABLE_New ();
	                                                retVal = BROKER_GetProvisionals ( {0}, brokerFeesTbl);
	                                                TABLE_AddCol(returnt, ""error_message"", COL_STRING);
	                                                if (retVal != OLF_RETURN_SUCCEED)
	                                                {{
	                                                   TABLE_Destroy(brokerFeesTbl);
	                                                   TABLE_SetString(returnt, 1, TABLE_AddRow(returnt), ""Failed to load Broker Table"");
	                                                   EXIT_Succeed();
	                                                }}
	                                                numRows = TABLE_GetNumRows(brokerFeesTbl);
	                                                if(numRows == 0)
	                                                {{
	                                                   TABLE_Destroy(brokerFeesTbl);
	                                                   TABLE_SetString(returnt, 1, TABLE_AddRow(returnt), ""No broker fees for QueryId [{0}]"");
	                                                   EXIT_Succeed();
	                                                }}
	                                                else
	                                                {{
                                                        for(i = 1; i <= numRows; i++)
                                                        {{
                                                            if(TABLE_GetIntN(brokerFeesTbl, ""prov_status"", i) == PAID_PROV_STATUS)
                                                            {{
                                                                TABLE_Destroy(brokerFeesTbl);
                                                                TABLE_SetString(returnt, 1, TABLE_AddRow(returnt), ""Broker Fee table contains at least one fee that has paid status for QueryId [{0}]"");
                                                                EXIT_Succeed();  
                                                            }}
                                                        }}

	                                                    distinct = TABLE_New();
	                                                    process = TABLE_CloneTable(brokerFeesTbl);
	                                                    TABLE_Select(distinct, brokerFeesTbl, ""DISTINCT, portfolio, ins_type, bunit, broker_id, prov_type, currency"", ""tran_num GT 0"");
			                                            numRows = TABLE_GetNumRows(distinct);
			                                            for(i = 1; i <= numRows; i++)
			                                            {{   
				                                            portfolio = TABLE_GetIntN(distinct, ""portfolio"", i);
				                                            ins_type = TABLE_GetIntN(distinct, ""ins_type"", i);
				                                            bunit = TABLE_GetIntN(distinct, ""bunit"", i);
				                                            broker_id = TABLE_GetIntN(distinct, ""broker_id"", i);
				                                            prov_type = TABLE_GetIntN(distinct, ""prov_type"", i);
				                                            ccy = TABLE_GetIntN(distinct, ""currency"", i);
	
				                                            TABLE_Select(process, brokerFeesTbl, ""*"", ""portfolio EQ "" + portfolio 
															                                             + "" AND ins_type EQ "" + ins_type
															                                             + "" AND bunit EQ "" + bunit
															                                             + "" AND broker_id EQ "" + broker_id
															                                             + "" AND prov_type EQ "" + prov_type
															                                             + "" AND currency EQ "" + ccy);
	
				                                            message = ""\nPortfolio:   "" + REF_GetName(PORTFOLIO_TABLE,  portfolio) + 
					                                              ""\nInstrument Type:   "" + REF_GetName(INSTRUMENTS_TABLE,  ins_type) + 
					                                              ""\nInternal Bunit:   "" + REF_GetName(PARTY_TABLE,  bunit) + 
					                                              ""\nBroker:   "" + REF_GetName(PARTY_TABLE,  broker_id) + 
					                                              ""\nProv Type:   "" + REF_GetName(PROV_TYPE_TABLE,  prov_type) + 
					                                              ""\nCurrency:   "" + REF_GetName(CURRENCY_TABLE,  ccy);
				                                            print(""\nProcessBrokerFeesForQuery: Processing "" + TABLE_GetNumRows(process) + "" fees for following criteria\n"" + message);
	
				                                            if ( BROKER_ProcessBrokerFees (process) != OLF_RETURN_SUCCEED )
				                                            {{
				                                               print ( ""\nBROKER_ProcessBrokerFees() failed for criteria "" + message);
				                                               TABLE_SetString(returnt, 1, TABLE_AddRow(returnt), ""BROKER_ProcessBrokerFees() failed for criteria "" + message);
				                                               TABLE_ClearDataRows(process);
				                                               continue; 
				                                            }}            
	
				                                            TABLE_ClearDataRows(process);
			                                            }}
	                                                    TABLE_Destroy(process);
	                                                    TABLE_Destroy(distinct); 
	                                                }}
													TABLE_Destroy(brokerFeesTbl);
	                                            }}";
        #endregion

        [Parameter(Mandatory = true, Position = 0)]
        public int QueryResultId { get; set; }

        private List<int> _queryResultIds;

        protected override void BeginProcessing()
        {
            this._queryResultIds = new List<int>();
        }

        protected override void ProcessRecord()
        {
            this._queryResultIds.Add(this.QueryResultId);
        }

        protected override void EndProcessing()
        {
            foreach (var queryId in _queryResultIds)
            {
                using (var returnt = Session.ControlFactory.RunScriptText(String.Format(AvsScript, queryId)))
                {
                    if (returnt == null || returnt.RowCount <= 0) continue;
                    var sb = new StringBuilder();
                    for (var i = 0; i < returnt.RowCount; i++)
                        sb.Append(returnt.GetString(0, i));

                    throw new Exception(String.Format("Failed to create broker cash:\n{0}", sb));
                }
            }
        }
    }
}
