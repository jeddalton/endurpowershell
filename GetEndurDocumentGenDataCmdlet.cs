using System;
using System.Management.Automation;
using Gazprom.MT.PowerShell.Endur.Code;
using Olf.Openrisk.BackOffice;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsCommon.Get, "EndurDocumentGenData")]
    public class GetEndurDocumentGenDataCmdlet : EndurCmdletBase
    {
        [Parameter(Mandatory = true, Position = 0)]
        public Int32 DocumentNumber { get; set; }

        [Parameter(Mandatory = false, Position = 1)]
        public string[] IncludeCols { get; set; }

        [Parameter(Mandatory = false, Position = 2)]
        public string[] ExcludeCols { get; set; }

        [Parameter]
        public Int32 Version { get; set; }

        [Parameter]
        public SwitchParameter TableAsXml { get; set; }

        public GetEndurDocumentGenDataCmdlet()
        {
            this.Version = -1;
        }

        protected override void ProcessRecord()
        {
            using (var dc = new DisposableCollection())
            {
                var document = dc.Add(GetDocument());
                var generationDataType = dc.Add(this.Session.StaticDataFactory.GetReferenceObject<DocumentDataType>("Generation Data"));
                var generationData = dc.Add(document.LoadDocumentDataTable(generationDataType));
                
                if (IncludeCols != null)
                {
                    var columnNames = String.Join(",", this.IncludeCols);
                    generationData.Sort(new[] {"col_name", "col_type", "doc_table"}, true);
                    var filteredGenData = dc.Add(generationData.CreateView(columnNames));
                    WriteObject(EndurPowershellUtil.TableToPSObjects(filteredGenData, TableAsXml.IsPresent), true);
                }

                else if (ExcludeCols != null)
                {
                    var rowExpression = String.Format("[col_name] != '{0}' ",
                                                      String.Join("' AND [col_name] != '", ExcludeCols));
                    var filteredGenData = dc.Add(generationData.CreateView("*", rowExpression));
                    filteredGenData.Sort(new[] { "col_name", "col_type", "doc_table" }, true);
                    WriteObject(EndurPowershellUtil.TableToPSObjects(filteredGenData, TableAsXml.IsPresent), true);
                }
                else
                {
                    generationData.Sort(new[] { "col_name", "col_type", "doc_table" }, true);
                    WriteObject(EndurPowershellUtil.TableToPSObjects(generationData, TableAsXml.IsPresent), true);
                }


            }
        }

        private Document GetDocument()
        {
            return this.Version==-1 ? this.Session.BackOfficeFactory.RetrieveDocument(this.DocumentNumber) : this.Session.BackOfficeFactory.RetrieveDocument(this.DocumentNumber, this.Version);
        }
    }
}