﻿using System;
using System.IO;
using System.Management.Automation;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsCommon.Set, "EndurCrystalTemplate")]
    public class SetEndurCrystalTemplate : EndurCmdletBase
    {
        [Parameter(Mandatory = true, Position = 0, ValueFromPipeline = true)]
        public FileInfo File { get; set; }

        [Parameter(Mandatory = true, Position = 1)]
        public String DirNodePath { get; set; }

        protected override void ProcessRecord()
        {
            WriteVerbose($"Importing crystal template '{this.File.FullName}'");
            if (Externals.oCRYSTAL_ImportTemplateFileToDB(this.File.FullName, this.DirNodePath) != 1)
            {
             throw new InvalidOperationException($"Error importing crystal template '{this.File.FullName}' to '{this.DirNodePath}'");   
            }
        }
    }
}