﻿namespace Gazprom.MT.PowerShell.Endur
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Management.Automation;

    using Olf.Openrisk.Table;

    using TradeValidator.ServiceContracts;

    [Cmdlet(VerbsCommon.Set, "TradeValidatorValidate")]
    public class SetTradeValidatorValidateCmtlet : EndurCmdletBase
    {
        [Parameter(Mandatory = true, ParameterSetName = "TransactionId", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true, Position = 0)]
        public Int32 TransactionId { get; set; }

        [Alias("Url")]
        [Parameter(Mandatory = true,  Position = 1)]
        public string EnvironmentUrl { get; set; }

        [Alias("U")]
        [Parameter(Mandatory = true, Position = 2)]
        public string User { get; set; }

        [Parameter(Mandatory = false, Position = 3)] 
        public string AmendStrategy { get; set; }

        [Parameter(Mandatory = false, Position = 4)] 
        public string AmendUTI { get; set; }

        [Parameter(Mandatory = false, Position = 5)]
        public string AmendInternalBusinessUnit { get; set; }

        [Parameter(Mandatory = false, Position = 6)]
        public string AmendInternalContact { get; set; }

        [Parameter(Mandatory = false, Position = 7)]
        public string AmendInternalPortfolio { get; set; }

        [Parameter(Mandatory = false, Position = 8)]
        public string AmendSpread { get; set; }

        [Parameter(Mandatory = false, Position = 9)]
        public string AmendEfp { get; set; }

        [Parameter(Mandatory = false, Position = 10)]
        public string AmendSleeve { get; set; }

        [Parameter(Mandatory = false, Position = 11)]
        public int SetVersion { get; set; }

        [Parameter(Mandatory = false, Position = 12)]
        public int SetDealNum { get; set; }

        [Parameter(Mandatory = false, Position = 13)]
        public int SetTranNum { get; set; }

        [Parameter(Mandatory = false, Position = 14)]
        public string IntercompanyBU { get; set; }        

        protected override void ProcessRecord()
        {
            this.WriteVerbose("Version is " + this.SetVersion);

            List<string> sqlStoredProcToRun = new List<string>();
            sqlStoredProcToRun.Add("GetFuturesDeals");
            sqlStoredProcToRun.Add("GetPhysFixedDeals");
            sqlStoredProcToRun.Add("GetPhysIndexedDeals");
            sqlStoredProcToRun.Add("GetFxOptionDeals");
            sqlStoredProcToRun.Add("GetFxOutrightDeals");
            sqlStoredProcToRun.Add("GetExchangeOptionDeals");
            sqlStoredProcToRun.Add("GetOtcOptionDeals");
            sqlStoredProcToRun.Add("GetComSwapDeals"); 
             
            var client = new TradeValidatorClient(new Uri(EnvironmentUrl), User);
            this.WriteVerbose(string.Format("Trying to get the trade {0}", TransactionId.ToString(CultureInfo.InvariantCulture)));
            var list = new List<TradeData>();

            foreach (string sqlStoredProc in sqlStoredProcToRun){
                try
                {
                    using (var table = this.Session.IOFactory.RunSQL(string.Format("exec [trade].[{0}] @tran_status=2", sqlStoredProc)))
                    {
                        this.LoadTradesBySql(table, list);
                        if (list.Count > 0)
                        {
                            WriteVerbose("Transaction " + TransactionId + " listed in '" + sqlStoredProc + "'.");
                            break;
                        }
                    }
                }
                catch (Exception e)
                {
                    if (e.Message.Equals(string.Format("Failed to execute SQL: exec [trade].[{0}] @tran_status=2\nFatal Database Error:\nInternal Object missing from Database, See Your Administrator", sqlStoredProc)))
                    {
                        WriteVerbose("WARNING: " + sqlStoredProc + " missing from Database");
                    }
                    else
                    {
                        WriteVerbose(e.Message);
                        throw e;
                    }
                }
            }
          
            var result = client.ValidateTrades(list);
            foreach (var res in result.Trades)
            {
                this.WriteVerbose(
                    string.Format(
                        "Message Validated TranNum: {0}, Success: {1}, Status : {2}, Error {3}",
                        res.TranNum,
                        res.Success,
                        res.DisplayMessage,
                        res.ErrorDetail));
                this.WriteObject(res);
            }
            this.WriteObject(result, true);
        }

        private void LoadTradesBySql(Table table, List<TradeData> list)
        {
            var records = Enumerable.Range(0, table.RowCount).Select(
                                r => new TradeData()
                                {
                                    DealNum             = this.SetDealNum == 0 ? table.GetInt("DealNum", r) : this.SetDealNum,
                                    TranNum             = this.SetTranNum == 0 ? table.GetInt("TranNum", r) : this.SetTranNum,
                                    InternalBunit       = this.AmendInternalBusinessUnit == null ? table.GetString("InternalBusinessUnit", r) : this.AmendInternalBusinessUnit,
                                    Version             = this.SetVersion == 0 ? table.GetInt("Version", r) : this.SetVersion,
                                    InternalContact     = this.AmendInternalContact == null ? (table.ColumnNames.Contains("Trader") ? table.GetString("Trader", r) : null) : this.AmendInternalContact,
                                    InternalPortfolio   = this.AmendInternalPortfolio == null ? (table.ColumnNames.Contains("InternalPortfolio") ? table.GetString("InternalPortfolio", r) : null) : this.AmendInternalPortfolio,
                                    Strategy            = this.AmendStrategy == null ? (table.ColumnNames.Contains("Strategy") ? table.GetString("Strategy", r) : null) : this.AmendStrategy,
                                    UTI                 = this.AmendUTI == null ? (table.ColumnNames.Contains("UTI") ? table.GetString("UTI", r) : null) : this.AmendUTI,
                                    Spread              = this.AmendSpread == null ? (table.ColumnNames.Contains("Spread") && table.GetString("Spread", r) == "Yes") : (this.AmendSpread == "Yes"),
                                    Efp                 = this.AmendEfp == null ? (table.ColumnNames.Contains("Efp") && table.GetString("Efp", r) == "Yes") : (this.AmendSpread == "Yes") ,
                                    Sleeve              = this.AmendSleeve == null ? (table.ColumnNames.Contains("Sleeve") && table.GetString("Sleeve", r) == "Yes") : (this.AmendSleeve == "Yes")
                               //     IntercompanyBunit   = this.IntercompanyBU //Uncomment for new TradeValidator after PBI 105151 has been released and new nuget in proget
                                })
                            .Where(r => r.TranNum == TransactionId);
            list.AddRange(records);
        }
    }

}
