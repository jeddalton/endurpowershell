using System.IO;
using System.Management.Automation;
using Gazprom.MT.PowerShell.Endur.Code;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsCommon.Get, "EndurAssembly")]
    public class GetEndurAssemblyCmdlet : EndurCmdletBase
    {
        protected override void ProcessRecord()
        {
            WriteObject(OlfPackageWrapper.Create().GetAssemblies(), true);
        }
    }
}