using System.Management.Automation;
using Olf.Openrisk.Application;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsLifecycle.Unregister, "EndurSession")]
    public class UnregisterEndurSessionCmdlet : EndurCmdletBase
    {
        protected override void ProcessRecord()
        {
            var application = Application.Instance;
            application.Detach();

            this.Session.Dispose();
        }
    }
}