﻿using System;
using System.Management.Automation;
using Gazprom.MT.PowerShell.Endur.Code;
using Olf.Openrisk.Trading;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsCommon.Set, "EndurTransactionLock")]
    public class SetEndurTransactionLock : EndurCmdletBase
    {
        [Parameter(Mandatory = true, ParameterSetName = "TransactionId", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true, Position = 0)]
        public Int32 TransactionId { get; set; }

        [Parameter(Mandatory = true, ParameterSetName = "Transaction", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true, Position = 0)]
        public Transaction Transaction { get; set; }

        [Parameter]
        public SwitchParameter PassThru { get; set; }

        protected override void ProcessRecord()
        {
            using (var dc = new DisposableCollection())
            {
                if (this.ParameterSetName == "TransactionId")
                {
                    this.Transaction = dc.Add(this.Session.TradingFactory.RetrieveTransactionById(this.TransactionId));
                }

                if (!this.Transaction.IsLocked)
                {
                    this.Transaction.Lock(EnumDealLock.Permanent);
                }

                if (this.PassThru.IsPresent)
                {
                    if (this.ParameterSetName == "TransactionId")
                    {
                        this.WriteObject(this.Transaction.TransactionId);
                    }
                    else
                    {
                        this.WriteObject(this.Transaction);
                    }
                }
            }
        }
    }
}