using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Management.Automation;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsCommon.New, "EndurWorkflowJob")]
    public class NewEndurWorkflowJobCmdlet : EndurCmdletBase
    {
        [Parameter(Mandatory = true, ParameterSetName = "WorkflowId", Position = 0, ValueFromPipeline = true)]
        public int WorkflowId { get; set; }

        [Parameter(Mandatory = true, Position = 1)]
        public int Sequence { get; set; }

        [Parameter(Mandatory = true, Position = 2)]
        public string TaskName { get; set; }


        protected override void ProcessRecord()
        {
            var jobId = Session.GetNewWorkflowJobId();
            var taskId = GetTaskId(TaskName);
            var workflowSequence = GetWorkflowSequence(WorkflowId);

            var parameters = new Dictionary<string, string>
            {
                {"job_id", $"{jobId}"},
                {"wflow_id", $"{WorkflowId}"},
                {"type", "4"},
                {"sub_id", $"{taskId}"},
                {"name", "' '"},
                {"sequence", $"{Sequence}"},
                {"personnel_type", "0"},
                {"run_site", "0"},
                {"max_run", "0"},
                {"exc_type", "0"},
                {"exc_id", "0"},
                {"what_next", "0"},
                {"owner", "0"},
                {"scope", "0"},
                {"schedule_0", "0"},
                {"schedule_1", "0"},
                {"schedule_2", "5"},
                {"schedule_3", "1633428"},
                {"schedule_4", "184613856"},
                {"schedule_5", "1"},
                {"schedule_6", "168403224"},
                {"schedule_7", "4"},
                {"subscription", "' '"},
                {"wflow_sequence", $"{workflowSequence}"},
                {"pin_flag", "0"},
                {"service_type", "1"},
                {"service_group_type", "0"},
                {"job_run_site", "-2"},
                {"grid_scheduler", "0"},
                {"rerun_counter", "0"},
                {"script_log", "1"},
                {"user_id", $"{Session.User.Id}"},
                {"wflow_mgr_cat_id", "0"}
            };

            var parametersString = string.Join(",\n", parameters.Select(kvp => $"@{kvp.Key}={kvp.Value}"));

            using (Session.IOFactory.RunSQL($"EXEC insert_job_cfg\n{parametersString}"))
            {
            }

            Session.SendJobCfgChanged();

            WriteObject(WorkflowId);
        }

        private int GetTaskId(string taskName)
        {
            var taskSql = $"SELECT task_id FROM avs_task_def_view WHERE name='{taskName}'";

            using (var result = Session.IOFactory.RunSQL(taskSql))
            {
                if (result.RowCount == 0)
                {
                    throw new IOException($"Cannot find task named '{taskName}'");
                }

                return result.GetInt(0, 0);
            }
        }

        private int GetWorkflowSequence(int workflowId)
        {
            var sql = "SELECT jc.wflow_sequence \r\n" + "FROM job_cfg jc \r\n" + $"WHERE jc.wflow_id={workflowId} \r\n" + "AND jc.type=0 \r\n";

            using (var result = Session.IOFactory.RunSQL(sql))
            {
                if (result.RowCount == 0)
                {
                    throw new InvalidOperationException($"Cannot find workflow with ID {workflowId}");
                }

                return result.GetInt(0, 0);
            }
        }
    }
}