﻿
namespace Gazprom.MT.PowerShell.Endur
{
    using System;
    using System.Management.Automation;

    using Olf.NetToolkit;

    [Cmdlet(VerbsCommon.Get, "EndurNtkTransaction", DefaultParameterSetName = "TransactionId")]
    public class GetEndurNtkTransactionCmdlet : EndurCmdletBase
    {
        [Parameter(Mandatory = true, ValueFromPipeline = true, ValueFromPipelineByPropertyName = true, ParameterSetName = "TransactionId")]
        public Int32 TransactionId { get; set; }

        [Parameter(Mandatory = true, ParameterSetName = "RetrieveCopy")]
        public SwitchParameter Copy { get; set; }

        protected override void ProcessRecord()
        {
            WriteVerbose(String.Format("Attempting to retrieve transaction with tran num {0}", this.TransactionId));
            var tran = Transaction.Retrieve(this.TransactionId, this.Copy.IsPresent ? 1 : 0);
            if (Transaction.IsNull(tran) == 1)
            {
                WriteWarning(String.Format("Transaction with tran num {0} not found", this.TransactionId));
            }

            this.WriteObject(tran);
        }
    }
}
