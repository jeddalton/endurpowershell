using System;
using System.Management.Automation;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsData.Export, "EndurSimulationResult")]
    public class ExportEndurSimulationResultCmdlet : EndurCmdletBase
    {
        [Parameter]
        public Int32 SimulationRunId { get; set; }

        protected override void ProcessRecord()
        {
            using (var result = this.Session.SimulationFactory.RetrieveSimulationResults(this.SimulationRunId))
            {
                this.WriteVerbose("Extracting simulation results");
                result.ExtractResults();
            }
        }
    }
}