using System;
using System.Management.Automation;
using Olf.Openrisk.Table;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsData.Import, "EndurScript")]
    public class ImportEndurScriptCmdlet : EndurCmdletBase
    {
        private const String Script =
            "void main() \r\n" +
            "{ \r\n" +
            "   int result; \r\n" +
            "   string filename = TABLE_GetStringN(argt, \"script_file\", 1); \r\n" +
            "   string script_name = TABLE_GetStringN(argt, \"script_file\", 1); \r\n" +
            "   TABLE_AddCol(returnt, \"result\", COL_INT); \r\n" +
            "   result = SCRIPT_Import(filename, script_name, 1); \r\n" +
            "   TABLE_AddRow(returnt); \r\n" +
            "   TABLE_SetIntN(returnt, \"result\", 1, result); \r\n" +
            "} \r\n";

        [Parameter(Mandatory = true, Position = 0)]
        public String ScriptFile { get; set; }

        [Parameter(Mandatory = true, Position = 1)]
        public String ScriptName { get; set; }

        protected override void ProcessRecord()
        {

            using (var table = this.Session.TableFactory.CreateTable())
            {
                table.AddColumn("script_file", EnumColType.String);
                table.AddColumn("script_name", EnumColType.String);
                table.AddRow();
                table.SetString("script_file", 0, this.ScriptFile);
                table.SetString("script_name", 0, this.ScriptName);

                using (var result = this.Session.ControlFactory.RunScriptText(Script, table))
                {
                    var resultCode = result.GetInt("result", 0);
                    if (resultCode <= 0)
                    {
                        throw new PSInvalidOperationException("Endur Import Script failed");
                    }

                    WriteObject(resultCode);
                }
            }
        }
    }
}