using System;
using System.Management.Automation;
using Olf.Openrisk.Trading;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsCommon.Get, "EndurTransaction", DefaultParameterSetName = "TransactionId")]
    public class GetEndurTransactionCmdlet : EndurCmdletBase
    {
        [Parameter(Mandatory = true, ValueFromPipeline = true, ValueFromPipelineByPropertyName = true, ParameterSetName = "TransactionId")]
        public Int32 TransactionId { get; set; }

        [Parameter(Mandatory = true, ValueFromPipelineByPropertyName = true, ParameterSetName = "Reference")]
        public String Reference { get; set; }

        [Parameter(Mandatory = true, ValueFromPipelineByPropertyName = true, ParameterSetName = "DealNumber")]
        public Int32 DealNumber { get; set; }

        [Parameter(ParameterSetName = "Reference")]
        public String Status { get; set; }

        protected override void ProcessRecord()
        {
            switch (this.ParameterSetName)
            {
                case "TransactionId":
                    {
                        WriteVerbose(String.Format("Attempting to retrieve transaction with tran num {0}", this.TransactionId));
                        var trans = Session.TradingFactory.RetrieveTransactionById(this.TransactionId);
                        if (trans == null)
                        {
                            WriteWarning(String.Format("Transaction with tran num {0} not found", this.TransactionId));
                        }

                        this.WriteObject(trans);
                    }

                    break;

                case "Reference":
                    if (!String.IsNullOrEmpty(this.Status))
                    {
                        var tranStatus = (EnumTranStatus)Enum.Parse(typeof(EnumTranStatus), this.Status);
                        WriteObject(this.Session.TradingFactory.RetrieveTransactionByReference(this.Reference, tranStatus));
                    }
                    else
                    {
                        using (var trans1 = this.Session.TradingFactory.RetrieveTransactionByReference(this.Reference))
                        {
                            var dealTrackingId = trans1.DealTrackingId;
                            WriteObject(this.Session.TradingFactory.RetrieveTransactionByDeal(dealTrackingId));
                        }
                    }

                    break;

                case "DealNumber":
                    WriteObject(this.Session.TradingFactory.RetrieveTransactionByDeal(this.DealNumber));
                    break;
            }
        }
    }
}