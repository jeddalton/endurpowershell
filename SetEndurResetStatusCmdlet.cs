﻿using System;

namespace Gazprom.MT.PowerShell.Endur
{
    using System.Management.Automation;

    using Gazprom.MT.PowerShell.Endur.Code;

    using Olf.Openrisk.Trading;

    [Cmdlet(VerbsCommon.Set, "EndurResetStatus")]
    public class SetEndurResetStatusCmdlet : EndurCmdletBase
    {
        [Parameter(Mandatory = true, ParameterSetName = "TransactionId", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true, Position = 0)]
        public Int32 TransactionId { get; set; }

        [Parameter(Mandatory = true, ParameterSetName = "Transaction", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true, Position = 0)]
        public Transaction Transaction { get; set; }

        [Parameter(Mandatory = true, ValueFromPipeline = true, ValueFromPipelineByPropertyName = true, Position = 1, ParameterSetName = "TransactionId")]
        [Parameter(Mandatory = true, ValueFromPipeline = true, ValueFromPipelineByPropertyName = true, Position = 1, ParameterSetName = "Transaction")]
        
        [Parameter]
        public EnumResetMaintenance ResetMaintenance { get; set; }

        [Parameter]
        public SwitchParameter PassThru { get; set; }

        protected override void ProcessRecord()
        {
            using (var dc = new DisposableCollection())
            {
                if (this.ParameterSetName == "TransactionId")
                {
                    this.Transaction = dc.Add(this.Session.TradingFactory.RetrieveTransactionById(this.TransactionId));
                }

                switch (ResetMaintenance)
                {
                    case EnumResetMaintenance.RegenerateResets:
                        this.Transaction.Regenerate(EnumRegen.Reset);
                        break;
                    case EnumResetMaintenance.FixPriorResets:
                        this.Transaction.FixPriorResets();
                        break;
                }

                // if holding ins then authorize
                if ((int)this.Transaction.TransactionType == 2)
                {
                    this.Transaction.Instrument.Authorize();
                }
                else
                {
                    this.Transaction.Process(EnumTranStatus.Validated);
                }

                if (this.PassThru.IsPresent)
                {
                    if (this.ParameterSetName == "TransactionId")
                    {
                        this.WriteObject(this.Transaction.TransactionId);
                    }
                    else
                    {
                        this.WriteObject(this.Transaction);
                    }
                }
            }
        }

        public enum EnumResetMaintenance
        {
            RegenerateResets, FixPriorResets
        }
    }
}
