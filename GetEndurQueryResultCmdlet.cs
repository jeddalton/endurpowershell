using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Management.Automation;
using Olf.Openrisk.IO;
using Olf.Openrisk.Table;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsCommon.Get, "EndurQueryResult", DefaultParameterSetName = "FieldSet")]
    public class GetEndurQueryResultCmdlet : EndurCmdletBase
    {
        public GetEndurQueryResultCmdlet()
        {
            QueryType = EndurQueryType.Transaction;
        }

        [Parameter(Mandatory = false)]
        public EndurQueryType QueryType { get; set; }

        [Parameter(Mandatory = true, ParameterSetName = "ListFieldsSet")]
        public SwitchParameter ListFields { get; set; }

        [Parameter(Mandatory = false, Position = 0, ParameterSetName = "FieldSet")]
        public Hashtable Fields { get; set; }

        [Parameter(Mandatory = true, Position = 0, ParameterSetName = "QueryNameSet")]
        public string QueryName { get; set; }

        [Parameter]
        public string AdditionalCriteria { get; set; }

        protected override void ProcessRecord()
        {
            if (ParameterSetName == "ListFieldsSet")
            {
                var queryType = (EnumQueryType) QueryType;
                var queryFieldsMap = GetQueryFields(queryType);

                var fields = queryFieldsMap.SelectMany(m => m.Value, (m, f) => new {Table = m.Key, Column = f.Key});

                WriteObject(fields, true);
            }
            else
            {
                var results = new List<int>();

                Query query = null;
                try
                {
                    switch (ParameterSetName)
                    {
                        case "FieldSet":
                            query = GetQueryFromFields();
                            break;

                        case "QueryNameSet":
                        {
                            if (!string.IsNullOrEmpty(AdditionalCriteria))
                            {

                                var queryId =  Olf.NetToolkit.Query.Run(QueryName, null, null, AdditionalCriteria);
                                try
                                {
                                    using (query = GetQueryFromQueryName())
                                    {
                                        if (query == null)
                                        {
                                            throw new InvalidOperationException(
                                                string.Format("Could not find query called {0}", QueryName));
                                        }

                                        using (var result = Session.IOFactory.RetrieveQueryResult(queryId, query.Type))
                                        {
                                            WriteObject(result.ObjectIdsAsInt, true);
                                        }
                                    }
                                }
                                finally
                                {
                                    Olf.NetToolkit.Query.Clear(queryId);
                                }

                                return;
                            }
                            else
                            {
                                query = GetQueryFromQueryName();
                                if (query == null)
                                {
                                    throw new InvalidOperationException(
                                        string.Format("Could not find query called {0}", QueryName));
                                }
                            }
                        }
                            break;

                        default:
                            throw new PSInvalidOperationException(string.Format("Unknown ParameterSetName {0}", ParameterSetName));
                    }

                    using (var result = query.Execute())
                    {
                        results.AddRange(result.ObjectIdsAsInt);
                    }
                }
                finally
                {
                    if (query != null)
                    {
                        query.Dispose();
                    }
                }

                WriteObject(results, true);
            }
        }

        private Query GetQueryFromQueryName()
        {
            return Session.IOFactory.Queries.GetQuery(QueryName);
        }

        private Query GetQueryFromFields()
        {
            Query query = null;
            try
            {
                var queryType = (EnumQueryType) QueryType;

                query = Session.IOFactory.CreateQuery(queryType);

                var page = query.AddPage();
                if (!string.IsNullOrEmpty(AdditionalCriteria))
                {
                    page.AdditionalCriteria = AdditionalCriteria;
                }

                if (Fields != null)
                {
                    PopulateQueryFields(page, query, queryType);
                }
            }
            catch (Exception)
            {
                if (query != null)
                {
                    query.Dispose();
                }
                throw;
            }

            return query;
        }

        private void PopulateQueryFields(QueryPage page, Query query, EnumQueryType queryType)
        {
            var queryFieldValues = GetQueryFieldValues(Fields);
            ApplyFieldValuesToQuery(page, query, queryType, queryFieldValues);
        }

        private void ApplyFieldValuesToQuery(QueryPage page, Query query, EnumQueryType queryType, Dictionary<string, IList<string>> queryFieldValues)
        {
            var queryFieldsMap = GetQueryFields(queryType);
            foreach (var field in queryFieldValues)
            {
                var path = field.Key;

                var tableName = path.Substring(0, path.IndexOf('.'));
                var fieldName = path.Substring(path.IndexOf('.') + 1);

                var queryField = GetQueryField(query, fieldName, queryFieldsMap, tableName);

                var valuesGroupedByHavingNegationOperator =
                    field.Value.GroupBy(key => key.HasNegationOperator()).ToLookup(x => x.Key, x => x.Select(v => v));

                if (valuesGroupedByHavingNegationOperator.Count() != 1)
                {
                    throw new InvalidOperationException(
                        string.Format("Field {0} has a mix of negated and not negated values: {1}", path, string.Join(",", field.Value)));
                }

                var grouping = valuesGroupedByHavingNegationOperator.Single();
                var values = grouping.Flatten().Select(x => x.RemoveNegationOperator()).ToArray();
                page.SetValues(queryField, values.ToArray());
                if (grouping.Key)
                {
                    page.SetOmitted(queryField, true);
                }
            }
        }

        private static QueryConfigField GetQueryField(
            Query query,
            string providedName,
            Dictionary<string, Dictionary<string, string>> queryFieldsMap,
            string tableName)
        {
            var fieldName = providedName;

            // Get the actual field name
            var innerMap = queryFieldsMap[tableName];
            if (innerMap.ContainsKey(fieldName))
            {
                fieldName = innerMap[fieldName];
            }
            return GetQueryField(query, tableName, fieldName);
            ;
        }

        private static Dictionary<string, IList<string>> GetQueryFieldValues(IEnumerable fields)
        {
            var queryFieldValues = new Dictionary<string, IList<string>>();
            foreach (var field in fields.Cast<DictionaryEntry>())
            {
                var fieldName = field.Key.ToString();

                if (!fieldName.Contains("."))
                {
                    throw new InvalidOperationException(string.Format("Invalid field name {0}. Expected format <Table>.<Field>", fieldName));
                }

                if (!queryFieldValues.ContainsKey(fieldName))
                {
                    queryFieldValues.Add(fieldName, new List<string>());
                }

                var arrayValue = field.Value as object[];
                var fieldValues = arrayValue != null ? arrayValue.Select(x => x.ToString()) : new[] {field.Value.ToString()};

                foreach (var fieldValue in fieldValues)
                {
                    queryFieldValues[fieldName].Add(fieldValue);
                }
            }
            return queryFieldValues;
        }

        private static QueryConfigField GetQueryField(Query query, string tableName, string fieldName)
        {
            var configPage = query.ConfigPage;

            var configTable = configPage.GetConfigTable(tableName);
            if (configTable == null)
                throw new InvalidOperationException(string.Format("Could not find config table '{0}'", tableName));

            var configField = configTable.GetField(fieldName);
            if (configField == null)
                throw new InvalidOperationException(string.Format("Count not find config field '{0}'", fieldName));

            return configField;
        }

        private Dictionary<string, Dictionary<string, string>> GetQueryFields(EnumQueryType queryType)
        {
            var sql = string.Format(
                @"SELECT
    g.name,
    t.displayname,
    t.aliasname,
    d.colname,
    d.alias
FROM qry_groups g 
    INNER JOIN qry_group_table t ON t.group_id=g.id_number
    INNER JOIN qry_table_detail d ON d.tableid=t.tableid
WHERE g.id_number={0}
ORDER BY g.name, t.displayname, d.colname",
                (int) queryType);

            var map = new Dictionary<string, Dictionary<string, string>>();

            using (var table = Session.IOFactory.RunSQL(sql))
            {
                var tables = table.Rows
                    .Select(
                        r =>
                            new EndurQueryField
                            {
                                Group = r.Cells.GetItem(0).String,
                                Table = r.Cells.GetItem(1).String,
                                Column = r.Cells.GetItem(3).String,
                                ColumnAlias = r.Cells.GetItem(4).String
                            })
                    .GroupBy(f => f.Table);

                foreach (var tableGroup in tables)
                {
                    if (!map.ContainsKey(tableGroup.Key))
                    {
                        map.Add(tableGroup.Key, new Dictionary<string, string>());
                    }

                    var tableMap = map[tableGroup.Key];

                    foreach (var field in tableGroup.Where(field => !tableMap.ContainsKey(field.ColumnAlias)))
                    {
                        tableMap.Add(field.ColumnAlias, field.Column);
                    }
                }
            }

            return map;
        }
    }

    public static class FieldValueExtensions
    {
        public static bool HasNegationOperator(this string lhs)
        {
            return lhs.TrimStart().StartsWith("!");
        }

        public static string RemoveNegationOperator(this string lhs)
        {
            return lhs.TrimStart().TrimStart('!');
        }

        public static IEnumerable<T> Flatten<T>
            (this IEnumerable<IEnumerable<T>> source)
        {
            return source.SelectMany(x => x as IList<T> ?? x.ToList());
        }
    }

    public class EndurQueryField
    {
        public string Group { get; set; }
        public string Table { get; set; }
        public string Column { get; set; }
        public string ColumnAlias { get; set; }
    }


    public enum EndurQueryType
    {
        Account = 4,
        AcctAod = 0x18,
        AcctBalanceView = 0x11,
        AcctCancellation = 0x3a,
        AcctCorrectionEntries = 0x40,
        AcctCorrectionEntriesHist = 0x42,
        AcctExtRuleDefinition = 0x1f,
        AcctIncrementalEntries = 0x1a,
        AcctManualEntries = 0x17,
        AcctPartialExplode = 0x30,
        AcctPostingParam = 30,
        AcctProcessLog = 0x3b,
        AcctRuleBoDoc = 0x47,
        AcctRuleBoDocHist = 0x2710,
        AcctRuleDefinition = 13,
        AcctRuleQuery = 14,
        AcctRuleQueryDetails = 0x19,
        AcctRuleTran = 0x44,
        AcctRuleTranEvent = 0x45,
        AcctRuleTranProvDetail = 70,
        AcctSubledger = 0x13,
        AcctSubledgerEntries = 15,
        AcctSubledgerFeed = 0x10,
        Agreement = 0x1d,
        BarInventory = 0x1c,
        Cmotion = 60,
        CmotionDetails = 0x3d,
        CmotionViewManage = 0x43,
        CommEfp = 0x3f,
        Confirm = 11,
        ConnexMethodLog = 0x21,
        ConnexMonitorLog = 0x20,
        Contract = 0x49,
        CorpActions = 0x23,
        Credit = 20,
        Creditaux = 8,
        DividendManager = 0x39,
        Doc = 6,
        Group = 0x1b,
        Index = 0x16,
        Instrument = 0,
        Invalid = -1,
        NercEtag = 0x3e,
        NetAgreement = 10,
        NetbackExDef = 0x2711,
        OpsAuthorizedPayment = 0x22,
        Party = 3,
        PerformanceMeasurementPriceRecast = 0x34,
        PerformanceMeasurementReturnsReporter = 0x33,
        PerformanceStaticDataPerfSecResults = 0x36,
        PerformanceStaticDataPerfSecTranLog = 0x35,
        PerformanceStaticDataPerfSecValues = 0x37,
        Personnel = 7,
        Portfolio = 0x48,
        PowerDelivery = 0x27,
        PowerScheduleTran = 40,
        ReconHistEvent = 50,
        ReconHistStldocDocument = 0x2c,
        ReconHistStldocEvent = 0x2d,
        ReconHistStldocOutput = 0x2f,
        ReconHistTransaction = 0x31,
        ReconStldocDocument = 0x2a,
        ReconStldocEvent = 0x2b,
        ReconStldocOutput = 0x2e,
        Reset = 1,
        RiskExposureDefn = 12,
        SchedNom = 0x26,
        Settle = 5,
        StldocDocument = 0x24,
        StldocEvent = 0x25,
        StpExceptionHit = 0x29,
        TpmHistory = 0x41,
        TranEvent = 9,
        Transaction = 2,
        Tseries = 0x38
    }
}