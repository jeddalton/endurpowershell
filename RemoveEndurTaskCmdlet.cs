﻿using System.Management.Automation;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsCommon.Remove, "EndurTask")]
    public class RemoveEndurTaskCmdlet: EndurCmdletBase
    {
        [Parameter(Mandatory = true, Position = 0)]
        public string Name { get; set; }

        protected override void ProcessRecord()
        {
            WriteObject(string.Format("Removing task '{0}'", Name));

            if (!Session.ControlFactory.IsValidTaskDefinition(Name))
                throw new MethodException(string.Format("Could not retrieve task named '{0}' from Endur.", Name));

            Session.ControlFactory.RemoveTaskDefinition(Name);
        }
         
    }
}