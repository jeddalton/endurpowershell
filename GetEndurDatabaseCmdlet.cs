﻿using System;
using System.Management.Automation;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsCommon.Get, "EndurDatabase")]
    public class GetEndurDatabaseCmdlet : EndurCmdletBase
    {
        protected override void ProcessRecord()
        {
            WriteObject(this.Session.DatabaseName);
        }

    }
}
