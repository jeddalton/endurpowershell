﻿using System.Management.Automation;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsLifecycle.Stop, "EndurSession")]
    public class StopEndurSessionCmdlet : EndurCmdletBase
    {
        protected override void ProcessRecord()
        {
            ClearSession();
        }
    }
}