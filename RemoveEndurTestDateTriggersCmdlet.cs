namespace Gazprom.MT.PowerShell.Endur
{
    using System.Data.SqlClient;
    using System.Management.Automation;

    [Cmdlet(VerbsCommon.Remove, "EndurTestDateTriggers")]
    public class RemoveEndurTestDateTriggersCmdlet : EndurTestDateTriggersCmdletBase
    {
        protected override void ProcessRecord()
        {
            var statements = GetTriggerStatements();

            foreach (var statement in statements)
            {
                var triggerName = GetTriggerName(statement);
                if (string.IsNullOrEmpty(triggerName))
                {
                    continue;
                }

                this.WriteVerbose(string.Format("Checking trigger {0}", triggerName));

                var triggerExists = this.GetTriggerExists(triggerName);

                if (triggerExists)
                {
                    this.WriteVerbose("Dropping trigger as it already exists");
                    this.DropTrigger(triggerName);
                }
            }
        }
    }
}