﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Management.Automation;
using System.Text;
using Olf.Openrisk.Table;

namespace Gazprom.MT.PowerShell.Endur
{
    /// <summary>
    /// A Cmdlet to import forward curve data from a CSV file
    /// </summary>
    [Cmdlet(VerbsData.Import, "MarketData")]
    public class ImportMarketDataCmdlet : EndurCmdletBase
    {
        private const int COL_INDEX = 0;
        private const int COL_GRIDPOINT = 1;
        private const int COL_VALUE = 2;

        /// <summary>Gets or sets the market data CSV file.</summary>
        /// <value>The market data CSV file.</value>
        [Alias("File")]
        [Parameter(Mandatory = true, Position = 0, HelpMessage = "Forward curve data csv file")]
        [ValidateNotNullOrEmpty]
        public string MarketDataCsvFile { get; set; }

        private List<MarketData> _marketData;
        private Hashtable _indexList;

        private class IndexGpts
        {
            public string IndexName { get; set; }
            public Table GridPointTbl { get; set; }
        }

        protected override void ProcessRecord()
        {
            try
            {
                var idxGpts = new List<IndexGpts>();
                var fullPath = Path.GetFullPath(MarketDataCsvFile);
                if (File.Exists(fullPath))
                {
                    ConstTable gptTable;
                    using (var sr = new StreamReader(fullPath, Encoding.Default))
                    {
                        _marketData = new List<MarketData>();
                        _indexList = new Hashtable();
                        //Skip header row
                        sr.ReadLine();

                        while (sr.Peek() > 0)
                        {
                            var line = sr.ReadLine();
                            if (line != null)
                            {
                                var lineArray = line.Split(',');
                                double newValue;
                                if (lineArray[COL_INDEX].Length > 0
                                    && lineArray[COL_GRIDPOINT].Length > 0
                                    && double.TryParse(lineArray[COL_VALUE], out newValue))
                                {
                                    if (!_indexList.ContainsKey(lineArray[COL_INDEX]))
                                        _indexList.Add(lineArray[COL_INDEX], lineArray[COL_INDEX]);
                                    _marketData.Add(new MarketData()
                                                        {
                                                            IndexName = lineArray[COL_INDEX],
                                                            GridPoint = lineArray[COL_GRIDPOINT],
                                                            Price = newValue
                                                        });
                                }
                            }
                        }
                        foreach (var indexKey in _indexList.Keys)
                        {
                            var rowCount = 0;
                            var key = indexKey;
                            var indexMktData = _marketData.Where(mkd => mkd.IndexName.Equals(key.ToString()));
                            var gridPoints = new Table();
                            gridPoints.AddColumn("GridPoint", EnumColType.String);
                            gridPoints.AddColumn("Input", EnumColType.Double);
                            foreach (var gpt in indexMktData)
                            {
                                gridPoints.AddRow(rowCount.ToString());
                                gridPoints.SetString("GridPoint", rowCount, gpt.GridPoint);
                                gridPoints.SetDouble("Input", rowCount, gpt.Price);
                                rowCount++;
                            }
                            idxGpts.Add(new IndexGpts {IndexName = indexKey.ToString(), GridPointTbl = gridPoints});
                        }
                    }
                    try
                    {
                        SaveIndexPrices(idxGpts);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private void SaveIndexPrices(IEnumerable<IndexGpts> idxGpts)
        {
            var mkt = Session.Market;
            foreach (var indx in idxGpts)
            {
                try
                {
                    var idx = mkt.GetIndex(indx.IndexName);
                    WriteMessage("Index "+ indx.IndexName + " Updated");
                    idx.GridPoints.SetInputFromTable(indx.GridPointTbl);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            mkt.SaveUniversal();
        }
        
        private struct MarketData
        {
            internal string IndexName;
            internal string GridPoint;
            internal double Price;
        }
        private void WriteMessage(string msg)
        {
            this.Session.Debug.PrintLine(msg);
            this.WriteVerbose(msg);
        }
    }
}
