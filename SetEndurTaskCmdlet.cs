﻿namespace Gazprom.MT.PowerShell.Endur
{
    using System;
    using System.Linq;
    using System.Management.Automation;
    using Gazprom.MT.PowerShell.Endur.EndurTask;

    [Cmdlet(VerbsCommon.Set, "EndurTask")]
    public class SetEndurTaskCmdlet : EndurCmdletBase
    {
        [Parameter(Mandatory = true, Position = 0)]
        public String Name { get; set; }

        [Alias("Main")]
        [Parameter(Mandatory = true, Position = 1)]
        public String MainScript { get; set; }

        [Alias("Param")]
        [Parameter(Mandatory = false, Position = 2)]
        public String ParamScript { get; set; }

        [Alias("Output")]
        [Parameter(Mandatory = false, Position = 3)]
        public String OutputScript { get; set; }

        [Alias("Viewer")]
        [Parameter(Mandatory = false, Position = 4)]
        public String ViewerScript { get; set; }

        [Alias("SecGroups")]
        [Parameter(Mandatory = false, Position = 5)]
        public String[] SecurityGroups { get; set; }

        [Parameter(Position = 6)]
        public string Description { get; set; }

        protected override void ProcessRecord()
        {
            if (this.ParamScript.Trim() == "NULL")
            {
                this.ParamScript = null;
            }

            Name = Name.Trim();
            if (!Session.ControlFactory.IsValidTaskDefinition(Name))
            {
                AddTask();
            }
            else
            {
                UpdateTask();
            }

            if (this.SecurityGroups == null || this.SecurityGroups.Length <= 0)
            {
                return;
            }

            var securityGroupNames = this.SecurityGroups.Where(x => !string.IsNullOrEmpty(x.Trim())).Select(x => x.Trim()).ToArray();

            this.WriteVerbose(
                string.Format(
                    "Assigning security groups '{0}' to task '{1}'", string.Join(",", securityGroupNames), this.Name));
            new TaskSecurityGroupSetter(this.Session).Set(this.Name, securityGroupNames);
        }

        private void AddTask()
        {
            WriteVerbose(string.Format("Creating task '{0}'", Name));
            if (!Session.ControlFactory.CreateTaskDefinition(Name, ParamScript, MainScript, ViewerScript, OutputScript))
            {
                throw new MethodException("Task creation failed.");
            }
        }

        private void UpdateTask()
        {
            using (var task = Session.ControlFactory.RetrieveTaskDefinition(Name))
            {
                this.WriteVerbose(
                    string.Format(
                        "Task: '{0}' '{1}' '{2}' '{3}' '{4}' '{5}",
                        task.Name,
                        task.MainScript,
                        task.ParamScript,
                        task.OutputScript,
                        task.PageViewerScript,
                        task.Description));
                if (
                    (task.MainScript ?? String.Empty).Trim() == (MainScript ?? String.Empty).Trim() && 
                    (task.ParamScript ?? String.Empty).Trim() == (ParamScript ?? String.Empty).Trim() && 
                    (task.OutputScript ?? String.Empty).Trim() == (OutputScript ?? String.Empty).Trim() &&
                    (task.PageViewerScript ?? String.Empty).Trim() == (ViewerScript ?? String.Empty).Trim() &&
                    (task.Description ?? string.Empty).Trim() == (Description ?? string.Empty).Trim()) 
                {
                    WriteVerbose(String.Format("Skipping unchanged task {0}", Name));
                    return;
                }

                WriteVerbose(string.Format("Updating task '{0}'", Name));

                task.MainScript = MainScript;
                task.ParamScript = ParamScript;
                task.OutputScript = OutputScript;
                task.PageViewerScript = ViewerScript;
                task.Description = Description;

                task.Save();
            }
        }
    }
}