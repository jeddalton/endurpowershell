﻿using System.Management.Automation;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsCommon.Clear, "EndurCache")]
    public class ClearEndurCacheCmdlet : EndurCmdletBase
    {
        protected override void ProcessRecord()
        {
            this.Session.Debug.FlushCache();
        }
    }
}