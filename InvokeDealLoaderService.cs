﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Management.Automation;
using System.Reflection;
using System.ServiceModel;
using Aspose.Cells;
using Gazprom.MT.PowerShell.Endur.Code;
using Gazprom.MT.PowerShell.Endur.DataLoaderService;
using Gazprom.MT.PowerShell.Endur.DataLoaderService.Enums;
using Olf.Openrisk.Application;

namespace Gazprom.MT.PowerShell.Endur
{
    /// <summary>A Cmdlet to wrap deal loader service.
    /// Get 
    /// </summary>
    /// 
    [Cmdlet(VerbsLifecycle.Invoke, "DealLoaderTest")]
    public class InvokeDealLoaderService : PSCmdlet
    {
        private Session session;
        private Session Session
        {
            get
            {
                lock (this)
                {
                    if (session == null)
                    {
                        var app = Application.Instance;
                        session = app.Attach();
                    }
                    return session;
                }
            }
        }

        private DealBookingServiceClient dealBookingSvc;
        private DealBookingServiceClient DealBookingService
        {
            get
            {
                lock(this)
                {
                    return dealBookingSvc ?? (dealBookingSvc = GetInstanceOfDealBookingService());
                }
            }
        }

        public InvokeDealLoaderService()
        {
            var license = new License();
            license.SetLicense("Aspose.Cells.lic");
        }

        [Parameter(Mandatory = true, Position = 0)]
        public bool GetProcduct { get; set; }

        [Parameter(Mandatory = true, Position = 1)]
        public bool SaveProcuct { get; set; }

        [Parameter(Mandatory = false, Position = 2)]
        public string ProductRef { get; set; }

        [Parameter(Mandatory = false, Position = 3)]
        public string XlsFile { get; set; }

        [Parameter(Mandatory = false, Position = 4)]
        public string SheetName { get; set; }

        protected override void ProcessRecord()
        {
            try
            {
                if (GetProcduct)
                {
                    GetProductFromEndur();
                }
                if (SaveProcuct)
                {
                    SaveProductsToEndur();
                }
            }
            catch (Exception e)
            {
                WriteError(new ErrorRecord(e, "Invoke-DealLoaderTest", ErrorCategory.NotSpecified, null));
            }
        }

        private void GetProductFromEndur()
        {
            var products = this.DealBookingService.GetProductsByReference(ProductRef).Where(
                pr => pr.TranStatus != DataLoaderService.Enums.TranStatusEnum.Cancelled).OrderByDescending(pr => pr.ExternalRef);

            SaveProductsToFile(products);
              
            WriteObject(string.Format("Product with reference '{0}' retrieved successuflly", this.ProductRef));            
        }

        private void SaveProductsToFile(IOrderedEnumerable<ProductBase> products)
        {
            var dtProduct = ConvertProductsToDatatable(products);

            var workbook = new Workbook();
            var sheet = workbook.Worksheets[0];

            //data import
            sheet.Cells.ImportDataTable(dtProduct, true, "A1");

            var fileName = string.Format(@"{0}\{1:yyyyMMddHHmmss}", @"C:\Temp", DateTime.Now);

            workbook.Save(fileName + ".xlsx", SaveFormat.Xlsx);

            var product = products.FirstOrDefault();
            if (product != null)
            {
                var productXml = XmlSerailizerUtility.Serialize(product);
                File.WriteAllText(fileName + ".xml", productXml);
            }
        }

        private void SaveProductsToEndur()
        {
            var fullPath = Path.GetFullPath(XlsFile);
            if (File.Exists(fullPath))
            {
                var sheetList = new Hashtable();

                using (var fs = File.Open(XlsFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {

                    var workbook = new Workbook(fs);
                    var dealSheets = workbook.Worksheets;

                    foreach (var worksheet in dealSheets.Cast<Worksheet>().Where(worksheet => !sheetList.ContainsKey(worksheet.Name)))
                    {
                        sheetList.Add(worksheet.Name, worksheet.Name);
                    }

                    if (!string.IsNullOrEmpty(SheetName) && !SheetName.Equals("*"))
                    {
                        ProcessSheet(GetWorkSheet(ref dealSheets, SheetName));
                    }
                    else
                    {
                        foreach (var sheetKey in sheetList.Keys)
                        {
                            try
                            {
                                ProcessSheet(GetWorkSheet(ref dealSheets, (string)sheetList[sheetKey]));
                            }
                            catch (Exception ex)
                            {
                                this.Session.Debug.PrintLine(ex.Message);
                                WriteWarning(ex.Message);
                            }
                        }
                    }
                }
            }
            else { throw new Exception(String.Format("Specified Excel file '{0}' File not found.", XlsFile)); }
            WriteObject("Save Products" + XlsFile);
        }

        private void LoadProductsFromDataTable(DataTable dataTable)
        {
            var toolset = (string)dataTable.Rows[0]["Toolset"];
            var instrumentType = (string)dataTable.Rows[0]["InstrumentType"];
            var cashflowType = string.Empty;
            if (dataTable.Columns.Contains("CashflowType"))
            {
                cashflowType = (string)dataTable.Rows[0]["CashflowType"];
            }

            var dealBookingService = GetInstanceOfDealBookingService();
            var productTypeName = dealBookingService.GetProductTypeByDefinition(toolset, instrumentType, cashflowType);
            var productType = Assembly.GetExecutingAssembly().GetType(string.Format("{0}.{1}", typeof(ProductBase).Namespace,
                                                                      productTypeName));
            var productList = DataTableMapper.GetMappedList(productType, dataTable, null).ToList();

            var productResultList = new List<ProductResult>();
            productList.ForEach(p => productResultList.AddRange(SaveProduct(dealBookingService, p as ProductBase)));
            foreach (var result in productResultList)
            {
                if (String.IsNullOrEmpty(result.ErrorMessage))
                {
                    WriteObject(String.Format("Deal with reference '{0}' imported successfully.", result.OriginalProduct.Reference));
                }
                else
                {
                    WriteWarning(result.ErrorMessage);
                }
            }
        }

        private static IEnumerable<ProductResult> SaveProduct(IDealBookingService dealBookingService, ProductBase p)
        {

            var productResultList = new List<ProductResult>();
            var productResult = new ProductResult { OriginalProduct = p };
            try
            {
                var savedDealnumber = 0;
                savedDealnumber = dealBookingService.SaveProduct(p);
                p.DealNumber = savedDealnumber;
            }
            catch (Exception ex)
            {
                productResult.ErrorMessage = ex.Message;
            }
            try
            {
                productResult.SavedProducts.AddRange(
                    dealBookingService.GetProductsByReference(p.Reference).Where(
                        pr => pr.TranStatus != TranStatusEnum.Cancelled).
                        OrderByDescending(pr => pr.ExternalRef));
            }
            catch (Exception exception)
            {
                productResult.ErrorMessage = String.Format("{0}/tProduct Retrival Failed {1}", productResult.ErrorMessage, exception.ToString());
            }
            productResultList.Add(productResult);
            return productResultList;
        }

        private void ProcessSheet(Worksheet sheet)
        {
            if (sheet == null) return;

            var dealsTable = ConvertSheetToDataTable(sheet);
            if (dealsTable.Rows.Count == 0) throw new Exception(String.Format("'{0}' worksheet does not contain any deals to import.", sheet.Name));

            if (dealsTable.Columns.Contains("Toolset")
                && dealsTable.Columns.Contains("InstrumentType"))
            {
                LoadProductsFromDataTable(dealsTable);
            }
            else if (dealsTable.Columns.Contains("ProfileType"))
            {
                throw new ApplicationException("Profile cannot be saved on their own but should be saved as part of the product in deal booking service");
            }

            WriteObject(String.Format("Deal(s) processed from worksheet: '{0}'.", sheet.Name));
        }

        private Worksheet GetWorkSheet(ref WorksheetCollection sheets, string sheetName)
        {
            var worksheet = sheets[sheetName];
            if (worksheet == null) throw new Exception(String.Format("Worksheet '{0}' does not exist in specified excel file: {1}", sheetName, XlsFile));
            worksheet.Cells.DeleteBlankRows();
            worksheet.Cells.DeleteBlankColumns();
            return worksheet;
        }

        private static DataTable ConvertSheetToDataTable(Worksheet worksheet)
        {
            try
            {
                return worksheet.Cells.ExportDataTable(0, 0, worksheet.Cells.MaxDataRow + 1, worksheet.Cells.MaxDataColumn + 1, true, true);
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("Unable to process worksheet '{0}', due to: {1}", worksheet.Name, ex.Message));
            }
        }

        private DealBookingServiceClient GetInstanceOfDealBookingService()
        {
            var binding = new BasicHttpBinding(BasicHttpSecurityMode.TransportCredentialOnly) { MessageEncoding = WSMessageEncoding.Mtom };
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Ntlm;
            binding.Security.Transport.ProxyCredentialType = HttpProxyCredentialType.Ntlm;
            return new DealBookingServiceClient(binding, new EndpointAddress(String.Format(@"http://{0}:9000/DealLoader", AppServerName)));
        }

        private string AppServerName
        {
            get
            {
                /*NOTE: It is assumed that 32bit service name will be Appserver02*/
                var sql = string.Format("select workstation_name from service_mgr where service_name='Appserver02'");
                var table = Session.IOFactory.RunSQL(sql);
                return table.Rows.Select((row => row.Cells.GetItem(0).String.Trim())).SingleOrDefault();
            }
        }

        public DataTable ConvertArrayListToDatatable(ArrayList arrList)
        {
            return GetDataTableFromArryList(arrList);
        }

        public DataTable ConvertProductsToDatatable(IEnumerable<ProductBase> products)
        {
            var dt = new DataTable();
            try
            {
                if (products != null )
                {
                    var productList = new ArrayList();
                    productList.AddRange(products.ToArray());
                    dt = GetDataTableFromArryList(productList);
                }


                return dt;
            }
            catch 
            {
                return dt;
            }

        }

        private static DataTable GetDataTableFromArryList(ArrayList productList)
        {
            var dt = new DataTable();
            try
            {
                if (productList.Count > 0)
                {
                    var arrype = productList[0].GetType();
                    dt = new DataTable(arrype.Name);

                    foreach (var propInfo in arrype.GetProperties())
                    {
                        dt.Columns.Add(new DataColumn(propInfo.Name));
                    }

                    foreach (object obj in productList)
                    {
                        var dr = dt.NewRow();

                        foreach (DataColumn dc in dt.Columns)
                        {
                            dr[dc.ColumnName] = obj.GetType().GetProperty(dc.ColumnName).GetValue(obj, null);
                        }
                        dt.Rows.Add(dr);
                    }
                }
                return dt;
            }
            catch 
            {
                return dt;
            }
        }
    }
}
