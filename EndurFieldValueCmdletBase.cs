using System;
using System.Management.Automation;
using Olf.Openrisk.Trading;

namespace Gazprom.MT.PowerShell.Endur
{
    using System.Globalization;
    using System.Linq;

    using Gazprom.MT.PowerShell.Endur.Code;

    public abstract class EndurFieldValueCmdletBase : EndurCmdletBase
    {
        [Parameter(Mandatory = true, ParameterSetName = "TransactionId", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true, Position = 0)]
        public Int32 TransactionId { get; set; }

        [Parameter(Mandatory = true, ParameterSetName = "Transaction", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true, Position = 0)]
        public Transaction Transaction { get; set; }

        [Parameter(Mandatory = true, ParameterSetName = "TransactionId", ValueFromPipelineByPropertyName = true, Position = 1)]
        [Parameter(Mandatory = true, ParameterSetName = "Transaction", ValueFromPipelineByPropertyName = true, Position = 1)]
        public PSObject Field { get; set; }

        [Parameter(Mandatory = true, ParameterSetName = "TransactionId", ValueFromPipelineByPropertyName = true, Position = 2)]
        [Parameter(Mandatory = true, ParameterSetName = "Transaction", ValueFromPipelineByPropertyName = true, Position = 2)]
        public Int32 Side { get; set; }

        [Parameter]
        public Int32? Seq2 { get; set; }

        [Parameter]
        public Int32? Seq3 { get; set; }

        [Parameter]
        public Int32? Seq4 { get; set; }

        [Parameter]
        public Int32? Seq5 { get; set; }

        protected Field GetField(int tranfFieldId)
        {
            if (this.Seq5.HasValue)
            {
                if (!this.Seq2.HasValue || !this.Seq3.HasValue || !this.Seq4.HasValue)
                {
                    throw new InvalidOperationException("All sequence numbers must be supplied");
                }

                return this.Transaction.RetrieveField(tranfFieldId, this.Side, this.Seq2.Value, this.Seq3.Value, this.Seq4.Value, this.Seq5.Value);
            }

            if (this.Seq4.HasValue)
            {
                if (!this.Seq2.HasValue || !this.Seq3.HasValue)
                {
                    throw new InvalidOperationException("All sequence numbers must be supplied");
                }

                return this.Transaction.RetrieveField(tranfFieldId, this.Side, this.Seq2.Value, this.Seq3.Value, this.Seq4.Value);
            }

            if (this.Seq3.HasValue)
            {
                if (!this.Seq2.HasValue)
                {
                    throw new InvalidOperationException("All sequence numbers must be supplied");
                }

                return this.Transaction.RetrieveField(tranfFieldId, this.Side, this.Seq2.Value, this.Seq3.Value);
            }

            if (this.Seq2.HasValue)
            {
                return this.Transaction.RetrieveField(tranfFieldId, this.Side, this.Seq2.Value);
            }

            return this.Transaction.RetrieveField(tranfFieldId, this.Side);
        }

        protected int GetTranfFieldId()
        {
            var tranfFieldId = 0;

            if (this.Field.ImmediateBaseObject is String)
            {
                var fieldName = this.Field.ToString();

                // First try to convert to a known tranf field enum value
                try
                {
                    tranfFieldId = (Int32)((EnumTranfField)Enum.Parse(typeof(EnumTranfField), fieldName, true));
                }
                catch (ArgumentException)
                {
                    // Now try and convert to a tran/param/ins info field
                    var tranInfoId = this.GetFieldId(fieldName, EnumTradingObject.Transaction);

                    if (tranInfoId != 0)
                    {
                        return tranInfoId;
                    }

                    var paramInfoId = this.GetFieldId(fieldName, EnumTradingObject.Leg);
                    if (paramInfoId != 0)
                    {
                        return paramInfoId;
                    }

                    var insInfoId = this.GetFieldId(fieldName, EnumTradingObject.Instrument);
                    if (insInfoId != 0)
                    {
                        return insInfoId;
                    }
                }
            }
            else
            {
                tranfFieldId = Int32.Parse(this.Field.ToString(), CultureInfo.InvariantCulture);
            }

            return tranfFieldId;
        }

        private int GetFieldId(string fieldName, EnumTradingObject tradingObjectType)
        {
            using (var dc = new DisposableCollection())
            {
                var field =
                    dc.Add(this.Session.TradingFactory.GetFieldDescriptions(tradingObjectType))
                        .Select(dc.Add)
                        .SingleOrDefault(f => f.Name == fieldName);

                return field != null ? field.Id : 0;
            }
        }
    }
}