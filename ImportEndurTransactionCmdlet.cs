using System;
using System.IO;
using System.Management.Automation;
using Gazprom.MT.PowerShell.Endur.Code;
using Olf.Openrisk.Table;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsData.Import, "EndurTransaction")]
    public class ImportEndurTransactionCmdlet : EndurCmdletBase
    {
        private const String DealLoaderScriptName = "GGTP.ExcelDealLoader.Load";

        [Parameter(Mandatory = true, Position = 0)]
        public object ExcelFileName { get; set; }

        [Parameter(Mandatory = true, Position = 1)]
        public String SheetName { get; set; }

        [Parameter(Mandatory = false, Position = 2)]
        public String BaseDir { get; set; }

        protected override void ProcessRecord()
        {
            using (var table = this.Session.TableFactory.CreateTable())
            {
                table.AddColumn("ExcelFileName", EnumColType.String);
                table.AddColumn("SheetName", EnumColType.String);
                table.AddRow();

                var excelFileName = (this.ExcelFileName is FileInfo) ? (this.ExcelFileName as FileInfo).FullName : !String.IsNullOrEmpty(this.BaseDir) ? Path.Combine(this.BaseDir, this.ExcelFileName.ToString()) : this.ExcelFileName.ToString();

                table.SetString("ExcelFileName", 0, excelFileName);
                table.SetString("SheetName", 0, this.SheetName);

                var returnt = this.Session.ControlFactory.RunScript(DealLoaderScriptName, table);
                if (returnt == null) return;

                WriteObject(EndurPowershellUtil.TableToPSObjects(returnt));
                returnt.Dispose();
            }

        }
    }
}