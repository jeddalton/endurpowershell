﻿// -----------------------------------------------------------------------
// <copyright file="GetEndurDocumentCmdlet.cs" company="Gazprom Marketing & Trading">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Gazprom.MT.PowerShell.Endur
{
    using System;

    using System.Management.Automation;


    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    [Cmdlet(VerbsCommon.Get, "EndurDocument")]
    public class GetEndurDocumentCmdlet : EndurCmdletBase
    {
        [Parameter(Mandatory = true, Position = 0)]
        public Int32 DocumentNumber { get; set; }

        [Parameter]
        public Int32 Version { get; set; }

        public GetEndurDocumentCmdlet()
        {
            this.Version = -1;
        }

        protected override void ProcessRecord()
        {
            WriteObject(this.Version == -1
                       ? this.Session.BackOfficeFactory.RetrieveDocument(this.DocumentNumber)
                       : this.Session.BackOfficeFactory.RetrieveDocument(this.DocumentNumber, this.Version));
        }
    }
}
