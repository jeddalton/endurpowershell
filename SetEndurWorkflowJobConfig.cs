﻿namespace Gazprom.MT.PowerShell.Endur
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Management.Automation;

    using global::Endur.Common;

    [Cmdlet(VerbsCommon.Set, "EndurWorkflowJobConfig")]
    public class SetEndurWorkflowJobConfig : EndurCmdletBase
    {
        [Parameter(Mandatory = true)]
        public Int32 JobId { get; set; }

        [Parameter(Mandatory = true)]
        public Int32 WorkflowId { get; set; }

        [Parameter(Mandatory = true)]
        public string Dispatcher { get; set; }

        protected override void ProcessRecord()
        {
            SetConfig(this.WorkflowId, this.JobId, this.Dispatcher);
            this.WriteVerbose(string.Format("Job {0} updated", this.JobId));
        }

        private void SetConfig(Int32 workflowId, Int32 jobId, string dispatcher)
        {
            var jobConfig = this.GetConfig(workflowId, dispatcher);

            this.WriteObject(jobConfig);

            var sql =
                String.Format(
                    "EXEC update_job_cfg {0}, {1}, {2}, {3}, '{4}', {5}, {6}, {7}, {8},{9},{10}, {11}, {12}, {13}, {14}, {15}, {16}, {17}, {18}, {19},{20},{21}, '{22}', {23}, {24}, {25}, {26}, {27}, {28}, {29},{30},{31}"
                      ,jobId
                      ,WorkflowId
                      ,jobConfig.Type
                      ,jobConfig.SubId
                      ,jobConfig.Name
                      ,jobConfig.Sequence
                      ,jobConfig.PersonnelType
                      ,jobConfig.Dispatcher
                      ,jobConfig.MaxRun
                      ,jobConfig.ExcType
                      ,jobConfig.ExcId
                      ,jobConfig.WhatNext
                      ,jobConfig.Owner
                      ,jobConfig.Scope
                      ,jobConfig.Schedule0
                      ,jobConfig.Schedule1
                      ,jobConfig.Schedule2
                      ,jobConfig.Schedule3
                      ,jobConfig.Schedule4
                      ,jobConfig.Schedule5
                      ,jobConfig.Schedule6
                      ,jobConfig.Schedule7
                      ,jobConfig.Subscription
                      ,jobConfig.WflowSequence
                      ,jobConfig.PinFlag
                      ,jobConfig.ServiceType
                      ,jobConfig.ServiceGroupType
                      ,jobConfig.JobRunSite
                      ,jobConfig.GridScheduler
                      ,jobConfig.RerunCounter
                      ,jobConfig.ScriptLog
                      ,this.Session.User.Id);

            using (this.Session.IOFactory.RunSQL(sql))
            {

            }

            using (this.Session.IOFactory.RunSQL(string.Format("delete_job_cfg_announce {0}", 643611)))
            {
            }

            using (this.Session.IOFactory.RunSQL(string.Format("delete_job_cfg_pfolio {0}", 643611)))
            {
            }

            using (this.Session.IOFactory.RunSQL(string.Format("delete_job_cfg_personnel {0}", 643611)))
            {

            }
        }

        private JobConfig GetConfig(Int32 workflowId, string dispatcher)
        {
            var taskSql = String.Format(@"select
job_id JobId
,wflow_id WflowId
,type Type
,sub_id SubId
,ISNULL(name, ' ') Name
,sequence Sequence
,personnel_type  PersonnelType
,max_run MaxRun
,exc_id ExcId
,exc_type ExcType
,what_next WhatNext
,owner Owner
,scope Scope
,schedule_0 Schedule0
,schedule_1 Schedule1
,schedule_2 Schedule2
,schedule_3 Schedule3
,schedule_4 Schedule4
,schedule_5 Schedule5
,schedule_6 Schedule6
,schedule_7 Schedule7
,ISNULL(subscription, ' ') Subscription
,wflow_sequence WflowSequence
,pin_flag PinFlag
,service_type ServiceType
,service_group_type ServiceGroupType
,job_run_site JobRunSite
,grid_scheduler GridScheduler
,rerun_counter RerunCounter
,script_log ScriptLog
,(SELECT TOP 1 id from service_mgr where service_name = '{2}') Dispatcher

from job_cfg where wflow_id = {0} and job_id = {1}", workflowId, JobId, this.Dispatcher);

            var jobConfig = this.Session.IOFactory.FromSql<JobConfig>(taskSql).FirstOrDefault();

            return jobConfig;
        }

        internal class JobConfig
        {
            public int JobId { get; set; }

            public int WflowId { get; set; }

            public int Type { get; set; }

            public int SubId { get; set; }

            public string Name { get; set; }

            public int Sequence { get; set; }

            public int PersonnelType { get; set; }

            public int MaxRun { get; set; }

            public int ExcId { get; set; }

            public int ExcType { get; set; }

            public int WhatNext { get; set; }

            public int Owner { get; set; }

            public int Scope { get; set; }

            public int Schedule0 { get; set; }

            public int Schedule1 { get; set; }

            public int Schedule2 { get; set; }

            public int Schedule3 { get; set; }

            public int Schedule4 { get; set; }

            public int Schedule5 { get; set; }

            public int Schedule6 { get; set; }

            public int Schedule7 { get; set; }

            public string Subscription { get; set; }

            public int WflowSequence { get; set; }

            public int PinFlag { get; set; }

            public int  ServiceType { get; set; }

            public int ServiceGroupType { get; set; }

            public int JobRunSite { get; set; }

            public int  GridScheduler { get; set; }

            public int RerunCounter { get; set; }

            public int ScriptLog { get; set; }

            public int Dispatcher { get; set; }

        }
    }
}
