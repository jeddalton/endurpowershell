﻿namespace Gazprom.MT.PowerShell.Endur.Model
{
    using System;
    using System.Data;

    using Gazprom.MT.PowerShell.Endur.Code;

    public static class EndurEventMapper
    {
        public const string SelectSql =
            "SELECT \r\n" + "	ISNULL(te.event_num, 0) AS EventNum \r\n" + "	,ISNULL(te.tran_num, 0) AS TranNum \r\n"
            + "	,ISNULL(te.ins_num, 0) AS InsNum \r\n" + "	,ISNULL(te.document_num, 0) AS DocumentNum \r\n"
            + "	,et.name AS EventType \r\n" + "	,ISNULL(te.event_date, '1900-01-01') AS EventDate \r\n"
            + "	,ISNULL(te.para_ins_num, 0) AS ParaInsNum \r\n" + "	,ISNULL(te.para_tran_num, 0) AS ParaTranNum \r\n"
            + "	,ISNULL(te.para_price_rate, 0) AS ParaPriceRate \r\n"
            + "	,ISNULL(te.para_position, 0) AS ParaPosition \r\n"
            + "	,ISNULL(te.para_date, '1900-01-01') AS ParaDate \r\n"
            + "	,ISNULL(te.ins_para_seq_num, 0) AS InsParaSeqNum \r\n" + "	,ISNULL(te.ins_seq_num, 0) AS InsSeqNum \r\n"
            + "	,int_contact.name AS InternalContact \r\n" + "	,ext_contact.name AS ExternalContact \r\n"
            + "	,int_status.name AS InternalConfStatus \r\n" + "	,ext_status.name AS ExternalConfStatus \r\n"
            + "	,ISNULL(te.pymt_type, 0) AS PymtType \r\n" + "	,curr.name AS Currency \r\n"
            + "	,dt.name AS DeliveryType \r\n" + "	,es.event_source_name AS EventSource \r\n"
            + "	,iu.unit_label AS Unit \r\n" + "FROM ab_tran_event te \r\n"
            + "	INNER JOIN event_type et ON et.id_number=te.event_type \r\n"
            + "	LEFT OUTER JOIN personnel int_contact ON int_contact.id_number=te.internal_contact \r\n"
            + "	LEFT OUTER JOIN personnel ext_contact ON ext_contact.id_number=te.external_contact \r\n"
            + "	INNER JOIN outbound_doc_status int_status ON int_status.id_number=te.internal_conf_status \r\n"
            + "	INNER JOIN outbound_doc_status ext_status ON ext_status.id_number=te.external_conf_status \r\n"
            + "	LEFT OUTER JOIN currency curr ON curr.id_number=te.currency \r\n"
            + "	LEFT OUTER JOIN delivery_type dt ON dt.id_number=te.delivery_type \r\n"
            + "	INNER JOIN event_source es ON es.event_source_id=te.event_source \r\n"
            + "	LEFT OUTER JOIN idx_unit iu ON iu.unit_id=te.unit \r\n";

        public static EndurEvent Map(IDataRecord row)
        {
            var item = new EndurEvent
                           {
                               EventNum = row.GetField<Int32>(0),
                               TranNum = row.GetField<Int32>(1),
                               InsNum = row.GetField<Int32>(2),
                               DocumentNum = row.GetField<Int32>(3),
                               EventType = row.IsDBNull(4) ? null : row.GetField<String>(4),
                               EventDate = row.GetField<DateTime>(5),
                               ParaInsNum = row.GetField<Int32>(6),
                               ParaTranNum = row.GetField<Int32>(7),
                               ParaPriceRate = row.GetField<Double>(8),
                               ParaPosition = row.GetField<Double>(9),
                               ParaDate = row.GetField<DateTime>(10),
                               InsParaSeqNum = row.GetField<Int32>(11),
                               InsSeqNum = row.GetField<Int32>(12),
                               InternalContact = row.IsDBNull(13) ? null : row.GetField<String>(13),
                               ExternalContact = row.IsDBNull(14) ? null : row.GetField<String>(14),
                               InternalConfStatus = row.IsDBNull(15) ? null : row.GetField<String>(15),
                               ExternalConfStatus = row.IsDBNull(16) ? null : row.GetField<String>(16),
                               PymtType = row.GetField<Int16>(17),
                               Currency = row.IsDBNull(18) ? null : row.GetField<String>(18),
                               DeliveryType = row.IsDBNull(19) ? null : row.GetField<String>(19),
                               EventSource = row.IsDBNull(20) ? null : row.GetField<String>(20),
                               Unit = row.IsDBNull(21) ? null : row.GetField<String>(21)
                           };

            return item;
        } 
    }
}