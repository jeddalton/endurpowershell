﻿namespace Gazprom.MT.PowerShell.Endur.Model
{
    using System;

    public class EndurEvent
    {
        // event_num
        public virtual int EventNum { get; set; }

        // tran_num
        public virtual int TranNum { get; set; }

        // ins_num
        public virtual int InsNum { get; set; }

        // document_num
        public virtual int DocumentNum { get; set; }

        // event_type
        public virtual string EventType { get; set; }

        // event_date
        public virtual DateTime EventDate { get; set; }

        // para_ins_num
        public virtual int ParaInsNum { get; set; }

        // para_tran_num
        public virtual int ParaTranNum { get; set; }

        // para_price_rate
        public virtual double ParaPriceRate { get; set; }

        // para_position
        public virtual double ParaPosition { get; set; }

        // para_date
        public virtual DateTime ParaDate { get; set; }

        // ins_para_seq_num
        public virtual int InsParaSeqNum { get; set; }

        // ins_seq_num
        public virtual int InsSeqNum { get; set; }

        // internal_contact
        public virtual string InternalContact { get; set; }

        // external_contact
        public virtual string ExternalContact { get; set; }

        // internal_conf_status
        public virtual string InternalConfStatus { get; set; }

        // external_conf_status
        public virtual string ExternalConfStatus { get; set; }

        // pymt_type
        public virtual short PymtType { get; set; }

        // currency
        public virtual string Currency { get; set; }

        // delivery_type
        public virtual string DeliveryType { get; set; }

        // event_source
        public virtual string EventSource { get; set; }

        // unit
        public virtual string Unit { get; set; }
    }
}