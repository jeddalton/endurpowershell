using System;
using System.IO;
using System.Management.Automation;
using Gazprom.MT.GGTP.DealMigration.ExcelToEndur;
using Gazprom.MT.GGTP.Endur.Common.Context;
using log4net;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsData.Import, "EndurTransactionFromExcel")]
    public class ImportEndurTransactionFromExcel : EndurCmdletBase
    {
        private static readonly ILog Logger;

        static ImportEndurTransactionFromExcel()
        {
            Logger = LogManager.GetLogger("PowerShell");
        }

        [Parameter(Mandatory = true, Position = 0)]
        public FileInfo ExcelFileName { get; set; }

        [Parameter(Mandatory = true, Position = 1)]
        public string SheetName { get; set; }

        protected override void ProcessRecord()
        {
            var context = new EndurAppContext(Session) {Log = Logger};
            var loader = new ExcelToEndurDealLoader(context, ExcelFileName.FullName);

            try
            {
                var results = loader.Load(SheetName);

                WriteObject(results, true);
            }
            catch (DealLoadException ex)
            {
                var message = string.Format(
                    "{0}: {1}",
                    ex.Message,
                    ex.InnerException != null ? ex.InnerException.Message : string.Empty);

                throw new InvalidOperationException(message, ex);
            }
        }
    }
}