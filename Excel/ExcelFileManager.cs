﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;

using Aspose.Cells;

namespace Gazprom.MT.PowerShell.Endur.Excel
{
    public class ExcelFileManager
    {
        public Workbook Workbook { get; private set; }
        public IEnumerable<string> WorkSheetNames
        {
            get
            {
                return WorkSheets.Select(w => w.Name);
            }
        }

        public IEnumerable<Worksheet> WorkSheets
        {
            get
            {
                return this.Workbook.Worksheets.Cast<Worksheet>();
            }
        }

        public ExcelFileManager(string excelFileName)
        {
            if (!File.Exists(excelFileName))
                throw new ApplicationException(string.Format("'{0}' file does not exist.", excelFileName));

            var loadOptions = new LoadOptions(LoadFormat.Auto);
            this.Workbook = new Workbook(excelFileName, loadOptions) { Settings = { Shared = true } };
        }

        public DataColumnCollection GetColumns(string sheetName)
        {
            var worksheet = GetWorkSheet(sheetName);
            return worksheet.Cells.ExportDataTable(0, 0, 1, 1, true, true).Columns;
        }

        public DataTable Read(string sheetName)
        {
            var worksheet = GetWorkSheet(sheetName);
            return Read(worksheet);
        }

        public DataTable Read(Worksheet worksheet)
        {
            return worksheet.Cells.ExportDataTable(0, 0, worksheet.Cells.MaxDataRow + 1, worksheet.Cells.MaxDataColumn + 1, true, true);
        }

        public Worksheet GetWorkSheet(string sheetName)
        {
            var worksheet = Workbook.Worksheets[sheetName];
            worksheet.Cells.DeleteBlankRows();
            worksheet.Cells.DeleteBlankColumns();
            return worksheet;
        }
    }
}