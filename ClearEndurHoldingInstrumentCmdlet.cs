using System;
using System.Collections.Generic;
using System.Management.Automation;
using Olf.Openrisk.IO;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsCommon.Clear, "EndurHoldingInstrument")]
    public class ClearEndurHoldingInstrumentCmdlet : EndurCmdletBase
    {
        [Parameter(Mandatory = false)]
        public string[] ContractCodes { get; set; }

        protected override void ProcessRecord()
        {
            // Create the query
            WriteVerbose("Clearing transactions");
            using (var query = CreateTransactionQuery(this.ContractCodes))
            {
                using (var result = query.Execute())
                {
                    foreach (var tranNum in result.ObjectIds)
                    {
                        PurgeTransaction(tranNum);
                    }
                }
            }

            WriteVerbose("Clearing Holding Instruments");
            using (var query = CreateHoldingQuery(this.ContractCodes))
            {
                using (var result = query.Execute())
                {
                    foreach (var tranNum in result.ObjectIds)
                    {
                        PurgeTransaction(tranNum);
                    }
                }

            }
        }

        private void PurgeTransaction(int tranNum)
        {
            var sql = String.Format("EXEC [dbo].[USER_SP_RunSql] 'EXEC [dbo].[purge_transaction] {0}, 0, 0'",
                        tranNum);

            this.Session.IOFactory.RunSQL(sql).Dispose();
        }

        Query CreateQuery(IEnumerable<String> contractCodes)
        {
            var query = this.Session.IOFactory.CreateQuery(EnumQueryType.Transaction);

            var insBondFutConfigTable = query.ConfigPage.GetConfigTable("Ins Bond/Fut");
            var contractCodeField = insBondFutConfigTable.GetField("contract_code");


            var page = query.AddPage();

            foreach (var contractCode in contractCodes)
            {
                page.AddValue(contractCodeField, contractCode);
            }

            return query;
        }

        Query CreateTransactionQuery(IEnumerable<String> contractCodes)
        {
            var query = CreateQuery(contractCodes);
            var page = query.Pages.GetItem(0);

            var tranConfigTable = query.ConfigPage.GetConfigTable("Transaction");
            var tranTypeField = tranConfigTable.GetField("tran_type");
            var tranStatusField = tranConfigTable.GetField("tran_status");

            page.AddValue(tranTypeField, "Trading");

            var statuses = new[]
            {
                "Amended",
                "Amended New",
                "Cancelled",
                "Cancelled New",
                "Closeout",
                "Deleted",
                "Matured",
                "New",
                "Pending",
                "Proposed",
                "Validated",
            };


            foreach (var status in statuses)
            {
                page.AddValue(tranStatusField, status);
            }

            return query;
        }

        Query CreateHoldingQuery(IEnumerable<String> contractCodes)
        {
            var query = CreateQuery(contractCodes);
            var page = query.Pages.GetItem(0);

            var tranConfigTable = query.ConfigPage.GetConfigTable("Transaction");
            var tranTypeField = tranConfigTable.GetField("tran_type");

            page.AddValue(tranTypeField, "Holding");

            return query;
        }
    }
}