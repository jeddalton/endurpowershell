﻿namespace Gazprom.MT.PowerShell.Endur
{
    using System.Linq;
    using System.Management.Automation;

    using Gazprom.MT.PowerShell.Endur.Code;

    using Olf.Openrisk.Staticdata;
    using Olf.Openrisk.Trading;

    [Cmdlet(VerbsCommon.Get, "EndurBrokerFee")]
    public class GetEndurBrokerFee : EndurCmdletBase
    {
        [Parameter(Mandatory = true, Position = 0, ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        public int TransactionId { get; set; }

        protected override void ProcessRecord()
        {
            using (var transaction = this.Session.TradingFactory.RetrieveTransactionById(this.TransactionId))
            {
                if (transaction == null)
                {
                    return;
                }

                using (var field = transaction.GetField(EnumTransactionFieldId.BrokerFeeTable))
                {
                    if (!field.IsApplicable)
                    {
                        return;
                    }

                    using (var brokerFeeTable = field.GetValueAsTable())
                    {
                        for (var row = 0; row < brokerFeeTable.RowCount; row++)
                        {
                            var feeType = this.Session.StaticDataFactory.GetName(
                                EnumReferenceTable.ProvType, brokerFeeTable.GetInt("prov_type", row));
                            var calculationType = this.Session.StaticDataFactory.GetName(
                                EnumReferenceTable.ProvUnit, brokerFeeTable.GetInt("broker_unit", row));
                            var brokerName = this.Session.StaticDataFactory.GetName(
                                EnumReferenceTable.Party, brokerFeeTable.GetInt("broker_id", row));
                            var currency = this.Session.StaticDataFactory.GetName(
                                EnumReferenceTable.Currency, brokerFeeTable.GetInt("reserved_currency", row));
                            var status = this.Session.StaticDataFactory.GetName(
                                EnumReferenceTable.ProvStatus, brokerFeeTable.GetInt("prov_status", row));
                            var appliesTo = this.Session.StaticDataFactory.GetName(
                                EnumReferenceTable.ProvType, brokerFeeTable.GetInt("applies_to", row));
                            var usingFeeDefintion = this.Session.StaticDataFactory.GetName(
                                EnumReferenceTable.YesNo, brokerFeeTable.GetInt("using_fee_definition", row));

                            this.WriteObject(new
                                {
                                    transaction.TransactionId,
                                    DealNumber = transaction.DealTrackingId,
                                    Reference = transaction.GetValueAsString(EnumTransactionFieldId.ReferenceString),
                                    FeeType = feeType,
                                    CalculationType = calculationType,
                                    Rate = brokerFeeTable.GetDouble("broker_rate", row),
                                    Fee = brokerFeeTable.GetDouble("reserved_amt", row),
                                    Broker = brokerName,
                                    Currency = currency,
                                    Status = status,
                                    AppliesTo = appliesTo,
                                    UsingFeeDefinition = usingFeeDefintion,
                                    PartyBrokerInfoId = brokerFeeTable.GetInt("party_broker_info_id", row)
                                });
                        }
                    }
                }
            }
        }
    }
}