using System.Management.Automation;
using Gazprom.MT.PowerShell.Endur.Code;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsCommon.Remove, "EndurAssembly", DefaultParameterSetName = "NameSet")]
    public class RemoveEndurAssemblyCmdlet : EndurCmdletBase
    {
        [Parameter(Mandatory = true, Position = 0, ParameterSetName = "NameSet")]
        public string Name { get; set; }

        [Parameter(Mandatory = true, Position = 0, ParameterSetName = "AssemblySet", ValueFromPipeline = true)]
        public object Assembly { get; set; }

        protected override void ProcessRecord()
        {
            var olfPackageWrapper = OlfPackageWrapper.Create();

            switch (ParameterSetName)
            {
                case "NameSet":
                    olfPackageWrapper.RemoveAssembly(Name);
                    break;

                case "AssemblySet":
                    olfPackageWrapper.RemoveAssembly(Assembly);
                    break;
            }
        }
    }
}