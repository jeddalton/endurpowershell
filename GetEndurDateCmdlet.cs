namespace Gazprom.MT.PowerShell.Endur
{
    using System;
    using System.Linq;
    using System.Management.Automation;

    using Olf.Openrisk.Staticdata;

    [Cmdlet(VerbsCommon.Get, "EndurDate")]
    public class GetEndurDateCmdlet : EndurCmdletBase
    {
        [Parameter(Position = 0, ValueFromPipelineByPropertyName = true, ValueFromPipeline = true)]
        public string[] Locations { get; set; }

        protected override void ProcessRecord()
        {
            if (this.Locations == null || this.Locations.Length == 0)
            {
                this.Locations = this.Session.StaticDataFactory.GetReferenceChoices(EnumReferenceTable.TradingLocation).Select(
                    l =>
                    {
                        var name = l.Name;
                        l.Dispose();

                        return name;
                    })
                    .Union(new[] { "Global" })
                    .ToArray();
            }

            var locationIds =
                this.Locations.Select(
                    l =>
                    l.Equals("Global", StringComparison.InvariantCultureIgnoreCase)
                        ? 0
                        : this.Session.StaticDataFactory.GetId(EnumReferenceTable.TradingLocation, l)).ToArray();

            var sql = string.Format(
                "SELECT * FROM system_dates WHERE trading_location_id IN ({0})",
                string.Join(",", locationIds));

            using (var table = this.Session.IOFactory.RunSQL(sql))
            {
                var records =
                    Enumerable.Range(0, table.RowCount)
                        .Select(
                            r =>
                            new
                            {
                                TradingLocation = this.GetTradingLocationName(table.GetInt("trading_location_id", r)),
                                BusinessDate = table.GetDate("business_date", r),
                                PreviousBusinessDate = table.GetDate("prev_business_date", r),
                                ProcessingDate = table.GetDate("processing_date", r),
                                TradingDate = table.GetDate("trading_date", r),
                                EodDate = table.GetDate("eod_date", r)
                            })
                        .ToArray();

                this.WriteObject(records, true);
            }
        }

        private string GetTradingLocationName(int tradingLocationId)
        {
            return tradingLocationId == 0
                       ? "Global"
                       : this.Session.StaticDataFactory.GetName(EnumReferenceTable.TradingLocation, tradingLocationId);
        }
    }
}