﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management.Automation;
using System.Text;
using Olf.Openrisk.Table;

namespace Gazprom.MT.PowerShell.Endur
{
    /// <summary>
    /// A Cmdlet to Fix deals between a given date range
    /// </summary>
    [Cmdlet(VerbsCommon.Set, "FixDeals")]
    public class SetFixDealsCmdlet : EndurCmdletBase
    {
        [Alias("Start")]
        [Parameter(Mandatory = true, Position = 0)]
        public DateTime StartDate { get; set; }

        [Alias("End")]
        [Parameter(Mandatory = true, Position = 1)]
        public DateTime EndDate { get; set; }

        protected override void ProcessRecord()
        {
            using (var table = this.Session.TableFactory.CreateTable())
            {
                table.AddColumn("start_date", EnumColType.String);
                table.AddColumn("end_date", EnumColType.String);
                table.AddRow();
                table.SetString("start_date", 0, this.StartDate.ToString("yyyyMMdd"));
                table.SetString("end_date", 0, this.EndDate.ToString("yyyyMMdd"));

                this.Session.ControlFactory.RunScriptText(GetFixDealInlineScript(), table);
                WriteObject("Deals fixed successfully.");
            }
        }

        /// <summary>Gets the fix deal inline script. </summary>
        /// <returns>the script</returns>
        private static string GetFixDealInlineScript()
        {
            const String script =
                "void main() \r\n" +
                "{  \r\n" +
                "   string sFirstDay, sLastDay; \r\n" +
                "   int intFirstDay, intLastDay, i, j, intFailed, intQueryID; \r\n" +
                "   TablePtr tblDeals,tblArgs,tblReset; \r\n" +
                "\r\n" +
                "    sFirstDay = TABLE_GetStringN(argt, \"start_date\", 1); \r\n" +
                "    sLastDay = TABLE_GetStringN(argt, \"end_date\", 1); \r\n" +
                "    intFirstDay = DATE_ConvertYYYYMMDDToJd(sFirstDay); \r\n" +
                "    intLastDay = DATE_ConvertYYYYMMDDToJd(sLastDay); \r\n" +
                "\r\n" +
                "   tblArgs = TABLE_New(); \r\n" +
                "   TABLE_AddCol(tblArgs, \"date\", COL_INT); \r\n" +
                "   TABLE_AddRow(tblArgs); \r\n" +
                "\r\n" +
                "   for (i = intFirstDay; i <= intLastDay; i ++) \r\n" +
                "   {  \r\n" +
                "      DATE_SetCurrentDate(i); \r\n" +
                "      print(\"\\nFixing Deals for \" + TABLE_FormatDateInt(i) + \"... \"); \r\n" +
                "      tblDeals = TABLE_New();\r\n" +
                "\r\n" +
                "      TABLE_ExecISql(tblDeals, \"SELECT ab.* FROM ab_tran ab, reset r WHERE ab.ins_num = r.ins_num and r.value_status = 2 and r.reset_date = '\" + DATE_FormatJdForDbAccess(i) + \"' and ab.trade_flag = 1 and ab.tran_status IN(2,3,4,22) and ab.offset_tran_type in (0,1)\");\r\n" +
                "      if (TABLE_GetNumRows(tblDeals) == 0)\r\n" +
                "      {\r\n" +
                "         print(\"No resets needed\");\r\n" +
                "         continue;\r\n" +
                "      } \r\n" +
                "\r\n" +
                "      print(\"Reseting \" + TABLE_GetNumRows(tblDeals));\r\n" +
                "      intQueryID = TABLE_QueryInsertN(tblDeals, \"tran_num\");\r\n" +
                "      tblReset = EOD_ResetDealsByQid(intQueryID, i);\r\n" +
                "      intFailed = 0;\r\n" +
                "\r\n" +
                "       for (j = 1; j <= TABLE_GetNumRows(tblReset); j ++) \r\n" +
                "           if (!TABLE_GetIntN(tblReset, \"success\", j)) \r\n" +
                "               intFailed ++; \r\n" +
                "       print(\" \" + intFailed + \" failed\");\r\n" +
                "\r\n" +
                "       TABLE_Destroy(tblReset);\r\n" +
                "       TABLE_Destroy(tblDeals);\r\n" +
                "   }\r\n" +
                "\r\n" +
                "   TABLE_Destroy(tblArgs); \r\n" +
                "   print(\"\\nDone.\"); \r\n" +
                "}  \r\n";
            return script;
        }
    }
}
