﻿using System;
using System.IO;
using System.Linq;
using System.Management.Automation;
using System.Text;
using Gazprom.MT.PowerShell.Endur.Code;
using Olf.Openrisk.Staticdata;
using Olf.Openrisk.Table;

namespace Gazprom.MT.PowerShell.Endur
{
    /// <summary>
    ///     A Cmdlet to import historic prices from a CSV file
    /// </summary>
    [Cmdlet(VerbsData.Import, "HistoricPrices")]
    public class ImportHistoricPricesCmdlet : EndurCmdletBase
    {
        private const string IndexIdColumnName = "index_id";
        private const string RefSourceColumnName = "ref_source";
        private const string StartDateColumnName = "start_date";
        private const string IndexLocationColumnName = "index_location";
        private const string DateColumnName = "date";
        private const string EndDateColumnName = "end_date";
        private const string PriceColumnName = "price";


        public ImportHistoricPricesCmdlet()
        {
        }

        protected ImportHistoricPricesCmdlet(IStateRepository stateRepository) : base(stateRepository)
        {
        }

        /// <summary>Gets or sets the historic price data CSV file.</summary>
        /// <value>The historic price data CSV file.</value>
        [Alias("File")]
        [Parameter(Mandatory = true, Position = 0, HelpMessage = "Historic prices data csv file")]
        [ValidateNotNullOrEmpty]
        public string HistoricPricesCsvFile { get; set; }

        protected override void ProcessRecord()
        {
            try
            {
                var fullPath = Path.GetFullPath(HistoricPricesCsvFile);
                if (File.Exists(fullPath))
                {
                    using (var hpTable = PrepareHistoricPriceTable())
                    {
                        File.ReadAllLines(fullPath, Encoding.Default)
                            .Skip(1)
                            .Where(line => !string.IsNullOrWhiteSpace(line))
                            .Select(TryParse)
                            .Where(hp => hp != null)
                            .ForEach(hp => { AddRowToHistoricTable(hpTable, hp); });

                        if (hpTable.Rows.Any())
                        {
                            var result = Session.ControlFactory.RunScriptText("void main() { TABLE_ImportHistoricalPrices(argt); }", hpTable);
                            hpTable.ClearData();
                        }
                    }
                    WriteVerbose("Historic price loading completed.");
                }
                else throw new Exception(HistoricPricesCsvFile + " File not found.");
            }
            catch (Exception e)
            {
                var errorRecord = new ErrorRecord(e, "Import-HistoricPrices", ErrorCategory.NotSpecified, null);
                WriteError(errorRecord);
            }
        }

        private HistoricPrice TryParse(string line)
        {
            try
            {
                return HistoricPrice.Create(Session.CalendarFactory, line);
            }
            catch (Exception ex)
            {
                WriteWarning(
                    "Failed loading historic price for " + line + " due to: " + ex.Message);
            }

            return null;
        }


        private void AddRowToHistoricTable(Table hpTable, HistoricPrice historicPrice)
        {
            try
            {
                var choices = Session.StaticDataFactory.GetReferenceChoices(EnumReferenceTable.RefSource);

                var rowNumber = hpTable.AddRows(1);
                hpTable.SetInt(IndexIdColumnName, rowNumber, Session.Market.GetIndex(historicPrice.Index).Id);
                hpTable.SetInt(RefSourceColumnName, rowNumber, choices.GetId(historicPrice.Reference));
                hpTable.SetInt(DateColumnName, rowNumber, historicPrice.JDate);
                hpTable.SetInt(StartDateColumnName, rowNumber, historicPrice.JStartDate);
                hpTable.SetInt(EndDateColumnName, rowNumber, historicPrice.JEndDate);
                hpTable.SetInt(IndexLocationColumnName, rowNumber, historicPrice.LocationId);
                hpTable.SetDouble(PriceColumnName, rowNumber, historicPrice.NewValue);
            }
            catch (Exception ex)
            {
                WriteWarning(
                    "Failed loading historic price for " + historicPrice.Index + ", ref source: " + historicPrice.Reference + " on reset date: "
                    + historicPrice.JDate + " due to: " + ex.Message);
            }
        }

        private Table PrepareHistoricPriceTable()
        {
            var hpTable = Session.TableFactory.CreateTable("Historic_Prices");
            hpTable.AddColumn(IndexIdColumnName, EnumColType.Int);
            hpTable.AddColumn(RefSourceColumnName, EnumColType.Int);
            hpTable.AddColumn(DateColumnName, EnumColType.Int);
            hpTable.AddColumn(StartDateColumnName, EnumColType.Int);
            hpTable.AddColumn(EndDateColumnName, EnumColType.Int);
            hpTable.AddColumn(IndexLocationColumnName, EnumColType.Int);
            hpTable.AddColumn(PriceColumnName, EnumColType.Double);
            return hpTable;
        }
    }
}