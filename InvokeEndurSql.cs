using System;
using System.Management.Automation;
using Gazprom.MT.PowerShell.Endur.Code;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsLifecycle.Invoke, "EndurSql")]
    public class InvokeEndurSql : EndurCmdletBase
    {
        [Parameter(Mandatory = true, Position = 0, ValueFromPipeline = true)]
        public String Sql { get; set; }

        protected override void ProcessRecord()
        {
            using (var result = this.Session.IOFactory.RunSQL(this.Sql))
            {
                if (result != null && result.RowCount > 0)
                {
                    this.WriteObject(EndurPowershellUtil.TableToPSObjects(result));
                }
            }
        }
    }
}