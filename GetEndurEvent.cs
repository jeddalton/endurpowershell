﻿namespace Gazprom.MT.PowerShell.Endur
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Management.Automation;
    using System.Text;

    using Gazprom.MT.PowerShell.Endur.Code;
    using Gazprom.MT.PowerShell.Endur.Model;

    using Olf.Openrisk.Trading;

    [Cmdlet(VerbsCommon.Get, "EndurEvent")]
    public class GetEndurEvent : EndurCmdletBase
    {
        private const string TransactionIdSet = "TransactionIdSet";

        private const string TransactionSet = "TransactionSet";

        private const string DealNumberSet = "DealNumberSet";

        public GetEndurEvent()
        {
            this.FromDate = DateTime.MaxValue;
            this.ToDate = DateTime.MinValue;
        }

        [Parameter(Mandatory = true, Position = 0, ValueFromPipeline = true, ParameterSetName = TransactionIdSet)]
        public int[] TransactionId { get; set; }

        [Parameter(Mandatory = true, Position = 0, ValueFromPipeline = true, ParameterSetName = DealNumberSet)]
        public int[] DealNumber { get; set; }

        [Parameter(Mandatory = true, Position = 0, ValueFromPipeline = true, ParameterSetName = TransactionSet)]
        public Transaction[] Transaction { get; set; }

        [Parameter(Position = 1, ParameterSetName = TransactionSet)]
        [Parameter(Position = 1, ParameterSetName = TransactionIdSet)]
        [Parameter(Position = 1, ParameterSetName = DealNumberSet)]
        public string[] EventTypes { get; set; }

        [Parameter(ParameterSetName = TransactionSet)]
        [Parameter(ParameterSetName = TransactionIdSet)]
        [Parameter(ParameterSetName = DealNumberSet)]
        public DateTime FromDate { get; set; }

        [Parameter(ParameterSetName = TransactionSet)]
        [Parameter(ParameterSetName = TransactionIdSet)]
        [Parameter(ParameterSetName = DealNumberSet)]
        public DateTime ToDate { get; set; }

        [Parameter(ParameterSetName = TransactionIdSet)]
        [Parameter(ParameterSetName = TransactionSet)]
        public SwitchParameter AllEvents { get; set; }

        [Parameter(ParameterSetName = TransactionSet)]
        [Parameter(ParameterSetName = TransactionIdSet)]
        [Parameter(ParameterSetName = DealNumberSet)]
        public string[] EventSources { get; set; }

        [Parameter(ParameterSetName = TransactionSet)]
        [Parameter(ParameterSetName = TransactionIdSet)]
        [Parameter(ParameterSetName = DealNumberSet)]
        public string EventStatus{ get; set; }

        protected override void ProcessRecord()
        {
            var ids = this.GetIds();
            var statements = ids.Select(GenerateSelectStatement);

            foreach (var sql in statements)
            {
                using (var result = this.Session.IOFactory.RunSQL(sql))
                {
                    this.WriteObject(EndurPowershellUtil.TableToPSObjects(result), true);
                }
            }
        }

        private string GenerateSelectStatement(int id)
        {
            var sql = new StringBuilder();
            sql.AppendLine(EndurEventMapper.SelectSql);

            if ((this.ParameterSetName == TransactionIdSet || this.ParameterSetName == TransactionSet) && !this.AllEvents.IsPresent)
            {
                sql.AppendFormat("WHERE te.tran_num={0}", id).AppendLine();
            }
            else
            {
                sql.AppendLine("WHERE te.tran_num IN");
                sql.AppendLine("(");
                sql.AppendLine("SELECT t.tran_num FROM ab_tran t WHERE t.deal_tracking_num=");
                switch (this.ParameterSetName)
                {
                    case DealNumberSet:
                    case TransactionSet:
                        sql.AppendLine(id.ToString(CultureInfo.InvariantCulture));
                        break;

                    case TransactionIdSet:
                        sql.AppendFormat(
                            "( SELECT tt.deal_tracking_num FROM ab_tran tt WHERE tt.tran_num={0} )", id)
                           .AppendLine();
                        break;
                }

                sql.AppendLine(")");
            }

            if (this.EventTypes != null && this.EventTypes.Length > 0)
            {
                sql.AppendFormat(
                    "AND et.name IN ({0})", string.Join(",", this.EventTypes.Select(x => string.Format("'{0}'", x))))
                   .AppendLine();
            }

            if (this.FromDate != DateTime.MaxValue)
            {
                sql.AppendFormat("AND te.event_date>='{0}'", this.Session.CalendarFactory.GetSQLString(this.FromDate))
                   .AppendLine();
            }

            if (this.ToDate != DateTime.MinValue)
            {
                sql.AppendFormat("AND te.event_date<='{0}'", this.Session.CalendarFactory.GetSQLString(this.ToDate))
                   .AppendLine();
            }

            if (this.EventSources != null && this.EventSources.Length > 0)
            {
                sql.AppendFormat(
                    "AND es.event_source_name IN ({0})",
                    string.Join(",", this.EventSources.Select(x => string.Format("'{0}'", x)))).AppendLine();
            }

            if (!string.IsNullOrEmpty(this.EventStatus))
            {
                sql.AppendFormat("AND int_status.name='{0}'", this.EventStatus).AppendLine();
            }

            sql.AppendLine("ORDER BY te.event_num");

            return sql.ToString();
        }

        private IEnumerable<int> GetIds()
        {
            switch (this.ParameterSetName)
            {
                case TransactionIdSet:
                    return this.TransactionId;

                case TransactionSet:
                    return this.AllEvents.IsPresent ? this.Transaction.Select(x => x.DealTrackingId) : this.Transaction.Select(x => x.TransactionId);

                case DealNumberSet:
                    return this.DealNumber;

                default:
                    throw new InvalidOperationException("Unhandled parameter set " + this.ParameterSetName);
            }
        }
    }
}