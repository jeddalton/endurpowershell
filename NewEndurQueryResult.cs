﻿using System.Linq;
using System.Management.Automation;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsCommon.New, "EndurQueryResult")]
    public class NewEndurQueryResult : EndurCmdletBase
    {
        [Parameter(Mandatory = true, Position = 0)]
        public int[] Ids { get; set; }

        protected override void ProcessRecord()
        {
            var queryResult = Session.IOFactory.CreateQueryResult();
            queryResult.Add(Ids.Distinct().ToArray());

            WriteObject(queryResult.Id);
        }
    }
}
