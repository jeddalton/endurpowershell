﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management.Automation;
using Olf.Openrisk.Table;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsCommon.Set, "EndurMarketData")]
    public class SetEndurMarketDataCmdlet : EndurCmdletBase
    {
        [Parameter(Mandatory = true, ValueFromPipeline = true, Position = 0)]
        public PSObject Data { get; set; }

        //[Alias("SaveClose")]
        [Parameter(Mandatory = true, Position = 0)]
        public bool SaveClose { get; set; }

        private readonly List<MarketDataPoint> _data = new List<MarketDataPoint>();

        protected override void ProcessRecord()
        {
            var properties = this.Data.Properties.Where(x => x.IsGettable).ToArray();

            if (properties.Length < 3)
            {
                throw new PSInvalidOperationException("Data object does not have enough properties");
            }

            var marketDataPoint = new MarketDataPoint
                                  {
                                      IndexName = properties[0].Value.ToString(),
                                      GridPoint = properties[1].Value.ToString()
                                  };
            if (properties[2].TypeNameOfValue == "System.String")
            {
                Double value;
                if (!Double.TryParse(properties[2].Value.ToString(), out value))
                {
                    this.WriteWarning(String.Format("Cannot convert value '{0}' to number", properties[2].Value));
                    return;
                }
                marketDataPoint.Value = value;
            }
            else
            {
                try
                {
                    marketDataPoint.Value = Convert.ToDouble(properties[2].Value);
                }
                catch (Exception ex)
                {
                    this.WriteWarning(String.Format("Cannot convert value '{0}' to number ({1})", properties[2].Value, ex.Message));
                }
            }

            this._data.Add(marketDataPoint);
        }

        protected override void EndProcessing()
        {
            var indexes = this._data.GroupBy(x => x.IndexName);
            foreach (var indexGroup in indexes)
            {
                if (!SetMarketData(indexGroup)) continue;
                WriteOutputObject(indexGroup);
            }
        }

        private Boolean SetMarketData(IGrouping<string, MarketDataPoint> indexGroup)
        {
            using (var index = this.Session.Market.GetIndex(indexGroup.Key))
            {
                if (index == null)
                {
                    this.WriteWarning(String.Format("Cannot retrieve index '{0}'", indexGroup.Key));
                    return false;
                }

                index.LoadUniversal();

                var table = this.Session.TableFactory.CreateTable("market_data");
                table.AddColumn("GridPoint", EnumColType.String);
                table.AddColumn("Input", EnumColType.Double);

                var rowIndex = 0;
                foreach (var dataPoint in indexGroup)
                {
                    table.AddRow();
                    table.SetString(0, rowIndex, dataPoint.GridPoint);
                    table.SetDouble(1, rowIndex, dataPoint.Value);

                    rowIndex++;
                }

                this.WriteVerbose(String.Format("Setting grid point data for index {0}", index.Name));                
                index.GridPoints.SetInputFromTable(table);
                if (SaveClose) 
                    index.SaveClose();
                else index.SaveUniversal();
                
            }

            return true;
        }

        private void WriteOutputObject(IGrouping<string, MarketDataPoint> indexGroup)
        {
            var output = new PSObject();
            output.Properties.Add(new PSNoteProperty("Index", indexGroup.Key));
            output.Properties.Add(new PSNoteProperty("Data", indexGroup.Select(x => new PSObject(new {x.GridPoint, x.Value})).ToArray()));

            this.WriteObject(output);
        }

        private class MarketDataPoint
        {
            public string IndexName { get; set; }
            public string GridPoint { get; set; }
            public double Value { get; set; }
        }
    }
}