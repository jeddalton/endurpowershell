﻿namespace Gazprom.MT.PowerShell.Endur
{
    using System;
    using System.Management.Automation;

    using Gazprom.MT.PowerShell.Endur.Code;

    using Olf.Openrisk.Market;
    using Olf.Openrisk.Table;

    /// <summary>
    /// A Cmdlet to import forward curve data from a CSV file
    /// </summary>
    [Cmdlet(VerbsCommon.Get, "EndurVolData")]
    public class GetEndurVolDataCmdlet : EndurCmdletBase
    {        
        [Parameter(Mandatory = true, Position = 0, HelpMessage = "Volatility Name")]
        [ValidateNotNullOrEmpty]
        public String VolatilityName { get; set; }

        protected override void ProcessRecord()
        {
            using (var dc = new DisposableCollection())
            {
                var volatility =
                    dc.Add((Volatility)this.Session.Market.GetElement(EnumElementType.Volatility, this.VolatilityName));

                var layer = dc.Add(volatility.GetLayer(0));

                var outputTable = dc.Add(layer.InputTable.Clone() as Table);
                var dateColumn = dc.Add(outputTable.AddColumn("Date", EnumColType.DateTime));
                var sourceColumn = dc.Add(layer.Dimension.GridPointTable.GetColumn("DateTime"));
                outputTable.CopyColumnData(layer.Dimension.GridPointTable, sourceColumn.Number, dateColumn.Number);
                WriteObject(EndurPowershellUtil.TableToPSObjects(outputTable), true);
            }
        }
    }
}
