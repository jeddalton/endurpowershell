﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management.Automation;
using Olf.Openrisk.Market;

namespace Gazprom.MT.PowerShell.Endur
{
    public enum EndurIndexSaveType
    {
        Close,
        Universal
    }

    [Cmdlet(VerbsData.Save, "EndurIndex")]
    public class SaveEndurIndexCmdlet : EndurCmdletBase
    {
        [Parameter(ValueFromPipeline = true, Position = 0)]
        public String[] Names { get; set; }

        [Parameter(Position = 1)]
        public EndurIndexSaveType Type { get; set; }

        [Parameter]
        public SwitchParameter Bulk { get; set; }

        [Parameter]
        public SwitchParameter ClearCache { get; set; }

        [Parameter]
        public DateTime? Date { get; set; }

        private readonly List<String> _names = new List<string>();

        public SaveEndurIndexCmdlet()
        {
            this.Type = EndurIndexSaveType.Close;
        }

        protected override void ProcessRecord()
        {
            if (this.Names != null)
            {
                this._names.AddRange(this.Names);
            }
            else
            {
                this._names.AddRange(this.Session.Market.IndexChoices.Select(x => x.Name));
            }
        }

        protected override void EndProcessing()
        {
            Action<DataSet> method;
            switch (this.Type)
            {
                case EndurIndexSaveType.Close:
                    if (this.Date.HasValue)
                    {
                        method = x => x.SaveClose(this.Date.Value);
                    }
                    else
                    {
                        method = x => x.SaveClose();

                    }
                    break;

                case EndurIndexSaveType.Universal:
                    method = x => x.SaveUniversal();
                    break;

                default:
                    throw new PSNotSupportedException(String.Format("Type of {0} not supported", this.Type));
            }

            if (this.ClearCache.IsPresent)
            {
                this.Session.Debug.FlushCache();
            }

            foreach (var name in this._names)
            {
                var index = this.Session.Market.GetIndex(name);
                if (!this.Bulk.IsPresent)
                {
                    this.WriteVerbose(String.Format("Saving index {0} ({1})", index.Name, this.Type));
                    method(index);
                }
            }

            if (this.Bulk.IsPresent)
            {
                this.WriteVerbose(String.Format("Doing bulk save ({0})", this.Type));
                method(this.Session.Market);
            }
        }
    }
}