﻿using System;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;

namespace Gazprom.MT.PowerShell.Endur
{
    internal static class Externals
    {
        [DllImport("olf_sdb.dll")]
        public static extern IntPtr get_ssql(int connection);

        [DllImport("olf_sdb.dll")]
        public static extern Int32 get_unique_num(IntPtr ssqlPtr, string description);

        [DllImport("olf_sdb.dll")]
        public static extern String retrieve_company_name(IntPtr ssqlPtr);

        [DllImport("olf_oldb_odbc")]
        public static extern void syb_begin_tran_priv(IntPtr ssqlPtr);

        [DllImport("olf_oldb_odbc")]
        public static extern void syb_commit_tran_priv(IntPtr ssqlPtr);

        [DllImport("olf_oldb_odbc")]
        public static extern void syb_rollback_tran_priv(IntPtr ssqlPtr);

        [DllImport("olf_fnd_services")]
        public static extern Int32 WFLOW_StartWorkflow(String workflowName, Int32 seq);

        [DllImport("olfapi_reporting")]
        public static extern Int32 oCRYSTAL_ImportTemplateFileToDB(String filename, String dirNodeName);

        [DllImport("olf_wflow.dll")]
        public static extern void send_job_cfg_changed_nfy(int id);
    }
}
