using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Management.Automation;
using Olf.Openrisk.Trading;
using Gazprom.MT.PowerShell.Endur.Code;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsCommon.Get, "EndurTransactionFieldValue")]
    public class GetEndurTransactionFieldValueCmdlet : EndurCmdletBase
    {
        [Parameter(Mandatory = false)]
        public String[] FieldExpressions { get; set; }

        [Parameter(Mandatory = false)]
        public String[] TranfFields { get; set; }

        [Parameter(Mandatory = true, Position = 0, ValueFromPipeline = true, ParameterSetName = "TransactionIdSet")]
        public Int32 TransactionId { get; set; }

        [Parameter(Mandatory = true, Position = 0, ValueFromPipeline = true, ParameterSetName = "TransactionSet")]
        public Transaction Transaction { get; set; }

        protected override void ProcessRecord()
        {
            var fieldExpressions = new List<String>();

            switch (this.ParameterSetName)
            {
                case "FieldExpressionSet":
                    fieldExpressions.AddRange(this.FieldExpressions);
                    break;
            }

            var result = new PSObject();

            var fieldDefinitions = GetFieldDefinitions();

            Transaction transaction = null;

            var shouldDispose = false;

            switch (this.ParameterSetName)
            {
                case "TransactionIdSet":
                    transaction = GetTransaction();
                    shouldDispose = true;
                    break;

                case "TransactionSet":
                    transaction = this.Transaction;
                    break;
            }

            foreach (var fieldDefinition in fieldDefinitions)
            {
                var tradingObject = fieldDefinition.Expression(transaction);
                var field = GetTradingObjectField(tradingObject, fieldDefinition.FieldName);
                var fieldName = fieldDefinition.FieldName;

                AddField(field, fieldName, result);
            }

            foreach (var field in GetTranfFields(transaction))
            {
                AddField(field, field.TranfId.ToString(), result);
            }

            WriteObject(result);

            if (transaction != null && shouldDispose)
            {
                transaction.Dispose();
            }
        }

        private IEnumerable<Field> GetTranfFields(Transaction transaction)
        {
            var fields = new List<Field>();
            if (this.TranfFields == null) return fields;

            foreach (var fieldDefinition in this.TranfFields)
            {
                var parts = fieldDefinition.Split(',');

                var tranfEnum = (EnumTranfField)Enum.Parse(typeof(EnumTranfField), parts[0]);
                Field field = null;
                switch (parts.Length)
                {
                    case 2:
                        field = transaction.RetrieveField(tranfEnum, Int32.Parse(parts[1]));
                        break;

                    case 3:
                        field = transaction.RetrieveField(tranfEnum, Int32.Parse(parts[1]), Int32.Parse(parts[2]));
                        break;

                    case 4:
                        field = transaction.RetrieveField(tranfEnum, Int32.Parse(parts[1]), Int32.Parse(parts[2]), Int32.Parse(parts[3]));
                        break;

                    case 5:
                        field = transaction.RetrieveField(tranfEnum, Int32.Parse(parts[1]), Int32.Parse(parts[2]), Int32.Parse(parts[3]), Int32.Parse(parts[4]));
                        break;

                    case 6:
                        field = transaction.RetrieveField(tranfEnum, Int32.Parse(parts[1]), Int32.Parse(parts[2]), Int32.Parse(parts[3]), Int32.Parse(parts[4]), Int32.Parse(parts[5]));
                        break;

                    default:
                        throw new InvalidOperationException("Invalid number of Tranf sequence numbers. Accepts 1-5 sequences.");

                }

                fields.Add(field);
            }

            return fields;
        }

        private static void AddField(Field field, string fieldName, PSObject result)
        {
            Object value = null;
            if (field != null)
            {
                value = FieldSetter.GetFieldValue(field);
            }
            var property = new PSNoteProperty(fieldName, value);
            result.Properties.Add(property);
        }

        private Transaction GetTransaction()
        {
            return this.Session.TradingFactory.RetrieveTransactionById(this.TransactionId);
        }

        private IEnumerable<FieldDefinition> GetFieldDefinitions()
        {
            var fieldDefinitions = new List<FieldDefinition>();
            if (this.FieldExpressions == null) return fieldDefinitions;

            var transactionParameter = Expression.Parameter(typeof(Transaction), "t");

            foreach (var fieldExpression in this.FieldExpressions.Select(e => "t." + e))
            {
                var index = fieldExpression.LastIndexOf('.');
                var fieldName = fieldExpression.Substring(index + 1);
                var tradingObject = fieldExpression.Substring(0, index);

                var expression = (Expression<Func<Transaction, Object>>)DynamicLamda.ParseLambda(
                    new[] { transactionParameter },
                    typeof(Object),
                    tradingObject);

                fieldDefinitions.Add(new FieldDefinition { Expression = expression.Compile(), FieldName = fieldName });
            }

            return fieldDefinitions;
        }

        private static Field GetTradingObjectField(Object tradingObject, String fieldName)
        {
            var getFieldMethod = tradingObject.GetType().GetMethod("GetField", new[] { typeof(String) });
            var field = (Field)getFieldMethod.Invoke(tradingObject, new Object[] { fieldName });

            return field;
        }

        private class FieldDefinition
        {
            public Func<Transaction, Object> Expression { get; set; }
            public String FieldName { get; set; }
        }
    }
}