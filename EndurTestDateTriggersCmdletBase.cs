namespace Gazprom.MT.PowerShell.Endur
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.IO;
    using System.Text;
    using System.Text.RegularExpressions;

    using Gazprom.MT.PowerShell.Endur.Sql;

    public class EndurTestDateTriggersCmdletBase : EndurCmdletBase
    {
        private static readonly Regex TriggerRegex = new Regex(@"CREATE TRIGGER (?<name>\w+)");

        protected static string GetTriggerName(string statement)
        {
            var match = TriggerRegex.Match(statement);

            return match.Success ? match.Groups["name"].Value : null;
        }

        protected static IEnumerable<string> GetTriggerStatements()
        {
            var sql = SqlUtil.GetSql("CreateDateTriggers.sql");
            var statements = new List<string>();

            using (var reader = new StringReader(sql))
            {
                var currentStatement = new StringBuilder();
                string line;

                while ((line = reader.ReadLine()) != null)
                {
                    if (!line.Trim().Equals("GO", StringComparison.InvariantCultureIgnoreCase))
                    {
                        currentStatement.AppendLine(line);
                    }
                    else
                    {
                        AddCurrentStatement(currentStatement, statements);
                        currentStatement.Clear();
                    }
                }

                AddCurrentStatement(currentStatement, statements);
            }

            return statements;
        }

        private static void AddCurrentStatement(StringBuilder currentStatement, List<string> statements)
        {
            if (currentStatement.Length > 0)
            {
                statements.Add(currentStatement.ToString().Trim());
            }
        }

        protected bool GetTriggerExists(string triggerName)
        {
            var sql = string.Format(SqlUtil.GetSql("CheckTrigger.sql"), triggerName);

            using (var connection = new SqlConnection(this.EndurConnectionString))
            {
                connection.Open();

                using (var command = new SqlCommand(sql, connection))
                {
                    return command.ExecuteScalar() != null;
                }
            }
        }

        protected void DropTrigger(string triggerName)
        {
            var sql = string.Format(SqlUtil.GetSql("DropTrigger.sql"), triggerName);

            using (var connection = new SqlConnection(this.EndurConnectionString))
            {
                connection.Open();

                using (var command = new SqlCommand(sql, connection))
                {
                    command.ExecuteNonQuery();
                }
            }
        }
    }
}