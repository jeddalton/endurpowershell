﻿namespace Gazprom.MT.PowerShell.Endur
{
    using System.Management.Automation;

    using Gazprom.MT.PowerShell.Endur.Code;

    using Olf.NetToolkit;

    [Cmdlet(VerbsCommon.Get, "EndurSessionTable")]
    public class GetEndurSessionTableCmdlet : EndurCmdletBase
    {
        public GetEndurSessionTableCmdlet()
        {
            this.HostName = "*";
            this.Timeout = 1;
        }

        [Parameter(Position = 0, ValueFromPipelineByPropertyName = true)]
        public string HostName { get; set; }

        [Parameter(Position = 1, ValueFromPipelineByPropertyName = true)]
        public int Timeout { get; set; }

        [Parameter(Position = 2)]
        public SwitchParameter MasterOnly { get; set; }

        protected override void ProcessRecord()
        {
            var pingAll = this.MasterOnly.IsPresent ? 0 : 1;

            using (var ntkTable = SystemUtil.GetSessionTable(this.HostName, this.Timeout, pingAll))
            using (var table = this.Session.TableFactory.FromNtkTable(ntkTable))
            {
                this.WriteObject(EndurPowershellUtil.TableToPSObjects(table), true);
            }
        }
    }
}