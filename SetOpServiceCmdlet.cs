﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management.Automation;
using System.Text;
using Olf.Openrisk.Control;

namespace Gazprom.MT.PowerShell.Endur
{
    /// <summary>
    /// A Cmdlet to stop and start Operations Service
    /// It takes service name as first parameter
    /// and action i.e. stop or start it
    /// </summary>
    [Cmdlet(VerbsCommon.Set, "OpService")]
    public class SetOpServiceCmdlet : EndurCmdletBase
    {
        /// <summary>Gets or sets the name of the service.</summary>
        /// <value>The name of the service or * for all services.</value>
        [Parameter(Mandatory = true, Position = 0, HelpMessage = "Name of the service or use * for all services")]
        public string ServiceName { get; set; }

        [Alias("A")]
        [ValidatePattern("STOP|START")]
        [Parameter(Mandatory = true, Position = 1, HelpMessage = "Valid actions are STOP and START")]
        public string Action { get; set; }

        protected override void ProcessRecord()
        {
            try
            {
                foreach (var svc in GetServicesList())
                {
                    PerformActionOnOpsService(svc, this.Action);
                }
            }
            catch (Exception e)
            {
                WriteError(new ErrorRecord(e, "Set-OpService", ErrorCategory.NotSpecified, null));
            }
        }

        private IEnumerable<string> GetServicesList()
        {
            var sql = String.Format("select defn_name from rsk_exposure_defn  \r\n" +
                            "where defn_status = 1 \r\n");
            if (this.ServiceName != "*")
                sql += " and defn_name = '" + this.ServiceName + "' \r\n";
            var svcTable = this.Session.IOFactory.RunSQL(sql.ToString());

            var list = svcTable.Rows.Select(row => row.Cells.GetItem(0).String).ToList();
            return list;
        }

        private void PerformActionOnOpsService(string svcName, string action)
        {
            var msg = string.Empty;
            var script =
                "void main() \r\n" +
                "{ \r\n" +
                "   int result; \r\n";
                        
            switch (action.ToUpper())
            {
                case "STOP":
                    script += "   result = OPSERVICE_StopMonitoring(\"";
                    msg = "stop";
                    break;
                case "START":
                    script += "   result = OPSERVICE_StartMonitoring(\"";
                    msg = "start";
                    break;
            }
            script += svcName + "\"); \r\n"+
                    "   TABLE_AddCol(returnt, \"result\", COL_INT); \r\n" +
                    "   TABLE_AddRow(returnt); \r\n" +
                    "   TABLE_SetIntN(returnt, \"result\", 1, result); \r\n" +
                    "} \r\n";

            using (var result = this.Session.ControlFactory.RunScriptText(script))
            {
                var resultCode = result.GetInt("result", 0);
                if (resultCode == 0)
                {
                    this.Session.Debug.PrintLine("Failed to "+ msg + " service " + svcName);
                    WriteObject("failed to " + msg + " service " + svcName);
                }
                else
                {
                    if (msg == "start") msg = "started";
                    if (msg == "stop") msg = "stopped";
                    this.Session.Debug.PrintLine("Successfully " + msg + " service " + svcName);
                    WriteObject("Successfully " + msg + " service " + svcName);
                }
            }
        }
    }
}
