﻿using System;
using System.Management.Automation;
using Gazprom.MT.PowerShell.Endur.Code;
using Olf.Openrisk.Trading;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsCommon.Set, "EndurTransactionStatus")]
    public class SetEndurTransactionStatus : EndurCmdletBase
    {
        [Parameter(Mandatory = true, ParameterSetName = "TransactionId", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true, Position = 0)]
        public Int32 TransactionId { get; set; }

        [Parameter(Mandatory = true, ParameterSetName = "Transaction", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true, Position = 0)]
        public Transaction Transaction { get; set; }

        [Parameter(Mandatory = true, ValueFromPipelineByPropertyName = true, Position = 1, ParameterSetName = "TransactionId")]
        [Parameter(Mandatory = true, ValueFromPipelineByPropertyName = true, Position = 1, ParameterSetName = "Transaction")]
        public String Status { get; set; }

        [Parameter]
        public SwitchParameter PassThru { get; set; }

        [Parameter(Mandatory = true, ParameterSetName = "StatusValues")]
        public SwitchParameter StatusValues { get; set; }

        protected override void ProcessRecord()
        {
            if (this.StatusValues.IsPresent)
            {
                this.WriteObject(Enum.GetNames(typeof(EnumTranStatus)), true);
                return;
            }

            using (var dc = new DisposableCollection())
            {
                if (this.ParameterSetName == "TransactionId")
                {
                    this.Transaction = dc.Add(this.Session.TradingFactory.RetrieveTransactionById(this.TransactionId));
                }

                var status = (EnumTranStatus)Enum.Parse(typeof(EnumTranStatus), this.Status, true);

                try
                {
                    this.Transaction.Process(status);
                }
                catch (Exception ex)
                {
                    if (!this.Status.Equals("Amended"))
                    {
                        throw new InvalidOperationException(ex.Message, ex);
                    }
                }

                if (this.PassThru.IsPresent)
                {
                    if (this.ParameterSetName == "TransactionId")
                    {
                        this.WriteObject(this.Transaction.TransactionId);
                    }
                    else
                    {
                        this.WriteObject(this.Transaction);
                    }
                }
            }
        }
    }
}