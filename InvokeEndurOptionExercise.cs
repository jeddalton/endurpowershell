﻿namespace Gazprom.MT.PowerShell.Endur
{
    using System.Collections.Generic;
    using System.Management.Automation;

    using Gazprom.MT.PowerShell.Endur.Code;

    using Olf.Openrisk.Table;

    [Cmdlet(VerbsLifecycle.Invoke, "EndurOptionExercise")]
    public class InvokeEndurOptionExercise : EndurCmdletBase
    {
        private const string EventSql =
            "SELECT te.event_num, te.tran_num, te.tran_position, o.strike \r\n" + "FROM ab_tran_event_view te \r\n"
            + "	INNER JOIN ins_option o ON o.ins_num=te.ins_num AND te.ins_para_seq_num=o.param_seq_num AND te.ins_seq_num=o.option_seq_num \r\n"
            + "WHERE te.event_type=7 -- Exercise \r\n" + "AND te.event_num IN ({0}) \r\n";

        private const string AvsScript =
            "void main() \r\n" + "{ \r\n" + "	int newTranNum, numRows, row, eventNum, tranStatus; \r\n"
            + "	double exerciseAmount, strikePrice; \r\n" + "	numRows = TABLE_GetNumRows(argt); \r\n"
            + "	TABLE_AddCol(returnt, \"EventNum\", COL_INT); \r\n"
            + "	TABLE_AddCol(returnt, \"Result\", COL_INT); \r\n" + "	for (row=1; row<=numRows; row++) \r\n" + "	{ \r\n"
            + "		eventNum = TABLE_GetInt(argt, 1, row); \r\n" + "		exerciseAmount = TABLE_GetDouble(argt, 2, row); \r\n"
            + "		strikePrice = TABLE_GetDouble(argt, 3, row); \r\n" + "		tranStatus = TABLE_GetInt(argt, 4, row); \r\n"
            + "		print(\"Exercising \" + eventNum + \"\\r\\n\"); \r\n"
            + "		newTranNum = TRAN_OptionExerciseByEvent(eventNum, exerciseAmount, strikePrice, tranStatus); \r\n"
            + "		TABLE_AddRow(returnt); \r\n" + "		TABLE_SetCellInt(returnt, 1, row, eventNum); \r\n"
            + "		TABLE_SetCellInt(returnt, 2, row, newTranNum); \r\n" + "	} \r\n" + "} \r\n";

        private List<int> _eventNums;

        [Parameter(Mandatory = true, Position = 0, ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        public int EventNum { get; set; }

        protected override void BeginProcessing()
        {
            this._eventNums = new List<int>();
        }

        protected override void ProcessRecord()
        {
            this._eventNums.Add(this.EventNum);
        }

        protected override void EndProcessing()
        {
            using (var argt = this.Session.TableFactory.CreateTable())
            {
                argt.AddColumn("event_num", EnumColType.Int);
                argt.AddColumn("exercise_amount", EnumColType.Double);
                argt.AddColumn("strike_price", EnumColType.Double);
                argt.AddColumn("tran_status", EnumColType.Int);

                using (
                var details = this.Session.IOFactory.RunSQL(string.Format(EventSql, string.Join(",", this._eventNums))))
                {
                    foreach (var row in details.Rows)
                    {
                        var argtRow = argt.AddRow();

                        argt.SetInt(0, argtRow.Number, (int)details.GetLong(0, row.Number));
                        argt.SetDouble(1, argtRow.Number, details.GetDouble(2, row.Number));
                        argt.SetDouble(2, argtRow.Number, details.GetDouble(3, row.Number));
                        argt.SetInt(3, argtRow.Number, 3);
                    }
                }

                using (var returnt = this.Session.ControlFactory.RunScriptText(AvsScript, argt))
                {
                    this.WriteObject(EndurPowershellUtil.TableToPSObjects(returnt), true);
                }
            }
        }
    }
}