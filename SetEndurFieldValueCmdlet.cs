﻿using System;
using System.Linq;
using System.Management.Automation;
using Gazprom.MT.PowerShell.Endur.Code;
using Olf.Openrisk.Trading;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsCommon.Set, "EndurFieldValue")]
    public class SetEndurFieldValueCmdlet : EndurFieldValueCmdletBase
    {
        [Parameter(Mandatory = true, ParameterSetName = "TransactionId")]
        [Parameter(Mandatory = true, ParameterSetName = "Transaction")]
        public PSObject Value { get; set; }

        [Parameter]
        public SwitchParameter PassThru { get; set; }

        [Parameter(Mandatory = true, ParameterSetName = "ListFields")]
        public SwitchParameter ListFields { get; set; }

        protected override void ProcessRecord()
        {
            if (this.ListFields.IsPresent)
            {
                this.WriteObject(
                    Enum.GetValues(typeof(EnumTranfField)).Cast<EnumTranfField>().OrderBy(x => x.ToString()).Select(
                        x =>
                        {
                            var field = new PSObject();
                            field.Properties.Add(new PSNoteProperty("Name", x.ToString()));
                            field.Properties.Add(new PSNoteProperty("TranfId", (Int32)x));

                            return field;
                        }),
                    true);

                return;
            }

            using (var dc = new DisposableCollection())
            {
                if (this.ParameterSetName == "TransactionId")
                {
                    this.Transaction = dc.Add(this.Session.TradingFactory.RetrieveTransactionById(this.TransactionId));
                }

                var tranfFieldId = this.GetTranfFieldId();
                if (tranfFieldId == 0)
                {
                    throw new InvalidOperationException(string.Format("Could not find field named/with ID {0}", this.Field));
                }

                var field = dc.Add(GetField(tranfFieldId));
                FieldSetter.SetFieldValue(field, this.Value.ImmediateBaseObject);

                if (!this.PassThru.IsPresent)
                {
                    return;
                }

                if (this.ParameterSetName == "TransactionId")
                {
                    this.WriteObject(this.Transaction.TransactionId);
                }
                else
                {
                    this.WriteObject(this.Transaction);
                }
            }
        }
    }
}