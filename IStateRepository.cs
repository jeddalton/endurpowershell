using System;
using System.IO;
using System.Management.Automation;
using System.Reactive.Disposables;
using Olf.Openrisk.Application;

namespace Gazprom.MT.PowerShell.Endur
{
    public interface IStateRepository : IDisposable
    {
        Session Session { get; set; }
        string EndurConnectionString { get; set; }
        int EndurSessionId { get; set; }
        DirectoryInfo EndurBinaryFolder { get; set; }
        string EndurServerName { get; set; }
        string EndurDatabaseName { get; set; }
        void SetEndurSessionDisposer(IDisposable disposer);
        void DisposeEndurSession();
    }

    public class PsVariablesStateRepository : IStateRepository
    {
        private PSCmdlet _hostCmdlet;

        public PsVariablesStateRepository(PSCmdlet hostCmdlet = null)
        {
            _hostCmdlet = hostCmdlet;
        }

        public Session Session
        {
            get { return _hostCmdlet.GetVariableValue(VariableName.EndurSession, null) as Session; }
            set { SetStateVariable(VariableName.EndurSession, value); }
        }

        public string EndurConnectionString
        {
            get { return _hostCmdlet.GetVariableValue(VariableName.EndurConnectionString) as string; }
            set { SetStateVariable(VariableName.EndurConnectionString, value); }
        }

        public int EndurSessionId
        {
            get { return (int) _hostCmdlet.GetVariableValue(VariableName.EndurSessionId, 0); }
            set { SetStateVariable(VariableName.EndurSessionId, value); }
        }

        public DirectoryInfo EndurBinaryFolder
        {
            get { return new DirectoryInfo((string) _hostCmdlet.GetVariableValue(VariableName.EndurBinaryFolder, null)); }
            set
            {
                SetStateVariable(
                    VariableName.EndurBinaryFolder,
                    value.FullName);
            }
        }

        public string EndurServerName
        {
            get { return (string) _hostCmdlet.GetVariableValue(VariableName.EndurServerName, 0); }
            set { SetStateVariable(VariableName.EndurServerName, value); }
        }

        public string EndurDatabaseName
        {
            get { return (string) _hostCmdlet.GetVariableValue(VariableName.EndurDatabaseName, 0); }
            set { SetStateVariable(VariableName.EndurDatabaseName, value); }
        }

        public void SetEndurSessionDisposer(IDisposable disposer)
        {
            SetStateVariable(VariableName.EndurSessionDisposer, disposer);
        }

        public void Dispose()
        {
            DisposeEndurSession();

            _hostCmdlet.SessionState.PSVariable.Remove(VariableName.EndurBinaryFolder);
            _hostCmdlet.SessionState.PSVariable.Remove(VariableName.EndurSessionId);
            _hostCmdlet.SessionState.PSVariable.Remove(VariableName.EndurSession);
            _hostCmdlet.SessionState.PSVariable.Remove(VariableName.EndurServerName);
            _hostCmdlet.SessionState.PSVariable.Remove(VariableName.EndurDatabaseName);
            _hostCmdlet.SessionState.PSVariable.Remove(VariableName.EndurConnectionString);
        }


        public void DisposeEndurSession()
        {
            const string endurSessionDisposerVariableName = VariableName.EndurSessionDisposer;
            var sessionDisposer = _hostCmdlet.GetVariableValue(endurSessionDisposerVariableName) as IDisposable;
            (sessionDisposer ?? Disposable.Empty).Dispose();
            _hostCmdlet.SessionState.PSVariable.Remove(endurSessionDisposerVariableName);
        }

        public void Initialise(EndurCmdletBase hostCmdlet)
        {
            _hostCmdlet = hostCmdlet;
        }


        private static string NameInGlobalScope(string name)
        {
            return string.Format("Global:{0}", name);
        }

        private PSVariable GetStateVariable(string name)
        {
            var variable = _hostCmdlet.SessionState.PSVariable.Get(name) ?? _hostCmdlet.SessionState.PSVariable.Get(NameInGlobalScope(name));
            return variable;
        }

        private void SetStateVariable(string name, object value)
        {
            var variable = GetStateVariable(name);
            if (variable == null)
            {
                var newVariable = new PSVariable(NameInGlobalScope(name), value, ScopedItemOptions.AllScope);
                _hostCmdlet.SessionState.PSVariable.Set(newVariable);
            }
            else
            {
                variable.Value = value;
            }
        }

        private static class VariableName
        {
            public const string EndurBinaryFolder = "EndurBinaryFolder";
            public const string EndurDatabaseName = "EndurDatabaseName";
            public const string EndurConnectionString = "EndurConnectionString";
            public const string EndurServerName = "EndurServerName";
            public const string EndurSession = "EndurSession";
            public const string EndurSessionDisposer = "EndurSessionDisposer";
            public const string EndurSessionId = "EndurSessionId";
        }
    }
}