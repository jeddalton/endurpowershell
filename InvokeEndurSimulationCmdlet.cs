﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Management.Automation;
using Gazprom.MT.PowerShell.Endur.Code;
using Olf.Openrisk.IO;
using Olf.Openrisk.Simulation;
using Olf.Openrisk.Trading;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsLifecycle.Invoke, "EndurSimulation")]
    public class InvokeEndurSimulationCmdlet : EndurCmdletBase
    {
        [Parameter(Mandatory = true, ParameterSetName = "SimName", Position = 0)]
        public String Name { get; set; }

        [Parameter(Mandatory = true, ValueFromPipeline = true, Position = 1)]
        public Int32[] TransactionIds { get; set; }

        [Parameter]
        public String RevalType { get; set; }

        [Parameter]
        public SwitchParameter Save { get; set; }

        [Parameter]
        public SwitchParameter Extract { get; set; }

        protected override void ProcessRecord()
        {
            Simulation simulation = null;
            Transactions transactions = null;
            RevalType revalType = null;
            try
            {
                if (this.ParameterSetName == "SimName")
                {
                    simulation = this.Session.SimulationFactory.RetrieveSimulation(this.Name);
                    if (simulation == null)
                    {
                        throw new InvalidOperationException(String.Format("Simulation named '{0}' not found", this.Name));
                    }
                }
                Debug.Assert(simulation != null);

                transactions = this.Session.TradingFactory.CreateTransactions();
                transactions.AddRange(this.TransactionIds.Select(x => this.Session.TradingFactory.RetrieveTransactionById(x)));

                if (!String.IsNullOrEmpty(this.RevalType))
                {
                    revalType = this.Session.SimulationFactory.GetRevalType(this.RevalType);
                    simulation.RevalType = revalType;
                }

                this.WriteVerbose("Running simulation");
                var simRunId = 0;
                PSObject result = null;
                using (var simResult = simulation.Run(transactions))
                {
                    simRunId = simResult.RunId;


                    if (this.Save.IsPresent || this.Extract.IsPresent)
                    {
                        this.WriteVerbose("Saving simulation results");
                        simResult.Save();

                        using (var table = simResult.AsTable())
                        {
                            if (table.RowCount>0)
                            {
                                var values = table.GetRowValues(0);
                                var revalTypeName = (String)values[1];
                                var businessDate = (DateTime)values[8];
                                var portfolioId = (Int32)values[4];

                                var sql = String.Format(
                                    "SELECT MAX(sh.sim_run_id ) \r\n" +
                                    "FROM sim_header sh \r\n" +
                                    "	INNER JOIN reval_type rt ON rt.id_number=sh.run_type \r\n" +
                                    "WHERE rt.name = '{0}'  \r\n" +
                                    "AND sh.run_time = '{1}' \r\n" +
                                    "AND sh.pfolio = {2} \r\n", 
                                    revalTypeName, businessDate.ToString("yyyy-MM-ddT00:00:00", CultureInfo.InvariantCulture), portfolioId);

                                using (var simRunTable = this.Session.IOFactory.RunSQL(sql))
                                {
                                    simRunId = (Int32)simRunTable.GetRowValues(0)[0];
                                }
                            }
                        }
                    }

                    result = EndurPowershellUtil.MapSimResults(simResult, simRunId);
                }

                if (this.Extract.IsPresent)
                {
                    using (var simResult = this.Session.SimulationFactory.RetrieveSimulationResults(simRunId))
                    {
                        this.WriteVerbose("Extracting simulation results");
                        simResult.ExtractResults();
                    }
                }

                WriteObject(result);
            }
            finally
            {
                if (revalType != null) revalType.Dispose();
                if (simulation != null) simulation.Dispose();
                if (transactions != null)
                {
                    foreach (var transaction in transactions)
                    {
                        transaction.Dispose();
                    }
                    transactions.Dispose();
                }
            }
        }


    }
}