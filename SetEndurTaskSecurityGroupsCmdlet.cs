﻿using System;
using System.Management.Automation;
using Gazprom.MT.PowerShell.Endur.EndurTask;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsCommon.Set, "EndurTaskSecurityGroups")]
    public class SetEndurTaskSecurityGroupsCmdlet: EndurCmdletBase
    {
        [Parameter(Mandatory = true, Position = 0)]
        public String Name { get; set; }

        [Alias("SecGroups")]
        [Parameter(Mandatory = false, Position = 1)]
        public String[] SecurityGroups { get; set; }

        protected override void ProcessRecord()
        {
            WriteObject(string.Format("Assigning security groups to task '{0}'", Name));
            new TaskSecurityGroupSetter(Session).Set(Name, SecurityGroups);
        }
    }
}