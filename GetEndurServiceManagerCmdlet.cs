﻿using System;
using System.Management.Automation;
using Gazprom.MT.PowerShell.Endur.Code;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsCommon.Get, "EndurServiceManager")]
    public class GetEndurServiceManagerCmdlet : EndurCmdletBase
    {
        [Parameter]
        public String Name { get; set; }

        protected override void ProcessRecord()
        {
            const String serviceManagerSql =
                "SELECT  \r\n" +
                "	id AS 'Id'\r\n" +
                "	,service_name AS 'Name'\r\n" +
                "	,login_name AS 'LoginName'\r\n" +
                "	,workstation_name AS 'WorkstationName'\r\n" +
                "	,default_service AS 'DefaultService' \r\n" +
                "	,app_login_name AS 'ApplicationLoginName' \r\n" +
                "FROM service_mgr \r\n";

            var sql = String.IsNullOrEmpty(this.Name) ? serviceManagerSql : serviceManagerSql + String.Format("WHERE service_name='{0}'", this.Name);

            using (var result = this.Session.IOFactory.RunSQL(sql))
            {
                this.WriteObject(EndurPowershellUtil.TableToPSObjects(result), true);
            }

        }
    }
}