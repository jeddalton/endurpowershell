using System.Management.Automation;
using Olf.Openrisk.Application;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsLifecycle.Register, "EndurSession")]
    public class RegisterEndurSessionCmdlet : EndurCmdletBase
    {
        [Parameter]
        [ValidateRange(0, int.MaxValue)]
        public int SessionId { get; set; }

        [Parameter(ValueFromPipeline = true)]
        public Session SessionFromPipeline { get; set; }

        [Parameter]
        public SwitchParameter ProductionOverride { get; set; }

        protected override void ProcessRecord()
        {
            var session = SessionFromPipeline ?? AttachToSession(SessionId);
            SetSession(session);
            WriteObject(Session.SessionId);
        }
    }
}