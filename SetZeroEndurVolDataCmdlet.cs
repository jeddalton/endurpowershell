﻿namespace Gazprom.MT.PowerShell.Endur
{
    using System;
    using System.Management.Automation;

    using Gazprom.MT.PowerShell.Endur.Code;

    using Olf.Openrisk.Market;
    using Olf.Openrisk.Table;

    /// <summary>
    /// A Cmdlet to zero volatility data
    /// </summary>
    [Cmdlet(VerbsCommon.Set, "ZeroEndurVolData")]
    public class SetZeroEndurVolDataCmdlet : EndurCmdletBase
    {        
        [Parameter(Mandatory = true, Position = 0, HelpMessage = "Volatility Name")]
        [ValidateNotNullOrEmpty]
        public String VolatilityName { get; set; }

        protected override void ProcessRecord()
        {
            using (var dc = new DisposableCollection())
            {
                var volatility =
                    dc.Add((Volatility)this.Session.Market.GetElement(EnumElementType.Volatility, this.VolatilityName));

                var layer = dc.Add(volatility.GetLayer(0));

                var outputTable = dc.Add(layer.InputTable.Clone() as Table);
                for (var i = 0; i < outputTable.ColumnCount; i++)
                {
                    outputTable.SetColumnValues(i, 0.0);
                }

                layer.SetInputFromTable(outputTable);
                volatility.SaveUniversal();
            }
        }
    }
}
