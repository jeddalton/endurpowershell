using System;
using System.Management.Automation;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsCommon.Remove, "EndurWorkflow")]
    public class RemoveEndurWorkflowCmdlet : EndurCmdletBase
    {
        [Parameter(Mandatory = true, ParameterSetName = "WorkflowId", Position = 0, ValueFromPipeline = true)]
        public Int32 WorkflowId { get; set; }

        [Parameter(Mandatory = true, ParameterSetName = "WorkflowName", Position = 0, ValueFromPipeline = true)]
        public String WorkflowName { get; set; }

        protected override void ProcessRecord()
        {
            if (this.ParameterSetName == "WorkflowName")
            {
                this.WorkflowId = GetWorkflowId();
            }

            var sql = String.Format("EXEC delete_wflow {0}, {1}", this.WorkflowId, this.Session.User.Id);

            using (this.Session.IOFactory.RunSQL(sql))
            {
            }
        }

        private Int32 GetWorkflowId()
        {
            var sql = String.Format(
                "SELECT TOP 1 wflow_id \r\n" +
                "FROM job_cfg jc \r\n" +
                "WHERE jc.name='{0}' \r\n" +
                "AND jc.type=0 and jc.service_type=1", 
                this.WorkflowName);

            using (var result = this.Session.IOFactory.RunSQL(sql))
            {
                if (result.RowCount == 0)
                {
                    throw new InvalidOperationException(String.Format("Workflow named {0} not found", this.WorkflowName));
                }

                return result.GetInt(0, 0);
            }
        }
    }
}