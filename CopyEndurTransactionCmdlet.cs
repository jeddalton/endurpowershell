﻿namespace Gazprom.MT.PowerShell.Endur
{
    using System.Linq;
    using System.Management.Automation;

    using Gazprom.MT.PowerShell.Endur.Code;

    using Olf.Openrisk.Trading;

    [Cmdlet(VerbsCommon.Copy, "EndurTransaction")]
    public class CopyEndurTransactionCmdlet : EndurCmdletBase
    {
        [Parameter(Mandatory = true, ParameterSetName = "TransactionId", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true, Position = 0)]
        public int[] TransactionId { get; set; }

        [Parameter(Mandatory = true, ParameterSetName = "Transaction", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true, Position = 0)]
        public Transaction[] Transaction { get; set; }

        protected override void ProcessRecord()
        {
            using (var dc = new DisposableCollection())
            {
                var transactions = this.ParameterSetName == "TransactionId"
                                       ? this.TransactionId.Select(
                                           i => dc.Add(this.Session.TradingFactory.RetrieveTransactionById(i))).ToArray()
                                       : this.Transaction;

                transactions.ForEach(
                    t =>
                        {
                            var cloned = t.Clone();
                            cloned.Reset();

                            this.WriteObject(t);
                        });
            }
        }
    }
}