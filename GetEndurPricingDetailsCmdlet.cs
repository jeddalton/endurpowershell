﻿namespace Gazprom.MT.PowerShell.Endur
{
    using System.Management.Automation;

    [Cmdlet(VerbsCommon.Get, "EndurPricingDetails")]
    public class GetEndurPricingDetails : EndurCmdletBase
    {
        [Parameter(Mandatory = true, Position = 0, ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        public int TransactionId { get; set; }

        protected override void ProcessRecord()
        {
            using (var transaction = this.Session.TradingFactory.RetrieveTransactionById(this.TransactionId))
            {
                if (transaction == null)
                {
                    return;
                }

                using (var pricingDetails = transaction.PricingDetails.AsTable())
                {
                    this.WriteObject(pricingDetails.AsXmlString());
                }
            }
        }
    }
}