﻿using Olf.Openrisk.IO;

namespace Gazprom.MT.PowerShell.Endur
{
    using System;
    using System.Management.Automation;

    using Olf.NetToolkit;

    [Cmdlet(VerbsCommon.Clear, "EndurQueryTables")]
    public class ClearQueryTableCmdlet : EndurCmdletBase
    {
        [Parameter(Mandatory = false)]
        public string PurgeType { get; set; }

        protected override void ProcessRecord()
        {
            var startCount = 0;
            using (var startTable = this.Session.IOFactory.RunSQL("select count(*) records from query_result"))
            {
                startCount = startTable.GetInt(0,0);
            }

            this.WriteObject(string.Format("Beggining purge - currently {0} records", startCount));
            if (this.PurgeType == null)
            {
                Query.CleanQueryTables(1);
                Query.CleanQueryTables(2);
                Query.CleanQueryTables(4);
            }
            else if (this.PurgeType.Equals("query_info_purge_shutdown"))
            {
                Query.CleanQueryTables(1);
            }
            else if (this.PurgeType.Equals("query_info_purge_orphaned"))
            {
                Query.CleanQueryTables(2);
            }
            else if (this.PurgeType.Equals("purge_query_non_logged_users"))
            {
                Query.CleanQueryTables(4);
            }
            else
            {
                throw new Exception(string.Format("PurgeType {0} is not a valid purge type", this.PurgeType));
            }

            var endCount = 0;
            using (var endTable = this.Session.IOFactory.RunSQL("select count(*) records from query_result"))
            {
                endCount = endTable.GetInt(0,0);
            }

            this.WriteObject(string.Format("Purged {0} records", startCount-endCount));
        }
    }
}
