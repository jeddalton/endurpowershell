﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management.Automation;
using System.Text;
using Olf.Openrisk.Scheduling;
using Olf.Openrisk.Trading;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsCommon.Clear, "EndurTransactionVolume")]
    public class ClearEndurTransactionVolumeCmdlet : EndurCmdletBase
    {
        [Parameter(Mandatory = true, ValueFromPipeline = true, Position = 0, ParameterSetName = "Transaction")]
        public Transaction Transaction { get; set; }

        [Parameter(Mandatory = true, ValueFromPipeline = true, Position = 0, ParameterSetName = "TransactionId")]
        public Int32 TransactionId { get; set; }

        [Parameter(Position = 1)]
        public String Type { get; set; }

        [Parameter(Position = 2)]
        public DateTime[] Dates { get; set; }

        [Parameter]
        public Int32 Leg { get; set; }

        public ClearEndurTransactionVolumeCmdlet()
        {
            this.Leg = 1;
            this.Type = "DailyElection";
        }

        protected override void ProcessRecord()
        {
            EnumVolume type;

            if (!Enum.TryParse(this.Type, true, out type))
            {
                var error = new StringBuilder();
                error.AppendFormat("Specified volume type '{0}' is not valid. Valid types are:", this.Type).AppendLine();
                error.AppendLine(String.Join(Environment.NewLine, Enum.GetNames(typeof (EnumVolume)).Select(x => "\t" + x)));

                throw new InvalidOperationException(error.ToString());
            }

            if (ParameterSetName == "TransactionId")
            {
                this.Transaction = this.Session.TradingFactory.RetrieveTransactionById(this.TransactionId);
            }

            var leg = this.Transaction.Legs.GetItem(this.Leg);
            var startDate = leg.GetValueAsDate(EnumLegFieldId.StartDate);
            var endDate = leg.GetValueAsDate(EnumLegFieldId.MaturityDate);

            if (Dates == null)
            {
                this.Dates = GenerateDates(startDate, endDate).ToArray();
            }

            foreach (var date in this.Dates)
            {
                if (date < startDate || date > endDate)
                {
                    WriteWarning(String.Format("Specified date {0} is outside of the allowable range for transaction", date));
                    continue;
                }
                WriteVerbose(String.Format("Clearing volume for {0}:{1}:{2}", this.Transaction.TransactionId, type, date.ToShortDateString()));
                var field = Transaction.RetrieveField(EnumTranfField.CommDailyVolume, this.Leg, (Int32) type, GetJulianDateValue(date));
                if (!field.IsApplicable || field.IsReadOnly)
                {
                    WriteWarning(String.Format("Field for volume {0}:{1}:{2} is not applicable or is readonly", this.Transaction.TransactionId, type, date.ToShortDateString()));
                    continue;
                }
                field.SetValue((String) null);
            }

            this.Transaction.SaveIncremental();
        }

        private IEnumerable<DateTime> GenerateDates(DateTime startDate, DateTime endDate)
        {
            for (var date = startDate; date <= endDate; date=date.AddDays(1))
            {
                yield return date;
            }
        }

        private static readonly DateTime JuliaDateOffset = new DateTime(1900, 1, 1);

        private static int GetJulianDateValue(DateTime date)
        {
            return (Int32)(date.Subtract(JuliaDateOffset).TotalDays);
        }

    }
}