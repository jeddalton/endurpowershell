﻿namespace Gazprom.MT.PowerShell.Endur
{
    using System;
    using System.Management.Automation;

    using Gazprom.MT.PowerShell.Endur.Code;

    using Olf.Openrisk.Market;
    using Olf.Openrisk.Table;

    /// <summary>
    /// A Cmdlet to import forward curve data from a CSV file
    /// </summary>
    [Cmdlet(VerbsCommon.Get, "EndurForwardCurveData")]
    public class GetEndurForwardCurveDataCmdlet : EndurCmdletBase
    {
        [Parameter(Mandatory = true, Position = 0, HelpMessage = "Index Name")]
        [ValidateNotNullOrEmpty]
        public String CurveName { get; set; }

        protected override void ProcessRecord()
        {
            using (var dc = new DisposableCollection())
            {
                var curve = dc.Add(this.Session.Market.GetIndex(CurveName));
				var outputTable = curve.ActiveGridPoints.GetInputTableDetailed(false,EnumBmo.Mid);
				WriteObject(EndurPowershellUtil.TableToPSObjects(outputTable), true);
            }
        }
    }
}
