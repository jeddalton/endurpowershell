using System;
using System.IO;
using System.Management.Automation;
using Gazprom.MT.PowerShell.Endur.Code;
using Olf.Openrisk.Application;

namespace Gazprom.MT.PowerShell.Endur
{
    public abstract class EndurCmdletBase : PSCmdlet
    {
        private readonly IStateRepository _stateRepository;

        protected EndurCmdletBase() : this(new PsVariablesStateRepository())
        {
            ((PsVariablesStateRepository) _stateRepository).Initialise(this);
        }

        protected EndurCmdletBase(IStateRepository stateStore)
        {
            _stateRepository = stateStore;
        }

        protected Session Session
        {
            get
            {
                lock (this)
                {
                    return _stateRepository.Session ?? AttachToSession();
                }
            }
        }

        protected string EndurConnectionString { get { return _stateRepository.EndurConnectionString; } }

        public int EndurSessionId { get { return _stateRepository.EndurSessionId; } }

        public DirectoryInfo OlfBinDirectory
        {
            private set { _stateRepository.EndurBinaryFolder = value; }
            get { return _stateRepository.EndurBinaryFolder; }
        }

        protected Session AttachToSession(int sessionId = 0)
        {
            var implementationStrategy = new EndurAttachToExistingSessionFactory(sessionId);
            var session = CreateSession(implementationStrategy, implementationStrategy.GetOlfBinDirectory());
            return session.Session as Session;
        }


        public void SetSession(EndurSession session)
        {
            _stateRepository.SetEndurSessionDisposer(session);
            SetSession((Session) session.Session);
        }

        public void SetSession(Session session)
        {
            var serverName = session.ServerName;
            var databaseName = session.DatabaseName;

            _stateRepository.EndurSessionId = session.SessionId;
            _stateRepository.Session = session;
            _stateRepository.EndurServerName = serverName;
            _stateRepository.EndurDatabaseName = databaseName;
            _stateRepository.EndurConnectionString = string.Format(
                "Data Source={0};Initial Catalog={1};Integrated Security=SSPI",
                serverName,
                databaseName);
        }

        protected void ClearSession()
        {
            _stateRepository.Dispose();
        }


        protected EndurSession CreateSession(IEndurSessionFactory implementationStrategy, DirectoryInfo olfBinDirectory)
        {
            try
            {
                OlfBinDirectory = olfBinDirectory;

                var session = new EndurSessionFactory(implementationStrategy)
                    .Create(olfBinDirectory);

                SetSession(session);
                return session;
            }
            catch (InvalidOperationException ex)
            {
                WriteWarning(ex.Message);
                throw;
            }
        }
    }
}