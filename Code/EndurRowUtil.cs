namespace Gazprom.MT.PowerShell.Endur.Code
{
    using System;

    using Olf.NetToolkit;
    using Olf.Openrisk.Table;
    using Olf.NetToolkit.Enums;

    using Table = Olf.NetToolkit.Table;

    public static class EndurRowUtil
    {
        public static void SetCellValue(TableRow row, int cellIndex, object value)
        {
            if (value == null)
            {
                return;
            }

            var cell = row.Cells.GetItem(cellIndex);

            switch (cell.Type)
            {
                case EnumColType.Date:
                case EnumColType.DateTime:
                    cell.Date = Convert.ToDateTime(value);
                    break;

                case EnumColType.Double:
                    cell.Double = Convert.ToDouble(value);
                    break;

                case EnumColType.Int:
                    cell.Int = Convert.ToInt32(value);
                    break;

                case EnumColType.String:
                    cell.String = value.ToString();
                    break;

                default:
                    throw new InvalidOperationException(String.Format("Cannot store value '{0}' of type '{1}' for table cell type '{2}'", value, value.GetType(), cell.Type));
            }
        }

        public static void SetNtkTableValue(Table table, int colIndex, int rowIndex, object value)
        {
            if (value == null)
            {
                return;
            }

            var colType = (COL_TYPE_ENUM)table.GetColType(colIndex);

            switch (colType)
            {
                case COL_TYPE_ENUM.COL_DATE:
                case COL_TYPE_ENUM.COL_DATE_TIME:
                    var odt = ODateTime.StrToDateTime(Convert.ToDateTime(value).ToString("yyyy-MM-dd HH:mm:ss"));
                    table.SetDateTime(colIndex, rowIndex, odt);
                    odt.Destroy();
                    break;

                case COL_TYPE_ENUM.COL_DOUBLE:
                    table.SetDouble(colIndex, rowIndex, Convert.ToDouble(value));
                    break;

                case COL_TYPE_ENUM.COL_INT:
                    var intVal = value is DateTime
                        ? OCalendar.ParseString(Convert.ToDateTime(value).ToString("yyyy-MM-dd"))
                        : Convert.ToInt32(value);
                    table.SetInt(colIndex, rowIndex, intVal);
                    break;

                case COL_TYPE_ENUM.COL_STRING:
                    table.SetString(colIndex, rowIndex, Convert.ToString(value));
                    break;

                default:
                    throw new InvalidOperationException(String.Format("Cannot set value '{0}' of type '{1}' for NTK Table column num {2} type '{3}'", value, value.GetType(), colIndex, colType)); 
            }
        }
    }
}