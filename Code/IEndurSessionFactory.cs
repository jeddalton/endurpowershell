namespace Gazprom.MT.PowerShell.Endur.Code
{
    public interface IEndurSessionFactory
    {
        EndurSession Create();
    }
}