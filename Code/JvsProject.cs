﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Gazprom.MT.PowerShell.Endur.Code
{
    public class JvsProject
    {
        private readonly List<String> _dependencies = new List<string>();
        private readonly List<String> _projectFiles = new List<string>();
        private readonly FileInfo _projectFileInfo;

        #region Constructors
        public JvsProject(String path)
        {
            if (!File.Exists(path))
                throw new InvalidOperationException(String.Format("JVS Project file '{0}' does not exist", path));

            this.ProjectFilePath = path;
            this._projectFileInfo = new FileInfo(path);
            ParseProjectFile();
            GetProjectFiles();
        }
        #endregion

        #region Private Methods
        private void GetProjectFiles()
        {
            foreach (var projectFile in this._projectFileInfo.Directory.EnumerateFiles("*.java", SearchOption.AllDirectories))
            {
                this._projectFiles.Add(projectFile.FullName);
            }
        }

        private void ParseProjectFile()
        {
            using (var reader = File.OpenText(this.ProjectFilePath))
            {
                String line;
                while ((line = reader.ReadLine()) != null)
                {
                    if (!String.IsNullOrWhiteSpace(line))
                    {
                        var parts = line.Split(';');
                        foreach (var part in parts.Where(x => !String.IsNullOrWhiteSpace(x)))
                        {
                            this._dependencies.Add(part);
                        }
                    }
                }
            }
        }

        private String GetPathRelativeToSrc(String filename)
        {
            return filename.Replace(this.SourceFolder, "").TrimStart(Path.DirectorySeparatorChar);
        }

        private static String GetFilePathWithOutExtension(string fullName)
        {
            return Path.Combine(Path.GetDirectoryName(fullName), Path.GetFileNameWithoutExtension(fullName));
        }

        #endregion

        #region Public Methods

        public String GetScriptPath(String filename)
        {
            return String.Format("{0}.{1}", this.Name,
                                 GetPathRelativeToSrc(GetFilePathWithOutExtension(filename)).Replace(
                                     Path.DirectorySeparatorChar, '.'));
        }
        #endregion

        #region Public Properties
        public string ProjectFilePath { get; protected set; }

        public IEnumerable<String> Dependencies
        {
            get
            {
                return this._dependencies;
            }

        }

        public String Name
        {
            get
            {
                return Path.GetDirectoryName(this.ProjectFilePath).Split(Path.DirectorySeparatorChar).Last();
            }
        }

        public String SourceFolder
        {
            get
            {
                return Path.Combine(Path.GetDirectoryName(this.ProjectFilePath), "src");
            }
        }

        public String ProjectFolder
        {
            get
            {
                return this._projectFileInfo.DirectoryName;
            }
        }

        public String OutFilePath
        {
            get
            {
                return Path.Combine(this._projectFileInfo.Directory.Parent.FullName, this.Name + ".out");
            }
        }

        public IEnumerable<String> ProjectFiles
        {
            get
            {
                return this._projectFiles;
            }
        }
        #endregion

        #region Overrides
        public override int GetHashCode()
        {
            return this.ProjectFilePath.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj is JvsProject)
                return this.ProjectFilePath.Equals((obj as JvsProject).ProjectFilePath, StringComparison.InvariantCultureIgnoreCase);
            else if (obj is String)
            {
                var other = obj as String;
                return this.ProjectFilePath.Equals(other, StringComparison.InvariantCultureIgnoreCase) || this.Name.Equals(other, StringComparison.InvariantCultureIgnoreCase);
            }
            else
                return false;
        }

        public override string ToString()
        {
            return this.ProjectFilePath;
        }
        #endregion
    }
}