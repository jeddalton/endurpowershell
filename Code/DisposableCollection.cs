﻿using System;
using System.Collections.ObjectModel;

namespace Gazprom.MT.PowerShell.Endur.Code
{
    public class DisposableCollection : Collection<IDisposable>, IDisposable
    {

        public T Add<T>(T item) where T : class,IDisposable
        {
            if (item != null)
            {
                base.Add(item);
            }

            return item;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                foreach (var item in this)
                {
                    item.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
        }

        protected override void ClearItems()
        {
            this.Dispose();
            base.ClearItems();
        }
    }
}