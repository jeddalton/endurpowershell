using System.Collections.Generic;
using Gazprom.MT.PowerShell.Endur.DataLoaderService;

namespace Gazprom.MT.PowerShell.Endur.Code
{
    internal class ProductResult
    {
        public ProductBase OriginalProduct;
        public readonly List<ProductBase> SavedProducts = new List<ProductBase>();
        public string ErrorMessage;
    }
}