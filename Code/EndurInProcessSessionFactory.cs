using System;
using Olf.Openrisk.Application;

namespace Gazprom.MT.PowerShell.Endur.Code
{
    public class EndurInProcessSessionFactory : IEndurSessionFactory
    {
        private readonly EndurConfigFile _endurConfigFile;
        private readonly string _password;
        private readonly string _user;

        public EndurInProcessSessionFactory(string user, string password, EndurConfigFile endurConfigFile)
        {
            _user = user;
            _password = password;
            _endurConfigFile = endurConfigFile;
        }

        public EndurSession Create()
        {
            var application = Application.Instance;

            if (!application.IsAttached)
            {
                var sqlServerName = _endurConfigFile.GetSqlServerName();
                var sqlDatabaseName = _endurConfigFile.GetSqlDatabaseName();

                var commandLine = string.Format(
                    "-u {0} -p {1} -q mssql -s {2} -d {3} -olfcfg {4}",
                    _user,
                    _password,
                    sqlServerName,
                    sqlDatabaseName,
                    _endurConfigFile.File.FullName);

                var sessionName = "Powershell " + Guid.NewGuid();
                var olfBinDirectoryPath = _endurConfigFile.GetOlfBinDirectory().FullName;

                application.InitializeSession(commandLine, sessionName, olfBinDirectoryPath);
            }
            return new EndurSession(application);
        }
    }
}