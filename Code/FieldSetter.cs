using System;
using Olf.Openrisk.Staticdata;

namespace Gazprom.MT.PowerShell.Endur.Code
{
    public static class FieldSetter
    {
        public static void SetFieldValue(Field field, Object value)
        {
            //            Trace.WriteLine(String.Format("Setting {0} ({2}):{1}", field.Name, value??"<null>", field.Id));

            if (value is String)
            {
                field.SetValue((String)value);
            }
            else if (value is DateTime)
            {
                field.SetValue((DateTime)value);
            }
            else if (value is Int32)
            {
                field.SetValue((Int32)value);
            }
            else if (value is Double)
            {
                field.SetValue((Double)value);
            }
            else if (value is Boolean)
            {
                field.SetValue((Boolean)value);
            }
            else if (value is Int64)
            {
                field.SetValue((Int64)value);
            }
        }

        public static Object GetFieldValue(Field field)
        {
            switch (field.DataType)
            {
                case EnumFieldType.Boolean:
                    return field.GetValueAsBoolean();

                case EnumFieldType.Date:
                    return field.GetValueAsDate();

                case EnumFieldType.DateTime:
                    return field.GetValueAsDateTime();

                case EnumFieldType.Double:
                    return field.GetValueAsDouble();

                case EnumFieldType.Int:
                    return field.GetValueAsInt();

                case EnumFieldType.Long:
                    return field.GetValueAsLong();

                case EnumFieldType.Table:
                    return field.GetValueAsTable();

                case EnumFieldType.Time:
                    return field.GetValueAsTime();

                case EnumFieldType.UnsignedInt:
                    return field.GetValueAsLong();

                case EnumFieldType.String:
                case EnumFieldType.SymbolicDate:
                case EnumFieldType.Reference:
                case EnumFieldType.Period:
                case EnumFieldType.None:
                case EnumFieldType.HolidayList:
                case EnumFieldType.File:
                case EnumFieldType.Enum:
                    return field.GetValueAsString();

                default:
                    return field.GetValueAsString();
            }
        }
    }
}