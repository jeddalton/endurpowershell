using System;
using System.Collections;
using System.IO;
using System.Text.RegularExpressions;

namespace Gazprom.MT.PowerShell.Endur.Code
{
    public class EndurConfigFile
    {
        private static readonly Regex LineRegex = new Regex(@"^(SET )?(?<name>\w+)=(?<value>.*)");

        private static readonly Regex EmbeddedVariable = new Regex(@"%([\w_]+)%");

        public EndurConfigFile(string path) : this(new FileInfo(path))
        {
        }

        public EndurConfigFile(FileInfo file)
        {
            File = file;
        }

        public FileInfo File { get; private set; }

        // Define other methods and classes here
        public string ResolveVariable(string name)
        {
            var values = GetConfigFileValues();
            return ResolveVariable(name, values);
        }

        private static string ResolveVariable(string name, Hashtable values)
        {
            var value = values.ContainsKey(name) ? values[name].ToString() : Environment.GetEnvironmentVariable(name);

            if (value == null)
                throw new InvalidOperationException(
                    string.Format("Cannot find variable named {0} in the supplied values or environment variables", name));

            var match = EmbeddedVariable.Match(value);

            while (match.Success)
            {
                value = EmbeddedVariable.Replace(value, ResolveVariable(match.Groups[1].Value, values), 1);
                match = EmbeddedVariable.Match(value);
            }

            return value;
        }

        public Hashtable GetConfigFileValues()
        {
            if (!File.Exists)
            {
                throw new InvalidOperationException(string.Format("Specified configuration file {0} does not exist", File));
            }

            var values = new Hashtable();

            using (var reader = new StreamReader(System.IO.File.Open(File.FullName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    if (string.IsNullOrWhiteSpace(line)) continue;

                    var match = LineRegex.Match(line);
                    if (match.Success)
                    {
                        if (values.ContainsKey(match.Groups["name"].Value))
                        {
                            values[match.Groups["name"].Value] = match.Groups["value"].Value;
                        }
                        else
                        {
                            values.Add(match.Groups["name"].Value, match.Groups["value"].Value);
                        }
                    }
                }
            }
            return values;
        }

        public DirectoryInfo GetOlfBinDirectory()
        {
            return new DirectoryInfo(ResolveVariable("OLF_BIN"));
        }

        public string GetSqlServerName()
        {
            return ResolveVariable("AB_LOGON_SERVER");
        }

        public string GetSqlDatabaseName()
        {
            return ResolveVariable("AB_LOGON_DATABASE");
        }
    }
}