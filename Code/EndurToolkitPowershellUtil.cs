﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Olf.NetToolkit;
using Olf.NetToolkit.Enums;

namespace Gazprom.MT.PowerShell.Endur.Code
{
    using System.Management.Automation;

    public static class EndurToolkitPowershellUtil
    {
        public static void PSObjectsToNTKTable(Table table, IEnumerable<PSObject> objects, string[] properties)
        {
            var localProperties = properties;

            var row = 0;

            foreach (var o in objects)
            {
                if (localProperties == null)
                {
                    localProperties = o.Properties.Where(x => x.IsGettable).Select(x => x.Name).ToArray();
                }

                var nameToIndex = new Dictionary<string, int>();

                if (row == 0)
                {
                    foreach (var propertyName in localProperties)
                    {
                        var property = o.Properties[propertyName];
                        var type = GetEndurColType(property.TypeNameOfValue);

                        var columnIndex = table.AddCol(propertyName, type);
                        nameToIndex.Add(propertyName, columnIndex);
                    }
                }

                var tableRowIndex = table.AddRow();

                foreach (var propertyName in localProperties)
                {
                    var index = nameToIndex[propertyName];

                    EndurRowUtil.SetNtkTableValue(table, index, tableRowIndex, o.Properties[propertyName].Value);
                }

                row++;
            }
        }

        private static COL_TYPE_ENUM GetEndurColType(string type)
        {
            if (type == "System.String")
            {
                return COL_TYPE_ENUM.COL_STRING;
            }

            if (type == "System.DateTime")
            {
                return COL_TYPE_ENUM.COL_DATE_TIME;
            }

            if (type == "System.Double" || type == "System.Single" || type == "System.Decimal")
            {
                return COL_TYPE_ENUM.COL_DOUBLE;
            }

            if (type == "System.Int32")
            {
                return COL_TYPE_ENUM.COL_INT;
            }

            throw new InvalidOperationException(String.Format("Cannot map property type '{0}' to Endur COL_TYPE_ENUM", type));
        }
    }
}
