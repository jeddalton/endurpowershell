using System;
using System.Linq;
using Olf.Openrisk.Calendar;

namespace Gazprom.MT.PowerShell.Endur.Code
{
    public class HistoricPrice
    {
        private const int ColIndex = 0;
        private const int ColResetDate = 1;
        private const int ColStartDate = 2;
        private const int ColEndDate = 3;
        private const int ColRefSource = 5;
        private const int ColIndexLocation = 6;
        private const int ColValue = 7;
        private static readonly DateTime EndurEpochDate = new DateTime(1960, 1, 1);

        private HistoricPrice(string index, int jDate, int jStartDate, int jEndDate, string reference, int locationId, double newValue)
        {
            Index = index;
            JDate = jDate;
            JStartDate = jStartDate;
            JEndDate = jEndDate;
            Reference = reference;
            LocationId = locationId;
            NewValue = newValue;
        }

        public double NewValue { get; private set; }
        public int LocationId { get; private set; }
        public int JEndDate { get; private set; }
        public int JStartDate { get; private set; }
        public string Index { get; private set; }
        public int JDate { get; private set; }
        public string Reference { get; private set; }

        public static HistoricPrice Create(CalendarFactory calendarFactory, string line)
        {
            var lineArray = line.Split(',');

            var resetDate = TryGetJulianDate(calendarFactory, lineArray[ColResetDate]);
            var jStartDate = TryGetJulianDate(calendarFactory, lineArray[ColStartDate]);
            var jEndDate = TryGetJulianDate(calendarFactory, lineArray[ColEndDate]);
            var index = lineArray[ColIndex];
            var reference = lineArray[ColRefSource];
            var locationId = int.Parse(lineArray[ColIndexLocation]);
            var newValue = double.Parse(lineArray[ColValue]);

            return new HistoricPrice(
                index,
                resetDate,
                jStartDate,
                jEndDate,
                reference,
                locationId,
                newValue);
        }

        private static int TryGetJulianDate(CalendarFactory cf, string dateTimeString)
        {
            var dateTime =
                string.IsNullOrWhiteSpace(dateTimeString) ? EndurEpochDate : DateTime.Parse(dateTimeString);

            var maxDate = new[] {dateTime, EndurEpochDate}.Max();
            return cf.GetJulianDate(maxDate);
        }
    }
}