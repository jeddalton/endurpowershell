namespace Gazprom.MT.PowerShell.Endur.Code
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Management.Automation;
    using System.Xml;
    using Olf.Openrisk.Simulation;
    using Olf.Openrisk.Staticdata;
    using Olf.Openrisk.Table;

    public static class EndurPowershellUtil
    {
        public static IEnumerable<PSObject> TableToPSObjects(ConstTable table)
        {
            return TableToPSObjects(table, false);
        }

        // ReSharper disable InconsistentNaming
        public static IEnumerable<PSObject> TableToPSObjects(ConstTable table, Boolean asXml)
        // ReSharper restore InconsistentNaming
        {
            if (table == null)
            {
                return null;
            }

            return !asXml ? TableToPSObjectsImpl(table) : TableToXml(table);
        }

        // ReSharper disable InconsistentNaming
        public static IEnumerable<PSObject> TableToPSObjects(StaticTable staticTable, Boolean asXml)
        // ReSharper restore InconsistentNaming
        {
            if (staticTable == null)
            {
                return null;
            }

            return !asXml ? TableToPSObjectsImpl(staticTable) : TableToXml(staticTable);
        }

        public static PSObject MapSimResults(SimResults simResults, int simRunId)
        {
            var o = new PSObject();
            o.Properties.Add(new PSNoteProperty("BusinessDate", simResults.BusinessDate));
            o.Properties.Add(new PSNoteProperty("Portfolio", MapPortfolio(simResults.Portfolio)));
            o.Properties.Add(new PSNoteProperty("RunId", simRunId));

            return o;
        }

        public static PSObject MapPortfolio(Portfolio portfolio)
        {
            var o = new PSObject();
            o.Properties.Add(new PSNoteProperty("Id", portfolio.Id));
            o.Properties.Add(new PSNoteProperty("Name", portfolio.Name));

            return o;
        }

        public static void PSObjectsToTable(Table table, IEnumerable<PSObject> objects, string[] properties)
        {
            var localProperties = properties;
            var nameToIndex = new Dictionary<string, int>();

            var row = 0;

            foreach (var o in objects)
            {
                if (localProperties == null)
                {
                    localProperties = o.Properties.Where(x => x.IsGettable).Select(x => x.Name).ToArray();
                }

                if (row == 0)
                {
                    foreach (var propertyName in localProperties)
                    {
                        var property = o.Properties[propertyName];
                        var type = GetEndurColType(property.TypeNameOfValue);

                        var column = table.AddColumn(propertyName, type);
                        nameToIndex.Add(propertyName, column.Number);
                    }
                }

                var tableRow = table.AddRow();

                foreach (var propertyName in localProperties)
                {
                    var index = nameToIndex[propertyName];

                    EndurRowUtil.SetCellValue(tableRow, index, o.Properties[propertyName].Value);
                }

                row++;
            }
        }

        private static EnumColType GetEndurColType(string type)
        {
            if (type == "System.String")
            {
                return EnumColType.String;
            }

            if (type == "System.DateTime")
            {
                return EnumColType.DateTime;
            }

            if (type == "System.Double" || type == "System.Single" || type == "System.Decimal")
            {
                return EnumColType.Double;
            }

            if (type == "System.Int32")
            {
                return EnumColType.Int;
            }

            throw new InvalidOperationException(String.Format("Cannot map property type '{0}' to Endur EnumColType", type));
        }

        private static IEnumerable<PSObject> TableToXml(ConstTable table)
        {
            var xmlString = table.AsXmlString();

            // Replace dodgy Script:
            xmlString = xmlString.Replace("<Script:", "<Script_");
            xmlString = xmlString.Replace("</Script:", "</Script_");

            var doc = new XmlDocument();
            doc.LoadXml(xmlString);

            return new[] { new PSObject(doc) };
        }

        private static IEnumerable<PSObject> TableToXml(StaticTable staticTable)
        {
            var xmlString = staticTable.AsXmlString();

            // Replace dodgy Script:
            xmlString = xmlString.Replace("<Script:", "<Script_");
            xmlString = xmlString.Replace("</Script:", "</Script_");

            var doc = new XmlDocument();
            doc.LoadXml(xmlString);

            return new[] { new PSObject(doc) };
        }

        // ReSharper disable InconsistentNaming
        private static IEnumerable<PSObject> TableToPSObjectsImpl(ConstTable table)
        // ReSharper restore InconsistentNaming
        {
            var objectList = new List<PSObject>();

            foreach (var tableRow in table.ConstRows)
            {
                var o = new PSObject();

                for (var i = 0; i < table.ColumnCount; i++)
                {
                    var propertyName = table.GetColumn(i).Name;

                    using (var tableCell = tableRow.Cells.GetItem(i))
                    {
                        var propertyValue = EndurRowMapperUtil.GetCellValue(tableCell);

                        var property = new PSNoteProperty(propertyName, propertyValue);
                        o.Properties.Add(property);
                    }
                }

                objectList.Add(o);
            }

            return objectList;
        }

        // ReSharper disable InconsistentNaming
        private static IEnumerable<PSObject> TableToPSObjectsImpl(StaticTable staticTable)
        // ReSharper restore InconsistentNaming
        {
            var objectList = new List<PSObject>();

            foreach (var tableRow in staticTable.ConstRows)
            {
                var o = new PSObject();

                for (var i = 0; i < staticTable.ColumnCount; i++)
                {
                    var propertyName = staticTable.GetColumnName(i);

                    using (var tableCell = tableRow.Cells.GetItem(i))
                    {
                        var propertyValue = EndurRowMapperUtil.GetCellValue(tableCell);

                        var property = new PSNoteProperty(propertyName, propertyValue);
                        o.Properties.Add(property);
                    }
                }

                objectList.Add(o);
            }

            return objectList;
        }
    }
}