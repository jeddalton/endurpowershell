using System;
using System.Diagnostics;
using System.IO;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using Olf.Openrisk.Application;

namespace Gazprom.MT.PowerShell.Endur.Code
{
    public class EndurMasterExeSessionFactory : IEndurSessionFactory
    {
        private static readonly TimeSpan PollingInterval = TimeSpan.FromSeconds(0.25);
        private static readonly TimeSpan EndurSessionRegisteredTimeout = TimeSpan.FromSeconds(120);
        private readonly EndurConfigFile _endurConfigFile;
        private readonly string _password;
        private readonly string _user;

        public EndurMasterExeSessionFactory(string user, string password, EndurConfigFile endurConfigFile)
        {
            _user = user;
            _password = password;
            _endurConfigFile = endurConfigFile;
        }

        public EndurSession Create()
        {
            var consoleOutput = new StringBuilder();
            var process = StartMasterExeProcess(_user, _password, _endurConfigFile, _endurConfigFile.GetOlfBinDirectory(), consoleOutput);
            try
            {
                var application = Application.Instance;
                try
                {
                    WaitForSessionToRegister(process, application, consoleOutput);
                    return new EndurMasterExeSession(process, application);
                }
                catch
                {
                    // only dispose the application if initialisation failed
                    application.Dispose();
                    throw;
                }
            }
            catch
            {
                // only kill the process if initialisation failed - otherwise leave it running
                process.Kill();
                throw;
            }
        }

        private static void WaitForSessionToRegister(Process process, Application application, StringBuilder consoleOutput)
        {
            var result = new TaskCompletionSource<Session>();

            using (var subscriptions = new CompositeDisposable())
            {
                subscriptions.Add(
                    ProcessExitAsObservable(process)
                        .Subscribe(
                            _ => result.SetException(
                                new InvalidOperationException(
                                    string.Format("Process has exited. Console Output:{0}{1}", Environment.NewLine, consoleOutput.ToString())))));

                subscriptions.Add(
                    EndurSessionRegisteredAsObservable(process, application)
                        .Subscribe(s => result.SetResult(s)));

                var waitSuccess = result.Task.Wait(EndurSessionRegisteredTimeout);

                if (!waitSuccess)
                {
                    throw new TimeoutException("Timed out waiting for Endur session to register");
                }
            }
        }

        private static Process StartMasterExeProcess(
            string user,
            string password,
            EndurConfigFile endurConfigFile,
            DirectoryInfo olfBinDirectory,
            StringBuilder consoleOutput)
        {
            var masterExeFullName = Path.Combine(olfBinDirectory.FullName, "master.exe");

            if (!File.Exists(masterExeFullName))
            {
                throw new InvalidOperationException(string.Format("Binary '{0}' not found", masterExeFullName));
            }

            var commandLine = string.Format("-gui none -u {0} -p {1} -olfcfg \"{2}\"", user, password, endurConfigFile.File.FullName);

            var processStartInfo = new ProcessStartInfo
            {
                FileName = masterExeFullName,
                Arguments = commandLine,
                WorkingDirectory = olfBinDirectory.FullName,
                CreateNoWindow = true,
                UseShellExecute = false,
                RedirectStandardOutput = true,
                RedirectStandardError = true
            };

            var process = Process.Start(processStartInfo);

            if (process == null)
            {
                throw new InvalidOperationException(@"Endur Master.exe didn't start");
            }

            Task.Factory.StartNew(
                () =>
                {
                    ReadStream(process.StandardOutput, consoleOutput);
                    ReadStream(process.StandardError, consoleOutput);
                });

            return process;
        }

        private static void ReadStream(StreamReader reader, StringBuilder output)
        {
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                output.AppendLine(line);
            }
        }

        private static IObservable<Session> EndurSessionRegisteredAsObservable(Process process, Application application)
        {
            return
                Observable.Interval(PollingInterval)
                    .SelectMany(_ => application.SessionDescriptions)
                    .Where(s => s.Id == process.Id)
                    .FirstAsync()
                    .Select(application.Attach);
        }

        private static IObservable<Unit> ProcessExitAsObservable(Process process)
        {
            return Observable.Interval(PollingInterval)
                .Select(_ => process.HasExited)
                .FirstAsync(hasExited => hasExited)
                .Select(_ => Unit.Default);
        }
    }
}