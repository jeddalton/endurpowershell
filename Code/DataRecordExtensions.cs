﻿namespace Gazprom.MT.PowerShell.Endur.Code
{
    using System;
    using System.ComponentModel;
    using System.Data;

    public static class DataRecordExtensions
    {
        public static T GetField<T>(this IDataRecord record, String columnName)
        {
            try
            {
                var ordinal = record.GetOrdinal(columnName);
                return GetField<T>(record, ordinal);
            }
            catch (InvalidOperationException ex)
            {
                throw new InvalidOperationException(String.Format("Error getting value for column {0}. See inner exception", columnName), ex);
            }
        }

        public static T GetField<T>(this IDataRecord record, Int32 ordinal)
        {
            var type = typeof(T);
            var value = record.GetValue(ordinal);
            if (record.IsDBNull(ordinal))
            {
                if (type.IsValueType)
                {
                    throw new InvalidOperationException(String.Format("Value of column {0} is null and cannot be converted to {1}", ordinal, type.FullName));
                }
                return default(T);
            }

            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                if (record.IsDBNull(ordinal))
                {
                    return default(T);
                }

                var converter = new NullableConverter(type);
                type = converter.UnderlyingType;
            }

            return type == typeof(Object) ? (T)value : (T)Convert.ChangeType(value, type);
        }

    }
}