using System.IO;
using Olf.Openrisk.Application;

namespace Gazprom.MT.PowerShell.Endur.Code
{
    public class EndurAttachToExistingSessionFactory : IEndurSessionFactory
    {
        private readonly int _endurSessionId;

        public EndurAttachToExistingSessionFactory(int endurSessionId = 0)
        {
            _endurSessionId = endurSessionId;
        }

        public DirectoryInfo GetOlfBinDirectory()
        {
            return new DirectoryInfo(Application.BinDir);
        }

        public EndurSession Create()
        {
            using (var application = Application.Instance)
            {
                application.Attach(_endurSessionId);
                return new EndurSession(application);
            }
        }
    }
}