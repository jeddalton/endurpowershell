﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Gazprom.MT.PowerShell.Endur.Code
{
    public class OlfPackageWrapper
    {
        private readonly Type _dbAssemblyType;
        private readonly MethodInfo _exportToDbMethod;
        private readonly Type _olfAssemblyType;

        public OlfPackageWrapper(Type olfAssemblyType, Type dbAssemblyType, MethodInfo exportToDbMethod)
        {
            _olfAssemblyType = olfAssemblyType;
            _dbAssemblyType = dbAssemblyType;
            _exportToDbMethod = exportToDbMethod;
        }

        public static OlfPackageWrapper Create()
        {
            return Create(new OlfOpencomponentsPackageDll().GetOlfOpenComponentsPackageAssembly());
        }

        private static OlfPackageWrapper Create(Assembly packageAssembly)
        {
            var olfAssemblyType = packageAssembly.GetType("Olf.Embedded.Package.OlfAssembly");
            var dbAssemblyType = packageAssembly.GetType("Olf.Embedded.Package.DBAssembly");
            var exportToDbMethod = olfAssemblyType.GetMethod("ExportToDb");

            return new OlfPackageWrapper(olfAssemblyType, dbAssemblyType, exportToDbMethod);
        }

        public void ExportToDb(FileInfo assemblyFile, IList<string> referenceDirs)
        {
            var olfAssembly = CreateOlfAssembly(assemblyFile, referenceDirs);
            var parameters = _exportToDbMethod.GetParameters().Length > 0 ? new object[] {true} : null;
            _exportToDbMethod.Invoke(olfAssembly, parameters);
        }

        public void RemoveAssembly(string assemblyName)
        {
            var databaseAssembly =
                GetAssemblies()
                    .SingleOrDefault(d => GetAssemblyName(d).Equals(assemblyName, StringComparison.InvariantCultureIgnoreCase));

            if (databaseAssembly == null)
            {
                return;
            }

            RemoveAssembly(databaseAssembly);
        }

        public void RemoveAssembly(object databaseAssembly)
        {
            var deleteMethod = _dbAssemblyType.GetMethod("Delete");
            deleteMethod.Invoke(databaseAssembly, null);
        }

        public IEnumerable<object> GetAssemblies()
        {
            var getAssembliesMethod = _dbAssemblyType.GetMethod("GetAssemblies", BindingFlags.Public | BindingFlags.Static);
            return ((IEnumerable) getAssembliesMethod.Invoke(null, null)).Cast<object>();
        }

        private object CreateOlfAssembly(FileInfo assemblyFile, IList<string> referenceDirs)
        {
            return Activator.CreateInstance(_olfAssemblyType, assemblyFile.FullName, referenceDirs);
        }

        private string GetAssemblyName(object databaseAssembly)
        {
            var nameProperty = _dbAssemblyType.GetProperty("Name");

            return (string) nameProperty.GetValue(databaseAssembly, null);
        }

        public void Validate()
        {
            GetAssemblies();
        }
    }
}