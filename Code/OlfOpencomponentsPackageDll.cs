﻿using System;
using System.IO;
using System.Reflection;

namespace Gazprom.MT.PowerShell.Endur.Code
{
    public class OlfOpencomponentsPackageDll
    {
        private const string OlfOpenComponentsPackageDllRelativePath = @"opencomponents\Olf.Opencomponents.Package.dll";

        private static Assembly OlfOpenComponentsPackageAssembly { get; set; }

        public Assembly GetOlfOpenComponentsPackageAssembly()
        {
            
            if (OlfOpenComponentsPackageAssembly == null)
            {
                throw new InvalidOperationException(
                    "OlfOpencomponentsPackageDll.Initialise must be called once per AppDomain before OlfOpencomponentsPackageDll.GetOlfOpenComponentsPackageAssembly()");
            }

            return OlfOpenComponentsPackageAssembly;
        }

        public static void Initialise(DirectoryInfo olfBinDirectory)
        {
            OlfOpenComponentsPackageAssembly = Load(OlfOpenComponentsPackageDllRelativePath, olfBinDirectory);
        }

        private static Assembly Load(string assemblyFile, DirectoryInfo olfBinDirectory)
        {
            var assemblyPath = Path.Combine(olfBinDirectory.FullName, assemblyFile);
            var assembly = Assembly.LoadFrom(assemblyPath);

            if (assembly == null)
            {
                throw new InvalidOperationException(
                    string.Format("Unable to load assembly {0} from {1}", assemblyFile, assemblyPath));
            }

            return assembly;
        }
    }
}