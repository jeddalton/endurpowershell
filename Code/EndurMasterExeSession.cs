using System;
using System.Diagnostics;
using Olf.Openrisk.Application;

namespace Gazprom.MT.PowerShell.Endur.Code
{
    public class EndurMasterExeSession : EndurSession
    {
        private readonly Process _process;

        public EndurMasterExeSession(Process process, Application application) : base(application)
        {
            _process = process;
        }

        public override void Dispose()
        {
            base.Dispose();
            _process.Kill();
        }
    }

    public class EndurSession : IDisposable
    {
        private readonly Application _application;

        public EndurSession(Application application)
        {
            _application = application;
        }

        public object Session { get { return _application.CurrentSession; } }

        public virtual void Dispose()
        {
            _application.Detach();
            _application.Dispose();
        }
    }
}