﻿using System;
using System.Collections.Generic;

namespace Gazprom.MT.GGTP.Endur.ImportJvsProjects
{
    public class ProcessParameters
    {
        public String Command { get; set; }
        public String CommandLine { get; set; }
        public Int32 ResultCode { get; set; }
        public String WorkingDirectory { get; set; }
        public IDictionary<string, string> EnvironmentVariables { get; set; }
    }
}