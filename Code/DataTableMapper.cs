﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

namespace Gazprom.MT.PowerShell.Endur.Code
{
    public class DataTableMapper
    {
        /// <summary>
        /// Gets a list of property to column name mapping
        /// </summary>
        /// <param name="type">Type for which the properties are to be mapped</param>
        /// <param name="table">DataTable which has to be mapped to the object</param>
        /// <returns>mapping list of propertyInfo to column names</returns>
        public static IDictionary<string, PropertyInfo> GetColumnPropertyMapping(Type type, DataTable table)
        {
            if (table == null) throw new ArgumentNullException("table");

            var columnPropertyMappingList = new Dictionary<string, PropertyInfo>();
            var propertyInfoList = new List<PropertyInfo>(type.GetProperties());
            foreach (DataColumn column in table.Columns)
            {
                PropertyInfo property =
                    propertyInfoList.FirstOrDefault(
                        p => p.Name.Equals(column.ColumnName, StringComparison.InvariantCultureIgnoreCase));
                if (property != null)
                {
                    columnPropertyMappingList.Add(column.ColumnName, property);
                }
            }
            return columnPropertyMappingList;
        }

        /// <summary>
        /// Gets a mapped list of object from DataTable
        /// </summary>
        /// <param name="type">Type for which the properties are to be mapped</param>
        /// <param name="table">DataTable which has to be mapped to the object</param>
        /// <param name="fnGetRelatedData"></param>
        /// <returns>List of Instances of <b>type</b> mapped to each row in the given table</returns>
        public static IEnumerable<object> GetMappedList(Type type, DataTable table, Func<DataRow, PropertyInfo, string, object> fnGetRelatedData)
        {
            return GetMappedList(type, table, null, fnGetRelatedData);
        }

        /// <summary>
        /// Gets a mapped list of object from DataTable
        /// </summary>
        /// <param name="type">Type for which the properties are to be mapped</param>
        /// <param name="table">DataTable which has to be mapped to the object</param>
        /// <param name="filterExpression">any filter to be applied to data</param>
        /// <param name="fnGetRelatedData"></param>
        /// <returns>List of Instances of <b>type</b> mapped to each row in the given table</returns>
        public static IEnumerable<object> GetMappedList(Type type, DataTable table, string filterExpression, Func<DataRow, PropertyInfo, string, object> fnGetRelatedData)
        {
            if (table == null) throw new ArgumentNullException("table");
            if (type == null) throw new ArgumentNullException("type");

            var columnPropertyMappingList = GetColumnPropertyMapping(type, table);
            IEnumerable rowCollection = table.Rows;
            if (!string.IsNullOrEmpty(filterExpression))
            {
                rowCollection = table.Select(filterExpression);
            }

            foreach (DataRow row in rowCollection)
            {
                object instance = null;
                foreach (var columnPropertyMapping in columnPropertyMappingList)
                {
                    if (!Convert.IsDBNull(row[columnPropertyMapping.Key])
                        && row[columnPropertyMapping.Key].ToString().Trim().Length > 0
                        && columnPropertyMapping.Value.CanWrite)
                    {
                        if (instance == null) instance = Activator.CreateInstance(type);
                        object propertyValue = row[columnPropertyMapping.Key];
                        var propertyType = columnPropertyMapping.Value.PropertyType;
                        if (Nullable.GetUnderlyingType(propertyType) != null)
                            propertyType = Nullable.GetUnderlyingType(propertyType);
                        if (propertyType.IsEnum)
                        {
                            propertyValue = Enum.Parse(propertyType,
                                                       (string)propertyValue, true);
                        }
                        else if (propertyType != typeof(String)
                            && propertyType.GetInterface(typeof(IEnumerable).FullName) != null)
                        {
                            if (fnGetRelatedData == null) continue;
                            propertyValue = fnGetRelatedData(row, columnPropertyMapping.Value, (string)propertyValue);
                        }
                        if (propertyType.IsValueType
                            || propertyType == typeof(string))
                        {
                            propertyValue = Convert.ChangeType(propertyValue, propertyType);
                        }
                        columnPropertyMapping.Value.SetValue(instance, propertyValue, null);
                    }
                }
                if (instance != null) yield return instance;
            }
        }

    }
}