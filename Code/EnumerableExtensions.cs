﻿namespace Gazprom.MT.PowerShell.Endur.Code
{
    using System;
    using System.Collections.Generic;

    internal static class EnumerableExtensions
    {
        public static void ForEach<T>(this IEnumerable<T> input, Action<T> action)
        {
            if (action == null)
            {
                throw new ArgumentNullException("action");
            }

            foreach (var item in input)
            {
                action(item);
            }
        }
    }
}