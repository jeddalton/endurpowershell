﻿using System;
using Olf.Openrisk.Table;

namespace Gazprom.MT.PowerShell.Endur.Code
{
    public static class EndurRowMapperUtil
    {
        public static object GetCellValue(TableCell tableCell)
        {
            switch (tableCell.Type)
            {
                case EnumColType.Date:
                case EnumColType.DateTime:
                    return tableCell.Date;

                case EnumColType.Double:
                    return tableCell.Double;

                case EnumColType.Int:
                    return tableCell.Int;

                case EnumColType.String:
                    return tableCell.String;

                case EnumColType.Table:
                    return EndurPowershellUtil.TableToPSObjects(tableCell.Table);

                default:
                    return tableCell.DisplayString;
            }
        }

        public static Type GetCellType(TableCell tableCell)
        {
            switch (tableCell.Type)
            {
                case EnumColType.Date:
                case EnumColType.DateTime:
                    return typeof(DateTime);

                case EnumColType.Double:
                    return typeof(Double);

                case EnumColType.Int:
                    return typeof(int);
                
                case EnumColType.String:
                    return typeof(String);

                case EnumColType.Table:
                    return typeof(Table);

                default:
                    return typeof(String);
            }

        }
    }
}