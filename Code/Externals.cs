﻿using System;
using System.Runtime.InteropServices;

namespace Gazprom.MT.GGTP.Endur.PInvoke
{
    internal static class Externals
    {
        [DllImport("olf_sdb.dll")]
        public static extern IntPtr get_ssql(int connection);

        [DllImport("olf_sdb.dll")]
        public static extern Int32 get_unique_num(IntPtr ssqlPtr, Int32 a);

        [DllImport("olf_sdb.dll")]
        public static extern String retrieve_company_name(IntPtr ssqlPtr);

        [DllImport("olf_oldb_odbc")]
        public static extern void syb_begin_tran_priv(IntPtr ssqlPtr);

        [DllImport("olf_oldb_odbc")]
        public static extern void syb_commit_tran_priv(IntPtr ssqlPtr);

        [DllImport("olf_oldb_odbc")]
        public static extern void syb_rollback_tran_priv(IntPtr ssqlPtr);

        [DllImport("olfapi_reporting")]
        public static extern Int32 CRYSTAL_ImportTemplateFileToDB(String sourceFile, String dirNodePath);
    }
}
