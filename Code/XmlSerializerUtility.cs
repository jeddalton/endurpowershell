﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Gazprom.MT.PowerShell.Endur.Code
{
    public static class XmlSerailizerUtility
    {
        public static string Serialize<T>(T entity) where T : class
        {
            return Serialize(entity, entity.GetType());
        }

        public static string Serialize(object entity, Type objectType)
        {
            var xmldata = new StringBuilder();
            try
            {
                var serializer = new XmlSerializer(objectType);
                var settings = new XmlWriterSettings
                                   {
                                       Indent = true,
                                       Encoding = Encoding.UTF8,
                                       CloseOutput = false,
                                       CheckCharacters = true
                                   };

                using (var xmlWriter = XmlWriter.Create(xmldata, settings))
                {
                    serializer.Serialize(xmlWriter, entity);
                    xmlWriter.Flush();
                    xmlWriter.Close();
                }
            }
            catch (Exception)
            {
                xmldata.AppendFormat("{0} type failed to serialize", entity.GetType().Name);
            }
            return xmldata.ToString();
        }

        public static T Deserialize<T>(string xmlData) where T : class, new()
        {
            T result;
            XmlSerializer ser = new XmlSerializer(typeof(T));
            using (TextReader tr = new StringReader(xmlData))
            {
                result = (T)ser.Deserialize(tr);
            }
            return result;
        }
    }
}
