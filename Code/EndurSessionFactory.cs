using System;
using System.IO;
using Olf.NetToolkit;

namespace Gazprom.MT.PowerShell.Endur.Code
{
    public class EndurSessionFactory
    {
        private readonly IEndurSessionFactory _implementationStrategy;

        public EndurSessionFactory(IEndurSessionFactory implementationStrategy)
        {
            _implementationStrategy = implementationStrategy;
        }

        public EndurSession Create(DirectoryInfo olfBinDirectory)
        {
            var olfBinDirectoryPath = olfBinDirectory.FullName;
            AddFolderToPathEnvironmentVariable(olfBinDirectoryPath);
            Environment.CurrentDirectory = olfBinDirectoryPath;

            var endurSession = _implementationStrategy.Create();

            TkInitializerForOC.InitializeTkContext();

            return endurSession;
        }

        private static void AddFolderToPathEnvironmentVariable(string folder)
        {
            var path = Environment.GetEnvironmentVariable("PATH");
            if (path != null && path.Contains(folder))
            {
                return;
            }
            Environment.SetEnvironmentVariable("PATH", string.Format("{0};{1}", folder, path));
        }
    }
}