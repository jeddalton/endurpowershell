﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace Gazprom.MT.GGTP.Endur.ImportJvsProjects
{
    public class ProcessHelper
    {
        private Int32 _streamReaderCount;
        private readonly Object _lockObject = new Object();
        private Process _process;
        private ManualResetEvent _completeEvent;

        #region Constructors
        public ProcessHelper()
        {
            
        }

        public ProcessHelper(ProcessParameters processParameters)
        {
            this.ProcessParameters = processParameters;
        }
        #endregion

        #region Public Methods
        public void Start()
        {
            if (this._process != null)
            {
                throw new InvalidOperationException("Process already running");
            }
            Trace.WriteLine("Starting process");

            this._process =
                new Process
                {
                    StartInfo = GetStartInfo(this.ProcessParameters)
                };

            this._completeEvent = new ManualResetEvent(false);
            this._process.Start();

            this.ReadStreamHandler = new Action<StreamReader, EventHandler<ProcessInputReceivedEventArgs>>(ReadStream);
            this.ReadStreamHandler.BeginInvoke(this._process.StandardOutput, this.StandardOutputHandler, this.ReadStreamComplete, null);
            this.ReadStreamHandler.BeginInvoke(this._process.StandardError, this.ErrorOutputHandler, this.ReadStreamComplete, null);
        }

        public Int32 WaitForExit()
        {
            if (this._process == null)
            {
                throw new InvalidOperationException("Process not started");
            }

            try
            {
                this._completeEvent.WaitOne();
                Trace.WriteLine("Waiting for process to exit");
                this._process.WaitForExit();

                this.ExitCode = this._process.ExitCode;
                return this._process.ExitCode;

            }
            finally
            {
                this._process = null;
            }
        }
        #endregion

        #region Public Properties

        public ProcessParameters ProcessParameters { get; set; }
        public TextWriter StandardOutputWriter { get; set; }
        public TextWriter ErrorOutputWriter { get; set; }
        public Int32 ExitCode { get; private set; }

        #endregion

        #region Stream Reader
        private void ReadStreamComplete(IAsyncResult result)
        {
            lock (this._lockObject)
            {
                Trace.WriteLine("ReadStream complete");
                if (Interlocked.Decrement(ref this._streamReaderCount) == 0)
                {
                    Trace.WriteLine("All streams complete.");
                    this._completeEvent.Set();
                }
            }
        }

        private void ErrorOutputHandler(object sender, ProcessInputReceivedEventArgs e)
        {
            Trace.WriteLine(String.Format("ErrorOutputHandler: {0}", e.Input));
            if (this.ErrorOutputWriter != null)
            {
                this.ErrorOutputWriter.WriteLine(e.Input);
            }
        }

        private void StandardOutputHandler(object sender, ProcessInputReceivedEventArgs e)
        {
            Trace.WriteLine(String.Format("StandardOutputHandler: {0}", e.Input));
            if (this.StandardOutputWriter != null)
            {
                this.StandardOutputWriter.WriteLine(e.Input);
            }
        }

        private void ReadStream(StreamReader reader, EventHandler<ProcessInputReceivedEventArgs> eventHandler)
        {
            Interlocked.Increment(ref this._streamReaderCount);
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                if (eventHandler != null)
                {
                    eventHandler(this, new ProcessInputReceivedEventArgs(line));
                }
            }
        }

        private Action<StreamReader, EventHandler<ProcessInputReceivedEventArgs>> ReadStreamHandler { get; set; }

        private class ProcessInputReceivedEventArgs : EventArgs
        {
            public ProcessInputReceivedEventArgs(String input)
            {
                this.Input = input;
            }

            public String Input { get; private set; }
        }
        #endregion

        #region Private Methods
        private static ProcessStartInfo GetStartInfo(ProcessParameters processParameters)
        {
            var processStartInfo =
                new ProcessStartInfo
                {
                    FileName = processParameters.Command,
                    Arguments = processParameters.CommandLine,
                    CreateNoWindow = true,
                    UseShellExecute = false,
                    RedirectStandardError = true,
                    RedirectStandardOutput = true,
                    RedirectStandardInput = false,
                };

            if (!String.IsNullOrWhiteSpace(processParameters.WorkingDirectory))
            {
                processStartInfo.WorkingDirectory = processParameters.WorkingDirectory;
            }

            if (processParameters.EnvironmentVariables != null)
            {
                foreach (var variable in processParameters.EnvironmentVariables)
                {
                    processStartInfo.EnvironmentVariables[variable.Key] = variable.Value;
                }
            }

            return processStartInfo;
        }

        #endregion
    }
}
