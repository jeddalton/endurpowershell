﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management.Automation;
using Gazprom.MT.GGTP.Endur.ImportJvsProjects;
using Gazprom.MT.PowerShell.Endur.Code;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsData.Import, "EndurJvsProjects")]
    public class ImportEndurJvsProjects : EndurCmdletBase
    {
        [Parameter(Mandatory = true, ValueFromPipeline = true, Position = 0)]
        public string Folder { get; set; }

        protected override void ProcessRecord()
        {
            if (!Directory.Exists(Folder))
            {
                throw new InvalidOperationException(string.Format("Specified folder '{0}' does not exist", Folder));
            }

            var tempFolder = CopySourceToLocalTempFolder(Folder);

            try
            {
                var unorderedJvsProjects = GetUnorderedJvsProjects(tempFolder);

                var orderedJvsProjects = GetOrderedByDependencyJvsProjects(unorderedJvsProjects);

                ImportJvsProjectsToEndur(orderedJvsProjects, OlfBinDirectory, EndurSessionId);
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Error importing JVS projects - " + ex.Message, ex);
            }
            finally
            {
                if (Directory.Exists(tempFolder))
                {
                    Directory.Delete(tempFolder, true);
                }
            }
        }

        private void ImportJvsProjectsToEndur(IEnumerable<JvsProject> jvsProjects, DirectoryInfo olfBinDirectory, int sessionId)
        {
            jvsProjects.ForEach(jvsProject => ImportJvsProjectToEndur(jvsProject, olfBinDirectory, sessionId));
        }

        private void ImportJvsProjectToEndur(JvsProject jvsProject, DirectoryInfo olfBinDirectory, int sessionId)
        {
            CreateJvsProjectOutFile(jvsProject);
            RunAntImport(jvsProject, olfBinDirectory, sessionId);
        }

        private void RunAntImport(JvsProject jvsProject, DirectoryInfo olfBinDirectory, int sessionId)
        {
            var processParameters =
                new ProcessParameters
                {
                    Command = GetCommand(olfBinDirectory),
                    CommandLine = GetCommandLine(jvsProject, olfBinDirectory, sessionId),
                    WorkingDirectory = olfBinDirectory.FullName,
                    EnvironmentVariables = GetEnvironmentVariables(olfBinDirectory)
                };

            try
            {
                using (var writer = new StringWriter())
                {
                    WriteVerbose(string.Format("Attempting to run ant: {0}", processParameters.CommandLine));
                    var processHelper = new ProcessHelper(processParameters)
                    {
                        StandardOutputWriter = writer,
                        ErrorOutputWriter = writer
                    };
                    processHelper.Start();
                    processHelper.WaitForExit();

                    var output = writer.ToString();
                    if (output.Contains("BUILD FAILED"))
                    {
                        var message = string.Format("{0} - {1}", jvsProject.ProjectFolder, output);
                        WriteWarning(message);
                        throw new InvalidOperationException(message);
                    }
                    WriteVerbose(output);
                }
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Error running JVS ant import", ex);
            }
        }

        private static IDictionary<string, string> GetEnvironmentVariables(DirectoryInfo olfBinDirectory)
        {
            return new Dictionary<string, string>
            {
                {
                    "Path", string.Format(
                        "{0};{1}",
                        Path.Combine(olfBinDirectory.FullName, @"olf_dependencies\java\jdk1.7.0_07\bin"),
                        Path.Combine(olfBinDirectory.FullName, @"otk\openjvs\apache-ant\bin"))
                },
                {
                    "JAVA_HOME", Path.Combine(olfBinDirectory.FullName, @"olf_dependencies\java\jdk1.7.0_07")
                }
            };
        }

        private static string GetCommand(DirectoryInfo endurBinaryFolder)
        {
            return Path.Combine(endurBinaryFolder.FullName, @"otk\openjvs\apache-ant\bin\ant.bat");
        }

        private static string GetCommandLine(JvsProject jvsProject, DirectoryInfo olfBinDirectory, int sessionId)
        {
            return string.Format(
                "-e -q -f \"{0}\\otk\\openjvs\\lib\\olf_openjvs_build.xml\" import-project-by-manifest -Dopenjvs.debug=false -Dolf.root.dir=\"{0}\" -Dopenjvs.project.manifestFile=\"{1}\" -Dopenjvs.gui=false -Dolf.session.id={2}",
                olfBinDirectory.FullName,
                jvsProject.OutFilePath,
                sessionId);
        }

        private void CreateJvsProjectOutFile(JvsProject jvsProject)
        {
            if (File.Exists(jvsProject.OutFilePath))
            {
                WriteVerbose(string.Format("Out file '{0}' already exists. Skipping creation.", jvsProject.OutFilePath));
                return;
            }

            WriteVerbose(string.Format("Creating out file: {0}", jvsProject.OutFilePath));
            var rootFolder = Path.GetDirectoryName(jvsProject.OutFilePath);
            using (var writer = File.CreateText(jvsProject.OutFilePath))
            {
                writer.WriteLine("\"{0}\",29,\"\"", jvsProject.Name);
                writer.WriteLine("\"jproject_classpath\",8,\"{0}/jproject.classpath\"", jvsProject.Name);

                foreach (var projectFile in jvsProject.ProjectFiles)
                {
                    var filePath = projectFile.Replace(rootFolder + Path.DirectorySeparatorChar, "").Replace(Path.DirectorySeparatorChar, '/');
                    writer.WriteLine("\"{0}\",33,\"{1}\"", filePath, jvsProject.GetScriptPath(projectFile));
                }
            }
        }

        private IEnumerable<JvsProject> GetOrderedByDependencyJvsProjects(IList<JvsProject> unorderedJvsProjects)
        {
            Trace.WriteLine("Ordering projects based upon dependencies");
            var orderedJvsProjects = new List<JvsProject>();

            foreach (var jvsProject in unorderedJvsProjects.Except(orderedJvsProjects))
            {
                WriteVerbose(string.Format("Adding project {0}", jvsProject.Name));
                AddJvsProject(jvsProject, orderedJvsProjects, unorderedJvsProjects);
            }

            return orderedJvsProjects;
        }

        private void AddJvsProject(JvsProject jvsProject, ICollection<JvsProject> orderedJvsProjects, IList<JvsProject> sourceJvsProjects)
        {
            if (orderedJvsProjects.Contains(jvsProject)) return;

            foreach (var dependency in jvsProject.Dependencies)
            {
                var localDependency = dependency;
                WriteVerbose(string.Format("Adding dependency {0}", localDependency));

                var parentProject = sourceJvsProjects.Single(x => x.Name.Equals(localDependency));

                AddJvsProject(parentProject, orderedJvsProjects, sourceJvsProjects);
            }

            WriteVerbose(string.Format("Adding ordered project {0}", jvsProject.Name));
            orderedJvsProjects.Add(jvsProject);
        }

        private string CopySourceToLocalTempFolder(string sourceFolder)
        {
            var jvsFolder = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());

            WriteVerbose(string.Format("Copying from '{0}' to '{1}'", sourceFolder, jvsFolder));
            if (Directory.Exists(jvsFolder))
            {
                Directory.Delete(jvsFolder, true);
            }
            CopyFolder(sourceFolder, jvsFolder);

            return jvsFolder;
        }

        private static void CopyFolder(string sourceFolder, string destFolder)
        {
            if (!Directory.Exists(destFolder))
                Directory.CreateDirectory(destFolder);

            Directory.EnumerateFiles(sourceFolder).AsParallel()
                .Where(sourceFileFullName => !string.IsNullOrWhiteSpace(sourceFileFullName))
                .ForAll(
                    sourceFileFullName =>
                    {
                        var filename = Path.GetFileName(sourceFileFullName);
                        if (string.IsNullOrEmpty(filename)) return;
                        var destFilename = Path.Combine(destFolder, filename);
                        File.Copy(sourceFileFullName, destFilename, true);
                        var newAttr = File.GetAttributes(destFilename) & ~FileAttributes.ReadOnly;
                        File.SetAttributes(destFilename, newAttr);
                    });

            Directory.EnumerateDirectories(sourceFolder).AsParallel()
                .Where(directoryName => !string.IsNullOrWhiteSpace(directoryName))
                .ForAll(
                    sourceDirectoryFullName =>
                    {
                        var directoryName = Path.GetFileName(sourceDirectoryFullName);
                        if (string.IsNullOrEmpty(directoryName)) return;
                        var destDirectory = Path.Combine(destFolder, directoryName);
                        CopyFolder(sourceDirectoryFullName, destDirectory);
                    })
                ;
        }

        private static IEnumerable<string> GetProjectFiles(string folder)
        {
            return Directory.EnumerateFiles(folder, "jproject.classpath", SearchOption.AllDirectories);
        }

        private static IList<JvsProject> GetUnorderedJvsProjects(string tempFolder)
        {
            Trace.WriteLine("Getting unordered projects");
            return GetProjectFiles(tempFolder).Select(projectFileName => new JvsProject(projectFileName)).ToList();
        }
    }
}