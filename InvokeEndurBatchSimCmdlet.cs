﻿using System;
using System.Management.Automation;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsLifecycle.Invoke, "EndurBatchSim")]
    public class InvokeEndurBatchSimCmdlet : EndurCmdletBase
    {
        [Parameter(Mandatory = true, ValueFromPipeline = true, Position = 0)]
        public String Name { get; set; }

        protected override void ProcessRecord()
        {
            try
            {
                using (var batchSim = this.Session.SimulationFactory.RetrieveBatchSimulation(this.Name))
                {
                    if (batchSim == null)
                    {
                        throw new PSInvalidOperationException(String.Format("Batch simulation '{0}' not found", this.Name));
                    }
                    batchSim.Run();
                }

            }
            catch (Exception ex)
            {
                this.WriteWarning(ex.Message);
            }
        }
    }
}