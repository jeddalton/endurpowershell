﻿using System.IO;
using System.Management.Automation;
using Gazprom.MT.PowerShell.Endur.Code;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsCommunications.Read, "EndurConfigFile")]
    public class ReadEndurConfigFileCmdlet : PSCmdlet
    {
        [Parameter(Mandatory = true, Position = 0, ValueFromPipeline = true)]
        public object ConfigFile { get; set; }

        protected override void ProcessRecord()
        {
            var configFileName = ConfigFile as FileInfo ?? new FileInfo(ConfigFile.ToString());
            var values = new EndurConfigFile(configFileName).GetConfigFileValues();

            WriteObject(values);
        }
    }
}