BEGIN TRAN

EXECUTE [dbo].[save_idx_historical_fx_rates] 
   @fx_rate_date = $(fx_rate_date)
  ,@currency_id = $(currency_id)
  ,@reference_currency_id = $(reference_currency_id)
  ,@fx_rate_bid = $(fx_rate_bid)
  ,@fx_rate_mid = $(fx_rate_mid)
  ,@fx_rate_offer = $(fx_rate_offer)
  ,@data_set_type = $(data_set_type)
  ,@currency_convention = $(currency_convention)
  ,@personnel_id = $(personnel_id)

COMMIT TRAN