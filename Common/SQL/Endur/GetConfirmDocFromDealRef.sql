SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SELECT DISTINCT sdd.document_num, MAX(sdd.doc_version) as DocVersion,1 AS RowNum
--, sdds.doc_status_desc, sddt.doc_type_desc, sdd.event_num, et.name as EventType, MAX(sdd.doc_version) as MaxDocVersion
 FROM ab_tran at
INNER JOIN ab_tran_event ate ON ate.tran_num=at.tran_num
INNER JOIN stldoc_details sdd ON sdd.event_num=ate.event_num
INNER JOIN stldoc_header sdh ON sdh.document_num=sdd.document_num AND sdh.doc_version=sdd.doc_version
INNER JOIN stldoc_document_type sddt ON sddt.doc_type=sdh.doc_type
INNER JOIN event_type et ON et.id_number=sdd.event_type
INNER JOIN stldoc_document_status sdds ON sdds.doc_status=sdh.doc_status
INNER JOIN dbo.stldoc_definitions sddef ON sddef.stldoc_def_id=sdh.stldoc_def_id
WHERE 1=1
AND at.reference = '$(DealRef)'
AND sddef.stldoc_def_name = '$(DocDef)'
--AND at.reference = 'BackOffice_GAS_CapEntryExit_T001'
AND sddt.doc_type_desc LIKE '%Confirm%'
GROUP BY 
at.reference,sdd.document_num, sdds.doc_status_desc, sddt.doc_type_desc, sdd.event_num, et.name
ORDER BY 1