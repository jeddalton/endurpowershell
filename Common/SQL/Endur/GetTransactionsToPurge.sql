SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SELECT at.tran_num
FROM ab_tran at
INNER JOIN trans_status ts ON ts.trans_status_id=at.tran_status
INNER JOIN asset_type ast ON ast.id_number=at.asset_type
INNER JOIN trans_type tt ON tt.id_number=at.tran_type
LEFT JOIN ab_tran_info_view ativ ON ativ.tran_num=at.tran_num AND ativ.type_name='Ext Deal Source Ref'
WHERE 1=1
AND ts.name!='Template'
AND (at.reference LIKE '$(DealRef)%' ESCAPE '!' or ativ.value LIKE '$(DealRef)%' ESCAPE '!')
AND tt.name='Trading'
AND ast.name IN ('Trading','No Delivery') --Trading / FO Profile (aliased)