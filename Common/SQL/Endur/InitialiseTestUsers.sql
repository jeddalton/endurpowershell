BEGIN TRAN

-- Make sure all the test accounts are authorised, licensed users
UPDATE personnel SET status=1,personnel_type=2,password_never_expires=1 WHERE name LIKE 'test%'

-- SET SECURITY GROUP access
DELETE users_to_groups WHERE user_number IN
(SELECT p.id_number FROM personnel p WHERE p.name like 'test%')

-- Allow all Security Groups
INSERT INTO users_to_groups
SELECT p.id_number,g.id_number FROM personnel p,groups g
WHERE 1=1
AND p.name like 'test%'

-- SET BU & LE access
DELETE party_personnel WHERE personnel_id IN
(SELECT p.id_number FROM personnel p WHERE p.name like 'test%')

-- Clone the administrator for BU & LE access
INSERT INTO party_personnel
SELECT pp.party_id,p.id_number,pp.default_flag,1,getdate(),1 FROM personnel p,party_personnel pp
WHERE 1=1
AND p.name like 'test%'
AND pp.personnel_id=1

-- SET PORTFOLIO ACCESS
DELETE portfolio_personnel WHERE personnel_id IN
(SELECT p.id_number FROM personnel p WHERE p.name like 'test%')

-- Clone the administrator account for portfolio access
INSERT INTO portfolio_personnel
SELECT p.id_number,pp.portfolio_id,pp.access_type,pp.default_flag,pp.version_number,pp.user_id,GETDATE() FROM personnel p
INNER JOIN portfolio_personnel pp ON pp.personnel_id=1
WHERE 1=1
AND p.name like 'test%'

-- Functional groups
DELETE personnel_functional_group WHERE personnel_id IN
(SELECT p.id_number FROM personnel p WHERE p.name like 'test%')

INSERT INTO personnel_functional_group
SELECT p.id_number,fg.id_number,null FROM personnel p
INNER JOIN functional_group fg ON fg.id_number is not null
WHERE 1=1
AND p.name like 'test%'

-- Secured Indices
DELETE secured_index WHERE user_id IN
(SELECT p.id_number FROM personnel p WHERE p.name like 'test%')

INSERT INTO secured_index
SELECT p.id_number,id.index_id,1,getdate() FROM personnel p
INNER JOIN idx_def id ON id.secure_flag=1 AND id.db_status=1
WHERE 1=1
AND p.name like 'test%'


COMMIT TRAN
