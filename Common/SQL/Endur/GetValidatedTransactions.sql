SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SELECT at.tran_num
FROM ab_tran at
INNER JOIN trans_status ts ON ts.trans_status_id=at.tran_status
INNER JOIN offset_tran_type ott ON ott.id_number=at.offset_tran_type
INNER JOIN asset_type ast ON ast.id_number=at.asset_type
WHERE 1=1
AND ts.name='Validated'
AND at.reference LIKE '$(DealRef)%'
--AND at.reference LIKE '%PhysFixed%'
AND at.current_flag=1
AND ott.name IN ('No Offset','Original Offset')
AND ast.name IN ('Trading','No Delivery') --Trading / FO Profile (aliased)