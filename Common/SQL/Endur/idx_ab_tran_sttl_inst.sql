/****** Object:  Index [ab_tran_sttl_inst_idx2]    Script Date: 20/11/2012 15:57:15 ******/
DROP INDEX [ab_tran_sttl_inst_idx2] ON [dbo].[ab_tran_sttl_inst] WITH ( ONLINE = OFF )
GO

/****** Object:  Index [ab_tran_sttl_inst_idx2]    Script Date: 20/11/2012 15:57:15 ******/
CREATE CLUSTERED INDEX [ab_tran_sttl_inst_idx2] ON [dbo].[ab_tran_sttl_inst]
(
       [tran_num] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
