SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SELECT A12.tran_num
FROM ab_tran A12
  INNER JOIN trans_type T09 ON(T09.id_number=A12.tran_type AND T09.id_number=2)
  INNER JOIN trans_status T08 ON(T08.trans_status_id=A12.tran_status AND A12.tran_status IN(3,4))
  INNER JOIN instruments I05 ON(I05.id_number=A12.ins_type)
  INNER JOIN misc_ins M01 ON(M01.ins_num=A12.ins_num AND M01.param_seq_num=0)
  INNER JOIN contract_codes C04 ON(C04.contract_code_id=M01.contract_code)
  INNER JOIN parameter P55 ON(P55.ins_num = A12.ins_num AND P55.param_seq_num=0)
  INNER JOIN profile P54 ON(P54.ins_num = A12.ins_num AND P54.param_seq_num = P55.param_seq_num)
WHERE 1=1
  AND I05.name = '$(InsType)'
  AND C04.contract_code = '$(ContractCode)'
  AND P54.pymt_date < '$(EndDate)' --use profile pymt_date and not A12.settle_date

