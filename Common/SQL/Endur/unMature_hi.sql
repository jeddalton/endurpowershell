--un-mature holding instruments
UPDATE A12 SET A12.tran_status=3 
FROM ab_tran A12 
WHERE (A12.tran_type=2 AND A12.tran_status=4) 
  AND A12.reference IN(
						'0.000/C/B-Dec-13',
						'B-Dec-13',
						'BB-Dec-13',
						'G0B-Oct-13','G0B-Nov-13','G0B-Dec-13','G0B-Jan-14','G0B-Feb-14','G0B-Mar-14',
						'G0B-Q3-13','G0B-Q1-14',
						'PNEXT_TTF-Oct-13','PNEXT_TTF-Nov-13','PNEXT_TTF-Dec-13','PNEXT_TTF-Jan-14','PNEXT_TTF-Feb-14','PNEXT_TTF-Mar-14',
						'PNEXT_TTF-Q4-13','PNEXT_TTF-Q1-14',
						'PNEXT_TTF-WIN-13',
						'TTF-Oct-13','TTF-Nov-13','TTF-Dec-13','TTF-Jan-14','TTF-Feb-14','TTF-Mar-14'
						)