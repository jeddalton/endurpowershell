SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SELECT at.reference, at.tran_num
FROM ab_tran at
WHERE 1=1
AND at.current_flag=1
AND at.reference LIKE '$(DealRef)%%'
AND at.offset_tran_type IN (0,1)