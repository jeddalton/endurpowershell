SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SELECT
  CONVERT(VARCHAR,A12.input_date,103) AS xinput_date,A12.deal_tracking_num AS xdeal_num,A12.ins_num AS xins_num,A12.tran_num AS xtran_num,P104.name AS xlast_updated_by,CONVERT(VARCHAR,A12.last_update,103) + ' ' + CONVERT(VARCHAR,A12.last_update,114) AS xlast_update, --0
  T08.name AS TranStatus, --A
  A12.reference AS Reference, --B
  T05.name AS Toolset, --C
  I05.name AS InstrumentType, --D
  H03.ticker AS Ticker,  --E Zero Strike value for upload
  I07.index_name AS ProjectionIndex, --F
  P16.short_name AS InternalBusinessUnit, --G
  P06.name AS InternalPortfolio, --H
  P04.name AS Trader, --I
  P116.short_name AS ExternalBusinessUnit, --J
  P216.short_name AS ExecutionBroker, --K
  CONVERT(VARCHAR,A12.trade_date,103) AS TradeDate, --L
  CASE I29.put_call WHEN NULL THEN 'Put' ELSE P61.name END AS PutOrCall, --M
  CASE WHEN A12.buy_sell = 0 THEN 'Buy' ELSE 'Sell' END AS BuyOrSell, --N
  CASE I29.strike WHEN NULL THEN 0 ELSE I29.strike END AS Strike, --O
  ABS(A12.position) AS Position, --P
  A12.price AS Premium, --Q
  EDS.VALUE AS ExternalRefSource, --R
  EDR.VALUE AS ExternalRef, --S
  TTR.value AS TimeTraded, --T
  C04.contract_code --U used for sort only
FROM ab_tran A12 WITH(NOLOCK)
  INNER JOIN toolsets T05 ON (T05.id_number = A12.toolset)
  INNER JOIN instruments I05 ON (I05.id_number = A12.ins_type)
  INNER JOIN trans_status T08 ON (T08.trans_status_id = A12.tran_status)
  INNER JOIN parameter P55 ON (P55.ins_num = A12.ins_num) 
  INNER JOIN header H03 ON (H03.ins_num = A12.ins_num)
  INNER JOIN idx_def I07 ON (I07.index_id = P55.proj_index AND I07.db_status = 1)
  INNER JOIN party P16 ON (P16.party_id = A12.internal_bunit)
  INNER JOIN portfolio P06 ON (P06.id_number = A12.internal_portfolio)
  INNER JOIN misc_ins M01 ON (M01.ins_num = A12.ins_num AND M01.param_seq_num = P55.param_seq_num)
  INNER JOIN contract_codes C04 ON (C04.contract_code_id = M01.contract_code)
  INNER JOIN personnel P04 ON (P04.id_number = A12.internal_contact)
  INNER JOIN personnel P104 ON (P104.id_number = A12.personnel_id)
  INNER JOIN party P116 ON (P116.party_id = A12.external_bunit)
  LEFT OUTER JOIN party P216 ON (P216.party_id = A12.broker_id)
  INNER JOIN ins_option I29 ON (I29.ins_num = A12.ins_num AND I29.param_seq_num = P55.param_seq_num  AND I29.option_seq_num = 0)
  INNER JOIN put_call P61 ON (P61.id_number = I29.put_call)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 INNER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) INNER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'Ext Deal Source') EDS ON (EDS.TRAN_NUM = A12.tran_num)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 INNER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) INNER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'Ext Deal Source Ref') EDR ON (EDR.TRAN_NUM = A12.tran_num)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 LEFT OUTER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) LEFT OUTER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'Time Traded') TTR ON (TTR.TRAN_NUM = A12.tran_num)
WHERE (A12.trade_flag = 1 AND A12.asset_type = 2) -- Trading
  AND A12.tran_status IN(2,3)         -- 2=New; 3=Validated; 4=Matured; 5=Cancelled; 14=Deleted; 22=Closeout;
  AND A12.offset_tran_type IN(0,1)    -- 0=No Offset; 1=Original Offset
  AND I05.name IN ('ENGY-EXCH-OPT')   -- ins_type
  AND EDR.value LIKE '$(DealRef)%%'
ORDER BY
  EDR.value         --tran info external deal ref