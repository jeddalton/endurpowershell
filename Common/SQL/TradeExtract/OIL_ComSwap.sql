SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SELECT 
  CONVERT(VARCHAR,A12.input_date,103) AS xinput_date,A12.deal_tracking_num AS xdeal_num,A12.ins_num AS xins_num,A12.tran_num AS xtran_num,P104.name AS xlast_updated_by,CONVERT(VARCHAR,A12.last_update,103) + ' ' + CONVERT(VARCHAR,A12.last_update,114) AS xlast_update, --0
  T08.name AS TranStatus, --A
  A12.reference AS Reference, --B
  T05.name AS Toolset, --C
  I05.name AS InstrumentType, --D
  CONVERT(VARCHAR,A12.trade_date,103) AS TradeDate, --E
  A112.reference AS Template, --F       
  P16.short_name AS InternalBusinessUnit, --G
  P06.name AS InternalPortfolio, --H
  P04.name AS Trader, --I
  P116.short_name AS ExternalBusinessUnit, --J
  P106.name AS ExternalPortfolio, --K
  P216.short_name AS ExecutionBroker, --L
  atp.reserved_amt AS BrokerFee, --M
  CASE WHEN A12.buy_sell = 0 THEN 'Buy' ELSE 'Sell' END AS BuyOrSell, --N
  CONVERT(VARCHAR,A12.start_date,103) AS StartDate, --O
  CONVERT(VARCHAR,A12.maturity_date,103) AS EndDate, --P
  --CASE WHEN P55.daily_volume = 1 THEN P55.notnl ELSE NULL END AS daily_volume, --Q0
  CASE WHEN P55.daily_volume = 0 THEN P55.notnl ELSE NULL END AS Volume, --R0
  VOLU.VALUE AS Unit, --S1
  VOLU2.VALUE AS Unit2, --S2
  CASE 
	WHEN DUN.VALUE='Pence' THEN SIDE1.float_spd * 100 ELSE SIDE1.float_spd
  END AS RateSpread,
  CASE 
	WHEN I05.name='ENGY-SWAP' THEN 
		CASE WHEN DUN.VALUE='Pence' THEN SIDE2.rate * 100 ELSE SIDE2.rate END
	WHEN I05.name='ENGY-B-SWAP' THEN
		CASE WHEN DUN.VALUE='Pence' THEN SIDE2.float_spd * 100 ELSE SIDE2.float_spd END
  END AS RateSpread2,
  DUN.VALUE AS DenominationUnit, --V1
  PRU.VALUE AS PriceUnit, --W1
  PRU2.VALUE AS PriceUnit2, --W2
  EDS.VALUE AS ExternalRefSource, --X
  EDR.VALUE AS ExternalRef, --Y
  OCC.VALUE AS OTC_contract_code, --Z
  TRM.VALUE AS TransactionMethod, --AA
  IoA.VALUE AS InitiatorOrAggressor, --AB
  SLV.VALUE AS Sleeve, --AC
  TTR.VALUE AS TimeTraded, --AD
  CRF.VALUE AS CargoReference, --AE
  EOR.VALUE AS ExternalOrderReference --AF
FROM ab_tran A12 WITH(NOLOCK)
  INNER JOIN ab_tran A112 ON (A112.tran_num = A12.template_tran_num)
  INNER JOIN parameter P55 ON (P55.ins_num = A12.ins_num) 
  INNER JOIN trans_status T08 ON (T08.trans_status_id = A12.tran_status)
  INNER JOIN toolsets T05 ON (T05.id_number = A12.toolset)
  INNER JOIN instruments I05 ON (I05.id_number = A12.ins_type)
  INNER JOIN party P16 ON (P16.party_id = A12.internal_bunit)
  INNER JOIN portfolio P06 ON (P06.id_number = A12.internal_portfolio)
  INNER JOIN personnel P04 ON (P04.id_number = A12.internal_contact)
  INNER JOIN party P116 ON (P116.party_id = A12.external_bunit)
  LEFT OUTER JOIN portfolio P106 ON (P106.id_number = A12.external_portfolio)
  LEFT OUTER JOIN party P216 ON (P216.party_id = A12.broker_id)
  LEFT OUTER JOIN ab_tran_provisional atp ON (atp.tran_num = A12.tran_num AND atp.prov_type = 3)
  INNER JOIN personnel P104 ON (P104.id_number = A12.personnel_id)
  LEFT OUTER JOIN (SELECT A12.ins_num, I103.unit_label AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   INNER JOIN idx_unit I103 ON (I103.unit_id=P55.unit) WHERE P55.param_seq_num=0) VOLU ON (VOLU.ins_num=A12.ins_num) 
  LEFT OUTER JOIN (SELECT A12.ins_num, I103.unit_label AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   INNER JOIN idx_unit I103 ON (I103.unit_id=P55.unit) WHERE P55.param_seq_num=1) VOLU2 ON (VOLU2.ins_num=A12.ins_num) 
  LEFT OUTER JOIN (SELECT A12.ins_num, I203.unit_label AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   INNER JOIN idx_unit I203 ON (I203.unit_id=P55.price_unit) WHERE P55.param_seq_num=0) PRU ON (PRU.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, I203.unit_label AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   INNER JOIN idx_unit I203 ON (I203.unit_id=P55.price_unit) WHERE P55.param_seq_num=1) PRU2 ON (PRU2.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, P55.float_spd, P55.rate FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   WHERE P55.param_seq_num=0) SIDE1 ON (SIDE1.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, P55.float_spd, P55.rate FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   WHERE P55.param_seq_num=1) SIDE2 ON (SIDE2.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, DU.denomination_unit_name AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON(P55.ins_num=A12.ins_num)
                   INNER JOIN ins_parameter I35 ON(I35.ins_num=A12.ins_num AND I35.param_seq_num=P55.param_seq_num)
                   INNER JOIN denomination_unit DU ON (DU.unique_id = I35.denomination_unit_id) WHERE P55.param_seq_num=1) DUN ON (DUN.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 INNER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) INNER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'Ext Deal Source') EDS ON (EDS.TRAN_NUM = A12.tran_num)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 INNER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) INNER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'Ext Deal Source Ref') EDR ON (EDR.TRAN_NUM = A12.tran_num)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 INNER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) INNER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'OTC Contract Code') OCC ON (OCC.TRAN_NUM = A12.tran_num)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 INNER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) INNER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'Transaction Method') TRM ON (TRM.TRAN_NUM = A12.tran_num)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 INNER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) INNER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'Initiator/Aggressor') IoA ON (IoA.TRAN_NUM = A12.tran_num)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 INNER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) INNER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'Sleeve') SLV ON (SLV.TRAN_NUM = A12.tran_num) 
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 LEFT OUTER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) LEFT OUTER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'Time Traded') TTR ON (TTR.TRAN_NUM = A12.tran_num)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 LEFT OUTER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) LEFT OUTER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'Cargo Reference') CRF ON (CRF.TRAN_NUM = A12.tran_num)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 LEFT OUTER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) LEFT OUTER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'Ext Order Ref') EOR ON (EOR.TRAN_NUM = A12.tran_num)
WHERE (A12.trade_flag = 1 AND A12.asset_type = 2) -- Trading
  AND P55.param_seq_num = 0           -- show first leg only
  AND A12.tran_status IN(2,3)         -- 2=New; 3=Validated; 4=Matured; 5=Cancelled; 7=Give Up (Proposed); 15=Template; 14=Deleted; 22=Closeout;
  AND A12.offset_tran_type IN(0,1)    -- 0=No Offset; 1=Original Offset
  AND I05.name in ('ENGY-SWAP','ENGY-B-SWAP') -- ins_type
  AND A12.reference LIKE '$(DealRef)%%'
ORDER BY
  I05.name,           --ins_type
  A12.reference       --tran ref