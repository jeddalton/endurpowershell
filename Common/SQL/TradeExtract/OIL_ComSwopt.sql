SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SELECT DISTINCT
  CONVERT(VARCHAR,A12.input_date,103) AS xinput_date,A12.deal_tracking_num AS xdeal_num,A12.ins_num AS xins_num,A12.tran_num AS xtran_num,P104.name AS xlast_updated_by,CONVERT(VARCHAR,A12.last_update,103) + ' ' + CONVERT(VARCHAR,A12.last_update,114) AS xlast_update, --0
  T08.name AS TranStatus,
  A12.reference AS Reference,
  T05.name AS Toolset,
  I05.name AS InstrumentType,
  at1.reference AS Template,
  P16.short_name AS InternalBusinessUnit,
  P06.name AS InternalPortfolio,
  P04.name AS Trader,
  P116.short_name AS ExternalBusinessUnit,
  C05.iso_code AS TaxTranType,
  P106.name AS ExternalPortfolio,
  E03.event_source_name,
  I42.tax_payment,
  C03.name AS cflow_type,
  P216.short_name AS ExecutionBroker,
  atp.reserved_amt AS BrokerFee,
  CAST(A12.trade_date AS DATE) AS TradeDate,
  CAST(A12.start_date AS DATE) AS StartDate,
  CAST(A12.maturity_date AS DATE) AS EndDate,
  CASE I40.put_call WHEN 0 THEN 'Receiver''s Put' WHEN 1 THEN 'Payer''s Call' END AS PutOrCall,
  CASE I40.buy_sell WHEN 0 THEN 'Buy' WHEN 1 THEN 'Sell' END AS BuyOrSell,
  [PRATE2].[VALUE] AS Strike,
  [PNOTN0].[VALUE] AS Profile_Volume0,
  [PNOTN1].[VALUE] AS Profile_Volume1,
  [PNOTN2].[VALUE] AS Profile_Volume2,
  ABS(P69.cflow/P55.notnl) AS [Premium/Unit],
  P69.cflow AS Premium,
  C08.name AS PremiumCurrency,
  CAST(P69.cflow_date AS DATE) AS PremiumDate,
  DUN2.denomination_unit_name AS DenominationUnit2,
  VUNIT2.unit_label AS VolUnit2,
  I07.index_name AS ProjIndex,
  REFS1.name AS RefSource,
  [SETT0].[VALUE] AS Settlememt_Type,
  [OPTT0].[VALUE] AS Option_Type,
  E02.name AS ExpiryCut,
  CAST(I40.beg_exercise_date AS DATE) AS StartExerciseDate,
  CAST(I40.end_exercise_date AS DATE) AS EndExerciseDate,
  CAST([PPYMT0].[VALUE] AS DATE) AS ProfilePymtDate,
  [EDS].[VALUE] AS ExternalRefSource,
  [EDR].[VALUE] AS ExternalRef,
  [OCC].[VALUE] AS OTCContractCode
FROM ab_tran A12 WITH(NOLOCK)
  LEFT OUTER JOIN ab_tran_event A17 WITH(NOLOCK) ON(A17.tran_num=A12.tran_num AND A17.event_type=98) --14=Cash Settlement, 98=Tax Settlement
  LEFT OUTER JOIN ins_tax I42 WITH(NOLOCK) ON(I42.ins_num=A17.ins_num AND I42.param_seq_num=A17.ins_para_seq_num AND I42.tax_seq_num=A17.ins_seq_num)
  LEFT OUTER JOIN tax_rate T14 WITH(NOLOCK) ON(T14.tax_rate_id=I42.tax_rate_id)
  LEFT OUTER JOIN cflow_type C03 WITH(NOLOCK) ON(C03.id_number=A17.pymt_type)
  LEFT OUTER JOIN event_source E03 WITH(NOLOCK) ON(E03.event_source_id=A17.event_source)
  INNER JOIN trans_status T08 WITH(NOLOCK) ON(T08.trans_status_id=A12.tran_status)
  INNER JOIN toolsets T05 WITH(NOLOCK) ON(T05.id_number=A12.toolset)
  INNER JOIN ab_tran at1 WITH(NOLOCK) ON(at1.tran_num=A12.template_tran_num AND at1.tran_status=15)
  INNER JOIN parameter P55 WITH(NOLOCK) ON(P55.ins_num=A12.ins_num AND P55.param_seq_num=0)
  INNER JOIN ins_parameter DUN WITH(NOLOCK) ON(DUN.ins_num=A12.ins_num AND DUN.param_seq_num=2)
  INNER JOIN param_reset_header REFS WITH(NOLOCK) ON(REFS.ins_num=A12.ins_num AND REFS.param_seq_num=1)
  INNER JOIN physcash P69 WITH(NOLOCK) ON(P69.ins_num=A12.ins_num)
  INNER JOIN currency C08 WITH(NOLOCK) ON(C08.id_number=P69.currency)
  INNER JOIN personnel P104 WITH(NOLOCK) ON(P104.id_number=A12.personnel_id)
  INNER JOIN instruments I05 WITH(NOLOCK) ON(I05.id_number=A12.ins_type)
  INNER JOIN idx_def I07 WITH(NOLOCK) ON(I07.index_id=P55.proj_index)
  INNER JOIN party P16 WITH(NOLOCK) ON(P16.party_id=A12.internal_bunit)
  INNER JOIN portfolio P06 WITH(NOLOCK) ON(P06.id_number=A12.internal_portfolio)
  INNER JOIN personnel P04 WITH(NOLOCK) ON(P04.id_number=A12.internal_contact)
  INNER JOIN party P116 WITH(NOLOCK) ON(P116.party_id=A12.external_bunit)
  INNER JOIN party_address P28 WITH(NOLOCK) ON (P28.party_id = P116.party_id)
  INNER JOIN country C05 WITH(NOLOCK) ON (C05.id_number = P28.country)
  INNER JOIN ins_option I40 WITH(NOLOCK) ON(I40.ins_num=A12.ins_num)
  INNER JOIN expiry_cut E02 WITH(NOLOCK) ON(E02.id_number=I40.expiry_cut)
  INNER JOIN denomination_unit DUN2 WITH(NOLOCK) ON(DUN2.unique_id=DUN.denomination_unit_id)
  INNER JOIN idx_unit VUNIT2 WITH(NOLOCK) ON(VUNIT2.unit_id=DUN.unit)
  INNER JOIN ref_source REFS1 WITH(NOLOCK) ON(REFS1.id_number=REFS.ref_source)
  LEFT OUTER JOIN portfolio P106 WITH(NOLOCK) ON(P106.id_number=A12.external_portfolio)
  LEFT OUTER JOIN party P216 WITH(NOLOCK) ON(P216.party_id=A12.broker_id)
  LEFT OUTER JOIN ab_tran_provisional atp WITH(NOLOCK) ON(atp.tran_num=A12.tran_num AND atp.prov_type=3)  --OTC
  LEFT OUTER JOIN (SELECT A12.ins_num, P54.rate AS [VALUE] FROM ab_tran A12 INNER JOIN profile P54 WITH(NOLOCK) ON(P54.ins_num=A12.ins_num) 
                   WHERE P54.param_seq_num=2) PRATE2 ON(PRATE2.ins_num=A12.ins_num) --Prec Rate
  LEFT OUTER JOIN (SELECT A12.ins_num, S26.name AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 WITH(NOLOCK) ON(P55.ins_num=A12.ins_num) INNER JOIN settle_type S26 WITH(NOLOCK) ON(S26.id_number=P55.settlement_type)
                   WHERE P55.param_seq_num=0) SETT0 ON(SETT0.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, O02.name AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 WITH(NOLOCK) ON(P55.ins_num=A12.ins_num) INNER JOIN option_type O02 WITH(NOLOCK) ON(O02.id_number=P55.option_type)
                   WHERE P55.param_seq_num=0) OPTT0 ON(OPTT0.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, P54.pymt_date AS [VALUE] FROM ab_tran A12 INNER JOIN profile P54 WITH(NOLOCK) ON(P54.ins_num=A12.ins_num) 
                   WHERE P54.param_seq_num=0) PPYMT0 ON(PPYMT0.ins_num=A12.ins_num) 
  LEFT OUTER JOIN (SELECT A12.ins_num, P54.notnl AS [VALUE] FROM ab_tran A12 INNER JOIN profile P54 WITH(NOLOCK) ON(P54.ins_num=A12.ins_num) 
                   WHERE P54.param_seq_num=0) PNOTN0 ON(PNOTN0.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, P54.notnl AS [VALUE] FROM ab_tran A12 INNER JOIN profile P54 WITH(NOLOCK) ON(P54.ins_num=A12.ins_num) 
                   WHERE P54.param_seq_num=1) PNOTN1 ON(PNOTN1.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, P54.notnl AS [VALUE] FROM ab_tran A12 INNER JOIN profile P54 WITH(NOLOCK) ON(P54.ins_num=A12.ins_num) 
                   WHERE P54.param_seq_num=2) PNOTN2 ON(PNOTN2.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 INNER JOIN ab_tran_info A15 WITH(NOLOCK) ON(A15.tran_num=A12.tran_num) INNER JOIN tran_info_types T01 WITH(NOLOCK) ON(T01.type_id=A15.type_id)
                   WHERE T01.type_name='Ext Deal Source') EDS ON(EDS.TRAN_NUM=A12.tran_num)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 INNER JOIN ab_tran_info A15 WITH(NOLOCK) ON(A15.tran_num=A12.tran_num) INNER JOIN tran_info_types T01 WITH(NOLOCK) ON(T01.type_id=A15.type_id)
                   WHERE T01.type_name='Ext Deal Source Ref') EDR ON(EDR.TRAN_NUM=A12.tran_num)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 INNER JOIN ab_tran_info A15 WITH(NOLOCK) ON(A15.tran_num=A12.tran_num) INNER JOIN tran_info_types T01 WITH(NOLOCK) ON(T01.type_id=A15.type_id)
                   WHERE T01.type_name='OTC Contract Code') OCC ON(OCC.TRAN_NUM=A12.tran_num)
WHERE (A12.trade_flag=1 AND A12.asset_type=2) -- Trading
  AND A12.tran_status IN(2,3)         -- 2=New; 3=Validated; 4=Matured; 5=Cancelled; 14=Deleted; 22=Closeout;
  AND A12.offset_tran_type IN(0,1)    -- 0=No Offset; 1=Original Offset
  AND I05.name IN ('ENGY-SO-P', 'ENGY-SO-R') -- ins_type
  AND A12.reference LIKE '$(DealRef)%%'
ORDER BY
  A12.reference, 
  E03.event_source_name --temp till LNG_CH tax config is correct