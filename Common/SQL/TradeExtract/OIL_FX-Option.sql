SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SELECT
  CONVERT(VARCHAR,A12.input_date,103) AS xinput_date,A12.deal_tracking_num AS xdeal_num,A12.ins_num AS xins_num,A12.tran_num AS xtran_num,P104.name AS xlast_updated_by,CONVERT(VARCHAR,A12.last_update,103) + ' ' + CONVERT(VARCHAR,A12.last_update,114) AS xlast_update, --0
  T08.name AS TranStatus, --A
  A12.reference AS Reference, --B
  T05.name AS Toolset, --C
  I05.name AS InstrumentType, --D
  CONVERT(VARCHAR,A12.trade_date,103) AS TradeDate, --E
  A112.reference AS Template, --F       
  P16.short_name AS InternalBusinessUnit, --G
  P06.name AS InternalPortfolio, --H
  P04.name AS Trader, --I
  P116.short_name AS ExternalBusinessUnit, --J
  P106.name AS ExternalPortfolio, --K
  CONVERT(VARCHAR,I29.beg_exercise_date,103) AS StartDate, --L
  CONVERT(VARCHAR,I29.end_exercise_date,103) AS ExpiryDate, --M
  CASE WHEN I29.expiry_cut = 0 THEN 'New York Cut 10am' ELSE 'Tokyo Cut 3pm' END AS ExpiryCut, --N
  CONVERT(VARCHAR,P54.pymt_date,103) AS SettleDate, --O
  CASE WHEN I29.buy_sell = 0 THEN 'Buy' ELSE 'Sell' END AS BuyOrSell, --P
  CASE I29.put_call WHEN NULL THEN 'Put' ELSE P61.name END AS PutOrCall, --Q
  CASE I29.strike WHEN NULL THEN 0 ELSE I29.strike END AS Strike, --R
  ABS(A12.position) AS BaseQuantity, --S
  CONVERT(VARCHAR,PC.cflow_date,103) AS PremiumDate, --T
  PC.cflow AS Premium, --U
  C08.name AS PremiumCurrency, --V
  PRE.value AS PremiumPercentage, --W
  PIP.value AS PIPSValue, --X
  TTR.value AS TimeTraded --Y
FROM ab_tran A12 WITH(NOLOCK)
  INNER JOIN ab_tran A112 ON (A112.tran_num = A12.template_tran_num)
  INNER JOIN toolsets T05 ON (T05.id_number = A12.toolset)
  INNER JOIN instruments I05 ON (I05.id_number = A12.ins_type)
  INNER JOIN trans_status T08 ON (T08.trans_status_id = A12.tran_status)
  INNER JOIN parameter P55 ON (P55.ins_num = A12.ins_num)
  INNER JOIN idx_def I07 ON (I07.index_id = P55.proj_index AND I07.db_status = 1)
  INNER JOIN profile P54 ON (P54.ins_num = A12.ins_num AND P54.param_seq_num = P55.param_seq_num)
  INNER JOIN ins_option I29 ON (I29.ins_num = A12.ins_num AND I29.param_seq_num = P55.param_seq_num  AND I29.option_seq_num = 0)
  INNER JOIN header H03 ON (H03.ins_num = A12.ins_num)
  INNER JOIN physcash PC ON (PC.ins_num = A12.ins_num AND PC.cflow_seq_num = 0)
  INNER JOIN party P16 ON (P16.party_id = A12.internal_bunit)
  INNER JOIN party P116 ON (P116.party_id = A12.external_bunit)
  INNER JOIN personnel P04 ON (P04.id_number = A12.internal_contact)
  INNER JOIN personnel P104 ON (P104.id_number = A12.personnel_id)
  INNER JOIN portfolio P06 ON (P06.id_number = A12.internal_portfolio)
  LEFT OUTER JOIN portfolio P106 ON (P106.id_number = A12.external_portfolio)
  INNER JOIN currency C08 ON (C08.id_number = PC.currency)
  INNER JOIN put_call P61 ON (P61.id_number = I29.put_call)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 LEFT OUTER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) LEFT OUTER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'PIPS Value') PIP ON (PIP.TRAN_NUM = A12.tran_num)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 LEFT OUTER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) LEFT OUTER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'Premium %') PRE ON (PRE.TRAN_NUM = A12.tran_num)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 LEFT OUTER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) LEFT OUTER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'Time Traded') TTR ON (TTR.TRAN_NUM = A12.tran_num)
WHERE (A12.trade_flag = 1 AND A12.asset_type = 2) -- Trading
  AND A12.tran_status IN(2,3)         -- 2=New; 3=Validated; 4=Matured; 5=Cancelled; 14=Deleted; 22=Closeout;
  AND A12.offset_tran_type IN(0,1)    -- 0=No Offset; 1=Original Offset
  AND T05.name = 'FX-Option'          -- Toolset
  AND A12.reference LIKE '$(DealRef)%%'
ORDER BY
  I05.name,           --ins_type
  A12.reference       --tran ref
