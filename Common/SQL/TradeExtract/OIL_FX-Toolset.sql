SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SELECT
  CONVERT(VARCHAR,A12.input_date,103) AS xinput_date,A12.deal_tracking_num AS xdeal_num,A12.ins_num AS xins_num,A12.tran_num AS xtran_num,P104.name AS xlast_updated_by,CONVERT(VARCHAR,A12.last_update,103) + ' ' + CONVERT(VARCHAR,A12.last_update,114) AS xlast_update, --0
  T05.name AS Toolset, --A
  I05.name AS InstrumentType, --B
  C03.name AS CashFlowType, --C
  T08.name AS TranStatus, --D
  A12.reference AS Reference, --E
  CONVERT(VARCHAR,A12.trade_date,103) AS TradeDate, --F
  H03.ticker AS Ticker,  --G
  P16.short_name AS InternalBusinessUnit, --H
  P06.name AS InternalPortfolio, --I
  P116.short_name AS ExternalBusinessUnit, --J
  P106.name AS ExternalPortfolio, --K
  P216.short_name AS ExecutionBroker, --L
  atp.reserved_amt AS BrokerFee, --M
  CCY1.name AS BaseCurrency, --N
  CCY2.name AS BoughtCurrency, --O
  CONVERT(VARCHAR,CAST(F08.spot_date as datetime),103) AS ValueDate, --P
  CASE A12.buy_sell WHEN 0 THEN 'Buy' ELSE 'Sell' END AS BuyOrSell, --Q
  F08.d_amt AS DealtAmount, --R
  F08.c_amt AS CounterAmount, --S
  F08.rate AS SpotRate, --T
  F08.dealt AS DealtRate, --U
  CONVERT(VARCHAR,A12.start_date,103) AS FwdWindowStartDate, -- V
  P04.name AS Trader, --W
  EDS.VALUE AS ExternalRefSource, --X
  EDR.VALUE AS ExternalRef, --Y
  TTR.VALUE AS TimeTraded, --Z
  TRM.VALUE AS TransactionMethod, --AA
  EOR.VALUE AS ExternalOrderReference, --AB
  DUM.VALUE AS DummyRate --AC
FROM ab_tran A12 WITH(NOLOCK)
  INNER JOIN toolsets T05 ON (T05.id_number = A12.toolset)
  INNER JOIN instruments I05 ON (I05.id_number = A12.ins_type AND A12.ins_type = 26001)
  INNER JOIN cflow_type C03 ON (C03.id_number = A12.cflow_type AND A12.cflow_type IN(36,13))
  INNER JOIN trans_status T08 ON (T08.trans_status_id = A12.tran_status)
  INNER JOIN party P16 ON (P16.party_id = A12.internal_bunit)
  INNER JOIN header H03 ON (H03.ins_num = A12.ins_num)
  INNER JOIN fx_tran_aux_data F08 ON (F08.tran_num = A12.tran_num)
  INNER JOIN currency CCY1 ON (CCY1.id_number = F08.ccy1)
  INNER JOIN currency CCY2 ON (CCY2.id_number = F08.ccy2)
  INNER JOIN party P116 ON (P116.party_id = A12.external_bunit)
  INNER JOIN portfolio P06 ON (P06.id_number = A12.internal_portfolio) 
  LEFT OUTER JOIN portfolio P106 ON (P106.id_number = A12.external_portfolio)
  INNER JOIN personnel P04 ON (P04.id_number = A12.internal_contact)
  INNER JOIN personnel P104 ON (P104.id_number = A12.personnel_id)
  LEFT OUTER JOIN party P216 ON (P216.party_id = A12.broker_id)
  LEFT OUTER JOIN ab_tran_provisional atp ON (atp.tran_num = A12.tran_num AND atp.prov_type = 3)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 LEFT OUTER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) LEFT OUTER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'Ext Deal Source') EDS ON (EDS.TRAN_NUM = A12.tran_num)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 LEFT OUTER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) LEFT OUTER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'Ext Deal Source Ref') EDR ON (EDR.TRAN_NUM = A12.tran_num)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 LEFT OUTER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) LEFT OUTER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'Time Traded') TTR ON (TTR.TRAN_NUM = A12.tran_num)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 LEFT OUTER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) LEFT OUTER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'Transaction Method') TRM ON (TRM.TRAN_NUM = A12.tran_num)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 LEFT OUTER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) LEFT OUTER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'Ext Order Ref') EOR ON (EOR.TRAN_NUM = A12.tran_num)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 LEFT OUTER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) LEFT OUTER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'Dummy Rate') DUM ON (DUM.TRAN_NUM = A12.tran_num)
WHERE (A12.trade_flag = 1 AND A12.asset_type = 2) -- Trading
  AND A12.tran_status IN(2,3,4)       -- 2=New; 3=Validated; 4=Matured; 5=Cancelled; 14=Deleted; 22=Closeout;
  AND A12.offset_tran_type IN(0,1)    -- 0=No Offset; 1=Original Offset
  AND I05.name = 'FX'                 -- ins_type
  AND A12.reference LIKE '$(DealRef)%%'
ORDER BY
  C03.name,           --CashFlowType
  A12.reference       --tran ref