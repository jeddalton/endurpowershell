SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SELECT 
  CONVERT(VARCHAR,A12.input_date,103) AS xinput_date,A12.deal_tracking_num AS xdeal_num,A12.ins_num AS xins_num,A12.tran_num AS xtran_num,P104.name AS xlast_updated_by,CONVERT(VARCHAR,A12.last_update,103) + ' ' + CONVERT(VARCHAR,A12.last_update,114) AS xlast_update, --0
  T08.name AS TranStatus, --A0
  A12.reference AS Reference, --B0
  T05.name AS Toolset, --C0
  I05.name AS InstrumentType, --D0
  at1.reference AS Template, --E0
  P16.short_name AS InternalBusinessUnit, --F0
  P06.name AS InternalPortfolio, --G0
  P04.name AS Trader, --H0
  P116.short_name AS ExternalBusinessUnit, --I0
  P106.name AS ExternalPortfolio, --J0
  CAST(A12.trade_date AS DATE) AS TradeDate, --K0
  CASE A12.buy_sell WHEN 0 THEN 'Buy' ELSE 'Sell' END AS BuyOrSell, --L0
  FD.fee_short_name AS FeeType, --M0
  CASE WHEN FD.fee_short_name IS NOT NULL THEN IFP.param_seq_num  ELSE 0 END AS Leg, --N0
  CASE WHEN FD.fee_short_name IS NOT NULL THEN IFP.fee_seq_num ELSE 0 END AS Seq, --O0
  CASE WHEN FD.fee_short_name IS NOT NULL THEN CASE IFP.pay_rec WHEN 0 THEN 'Receive' ELSE 'Pay' END 
                                          ELSE NULL END AS PayRec, --P0
  IFP.pymt_period AS PayFreq, --Q0
  CAST(IFP.one_time_pymt_dt AS DATE) AS OneTimePymtDate, --R0
  IFP.fee AS Fee, --S0
  IFP.max_fee AS MaxFee, --T0
  CASE IFP.withdraw_inject WHEN 0 THEN '0'      WHEN 1 THEN 'Gross Inject' WHEN 3 THEN 'Net Withdraw' WHEN 4 THEN 'Net Inject' 
                           ELSE CONVERT(VARCHAR,IFP.withdraw_inject) END AS WithdrawInject, --U0
  C08.name AS FeeCurrency, --V0
  CASE IFP.fee_calc_type WHEN 1 THEN 'Flat' WHEN 2 THEN 'Volumetric' END AS VolCalcType, --W0
  IFP.volume AS CalcVolume, --X0
  I03.unit_label AS PriceUnit, --Y0
  I103.unit_label AS FeeUnit, --Z0
  CASE IFP.calc_period WHEN 0 THEN 'One-Time' WHEN 1 THEN 'Hourly' WHEN 2 THEN 'Daily' WHEN 3 THEN 'Weekly' 
                       WHEN 4 THEN 'Monthly' WHEN 5 THEN 'Quarterly' WHEN 6 THEN 'Yearly' END AS CalcPeriod, --AA0
  IFP.pymt_date_offset AS PymtDateOffset, --AB0
  CAST(IFP.start_date AS DATE) AS FeeStartDate, --AC0
  CAST(IFP.end_date AS DATE) AS FeeEndDate, --AD0
  [EDS].[VALUE] AS ExternalRefSource, --AE0
  [EDR].[VALUE] AS ExternalRef, --AF0
  [COM].[VALUE] AS Comment --AG0
FROM ab_tran A12 WITH(NOLOCK)
  INNER JOIN trans_status T08 ON (T08.trans_status_id = A12.tran_status)
  INNER JOIN toolsets T05 ON (T05.id_number = A12.toolset)
  INNER JOIN ab_tran at1 ON (at1.tran_num = A12.template_tran_num AND at1.tran_status = 15)
  INNER JOIN parameter P55 ON (P55.ins_num = A12.ins_num AND P55.param_seq_num = 0)
  INNER JOIN personnel P104 ON (P104.id_number = A12.personnel_id)
  INNER JOIN instruments I05 ON (I05.id_number = A12.ins_type)
  INNER JOIN party P16 ON (P16.party_id = A12.internal_bunit)
  INNER JOIN portfolio P06 ON (P06.id_number = A12.internal_portfolio)
  INNER JOIN personnel P04 ON (P04.id_number = A12.internal_contact)
  INNER JOIN party P116 ON (P116.party_id = A12.external_bunit)
  INNER JOIN ins_fee_param IFP ON (IFP.ins_num = A12.ins_num) --AND IFP.param_seq_num = 0)
  INNER JOIN fee_def FD ON(FD.fee_def_id=IFP.fee_def_id)
  INNER JOIN currency C08 ON (C08.id_number = IFP.currency)
  INNER JOIN idx_unit I03 ON (I03.unit_id = IFP.price_unit)
  INNER JOIN idx_unit I103 ON (I103.unit_id=IFP.unit)
  LEFT OUTER JOIN portfolio P106 ON (P106.id_number = A12.external_portfolio)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 INNER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) INNER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'Ext Deal Source') EDS ON (EDS.TRAN_NUM = A12.tran_num)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 INNER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) INNER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'Ext Deal Source Ref') EDR ON (EDR.TRAN_NUM = A12.tran_num)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 INNER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) INNER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'Comment') COM ON (COM.TRAN_NUM = A12.tran_num)
WHERE (A12.trade_flag = 1 AND A12.asset_type = 2) -- Trading
  AND A12.tran_status IN(2,3)         -- 2=New; 3=Validated; 4=Matured; 5=Cancelled; 14=Deleted; 22=Closeout;
  AND A12.offset_tran_type IN(0,1)    -- 0=No Offset; 1=Original Offset
  AND I05.name = 'COMM-FEE' -- ins_type
  AND A12.reference LIKE '$(DealRef)%%'
ORDER BY
  A12.reference, IFP.param_seq_num, IFP.fee_seq_num