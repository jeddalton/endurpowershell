SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SELECT 
  CONVERT(VARCHAR,A12.input_date,103) AS xinput_date,A12.deal_tracking_num AS xdeal_num,A12.ins_num AS xins_num,A12.tran_num AS xtran_num,P104.name AS xlast_updated_by,CONVERT(VARCHAR,A12.last_update,103) + ' ' + CONVERT(VARCHAR,A12.last_update,114) AS xlast_update, --0
  T08.name AS TranStatus,
  A12.reference AS Reference,
  T05.name AS Toolset,
  I05.name AS InstrumentType,
  at1.reference AS Template,
  P16.short_name AS InternalBusinessUnit,
  P06.name AS InternalPortfolio,
  P04.name AS Trader,
  P116.short_name AS ExternalBusinessUnit,
  P106.name AS ExternalPortfolio,
  P216.short_name AS ExecutionBroker,
  atp.reserved_amt AS BrokerFee,
  CAST(A12.trade_date AS DATE) AS TradeDate,
  CAST(A12.start_date AS DATE) AS StartDate,
  CAST(A12.maturity_date AS DATE) AS EndDate,
  [ZONE].[VALUE] AS Zone,
  [LOC].[VALUE] AS Location,
  CASE A12.buy_sell WHEN 0 THEN 'Buy' ELSE 'Sell' END AS BuyOrSell,
  CASE WHEN [DUN].[VALUE] = 'Pence' THEN A12.price * 100 ELSE A12.price END AS RateSpread,
  [CUR].[VALUE] AS Currency,
  [DUN].[VALUE] AS DenominationUnit,
  CASE WHEN A12.buy_sell = 1 THEN A12.position * -1 ELSE A12.position END AS Volume,
  [VOLU].[VALUE] AS Unit,
  [PRUphys].[VALUE] AS PriceUnitPhys,
  [PRUfin].[VALUE] AS PriceUnitFin,
  [DVT].[VALUE] AS DealVolumeType,
  [PDOphys].[VALUE] AS PymtOffsetPhys,
  [PDOfin].[VALUE] AS PymtOffsetFin,
  [CCT].[VALUE] AS CapacityContractType,
  [CVP].[VALUE] AS [ContractValue],
  S20.service_type_name AS ServiceType,
  [EDS].[VALUE] AS ExternalRefSource,
  [EDR].[VALUE] AS ExternalRef,
  [CVM].[VALUE] AS CapacityValuationMode,
  [CPR].[VALUE] AS CapacityPrice,
  --[COM].[VALUE] AS TIComment,
  [LNM].[VALUE] AS LineName,
  [PRO].[VALUE] AS [Product],
  FD.fee_short_name AS FeeType,
  CASE WHEN FD.fee_short_name IS NOT NULL THEN IFP.param_seq_num  ELSE 0 END AS Leg,
  CASE WHEN FD.fee_short_name IS NOT NULL THEN IFP.fee_seq_num ELSE 0 END AS Seq,
  CASE WHEN FD.fee_short_name IS NOT NULL THEN CASE IFP.pay_rec WHEN 0 THEN 'Receive' ELSE 'Pay' END 
                                          ELSE NULL END AS PayRec,
  IFP.pymt_period AS PayFreq,
  CAST(IFP.one_time_pymt_dt AS DATE) AS OneTimePymtDate,
  IFP.fee AS Fee,
  IFP.max_fee AS MaxFee,
  CASE IFP.withdraw_inject WHEN 0 THEN NULL             WHEN 1 THEN 'Gross Inject' WHEN 3 THEN 'Net Withdraw' WHEN 4 THEN 'Net Inject' 
                           ELSE CONVERT(VARCHAR,IFP.withdraw_inject) END AS WithdrawInject,
  C08.name AS FeeCurrency,
  CASE IFP.fee_calc_type WHEN 1 THEN 'Flat' WHEN 2 THEN 'Volumetric' END AS VolCalcType,
  IFP.volume AS CalcVolume,
  I03.unit_label AS PriceUnit,
  I103.unit_label AS FeeUnit,
  CASE IFP.calc_period WHEN 0 THEN 'One-Time' WHEN 1 THEN 'Hourly' WHEN 2 THEN 'Daily' WHEN 3 THEN 'Weekly' 
                       WHEN 4 THEN 'Monthly' WHEN 5 THEN 'Quarterly' WHEN 6 THEN 'Yearly' END AS CalcPeriod,
  IFP.pymt_date_offset AS PymtDateOffset,
  CAST(IFP.start_date AS DATE) AS FeeStartDate,
  CAST(IFP.end_date AS DATE) AS FeeEndDate
FROM ab_tran A12 WITH(NOLOCK)
  INNER JOIN trans_status T08 ON (T08.trans_status_id = A12.tran_status)
  INNER JOIN toolsets T05 ON (T05.id_number = A12.toolset)
  INNER JOIN ab_tran at1 ON (at1.tran_num = A12.template_tran_num AND at1.tran_status = 15)
  INNER JOIN parameter P55 ON (P55.ins_num = A12.ins_num AND P55.param_seq_num = 0)
  INNER JOIN personnel P104 ON (P104.id_number = A12.personnel_id)
  INNER JOIN instruments I05 ON (I05.id_number = A12.ins_type)
  INNER JOIN party P16 ON (P16.party_id = A12.internal_bunit)
  INNER JOIN portfolio P06 ON (P06.id_number = A12.internal_portfolio)
  INNER JOIN personnel P04 ON (P04.id_number = A12.internal_contact)
  INNER JOIN party P116 ON (P116.party_id = A12.external_bunit)
  LEFT OUTER JOIN party P216 ON (P216.party_id = A12.broker_id)
  LEFT OUTER JOIN ab_tran_provisional atp ON (atp.tran_num = A12.tran_num AND atp.prov_type = 3)  --OTC
  LEFT OUTER JOIN phys_header PH ON (PH.ins_num = A12.ins_num)
  LEFT OUTER JOIN service_type S20 ON (S20.service_type_id = PH.service_type)
  LEFT OUTER JOIN portfolio P106 ON (P106.id_number = A12.external_portfolio)
  INNER JOIN ins_price_complex IPC ON(IPC.id_number = P55.fx_flt)
  LEFT OUTER JOIN ins_fee_param IFP ON (IFP.ins_num = A12.ins_num) --AND IFP.param_seq_num = 0)
  LEFT OUTER JOIN fee_def FD ON(FD.fee_def_id=IFP.fee_def_id)
  LEFT OUTER JOIN currency C08 ON (C08.id_number = IFP.currency)
  LEFT OUTER JOIN idx_unit I03 ON (I03.unit_id = IFP.price_unit)
  LEFT OUTER JOIN idx_unit I103 ON (I103.unit_id=IFP.unit)
  LEFT OUTER JOIN (SELECT A12.ins_num, G08.zone_name AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   INNER JOIN gas_phys_param_view G13 ON (G13.ins_num=A12.ins_num AND G13.param_seq_num=P55.param_seq_num)
                   INNER JOIN gas_phys_zones G08 ON (G08.zone_id = G13.zone_id) WHERE P55.param_seq_num=1) ZONE ON (ZONE.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, G06.location_name AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   INNER JOIN gas_phys_param_view G13 ON (G13.ins_num=A12.ins_num AND G13.param_seq_num=P55.param_seq_num)
                   INNER JOIN gas_phys_location G06 ON (G06.location_id=G13.location_id) WHERE P55.param_seq_num=1) LOC ON (LOC.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, C108.name AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   INNER JOIN currency C108 ON (C108.id_number = P55.currency) WHERE P55.param_seq_num=2) CUR ON (CUR.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, DU.denomination_unit_name AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON(P55.ins_num=A12.ins_num)
                   INNER JOIN ins_parameter I35 ON(I35.ins_num=A12.ins_num AND I35.param_seq_num=P55.param_seq_num)
                   INNER JOIN denomination_unit DU ON (DU.unique_id = I35.denomination_unit_id) WHERE P55.param_seq_num=2) DUN ON (DUN.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, I203.unit_label AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   INNER JOIN idx_unit I203 ON (I203.unit_id = P55.price_unit) WHERE P55.param_seq_num=1) PRUphys ON (PRUphys.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, I203.unit_label AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   INNER JOIN idx_unit I203 ON (I203.unit_id = P55.price_unit) WHERE P55.param_seq_num=2) PRUfin ON (PRUfin.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, I103.unit_label AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   INNER JOIN idx_unit I103 ON (I103.unit_id=P55.unit) WHERE P55.param_seq_num=2) VOLU ON (VOLU.ins_num=A12.ins_num) 
  LEFT OUTER JOIN (SELECT A12.ins_num, D15.deal_volume_type_name AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   INNER JOIN ins_parameter I35 ON (I35.ins_num=A12.ins_num AND I35.param_seq_num=P55.param_seq_num)
                   INNER JOIN deal_volume_type D15 ON (D15.deal_volume_type_id=I35.deal_volume_type) WHERE P55.param_seq_num=1) DVT ON (DVT.ins_num=A12.ins_num) 
  LEFT OUTER JOIN (SELECT A12.ins_num, P55.pymt_date_offset AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   WHERE P55.param_seq_num=1) PDOphys ON (PDOphys.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, P55.pymt_date_offset AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   WHERE P55.param_seq_num=2) PDOfin ON (PDOfin.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 INNER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) INNER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'Ext Deal Source') EDS ON (EDS.TRAN_NUM = A12.tran_num)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 INNER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) INNER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'Ext Deal Source Ref') EDR ON (EDR.TRAN_NUM = A12.tran_num)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 INNER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) INNER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'Contract Value / Price') CVP ON (CVP.TRAN_NUM = A12.tran_num)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 INNER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) INNER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'Capacity Contract Type') CCT ON (CCT.TRAN_NUM = A12.tran_num)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 INNER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) INNER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'Capacity Valuation Mode') CVM ON (CVM.TRAN_NUM = A12.tran_num)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 INNER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) INNER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'Capacity Price') CPR ON (CPR.TRAN_NUM = A12.tran_num)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 INNER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) INNER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'Comment') COM ON (COM.TRAN_NUM = A12.tran_num)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 INNER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) INNER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'Line Name') LNM ON (LNM.TRAN_NUM = A12.tran_num)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 INNER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) INNER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'Product') PRO ON (PRO.TRAN_NUM = A12.tran_num)
WHERE (A12.trade_flag = 1 AND A12.asset_type = 2) -- Trading
  AND A12.tran_status IN(2,3)         -- 2=New; 3=Validated; 4=Matured; 5=Cancelled; 14=Deleted; 22=Closeout;
  AND A12.offset_tran_type IN(0,1)    -- 0=No Offset; 1=Original Offset
  AND I05.name IN('COMM-CAP-ENTRY', 'COMM-CAP-EXIT') -- ins_type
  AND A12.reference LIKE '$(DealRef)%%'
  --AND CAST(A12.start_date AS DATE) >= '01-Sep-2012'
ORDER BY
  A12.reference, IFP.param_seq_num, IFP.fee_seq_num
