SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SELECT DISTINCT
  CONVERT(VARCHAR,A12.input_date,103) AS xinput_date,A12.deal_tracking_num AS xdeal_num,A12.ins_num AS xins_num,A12.tran_num AS xtran_num,P104.name AS xlast_updated_by,CONVERT(VARCHAR,A12.last_update,103) + ' ' + CONVERT(VARCHAR,A12.last_update,114) AS xlast_update, --0
  T08.name AS TranStatus, --A0
  A12.reference AS Reference, --B0
  T05.name AS Toolset, --C0
  I05.name AS InstrumentType, --D0
  at1.reference AS Template, --E0
  P16.short_name AS InternalBusinessUnit, --F0
  P06.name AS InternalPortfolio, --G0
  P04.name AS Trader, --H0
  P116.short_name AS ExternalBusinessUnit, --I0
  P106.name AS ExternalPortfolio, --J0
  P216.short_name AS ExecutionBroker, --K0
  atp.reserved_amt AS BrokerFee, --L0
  CAST(A12.trade_date AS DATE) AS TradeDate, --M0
  CAST(A12.start_date AS DATE) AS StartDate, --N0
  CAST(A12.maturity_date AS DATE) AS EndDate, --O0
  CASE WHEN H03.base_ins_type = 48510 THEN 'Call'
       WHEN H03.base_ins_type = 48515 THEN 'Put'
       WHEN H03.base_ins_type = 48530 THEN 'Call'
       WHEN H03.base_ins_type = 48535 THEN 'Put'    END AS PutOrCall, --P0
  CASE WHEN A12.buy_sell = 0 THEN 'Buy' ELSE 'Sell' END AS BuyOrSell, --Q0
  CASE WHEN [DUN].[VALUE] = 'Pence' THEN A12.price * 100 ELSE A12.price END AS Strike, --R0
  [DUN].[VALUE] AS DenominationUnit, --S1
  --CAST(FS1.notify_date+2 AS DATE) AS ExerciseDate, --T1
  [NOTNL].[VALUE] AS Volume, --U1
  [VOLU1].[VALUE] AS VolumeUnit, --V1
  CASE WHEN A12.buy_sell = 0 THEN [PCASH].[VALUE] * -1 ELSE [PCASH].[VALUE] END AS Premium, --WX2
  [PPT].[VALUE] AS PremiumPaymentType, --Y0
  [EDS].[VALUE] AS ExternalRefSource, --AA0
  [EDR].[VALUE] AS ExternalRef, --AB0
  [ZONE].[VALUE] AS Zone, --XX1
  [LOC].[VALUE] AS Location --XX1
FROM ab_tran A12 WITH(NOLOCK)
  INNER JOIN trans_status T08 ON (T08.trans_status_id = A12.tran_status)
  INNER JOIN toolsets T05 ON (T05.id_number = A12.toolset)
  INNER JOIN ab_tran at1 ON (at1.tran_num = A12.template_tran_num AND at1.tran_status = 15)
  INNER JOIN parameter P55 ON (P55.ins_num = A12.ins_num AND P55.param_seq_num = 0)
  INNER JOIN personnel P104 ON (P104.id_number = A12.personnel_id)
  INNER JOIN instruments I05 ON (I05.id_number = A12.ins_type)
  INNER JOIN party P16 ON (P16.party_id = A12.internal_bunit)
  INNER JOIN portfolio P06 ON (P06.id_number = A12.internal_portfolio)
  INNER JOIN personnel P04 ON (P04.id_number = A12.internal_contact)
  INNER JOIN party P116 ON (P116.party_id = A12.external_bunit)
  LEFT OUTER JOIN portfolio P106 ON (P106.id_number = A12.external_portfolio)
  LEFT OUTER JOIN party P216 ON (P216.party_id = A12.broker_id)
  LEFT OUTER JOIN ab_tran_provisional atp ON (atp.tran_num = A12.tran_num AND atp.prov_type = 3)  --OTC
  --INNER JOIN fom_swing FS1 ON (FS1.ins_num = A12.ins_num)
  INNER JOIN header H03 ON (H03.ins_num = A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, DU.denomination_unit_name AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON(P55.ins_num=A12.ins_num)
                   INNER JOIN ins_parameter I35 ON(I35.ins_num=A12.ins_num AND I35.param_seq_num=P55.param_seq_num)
                   INNER JOIN denomination_unit DU ON (DU.unique_id = I35.denomination_unit_id) WHERE P55.param_seq_num=1) DUN ON (DUN.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, I103.unit_label AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   INNER JOIN idx_unit I103 ON (I103.unit_id=P55.unit) WHERE P55.param_seq_num=1) VOLU1 ON (VOLU1.ins_num=A12.ins_num) 
  LEFT OUTER JOIN (SELECT A12.ins_num, P55.notnl AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   WHERE P55.param_seq_num=1) NOTNL ON (NOTNL.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, PC.cflow AS [VALUE] FROM ab_tran A12 INNER JOIN physcash PC ON (PC.ins_num=A12.ins_num) 
                   WHERE PC.param_seq_num=2) PCASH ON (PCASH.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, G08.zone_name AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   INNER JOIN gas_phys_param_view G13 ON (G13.ins_num=A12.ins_num AND G13.param_seq_num=P55.param_seq_num)
                   INNER JOIN gas_phys_zones G08 ON (G08.zone_id = G13.zone_id) WHERE P55.param_seq_num=1) ZONE ON (ZONE.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, G06.location_name AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   INNER JOIN gas_phys_param_view G13 ON (G13.ins_num=A12.ins_num AND G13.param_seq_num=P55.param_seq_num)
                   INNER JOIN gas_phys_location G06 ON (G06.location_id=G13.location_id) WHERE P55.param_seq_num=1) LOC ON (LOC.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 INNER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) INNER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'Premium Payment Type') PPT ON (PPT.TRAN_NUM = A12.tran_num)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 INNER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) INNER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'Ext Deal Source') EDS ON (EDS.TRAN_NUM = A12.tran_num)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 INNER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) INNER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'Ext Deal Source Ref') EDR ON (EDR.TRAN_NUM = A12.tran_num)
WHERE (A12.trade_flag = 1 AND A12.asset_type = 2) -- Trading
  AND A12.tran_status IN(2,3)         -- 2=New; 3=Validated; 4=Matured; 5=Cancelled; 14=Deleted; 22=Closeout;
  AND A12.offset_tran_type IN(0,1)    -- 0=No Offset; 1=Original Offset
  AND I05.name IN ('CO-CALL-D', 'CO-PUT-D', 'CO-CALL-M', 'CO-PUT-M') -- ins_type
  AND A12.reference LIKE '$(DealRef)%%'
ORDER BY
  I05.name,           --ins_type
  A12.reference       --tran ref
