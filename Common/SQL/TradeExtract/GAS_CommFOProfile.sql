SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SELECT
  CONVERT(VARCHAR,A12.input_date,103) AS xinput_date,A12.deal_tracking_num AS xdeal_num,A12.ins_num AS xins_num,A12.tran_num AS xtran_num,P204.name AS xlast_updated_by, CONVERT(VARCHAR,A12.last_update,103) + ' ' + CONVERT(VARCHAR,A12.last_update,114) AS xlast_update,
  T08.name AS TranStatus,  --A0
  A12.reference AS Reference, --B0
  T05.name AS Toolset, --C0
  I05.name AS InstrumentType, --D0
  at1.reference AS Template, --E0
  P16.short_name AS InternalBusinessUnit, --F0
  P06.name AS InternalPortfolio, --G0
  P04.name AS Trader, --H0
  P116.short_name AS ExternalBusinessUnit, --I0
  P106.name AS ExternalPortfolio, --J0
  CAST(A12.trade_date AS DATE) AS TradeDate, --K0
  CAST(A12.start_date AS DATE) AS StartDate, --L0
  CAST(A12.maturity_date AS DATE) AS EndDate, --M0
  [PIPE1].[VALUE] AS Pipeline1, --N1
  [ZONE1].[VALUE] AS Zone1, --O1
  [LOC1].[VALUE] AS Location1, --P1
  [PIPE2].[VALUE] AS Pipeline2, --Q3
  [ZONE2].[VALUE] AS Zone2, --R3
  [LOC2].[VALUE] AS Location2, --S3
  CASE A12.buy_sell WHEN 0 THEN 'Buy' ELSE 'Sell' END AS BuyOrSell, --T0
  [RATE1].[VALUE] AS RateSpread1, --U2
  [CUR1].[VALUE] AS Currency1, --V2
  [PUphy1].[VALUE] AS PriceUnit1Phys, --W1
  [PUfin1].[VALUE] AS PriceUnit1Fin, --X2
  [VOL1].VALUE AS Volume1, --Y1
  [VUphy1].[VALUE] AS Unit1Phys, --Z1
  [VUfin1].[VALUE] AS Unit1Fin, --AA2
  [DVT1].[VALUE] AS DealVolumeType1, --AB1
  [RATE2].[VALUE] AS RateSpread2, --AC4
  [CUR2].[VALUE] AS Currency2, --AD4
  [PUphy2].[VALUE] AS PriceUnit2Phys, --AE3
  [PUfin2].[VALUE] AS PriceUnit2Fin, --AF4
  [VOL2].VALUE AS Volume2, --AG3
  [VUphy2].[VALUE] AS Unit2Phys, --AH3
  [VUfin2].[VALUE] AS Unit2Fin, --AI4
  [DVT2].[VALUE] AS DealVolumeType2, --AJ3
  [EDR].[VALUE] AS ExternalRef, --AK0
  [PAL].[VALUE] AS ProfileAllowed --AL0
FROM ab_tran A12 WITH(NOLOCK)
  INNER JOIN trans_status T08 ON (T08.trans_status_id = A12.tran_status)
  INNER JOIN toolsets T05 ON (T05.id_number = A12.toolset)
  INNER JOIN instruments I05 ON (I05.id_number = A12.ins_type)
  INNER JOIN ab_tran at1 ON (at1.tran_num = A12.template_tran_num AND at1.tran_status = 15)
  INNER JOIN party P16 ON (P16.party_id = A12.internal_bunit)
  INNER JOIN portfolio P06 ON (P06.id_number = A12.internal_portfolio)
  INNER JOIN personnel P04 ON (P04.id_number = A12.internal_contact)
  LEFT OUTER JOIN party P116 ON (P116.party_id = A12.external_bunit)
  LEFT OUTER JOIN portfolio P106 ON (P106.id_number = A12.external_portfolio)
  LEFT OUTER JOIN personnel P204 ON (P204.id_number = A12.personnel_id)
  INNER JOIN parameter P55 ON (P55.ins_num = A12.ins_num AND P55.param_seq_num = 0)
  LEFT OUTER JOIN (SELECT A12.ins_num, G03.pipeline_name AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   INNER JOIN gas_phys_param_view G13 ON (G13.ins_num=A12.ins_num AND G13.param_seq_num=P55.param_seq_num)
                   INNER JOIN gas_phys_pipelines G03 ON (G03.pipeline_id = G13.pipeline_id) WHERE P55.param_seq_num=1) PIPE1 ON (PIPE1.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, G08.zone_name AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   INNER JOIN gas_phys_param_view G13 ON (G13.ins_num=A12.ins_num AND G13.param_seq_num=P55.param_seq_num)
                   INNER JOIN gas_phys_zones G08 ON (G08.zone_id = G13.zone_id) WHERE P55.param_seq_num=1) ZONE1 ON (ZONE1.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, G06.location_name AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   INNER JOIN gas_phys_param_view G13 ON (G13.ins_num=A12.ins_num AND G13.param_seq_num=P55.param_seq_num)
                   INNER JOIN gas_phys_location G06 ON (G06.location_id=G13.location_id) WHERE P55.param_seq_num=1) LOC1 ON (LOC1.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, G03.pipeline_name AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   INNER JOIN gas_phys_param_view G13 ON (G13.ins_num=A12.ins_num AND G13.param_seq_num=P55.param_seq_num)
                   INNER JOIN gas_phys_pipelines G03 ON (G03.pipeline_id = G13.pipeline_id) WHERE P55.param_seq_num=3) PIPE2 ON (PIPE2.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, G08.zone_name AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   INNER JOIN gas_phys_param_view G13 ON (G13.ins_num=A12.ins_num AND G13.param_seq_num=P55.param_seq_num)
                   INNER JOIN gas_phys_zones G08 ON (G08.zone_id = G13.zone_id) WHERE P55.param_seq_num=3) ZONE2 ON (ZONE2.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, G06.location_name AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   INNER JOIN gas_phys_param_view G13 ON (G13.ins_num=A12.ins_num AND G13.param_seq_num=P55.param_seq_num)
                   INNER JOIN gas_phys_location G06 ON (G06.location_id=G13.location_id) WHERE P55.param_seq_num=3) LOC2 ON (LOC2.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, P55.rate AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   WHERE P55.param_seq_num=2) RATE1 ON (RATE1.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, C108.name AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   INNER JOIN currency C108 ON (C108.id_number = P55.currency) WHERE P55.param_seq_num=2) CUR1 ON (CUR1.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, I203.unit_label AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   INNER JOIN idx_unit I203 ON (I203.unit_id = P55.price_unit) WHERE P55.param_seq_num=1) PUphy1 ON (PUphy1.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, I203.unit_label AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   INNER JOIN idx_unit I203 ON (I203.unit_id = P55.price_unit) WHERE P55.param_seq_num=2) PUfin1 ON (PUfin1.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, P55.notnl AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   WHERE P55.param_seq_num=1) VOL1 ON (VOL1.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, I103.unit_label AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   INNER JOIN idx_unit I103 ON (I103.unit_id=P55.unit) WHERE P55.param_seq_num=1) VUphy1 ON (VUphy1.ins_num=A12.ins_num) 
  LEFT OUTER JOIN (SELECT A12.ins_num, I103.unit_label AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   INNER JOIN idx_unit I103 ON (I103.unit_id=P55.unit) WHERE P55.param_seq_num=2) VUfin1 ON (VUfin1.ins_num=A12.ins_num) 
  LEFT OUTER JOIN (SELECT A12.ins_num, D15.deal_volume_type_name AS [VALUE] FROM ab_tran A12 INNER JOIN ins_parameter I35 ON (I35.ins_num=A12.ins_num) 
                   INNER JOIN deal_volume_type D15 ON (D15.deal_volume_type_id=I35.deal_volume_type) WHERE I35.param_seq_num=1) DVT1 ON (DVT1.ins_num=A12.ins_num) 
  LEFT OUTER JOIN (SELECT A12.ins_num, P55.rate AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   WHERE P55.param_seq_num=4) RATE2 ON (RATE2.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, C108.name AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   INNER JOIN currency C108 ON (C108.id_number = P55.currency) WHERE P55.param_seq_num=4) CUR2 ON (CUR2.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, I203.unit_label AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   INNER JOIN idx_unit I203 ON (I203.unit_id = P55.price_unit) WHERE P55.param_seq_num=3) PUphy2 ON (PUphy2.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, I203.unit_label AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   INNER JOIN idx_unit I203 ON (I203.unit_id = P55.price_unit) WHERE P55.param_seq_num=4) PUfin2 ON (PUfin2.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, P55.notnl AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   WHERE P55.param_seq_num=3) VOL2 ON (VOL2.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, I103.unit_label AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   INNER JOIN idx_unit I103 ON (I103.unit_id=P55.unit) WHERE P55.param_seq_num=3) VUphy2 ON (VUphy2.ins_num=A12.ins_num) 
  LEFT OUTER JOIN (SELECT A12.ins_num, I103.unit_label AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   INNER JOIN idx_unit I103 ON (I103.unit_id=P55.unit) WHERE P55.param_seq_num=4) VUfin2 ON (VUfin2.ins_num=A12.ins_num) 
  LEFT OUTER JOIN (SELECT A12.ins_num, D15.deal_volume_type_name AS [VALUE] FROM ab_tran A12 INNER JOIN ins_parameter I35 ON (I35.ins_num=A12.ins_num) 
                   INNER JOIN deal_volume_type D15 ON (D15.deal_volume_type_id=I35.deal_volume_type) WHERE I35.param_seq_num=3) DVT2 ON (DVT2.ins_num=A12.ins_num) 
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 INNER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) INNER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'Profile Allowed') PAL ON (PAL.TRAN_NUM = A12.tran_num) 
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 INNER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) INNER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'Ext Deal Source Ref') EDR ON (EDR.TRAN_NUM = A12.tran_num)
WHERE A12.tran_status IN (2,3)       -- 2=New; 3=Validated; 4=Matured; 5=Cancelled; 14=Deleted; 22=Closeout;
  AND I05.name = 'COMM-FO-PROFILE'         -- ins_type
  AND A12.tran_type = 0              -- 0=Trading
  AND A12.offset_tran_type IN(0,1)   -- 0=No Offset; 1=Original Offset
  AND A12.reference LIKE '$(DealRef)%%'
ORDER BY
  I05.name,           --ins_type
  A12.reference       --tran ref