SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SELECT DISTINCT
  CONVERT(VARCHAR,A12.input_date,103) AS xinput_date,A12.deal_tracking_num AS xdeal_num,A12.ins_num AS xins_num,A12.tran_num AS xtran_num,P104.name AS xlast_updated_by,CONVERT(VARCHAR,A12.last_update,103) + ' ' + CONVERT(VARCHAR,A12.last_update,114) AS xlast_update, --0
  T08.name AS TranStatus, --A0
  A12.reference AS Reference, --B0
  T05.name AS Toolset, --C0
  I05.name AS InstrumentType, --D0
  at1.reference AS Template, --E0
  P16.short_name AS InternalBusinessUnit, --F0
  P06.name AS InternalPortfolio, --G0
  P04.name AS Trader, --H0
  P116.short_name AS ExternalBusinessUnit, --I0
  P106.name AS ExternalPortfolio, --J0
  P216.short_name AS ExecutionBroker, --K0
  atp.reserved_amt AS BrokerFee, --L0
  CAST(A12.trade_date AS DATE) AS TradeDate, --M0
  CAST(A12.start_date AS DATE) AS StartDate, --N0
  CAST(A12.maturity_date AS DATE) AS EndDate, --O0
  CASE [IOPC0].[VALUE] WHEN 0 THEN 'Put' WHEN 1 THEN 'Call' END AS PutOrCall_1, --P0
  CASE [IOBS0].[VALUE] WHEN 0 THEN 'Buy' WHEN 1 THEN 'Sell' END AS BuyOrSell_1, --Q0
  [IOST0].[VALUE] AS Rate_1, --R0
  [NOTN0].[VALUE] AS Volume_1, --S0
  CASE WHEN A12.buy_sell = 0 THEN [PCAMT0].[VALUE] * -1 ELSE [PCAMT0].[VALUE] END AS Premium_1, --T0
  CASE [IOPC1].[VALUE] WHEN 0 THEN 'Put' WHEN 1 THEN 'Call' END AS PutOrCall_2, --U1
  CASE [IOBS1].[VALUE] WHEN 0 THEN 'Buy' WHEN 1 THEN 'Sell' END AS BuyOrSell_2, --V1
  [IOST1].[VALUE] AS Rate_2, --W1
  [NOTN1].[VALUE] AS Volume_2, --X1
  CASE WHEN A12.buy_sell = 0 THEN [PCAMT1].[VALUE] * -1 ELSE [PCAMT1].[VALUE] END AS Premium_2,  --Y1
  [PPT].[VALUE] AS PremiumPaymentType, --Z0
  [EDS].[VALUE] AS ExternalRefSource, --AA0
  [EDR].[VALUE] AS ExternalRef --AB0
FROM ab_tran A12 WITH(NOLOCK)
  INNER JOIN trans_status T08 ON (T08.trans_status_id = A12.tran_status)
  INNER JOIN toolsets T05 ON (T05.id_number = A12.toolset)
  INNER JOIN ab_tran at1 ON (at1.tran_num = A12.template_tran_num AND at1.tran_status = 15)
  INNER JOIN parameter P55 ON (P55.ins_num = A12.ins_num AND P55.param_seq_num = 0)
  INNER JOIN personnel P104 ON (P104.id_number = A12.personnel_id)
  INNER JOIN instruments I05 ON (I05.id_number = A12.ins_type)
  INNER JOIN party P16 ON (P16.party_id = A12.internal_bunit)
  INNER JOIN portfolio P06 ON (P06.id_number = A12.internal_portfolio)
  INNER JOIN personnel P04 ON (P04.id_number = A12.internal_contact)
  INNER JOIN party P116 ON (P116.party_id = A12.external_bunit)
  LEFT OUTER JOIN portfolio P106 ON (P106.id_number = A12.external_portfolio)
  LEFT OUTER JOIN party P216 ON (P216.party_id = A12.broker_id)
  LEFT OUTER JOIN ab_tran_provisional atp ON (atp.tran_num = A12.tran_num AND atp.prov_type = 3)  --OTC
  INNER JOIN header H03 ON (H03.ins_num = A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, IO.put_call AS [VALUE] FROM ab_tran A12 INNER JOIN ins_option IO ON (IO.ins_num=A12.ins_num) 
                   WHERE option_seq_num=0 AND IO.param_seq_num=0) IOPC0 ON (IOPC0.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, IO.put_call AS [VALUE] FROM ab_tran A12 INNER JOIN ins_option IO ON (IO.ins_num=A12.ins_num) 
                   WHERE option_seq_num=0 AND IO.param_seq_num=1) IOPC1 ON (IOPC1.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, IO.buy_sell AS [VALUE] FROM ab_tran A12 INNER JOIN ins_option IO ON (IO.ins_num=A12.ins_num) 
                   WHERE option_seq_num=0 AND IO.param_seq_num=0) IOBS0 ON (IOBS0.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, IO.buy_sell AS [VALUE] FROM ab_tran A12 INNER JOIN ins_option IO ON (IO.ins_num=A12.ins_num) 
                   WHERE option_seq_num=0 AND IO.param_seq_num=1) IOBS1 ON (IOBS1.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, IO.strike AS [VALUE] FROM ab_tran A12 INNER JOIN ins_option IO ON (IO.ins_num=A12.ins_num) 
                   WHERE option_seq_num=0 AND IO.param_seq_num=0) IOST0 ON (IOST0.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, IO.strike AS [VALUE] FROM ab_tran A12 INNER JOIN ins_option IO ON (IO.ins_num=A12.ins_num) 
                   WHERE option_seq_num=0 AND IO.param_seq_num=1) IOST1 ON (IOST1.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, PC.cflow AS [VALUE] FROM ab_tran A12 INNER JOIN physcash PC ON (PC.ins_num=A12.ins_num AND PC.cflow_type=4) 
                   WHERE PC.param_seq_num=0) PCAMT0 ON (PCAMT0.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, PC.cflow AS [VALUE] FROM ab_tran A12 INNER JOIN physcash PC ON (PC.ins_num=A12.ins_num AND PC.cflow_type=4) 
                   WHERE PC.param_seq_num=1) PCAMT1 ON (PCAMT1.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, P55.notnl AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   WHERE P55.param_seq_num=0) NOTN0 ON (NOTN0.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, P55.notnl AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   WHERE P55.param_seq_num=1) NOTN1 ON (NOTN1.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 INNER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) INNER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'Premium Payment Type') PPT ON (PPT.TRAN_NUM = A12.tran_num)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 INNER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) INNER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'Ext Deal Source') EDS ON (EDS.TRAN_NUM = A12.tran_num)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 INNER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) INNER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'Ext Deal Source Ref') EDR ON (EDR.TRAN_NUM = A12.tran_num)
WHERE (A12.trade_flag = 1 AND A12.asset_type = 2) -- Trading
  AND A12.tran_status IN(2,3)         -- 2=New; 3=Validated; 4=Matured; 5=Cancelled; 14=Deleted; 22=Closeout;
  AND A12.offset_tran_type IN(0,1)    -- 0=No Offset; 1=Original Offset
  AND I05.name IN ('EO-CALL', 'EO-COLLAR', 'EO-PUT') -- ins_type
  AND A12.reference LIKE '$(DealRef)%%'
ORDER BY
  A12.reference
