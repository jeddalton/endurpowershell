SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SELECT
  CONVERT(VARCHAR,A12.input_date,103) AS xinput_date,A12.deal_tracking_num AS xdeal_num,A12.ins_num AS xins_num,A12.tran_num AS xtran_num,P204.name AS xlast_updated_by, CONVERT(VARCHAR,A12.last_update,103) + ' ' + CONVERT(VARCHAR,A12.last_update,114) AS xlast_update,
  T08.name AS TranStatus,  --A0
  A12.reference AS Reference, --B0
  T05.name AS Toolset, --C0
  I05.name AS InstrumentType, --D0
  at1.reference AS Template, --E0
  P16.short_name AS InternalBusinessUnit, --F0
  P06.name AS InternalPortfolio, --G0
  P04.name AS Trader, --H0
  P116.short_name AS ExternalBusinessUnit, --I0
  P106.name AS ExternalPortfolio, --J0
  P104.name AS ExternalContact, --K0
  P216.short_name AS ExecutionBroker, --L0
  atp.reserved_amt AS BrokerFee, --M0
  CAST(A12.trade_date AS DATE) AS TradeDate, --N0
  CAST(A12.start_date AS DATE) AS StartDate, --O0
  CAST(A12.maturity_date AS DATE) AS EndDate, --P0
  [ZONE].[VALUE] AS Zone, --Q1
  [LOC].[VALUE] AS Location, --R1
  CASE A12.buy_sell WHEN 0 THEN 'Buy' ELSE 'Sell' END AS BuyOrSell, --S0
  CASE WHEN [DUN].[VALUE] = 'Pence' THEN A12.price * 100 ELSE A12.price END AS Price, --T0
  CASE WHEN [DUN].[VALUE] = 'Pence' THEN [FSP].[VALUE] * 100 ELSE [FSP].[VALUE] END AS RateSpread, --T0
  [CUR].[VALUE] AS Currency, --U2
  [DUN].[VALUE] AS DenominationUnit, --V2
  [PRU].[VALUE] AS PriceUnit, --W2
  CASE WHEN A12.buy_sell = 1 THEN A12.position * -1 ELSE A12.position END AS Volume, --X0
  [VOLU1].[VALUE] AS Unit, --Y1
  [VOLU2].[VALUE] AS Unit2, --Z2
  [DVT].[VALUE] AS DealVolumeType, --AA1
  [EDS].[VALUE] AS ExternalRefSource, --AB0
  [EDR].[VALUE] AS ExternalRef, --AC0
  [SPR].[VALUE] AS Spread, --AD0
  [TRM].[VALUE] AS TransactionMethod, --AE0
  [SLV].[VALUE] AS Sleeve, --AF0
  [IoA].[VALUE] AS [InitiatorOrAggressor], --AG0
  [FOF].[VALUE] AS [Fix-Float], --AH0
  [PAL].[VALUE] AS [ProfileAllowed], --AH0
  --[VP].[VALUE] AS [Volume_Precion] --AI0
  [REFS].[VALUE] AS [RefSource], --AJ2
  [IXLOC].[VALUE] AS [IndexLocation], --AK2
  FD.fee_short_name AS FeeType, --AL0
  CASE WHEN FD.fee_short_name IS NOT NULL THEN IFP.param_seq_num  ELSE 0 END AS Leg, --AM0
  CASE WHEN FD.fee_short_name IS NOT NULL THEN IFP.fee_seq_num ELSE 0 END AS Seq, --AN0
  CASE WHEN FD.fee_short_name IS NOT NULL THEN CASE IFP.pay_rec WHEN 0 THEN 'Receive' ELSE 'Pay' END 
                                          ELSE NULL END AS PayRec, --AO0
  IFP.pymt_period AS PayFreq, --AP0
  CAST(IFP.one_time_pymt_dt AS DATE) AS OneTimePymtDate, --AQ0
  IFP.fee AS Fee, --AR0
  IFP.max_fee AS MaxFee, --AS0
  CASE IFP.withdraw_inject WHEN 0 THEN NULL             WHEN 1 THEN 'Gross Inject' WHEN 3 THEN 'Net Withdraw' WHEN 4 THEN 'Net Inject' 
                           ELSE CONVERT(VARCHAR,IFP.withdraw_inject) END AS WithdrawInject, --AT0
  C08.name AS FeeCurrency, --AU0
  CASE IFP.fee_calc_type WHEN 1 THEN 'Flat' WHEN 2 THEN 'Volumetric' END AS VolCalcType, --AV0
  IFP.volume AS CalcVolume, --AW0
  I03.unit_label AS FeePriceUnit, --AX0
  I103.unit_label AS FeeUnit, --AY0
  CASE IFP.calc_period WHEN 0 THEN 'One-Time' WHEN 1 THEN 'Hourly' WHEN 2 THEN 'Daily' WHEN 3 THEN 'Weekly' 
                       WHEN 4 THEN 'Monthly' WHEN 5 THEN 'Quarterly' WHEN 6 THEN 'Yearly' END AS CalcPeriod, --AZ0
  IFP.pymt_date_offset AS PymtDateOffset, --BA0
  CAST(IFP.start_date AS DATE) AS FeeStartDate, --BB0
  CAST(IFP.end_date AS DATE) AS FeeEndDate --BC0
FROM ab_tran A12 WITH(NOLOCK)
  INNER JOIN trans_status T08 ON (T08.trans_status_id = A12.tran_status)
  INNER JOIN toolsets T05 ON (T05.id_number = A12.toolset)
  INNER JOIN instruments I05 ON (I05.id_number = A12.ins_type)
  INNER JOIN ab_tran at1 ON (at1.tran_num = A12.template_tran_num AND at1.tran_status = 15)
  INNER JOIN party P16 ON (P16.party_id = A12.internal_bunit)
  INNER JOIN portfolio P06 ON (P06.id_number = A12.internal_portfolio)
  INNER JOIN personnel P04 ON (P04.id_number = A12.internal_contact)
  LEFT OUTER JOIN party P116 ON (P116.party_id = A12.external_bunit)
  LEFT OUTER JOIN portfolio P106 ON (P106.id_number = A12.external_portfolio)
  LEFT OUTER JOIN personnel P104 ON (P104.id_number = A12.external_contact)
  LEFT OUTER JOIN personnel P204 ON (P204.id_number = A12.personnel_id)
  LEFT OUTER JOIN party P216 ON (P216.party_id = A12.broker_id)
  LEFT OUTER JOIN ab_tran_provisional atp ON (atp.tran_num = A12.tran_num AND atp.prov_type = 3)  --OTC
  INNER JOIN parameter P55 ON (P55.ins_num = A12.ins_num AND P55.param_seq_num = 0)
  LEFT OUTER JOIN ins_fee_param IFP ON (IFP.ins_num = A12.ins_num) --AND IFP.param_seq_num = 0)
  LEFT OUTER JOIN fee_def FD ON(FD.fee_def_id=IFP.fee_def_id)
  LEFT OUTER JOIN currency C08 ON (C08.id_number = IFP.currency)
  LEFT OUTER JOIN idx_unit I03 ON (I03.unit_id = IFP.price_unit)
  LEFT OUTER JOIN idx_unit I103 ON (I103.unit_id=IFP.unit)
  --INNER JOIN (SELECT A12.ins_num, G13.volume_precision AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
  --                 INNER JOIN gas_phys_param_view G13 ON (G13.ins_num=A12.ins_num AND G13.param_seq_num=P55.param_seq_num)
  --                 WHERE P55.param_seq_num=1) VP ON (VP.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, G08.zone_name AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   INNER JOIN gas_phys_param_view G13 ON (G13.ins_num=A12.ins_num AND G13.param_seq_num=P55.param_seq_num)
                   INNER JOIN gas_phys_zones G08 ON (G08.zone_id = G13.zone_id) WHERE P55.param_seq_num=1) ZONE ON (ZONE.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, G06.location_name AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   INNER JOIN gas_phys_param_view G13 ON (G13.ins_num=A12.ins_num AND G13.param_seq_num=P55.param_seq_num)
                   INNER JOIN gas_phys_location G06 ON (G06.location_id=G13.location_id) WHERE P55.param_seq_num=1) LOC ON (LOC.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, C108.name AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   INNER JOIN currency C108 ON (C108.id_number = P55.currency) WHERE P55.param_seq_num=2) CUR ON (CUR.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, P55.float_spd AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   WHERE P55.param_seq_num=2) FSP ON (FSP.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, DU.denomination_unit_name AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON(P55.ins_num=A12.ins_num)
                   INNER JOIN ins_parameter I35 ON(I35.ins_num=A12.ins_num AND I35.param_seq_num=P55.param_seq_num)
                   INNER JOIN denomination_unit DU ON (DU.unique_id = I35.denomination_unit_id) WHERE P55.param_seq_num=2) DUN ON (DUN.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, I203.unit_label AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   INNER JOIN idx_unit I203 ON (I203.unit_id = P55.price_unit) WHERE P55.param_seq_num=2) PRU ON (PRU.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, I103.unit_label AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   INNER JOIN idx_unit I103 ON (I103.unit_id=P55.unit) WHERE P55.param_seq_num=1) VOLU1 ON (VOLU1.ins_num=A12.ins_num) 
  LEFT OUTER JOIN (SELECT A12.ins_num, I103.unit_label AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   INNER JOIN idx_unit I103 ON (I103.unit_id=P55.unit) WHERE P55.param_seq_num=2) VOLU2 ON (VOLU2.ins_num=A12.ins_num) 
  LEFT OUTER JOIN (SELECT A12.ins_num, D15.deal_volume_type_name AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num) 
                   INNER JOIN ins_parameter I35 ON (I35.ins_num=A12.ins_num AND I35.param_seq_num=P55.param_seq_num)
                   INNER JOIN deal_volume_type D15 ON (D15.deal_volume_type_id=I35.deal_volume_type) WHERE P55.param_seq_num=1) DVT ON (DVT.ins_num=A12.ins_num) 
  LEFT OUTER JOIN (SELECT A12.ins_num, RS.name AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num)
                   LEFT OUTER JOIN param_reset_header P63 ON (P63.ins_num = A12.ins_num AND P63.param_seq_num = P55.param_seq_num AND P63.param_reset_header_seq_num=0)
                   LEFT OUTER JOIN ref_source RS ON (RS.id_number = P63.ref_source) WHERE P55.param_seq_num=2) REFS ON (REFS.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.ins_num, IL.index_loc_name AS [VALUE] FROM ab_tran A12 INNER JOIN parameter P55 ON (P55.ins_num=A12.ins_num)
                   LEFT OUTER JOIN index_location IL ON (IL.index_loc_id = P55.index_loc_id) WHERE P55.param_seq_num=2) IXLOC ON (IXLOC.ins_num=A12.ins_num)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 INNER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) INNER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'Ext Deal Source') EDS ON (EDS.TRAN_NUM = A12.tran_num)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 INNER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) INNER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'Ext Deal Source Ref') EDR ON (EDR.TRAN_NUM = A12.tran_num)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 INNER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) INNER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'Spread') SPR ON (SPR.TRAN_NUM = A12.tran_num)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 INNER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) INNER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'Transaction Method') TRM ON (TRM.TRAN_NUM = A12.tran_num)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 INNER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) INNER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'Sleeve') SLV ON (SLV.TRAN_NUM = A12.tran_num) 
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 INNER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) INNER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'Initiator/Aggressor') IoA ON (IoA.TRAN_NUM = A12.tran_num)
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 INNER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) INNER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'Fix-Float') FOF ON (FOF.TRAN_NUM = A12.tran_num) 
  LEFT OUTER JOIN (SELECT A12.tran_num, A15.value FROM ab_tran A12 INNER JOIN ab_tran_info A15 ON (A15.tran_num = A12.tran_num) INNER JOIN tran_info_types T01 ON (T01.type_id = A15.type_id)
                   WHERE T01.type_name = 'ProfileAllowed') PAL ON (PAL.TRAN_NUM = A12.tran_num) 
WHERE A12.tran_status IN (2,3)       -- 2=New; 3=Validated; 4=Matured; 5=Cancelled; 14=Deleted; 22=Closeout;
  AND I05.name = 'COMM-PHYS'         -- ins_type
  AND A12.tran_type = 0              -- 0=Trading
  AND A12.offset_tran_type IN(0,1)   -- 0=No Offset; 1=Original Offset
  AND [FOF].[VALUE] = 'Fix'
  AND A12.reference LIKE '$(DealRef)%%'
ORDER BY
  A12.reference, IFP.param_seq_num, IFP.fee_seq_num
