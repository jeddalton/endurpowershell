.(Join-Path $StartupScriptFolder GlobalVars.ps1)
if(!$TestAssetBaseDir){
	$Global:TestAssetBaseDir		="C:\GGTP\Test Assets\"
}
$Global:Environment					="END_BAUQA1"
$Global:EndurServerName				="ENDVSQL12LON\ENDUR_BAU"
$Global:EndurDatabaseName			="END_DB_BAUQA1"
$Global:ReportingServerName			="ENDVSQL12LON\ENDUR_BAU"
$Global:ReportingDatabaseName		="END_DB_BAUQA1_REPORTING"
$Global:EndurConfigFile				="\\endfsdv01\endur_bau\END_BAUQA1\Resources\ENDUR_END_BAUQA1.cfg"
$Global:RabbitString				="edevrmq02dv:26003"
$Global:ZemaRabbitQueue				="Zema_BAUQA1"
$Global:ZemaExchange				="BAUQA1_TESTEXCH"
$Global:EndurAppServer1				="endapp01bq01"
$Global:EndurAppServer2				="endapp02bq01"
$Global:PacManServerName			="PACSQC11UT\PACMAN_UAT"
$Global:PacManDatabaseName			="END_DB_BAUQA1_PACMAN"
$Global:PacmanSSASServer            ="PACSQC12UT\PACMAN_SSAS_UAT"
$Global:PacmanSSASDataBase          ="END_DB_BAUQA1_PACMAN_SSAS"
$Global:PacManDBConnString			="Data Source=ENDTST07DV\SQL;Initial Catalog=END_DB_BAUQA1_PACMAN;Provider=SQLNCLI10.1;Integrated Security=SSPI;"
$Global:PacManDBConnShort			="Data Source=ENDTST07DV\SQL;Initial Catalog=END_DB_BAUQA1_PACMAN;Integrated Security=SSPI"
$Global:DealLoaderAppServer			="ENDAPP02BQ01"

#Added to support TradeValidatorValidate.ps1 and to future TradeVolumeUpload.ps1 and TradeVolumeDownload.ps1
$Global:TradeValidatorEnvURLShort	="https://$endurappserver2:9013/tradevalidator"
$Global:TradeVolumeEnvUrlShort		="https://$endurappserver2:9011/tradevolume"
$Global:TradeValidatorEnvUrl		="https://$endurappserver2.gazpromuk.intra:9013/tradevalidator"
$Global:TradeVolumeEnvUrl			="https://$endurappserver2.gazpromuk.intra:9011/tradevolume"
$Global:ServiceUser					="SVC-GGTP-ENDUR-DEV@Gazpromuk.intra"

# Builds all env Rabbit Variables
.(Join-Path $StartupScriptFolder RabbitVariables.ps1)