 . (Join-Path $StartupScriptFolder GlobalVars.ps1) 
$Global:Environment           ="ENDUR_DV_50"
$Global:EndurServerName       ="ENDSQC54DV\END_DEV"
$Global:EndurDatabaseName     ="END_DB_DV50"
$Global:ReportingServerName   ="ENDSQC54DV\END_DEV"
$Global:ReportingDatabaseName ="END_DB_DV50_REPORTING"
$Global:EndurConfigFile       ="\\fasnod02dv\endur14_dev\Endur_DV_50\Resources\Endur.cfg"
$Global:RabbitString          ="edevrmq02dv:26003"
$Global:ZemaRabbitQueue       ="ZEMA_CORETEST1"
$Global:ZemaExchange          ="CORETEST1_MARKET"
$Global:EndurAppServer1       ="ENDAPP11DV"
$Global:EndurAppServer2       ="ENDAPP12DV"
$Global:EndurWebAPIServer     ="endapp13dv"
$Global:EndurCdxServer        ="ENDAPP14DV"
$Global:EndurEventNotifServer ="ENDAPP14DV"
$Global:PacManDatabaseName    ="END_DB_DV50_PACMAN"
$Global:PacManServerName      ="PACSQS01DV\PACMAN_DEV"
$Global:TradeValidatorEnvURL  ="http://ENDAPP11DV.gazpromuk.intra:9013/TradeValidator"
$Global:ServiceUser           ="GAZPROMUK\SVC-ENDSVC-DV"
$Global:TradeVolumeEnvURL     ="http://ENDAPP11DV:9011/TradeVolume"