Import-Module SQLPS


$StartupScriptFolder = "C:\GGTP\Test AssetsV14\Common\EnvVariables"
 .(Join-Path $StartupScriptFolder GlobalVars.ps1) 

if(!$TestAssetBaseDir){
	$Global:TestAssetBaseDir		="C:\GGTP\Test AssetsV14\"
}
$Global:Environment                 ="Endur_QA_02"
$Global:EndurServerName             ="ENDSQC52DV\END_TEST"
$Global:EndurDatabaseName           ="END_DB_QA02"
$Global:ReportingServerName         =$EndurServerName
$Global:ReportingDatabaseName       =$Global:EndurDatabaseName + "_REPORTING"
$Global:FXReportingServerName       =$EndurServerName
$Global:FXReportingDatabaseName     =$Global:EndurDatabaseName + "_FXREPORTING"
$Global:FxReportingSSISPackage      ="\FxReporting_QA2/FxReporting"
$Global:FxReportingDBConnString     ="Data Source=$EndurServerName;Initial Catalog=$FXReportingDatabaseName;Provider=SQLNCLI10.1;Integrated Security=SSPI;"
$Global:FxReportingDBConnShort      ="Data Source=$EndurServerName;Initial Catalog=$FXReportingDatabaseName;Integrated Security=SSPI"
$Global:EndurConfigFile             ="\\fasnod02dv\endur14_tst\Endur_QA_02\Resources\Endur.cfg"
$Global:RabbitString                ="edevrmq02dv:26003"
$Global:ZemaRabbitQueue             ="Zema_ENDQA02"
$Global:ZemaExchange                ="ENDQA02_MARKET"
$Global:EndurAppServer1             ="endapp52qa"
$Global:EndurAppServer2             ="endapp72qa"
$Global:PacManServerName            ="PACSQC11UT\PACMAN_UAT"
$Global:PacManDatabaseName          =$Global:EndurDatabaseName + "_PACMAN"
$Global:PacmanSSASServer            ="PACSQC12UT\PACMAN_SSAS_UAT"
$Global:PacmanSSASDataBase          =$Global:EndurDatabaseName + "_PACMAN_SSAS"
$Global:PacManDBConnString          ="Data Source=$PacManServerName;Initial Catalog=$PacManDatabaseName;Provider=SQLNCLI10.1;Integrated Security=SSPI;"
$Global:PacManDBConnShort           ="Data Source=$PacManServerName;Initial Catalog=$PacManDatabaseName;Integrated Security=SSPI"
$Global:DealLoaderAppServer         =$EndurAppServer2
$Global:SSISServerInstance          ="SSISQS01DV\SSIS_DEV2"
$Global:SSISDatabaseName            ="SSISRunner_DV"

#Added to support TradeValidatorValidate.ps1 and to future TradeVolumeUpload.ps1 and TradeVolumeDownload.ps1
$Global:TradeValidatorEnvURLShort   ="http://$endurappserver1:9013/tradevalidator"
$Global:TradeUploaderEnvUrlShort    ="http://$endurappserver1:9012/tradeuploader"
$Global:TradeVolumeEnvUrlShort      ="http://$endurappserver1:9011/tradevolume"

$Global:TradeValidatorEnvUrl        ="http://$endurappserver1.gazpromuk.intra:9013/tradevalidator"
$Global:TradeUplaoderEnvUrl         ="http://$endurappserver1.gazpromuk.intra:9012/tradeuploader"
$Global:TradeVolumeEnvUrl           ="http://$endurappserver1.gazpromuk.intra:9011/tradevolume"
$Global:ServiceUser                 ="GAZPROMUK\SVC-ENDSVC-QA"

$Global:EndurDBServer = $EndurServerName
$Global:EndurDB = $EndurDatabaseName

# Builds all env Rabbit Variables
.(Join-Path $StartupScriptFolder RabbitVariables.ps1)

$Global:Timeout = 360
