Import-Module SQLPS


$StartupScriptFolder = "C:\GGTP\Test AssetsV14\Common\EnvVariables"
 .(Join-Path $StartupScriptFolder GlobalVars.ps1) 

if(!$TestAssetBaseDir){
	$Global:TestAssetBaseDir		="C:\GGTP\Test AssetsV14\"
}
$Global:Environment                 ="Endur_DV_51"
$Global:EndurServerName             ="ENDSQC54DV\END_DEV"
$Global:EndurDatabaseName           ="END_DB_DV51"
$Global:ReportingServerName         =$EndurServerName
$Global:ReportingDatabaseName       =$Global:EndurDatabaseName + "_REPORTING"





$Global:EndurConfigFile             ="\\fasnod02dv\endur14_dev\Endur_DV_51\Resources\Endur.cfg"
$Global:RabbitString                ="edevrmq02dv:26003"


$Global:EndurAppServer1             ="ENDAPP05DV"
$Global:EndurAppServer2             ="ENDAPP06DV"
$Global:PacManServerName            ="PACSQS01DV\PACMAN_DEV"
$Global:PacManDatabaseName          =$Global:EndurDatabaseName + "_PACMAN"
$Global:PacmanSSASServer            ="PACSQS01DV\PACMAN_DEV"
$Global:PacmanSSASDataBase          =$Global:EndurDatabaseName + "_PACMAN_SSAS"
$Global:PacManDBConnString          ="Data Source=$PacManServerName;Initial Catalog=$PacManDatabaseName;Provider=SQLNCLI10.1;Integrated Security=SSPI;"
$Global:PacManDBConnShort           ="Data Source=$PacManServerName;Initial Catalog=$PacManDatabaseName;Integrated Security=SSPI"
$Global:DealLoaderAppServer         =$EndurAppServer2
$Global:SSISServerInstance          ="SSISQS01DV\SSIS_DEV2"
$Global:SSISDatabaseName            ="SSISRunner_DV"

#Added to support TradeValidatorValidate.ps1 and to future TradeVolumeUpload.ps1 and TradeVolumeDownload.ps1
$Global:TradeValidatorEnvURLShort   ="http://$endurappserver1:9013/tradevalidator"
$Global:TradeUploaderEnvUrlShort    ="http://$endurappserver1:9012/tradeuploader"
$Global:TradeVolumeEnvUrlShort      ="http://$endurappserver1:9011/tradevolume"

$Global:TradeValidatorEnvUrl        ="http://$endurappserver1.gazpromuk.intra:9013/tradevalidator"
$Global:TradeUplaoderEnvUrl         ="http://$endurappserver1.gazpromuk.intra:9012/tradeuploader"
$Global:TradeVolumeEnvUrl           ="http://$endurappserver1.gazpromuk.intra:9011/tradevolume"
$Global:ServiceUser					="GAZPROMUK\SVC-ENDSVC-DV"

$Global:EndurDBServer = $EndurServerName
$Global:EndurDB = $EndurDatabaseName

# Builds all env Rabbit Variables
.(Join-Path $StartupScriptFolder RabbitVariables.ps1)

$Global:Timeout = 360