 .(Join-Path $StartupScriptFolder GlobalVars.ps1) 
if(!$TestAssetBaseDir){
	$Global:TestAssetBaseDir		="C:\GGTP\Test Assets\"
}
$Global:Environment             ="END_CORETEST2"
$Global:EndurServerName         ="ENDVSQL10LON\ENDUR_DEV"
$Global:EndurDatabaseName       ="END_DB_CORETEST2"
$Global:ReportingServerName     ="ENDVSQL10LON\ENDUR_DEV"
$Global:ReportingDatabaseName   ="END_DB_CORETEST2_REPORTING"
$Global:FXReportingServerName   ="ENDVSQL10LON\ENDUR_DEV"
$Global:FXReportingDatabaseName ="END_DB_CORETEST2_FXREPORTING"
$Global:FxReportingSSISPackage  ="\FxReporting_CORETEST2/FxReporting"
$Global:FxReportingDBConnString ="Data Source=ENDVSQL10LON\ENDUR_DEV;Initial Catalog=END_DB_CORETEST2_FXREPORTING;Provider=SQLNCLI10.1;Integrated Security=SSPI;"
$Global:FxReportingDBConnShort  ="Data Source=ENDVSQL10LON\ENDUR_DEV;Initial Catalog=END_DB_CORETEST2_FXREPORTING;Integrated Security=SSPI"
$Global:EndurConfigFile         ="\\endfsdv01\endur_dev\END_CORETEST2\Resources\ENDUR_END_CORETEST2.cfg"
$Global:RabbitString            ="edevrmq02dv:26003"
$Global:ZemaRabbitQueue         ="Zema_CORETEST2"
$Global:ZemaExchange            ="CORETEST2_MARKET"
$Global:EndurAppServer1         ="edevct2ap1lonuk"
$Global:EndurAppServer2         ="edevct2ap2lonuk"
$Global:PacManServerName        ="PACSQS01DV\PACMAN_DEV"
$Global:PacManDatabaseName      ="END_DB_CORETEST2_PACMAN"
$Global:PacManDBConnString      ="Data Source=PACSQS01DV\PACMAN_DEV;Initial Catalog=END_DB_CORETEST2_PACMAN;Provider=SQLNCLI10.1;Integrated Security=SSPI;"
$Global:PacManDBConnShort       ="Data Source=PACSQS01DV\PACMAN_DEV;Initial Catalog=END_DB_CORETEST2_PACMAN;Integrated Security=SSPI"
$Global:DealLoaderAppServer     ="edevct2ap2lonuk"

#Added to support TradeValidatorValidate.ps1 and to future TradeVolumeUpload.ps1 and TradeVolumeDownload.ps1
$Global:TradeValidatorEnvURLShort	="https://$endurappserver2:9013/tradevalidator"
$Global:TradeVolumeEnvUrlShort		="https://$endurappserver2:9011/tradevolume"
$Global:TradeValidatorEnvUrl		="https://$endurappserver2.gazpromuk.intra:9013/tradevalidator"
$Global:TradeVolumeEnvUrl			="https://$endurappserver2.gazpromuk.intra:9011/tradevolume"
$Global:ServiceUser					="SVC-GGTP-ENDUR-DEV@Gazpromuk.intra"

# Builds all env Rabbit Variables
.(Join-Path $StartupScriptFolder RabbitVariables.ps1)