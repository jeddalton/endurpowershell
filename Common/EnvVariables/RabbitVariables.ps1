﻿$EnvIdentity								   = $Environment

$Global:RabbitExchangeSTSIncoming              ="Endur.STS.Trade.Incoming_" + $EnvIdentity
$Global:RabbitQueueSTSIncoming                 ="Endur.STS.Trade.Incoming_" + $EnvIdentity
$Global:RabbitQueueSTSIncomingLog              ="Endur.STS.Trade.Incoming_" + $EnvIdentity + "_Log"

$Global:RabbitExchangeSTSOutgoing              ="Endur.STS.Trade.Outgoing_" + $EnvIdentity
$Global:RabbitQueueSTSOutgoing                 ="Endur.STS.Trade.Outgoing_" + $EnvIdentity
$Global:RabbitQueueSTSOutgoingLog              ="Endur.STS.Trade.Outgoing_" + $EnvIdentity + "_Log"

$Global:RabbitExchangeConfirmEvent	           ="EndurConfirmEventSts_" + $EnvIdentity
$Global:RabbitQueueConfirmEvent		           ="EndurConfirmEventSts_" + $EnvIdentity
$Global:RabbitQueueConfirmEventLog	           ="EndurConfirmEventSts_" + $EnvIdentity + "_Log"

$Global:RabbitExchangeTradeEvent	           ="EndurTradeEvent_" + $EnvIdentity
$Global:RabbitQueueTradeEvent		           ="EndurTradeEvent_" + $EnvIdentity
$Global:RabbitQueueTradeEventLog	           ="EndurTradeEvent_" + $EnvIdentity + "_Log"

$Global:RabbitExchangeSTSTimeSeriesIncoming    ="Endur.STS.TimeSeries.Incoming_" + $EnvIdentity
$Global:RabbitQueueSTSTimeSeriesIncoming       ="Endur.STS.TimeSeries.Incoming_" + $EnvIdentity

$Global:RabbitCDXValuationExchange             ="CdxValuationRequest_" + $EnvIdentity

$Global:RabbitMgtPort				           = 26103
$Global:RabbitComPort				           = 26003
$Global:RabbitHost					           ="edevrmq02dv"
$Global:RabbitString                           =$RabbitHost + ":" + $RabbitComPort

$Global:RabbitUser					           ="guest"
$Global:RabbitPwd					           ="guest"
$Global:guest					               ="guest"

