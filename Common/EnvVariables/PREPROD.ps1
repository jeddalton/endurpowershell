 . (Join-Path $StartupScriptFolder GlobalVars.ps1) 
$Global:Environment           ="END_DB_PREPROD"
$Global:EndurServerName       ="ENDSQC01PP\ENDUR_PP"
$Global:EndurDatabaseName     ="END_DB_PREPROD"
$Global:ReportingServerName   ="ENDSQC02PP\ENDUR_PPREP"
$Global:ReportingDatabaseName ="END_DB_PREPROD_REPORTING"
$Global:EndurConfigFile       ="\\endfspr01\AppData\GGTP\endur_pp\Resources\ENDUR_END_PREPROD.cfg"
$Global:RabbitString          = "edevrmq02dv:26003"
$Global:ZemaRabbitQueue       = "ZEMA_PREPROD"
$Global:ZemaExchange          = "PREPROD_MARKET"
$Global:EndurAppServer2       = "ENDAPP02PP"
