.(Join-Path $StartupScriptFolder GlobalVars.ps1) 
if(!$TestAssetBaseDir){
	$Global:TestAssetBaseDir		="C:\GGTP\Test AssetsV14\"
}
$Global:Environment                 ="Endur_QA_13"
$Global:EndurServerName             ="ENDSQC52DV\END_TEST"
$Global:EndurDatabaseName           ="END_DB_QA13"
$Global:ReportingServerName         ="ENDSQC52DV\END_TEST"
$Global:ReportingDatabaseName       ="END_DB_QA13_REPORTING"
$Global:EndurConfigFile             ="\\fasnod02dv\endur14_tst\Endur_QA_13\Resources\Endur.cfg"
$Global:RabbitString                ="edevrmq02dv:26003"
$Global:ZemaRabbitQueue             ="Zema_BAUQA2"
$Global:ZemaExchange                ="BAUQA2_MARKET"
$Global:EndurAppServer1             ="ENDAPP90QA"
$Global:EndurAppServer2             ="ENDAPP91QA"

$Global:PacManServerName            ="PACSQC11UT\PACMAN_UAT"
$Global:PacManDatabaseName          ="END_DB_QA13_PACMAN"
$Global:PacmanSSASServer            ="PACSQC12UT\PACMAN_SSAS_UAT"
$Global:PacmanSSASDataBase          ="END_DB_QA13_PACMAN_SSAS"
$Global:PacManDBConnString          ="Data Source=PACSQC11UT\PACMAN_UAT;Initial Catalog=END_DB_QA13_PACMAN;Provider=SQLNCLI10.1;Integrated Security=SSPI;"
$Global:PacManDBConnShort           ="Data Source=PACSQC11UT\PACMAN_UAT;Initial Catalog=END_DB_QA13_PACMAN;Integrated Security=SSPI"
$Global:DealLoaderAppServer         ="ENDAPP91QA"
$Global:SSISServerInstance          ="SSISQS01DV\SSIS_DEV2"
$Global:SSISDatabaseName            ="SSISRunner_DV"

#Added to support TradeValidatorValidate.ps1 and to future TradeVolumeUpload.ps1 and TradeVolumeDownload.ps1

$Global:TradeValidatorEnvURLShort ="http://$endurappserver1:9013/tradevalidator"
$Global:TradeUploaderEnvUrlShort ="http://$endurappserver1:9012/tradeuploader"
$Global:TradeVolumeEnvUrlShort  ="http://$endurappserver1:9011/tradevolume"

$Global:TradeValidatorEnvUrl  ="http://$endurappserver1.gazpromuk.intra:9013/tradevalidator"
$Global:TradeUplaoderEnvUrl   ="http://$endurappserver1.gazpromuk.intra:9012/tradeuploader"
$Global:TradeVolumeEnvUrl   ="http://$endurappserver1.gazpromuk.intra:9011/tradevolume"
$Global:ServiceUser     ="GAZPROMUK\SVC-ENDSVC-QA"

# Builds all env Rabbit Variables
.(Join-Path $StartupScriptFolder RabbitVariables.ps1)

#Import-Module SQLPS