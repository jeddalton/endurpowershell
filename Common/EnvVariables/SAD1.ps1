 . (Join-Path $StartupScriptFolder GlobalVars.ps1) 
$Global:Environment           ="END_STANDALONE_DEV1"
$Global:EndurServerName       ="ENDTST01DV\END_DV"
$Global:EndurDatabaseName     ="END_DB_STANDALONE_DEV1"
$Global:ReportingServerName   ="ENDTST01DV\END_DV"
$Global:ReportingDatabaseName ="END_DB_STANDALONE_DEV1_REPORTING"
$Global:EndurConfigFile       ="\\endfsdv01\endur_dev\END_STANDALONE_DEV1\Resources\ENDUR_END_STANDALONE_DEV1.cfg"
$Global:RabbitString          = "edevrmq02dv:26003"
$Global:ZemaRabbitQueue       = "Zema_STANDALONE_DEV1"
$Global:ZemaExchange          = "STANDALONE_DEV1_MARKET"