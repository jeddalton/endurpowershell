 .(Join-Path $StartupScriptFolder GlobalVars.ps1) 
if(!$TestAssetBaseDir){
	$Global:TestAssetBaseDir		="C:\GGTP\Test Assets\"
}
$Global:Environment                 ="END_BAUQA2"
$Global:EndurServerName             ="ENDVSQL12LON\ENDUR_BAU"
$Global:EndurDatabaseName           ="END_DB_BAUQA2"
$Global:ReportingServerName         ="ENDVSQL12LON\ENDUR_BAU"
$Global:ReportingDatabaseName       ="END_DB_BAUQA2_REPORTING"
$Global:FXReportingServerName       ="ENDVSQL12LON\ENDUR_BAU"
$Global:FXReportingDatabaseName     ="END_DB_BAUQA2_FXREPORTING"
$Global:FxReportingSSISPackage      ="\FxReporting_BAUQA2/FxReporting"
$Global:FxReportingDBConnString     ="Data Source=ENDVSQL12LON\ENDUR_BAU;Initial Catalog=END_DB_BAUQA2_FXREPORTING;Provider=SQLNCLI10.1;Integrated Security=SSPI;"
$Global:FxReportingDBConnShort      ="Data Source=ENDVSQL12LON\ENDUR_BAU;Initial Catalog=END_DB_BAUQA2_FXREPORTING;Integrated Security=SSPI"
$Global:EndurConfigFile             ="\\endfsdv01\endur_bau\END_BAUQA2\Resources\ENDUR_END_BAUQA2.cfg"
$Global:RabbitString                ="edevrmq02dv:26003"
$Global:ZemaRabbitQueue             ="Zema_BAUQA2"
$Global:ZemaExchange                ="BAUQA2_MARKET"
$Global:EndurAppServer1             ="endapp01bq02"
$Global:EndurAppServer2             ="endapp02bq02"
$Global:PacManServerName            ="PACSQC11UT\PACMAN_UAT"
$Global:PacManDatabaseName          ="END_DB_BAUQA2_PACMAN"
$Global:PacmanSSASServer            ="PACSQC12UT\PACMAN_SSAS_UAT"
$Global:PacmanSSASDataBase          ="END_DB_BAUQA2_PACMAN_SSAS"
$Global:PacManDBConnString          ="Data Source=PACSQC11UT\PACMAN_UAT;Initial Catalog=END_DB_BAUQA2_PACMAN;Provider=SQLNCLI10.1;Integrated Security=SSPI;"
$Global:PacManDBConnShort           ="Data Source=PACSQC11UT\PACMAN_UAT;Initial Catalog=END_DB_BAUQA2_PACMAN;Integrated Security=SSPI"
$Global:DealLoaderAppServer         ="ENDAPP02BQ02"

#Added to support TradeValidatorValidate.ps1 and to future TradeVolumeUpload.ps1 and TradeVolumeDownload.ps1
$Global:TradeValidatorEnvURLShort	="https://$endurappserver2:9013/tradevalidator"
$Global:TradeVolumeEnvUrlShort		="https://$endurappserver2:9011/tradevolume"
$Global:TradeValidatorEnvUrl		="https://$endurappserver2.gazpromuk.intra:9013/tradevalidator"
$Global:TradeVolumeEnvUrl			="https://$endurappserver2.gazpromuk.intra:9011/tradevolume"
$Global:ServiceUser					="SVC-GGTP-ENDUR-DEV@Gazpromuk.intra"

# Builds all env Rabbit Variables
.(Join-Path $StartupScriptFolder RabbitVariables.ps1)

#Import-Module SQLPS