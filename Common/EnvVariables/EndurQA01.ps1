 .(Join-Path $StartupScriptFolder GlobalVars.ps1) 
if(!$TestAssetBaseDir){
	$Global:TestAssetBaseDir		="C:\GGTP\Test AssetsV14\"
}
$Global:Environment                 ="Endur_QA_01"
$Global:EndurServerName             ="ENDSQC52DV\END_TEST"
$Global:EndurDatabaseName           ="END_DB_QA01"
$Global:ReportingServerName         ="ENDSQC52DV\END_TEST"
$Global:ReportingDatabaseName       ="END_DB_QA01_REPORTING"
$Global:FXReportingServerName       ="ENDVSQL12LON\ENDUR_BAU"
$Global:FXReportingDatabaseName     ="END_DB_BAUQA2_FXREPORTING"
$Global:FxReportingSSISPackage      ="\FxReporting_BAUQA2/FxReporting"
$Global:FxReportingDBConnString     ="Data Source=ENDVSQL12LON\ENDUR_BAU;Initial Catalog=END_DB_BAUQA2_FXREPORTING;Provider=SQLNCLI10.1;Integrated Security=SSPI;"
$Global:FxReportingDBConnShort      ="Data Source=ENDVSQL12LON\ENDUR_BAU;Initial Catalog=END_DB_BAUQA2_FXREPORTING;Integrated Security=SSPI"
$Global:EndurConfigFile             ="\\fasnod02dv\endur14_tst\Endur_QA_01\Resources\Endur.cfg"
$Global:RabbitString                ="edevrmq02dv:26003"
$Global:ZemaRabbitQueue             ="Zema_BAUQA2"
$Global:ZemaExchange                ="BAUQA2_MARKET"
$Global:EndurAppServer1             ="endapp51qa"
$Global:EndurAppServer2             ="ENDAPP71QA"
$Global:PacManServerName            ="PACSQC11UT\PACMAN_UAT"
$Global:PacManDatabaseName          ="END_DB_BAUQA2_PACMAN"
$Global:PacmanSSASServer            ="PACSQC12UT\PACMAN_SSAS_UAT"
$Global:PacmanSSASDataBase          ="END_DB_BAUQA2_PACMAN_SSAS"
$Global:PacManDBConnString          ="Data Source=PACSQC11UT\PACMAN_UAT;Initial Catalog=END_DB_BAUQA2_PACMAN;Provider=SQLNCLI10.1;Integrated Security=SSPI;"
$Global:PacManDBConnShort           ="Data Source=PACSQC11UT\PACMAN_UAT;Initial Catalog=END_DB_BAUQA2_PACMAN;Integrated Security=SSPI"
$Global:DealLoaderAppServer         ="ENDAPP02BQ02"
$Global:SSISServerInstance          ="SSISQS01DV\SSIS_DEV2"
$Global:SSISDatabaseName            ="SSISRunner_DV"

#Added to support TradeValidatorValidate.ps1 and to future TradeVolumeUpload.ps1 and TradeVolumeDownload.ps1
$Global:TradeValidatorEnvURLShort	="http://$endurappserver1:9013/tradevalidator"
$Global:TradeVolumeEnvUrlShort		="http://$endurappserver1:9011/tradevolume"
$Global:TradeValidatorEnvUrl		="http://$endurappserver1.gazpromuk.intra:9013/tradevalidator"
$Global:TradeVolumeEnvUrl			="http://$endurappserver1.gazpromuk.intra:9011/tradevolume"
$Global:ServiceUser					="GAZPROMUK\SVC-ENDSVC-QA"

# Builds all env Rabbit Variables
.(Join-Path $StartupScriptFolder RabbitVariables.ps1)

