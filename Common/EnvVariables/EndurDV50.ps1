 .(Join-Path $StartupScriptFolder GlobalVars.ps1) 
if(!$TestAssetBaseDir){
	$Global:TestAssetBaseDir		="C:\GGTP\Test AssetsV14\"
}
$Global:Environment                 ="Endur_DV_50"
$Global:EndurServerName             ="ENDSQC54DV\END_DEV"
$Global:EndurDatabaseName           ="END_DB_DV50"
$Global:ReportingServerName         ="ENDSQC54DV\END_DEV"
$Global:ReportingDatabaseName       ="END_DB_DV50_REPORTING"
$Global:EndurConfigFile             ="\\fasnod02dv\endur14_dev\Endur_DV_50\Resources\Endur.cfg"
$Global:RabbitString                ="edevrmq02dv:26003"
$Global:ZemaRabbitQueue             ="Zema_BAUQA2"
$Global:ZemaExchange                ="BAUQA2_MARKET"
$Global:EndurAppServer1             ="endapp11dv"
$Global:EndurAppServer2             ="endapp12dv"

$Global:PacManServerName            ="PACSQS01DV\PACMAN_DEV"
$Global:PacManDatabaseName          ="END_DB_DV50_PACMAN"
$Global:PacmanSSASServer            ="PACSQS01DV\PACMAN_DEV"
$Global:PacmanSSASDataBase          ="END_DB_DV50_PACMAN_SSAS"
$Global:PacManDBConnString          ="Data Source=PACSQS01DV\PACMAN_DEV;Initial Catalog=END_DB_DV50_PACMAN;Provider=SQLNCLI10.1;Integrated Security=SSPI;"
$Global:PacManDBConnShort           ="Data Source=PACSQS01DV\PACMAN_DEV;Initial Catalog=END_DB_DV50_PACMAN;Integrated Security=SSPI"
$Global:DealLoaderAppServer         ="endapp72qa"
$Global:SSISServerInstance          ="SSISQS01DV\SSIS_DEV2"
$Global:SSISDatabaseName            ="SSISRunner_DV"

#Added to support TradeValidatorValidate.ps1 and to future TradeVolumeUpload.ps1 and TradeVolumeDownload.ps1
$Global:TradeValidatorEnvURLShort	="https://$endurappserver2:9013/tradevalidator"
$Global:TradeVolumeEnvUrlShort		="https://$endurappserver2:9011/tradevolume"
$Global:TradeValidatorEnvUrl		="https://$endurappserver2.gazpromuk.intra:9013/tradevalidator"
$Global:TradeVolumeEnvUrl			="https://$endurappserver2.gazpromuk.intra:9011/tradevolume"
$Global:ServiceUser					="SVC-GGTP-ENDUR-DEV@Gazpromuk.intra"

# Builds all env Rabbit Variables
.(Join-Path $StartupScriptFolder RabbitVariables.ps1)

#Import-Module SQLPS