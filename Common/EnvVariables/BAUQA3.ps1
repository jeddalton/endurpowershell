write-host "Startup folder" $StartupScriptFolder
 .(Join-Path $StartupScriptFolder GlobalVars.ps1)
$Global:Environment             ="END_BAUQA3"
$Global:EndurServerName         ="ENDVSQL12LON\ENDUR_BAU"
$Global:EndurDatabaseName       ="END_DB_BAUQA3"
$Global:ReportingServerName     ="ENDVSQL12LON\ENDUR_BAU"
$Global:ReportingDatabaseName   ="END_DB_BAUQA3_REPORTING"
$Global:FXReportingServerName   ="ENDVSQL12LON\ENDUR_BAU"
$Global:FXReportingDatabaseName ="END_DB_BAUQA3_FXREPORTING"
$Global:FxReportingSSISPackage  ="\FxReporting_BAUQA3/FxReporting"
$Global:FxReportingDBConnString ="Data Source=ENDVSQL12LON\ENDUR_BAU;Initial Catalog=END_DB_BAUQA3_FXREPORTING;Provider=SQLNCLI10.1;Integrated Security=SSPI;"
$Global:FxReportingDBConnShort  ="Data Source=ENDVSQL12LON\ENDUR_BAU;Initial Catalog=END_DB_BAUQA3_FXREPORTING;Integrated Security=SSPI"
$Global:EndurConfigFile         ="\\endfsdv01\endur_bau\END_BAUQA3\Resources\ENDUR_END_BAUQA3.cfg"
$Global:RabbitString            ="edevrmq02dv:26003"
$Global:ZemaRabbitQueue         ="Zema_BAUQA3"
$Global:ZemaExchange            ="BAUQA3_MARKET"
$Global:EndurAppServer1         ="ENDAPP01BQ03"
$Global:EndurAppServer2         ="ENDAPP02BQ03"
$Global:PacManServerName        ="ENDTST07DV\SQL"
$Global:PacManDatabaseName      ="END_DB_BAUQA3_PACMAN"
$Global:PacManDBConnString      ="Data Source=ENDTST07DV\SQL;Initial Catalog=END_DB_BAUQA3_PACMAN;Provider=SQLNCLI10.1;Integrated Security=SSPI;"
$Global:PacManDBConnShort       ="Data Source=ENDTST07DV\SQL;Initial Catalog=END_DB_BAUQA3_PACMAN;Integrated Security=SSPI"

#Added to support TradeValidatorValidate.ps1 and to future TradeVolumeUpload.ps1 and TradeVolumeDownload.ps1
$Global:TradeValidatorEnvURLShort	="https://$endurappserver2:9013/tradevalidator"
$Global:TradeVolumeEnvUrlShort		="https://$endurappserver2:9011/tradevolume"
$Global:TradeValidatorEnvUrl		="https://$endurappserver2.gazpromuk.intra:9013/tradevalidator"
$Global:TradeVolumeEnvUrl			="https://$endurappserver2.gazpromuk.intra:9011/tradevolume"
$Global:ServiceUser					="SVC-GGTP-ENDUR-DEV@Gazpromuk.intra"

# Builds all env Rabbit Variables
.(Join-Path $StartupScriptFolder RabbitVariables.ps1)