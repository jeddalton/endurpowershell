 . (Join-Path $StartupScriptFolder GlobalVars.ps1) 
$Global:TigerDBConnStringShort  = "Data Source=TGRSQS02QA\TIGER_QA;Initial Catalog=TigerQA2;Integrated Security=SSPI"
$Global:TigerServerInstance     = "TGRSQS02QA\TIGER_QA"
$Global:TigerDatabaseName       = "TigerQA2"
$Global:EndurDBConnStringShort  = "Data Source=ENDVSQL14LON\ENDUR_TEST;Initial Catalog=END_DB_TEST5;Integrated Security=SSPI"
$Global:RabbitString            = "tgrapp02qa:26005"
$Global:RabbitExchange_Endur    = "EndurQA2"
$Global:RabbitQueue_Endur       = "EndurQA2"
$Global:RabbitExchange_Fees     = "FeesQA2"
$Global:RabbitQueue_Fees        = "FeesQA2"
$Global:RabbitExchange_Offers   = "OffersQA2"
$Global:RabbitQueue_Offers      = "OffersQA2"
$Global:RabbitExchange_VolumeFiles = "VolumeFilesQA2"
$Global:RabbitQueue_VolumeFiles = "VolumeFilesQA2"
$Global:RabbitExchange_OfferApprovals = "OfferApprovalQA2"
$Global:RabbitQueue_OfferApprovals = "OfferApprovalQA2"
$Global:ERFilename  =   ("$TestAssetBaseDir\Tests\Tiger\ExpectedResults\$ERFile")
$Global:SQLFile     =   ("$TestAssetBaseDir\Tests\Tiger\SQL\$SQLFilename")  