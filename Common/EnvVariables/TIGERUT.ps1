 . (Join-Path $StartupScriptFolder GlobalVars.ps1) 
$Global:TigerDBConnStringShort = "Data Source=TGRSQS03UT\TIGER_UAT;Initial Catalog=TigerUT;Integrated Security=SSPI"
$Global:TigerServerInstance     = "TGRSQS03UT\TIGER_UAT"
$Global:TigerDatabaseName       = "TigerUT"
$Global:RabbitString            = "tgrapp01ut:26005"
$Global:RabbitExchange_Endur                = "EndurUT"
$Global:RabbitQueue_Endur                                                      = "EndurUT"
$Global:RabbitExchange_Fees                   = "FeesUT"
$Global:RabbitQueue_Fees                                                        = "FeesUT"
$Global:RabbitExchange_Offers               = "OffersUT"
$Global:RabbitQueue_Offers                                                     = "OffersUT"
$Global:RabbitExchange_VolumeFiles                   = "VolumeFilesUT"
$Global:RabbitQueue_VolumeFiles                                         = "VolumeFilesUT"
$Global:RabbitExchange_OfferApprovals             = "OfferApprovalUT"
$Global:RabbitQueue_OfferApprovals                                                  = "OfferApprovalUT"
$Global:ERFilename  =   ("$TestAssetBaseDir\Tests\Tiger\ExpectedResults\$ERFile")
$Global:SQLFile     =   ("$TestAssetBaseDir\Tests\Tiger\SQL\$SQLFilename")  
