 . (Join-Path $StartupScriptFolder GlobalVars.ps1) 
$Global:TigerDBConnStringShort = "Data Source=TGRSQC01PP\TIGER_Preprod;Initial Catalog=Tiger_Preprod;Integrated Security=SSPI"
$Global:TigerServerInstance     = "TGRSQC01PP\TIGER_Preprod"
$Global:TigerDatabaseName       = "Tiger_Preprod"
$Global:RabbitString            = "tgrapp02pp:26005"
$Global:RabbitExchange_Endur          	= "EndurPP"
$Global:RabbitQueue_Endur				= "EndurPP"
$Global:RabbitExchange_Fees          	= "FeesPP"
$Global:RabbitQueue_Fees				= "FeesPP"
$Global:RabbitExchange_Offers          	= "OffersPP"
$Global:RabbitQueue_Offers				= "OffersPP"
$Global:RabbitExchange_VolumeFiles    	= "VolumeFilesPP"
$Global:RabbitQueue_VolumeFiles			= "VolumeFilesPP"
$Global:ERFilename  =   ("$TestAssetBaseDir\Tests\Tiger\ExpectedResults\$ERFile")
$Global:SQLFile     =   ("$TestAssetBaseDir\Tests\Tiger\SQL\$SQLFilename")  
