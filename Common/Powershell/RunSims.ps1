# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  
# |  $Global:ThisTest='RunSims'
# |  
# |  
# |  
# |  
# |  
# |  
# |  
# +-----------------------------------------------------------------------------------------------------------------------------------------------

# To assign variables and make their scope global; 
Function Initialise {
    
    cls
    # Runtime initialisation
    $Global:script     ="$TestAssetBaseDir\Common\Powershell\EndurUtils.ps1"
    . $script                # Loads functions into current scope
    ImportRunTimeModules
        
    EndurConn $ThisTest      # External function call to establish either a Manual (local) or Auto (batch) Endur session    
}

# To run the simulations
Function Main {

    Write-Host "`nProcessing batch SIMs ..."
    Invoke-EndurWorkflow -Workflowname  "TEST SIMS" -TimeoutInSeconds 600 -PollingFrequency 20 -Verbose
    Write-Host "`nProcessing Sim DW Staging GMT ALL ..."
    Invoke-EndurWorkflow -Workflowname  "TEST FX Options PNL" -TimeoutInSeconds 600 -PollingFrequency 20 -Verbose
}

# Mainline
try {
    Initialise
    Main
}

finally {
    if ($RunMode -eq "Auto") {Stop-EndurSession}
    Write-Host "`nDone $ThisTest"
}
