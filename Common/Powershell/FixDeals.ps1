# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  
# |  $Global:ThisTest='FixDeals'
# |  $Global:StartDate='30-Oct-2013'
# |  $Global:EndDate='16-Dec-2013'
# |  
# |  
# |  
# |  
# |  
# |  
# +-----------------------------------------------------------------------------------------------------------------------------------------------

# To assign variables and make their scope global; 
Function Initialise {
    
    cls
    # Runtime initialisation
    $Global:script     ="$TestAssetBaseDir\Common\Powershell\EndurUtils.ps1"
    . $script                # Loads functions into current scope
    ImportRunTimeModules

    EndurConn $ThisTest       # External function call to establish either a Manual (local) or Auto (batch) Endur session
}

# To 
Function Main {
    Set-FixDeals -StartDate $StartDate -EndDate $EndDate  # Fix deals between a given date range
    New-TestResult -Passed $true -Message "Deal Fixing Complete" -Comment "from $StartDate to $EndDate"
}

# Mainline
try {
    Initialise
    Main
}

finally {
    if ($RunMode -eq "Auto") {Stop-EndurSession}
    Write-Host "`nDone $ThisTest"
}
