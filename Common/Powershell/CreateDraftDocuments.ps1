# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  
# |  $Global:ThisTest='ThisTestName'
# |  
# |  
# |  
# |  
# |  
# |  
# +-----------------------------------------------------------------------------------------------------------------------------------------------

# To assign variables and make their scope global; 
Function Initialise {

    cls
    # Runtime initialisation
    $Global:script     ="$TestAssetBaseDir\Common\Powershell\EndurUtils.ps1"
    . $script                # Loads functions into current scope
    ImportRunTimeModules     # Imports various cmdlets, registers SQL snapins, etc.
    
    # Make Endur connection    
    EndurConn $ThisTest      # External function call to establish either a Manual (local) or Auto (batch) Endur session            
}

# Put the main test processing into this function
Function Main {
    Write-Host "Doing processing"
    Write-Host "Document Type is: $DocType"
    Write-Host "BO Query is: $BOQuery"
    Write-Host "BO Doc Def is: $DocDef"
           
    $DocTypeInt =   Get-EndurStaticDataId "StldocDocumentType" $DocType
    $Events     =   Get-EndurQueryResult -QueryName $BOQuery -AdditionalCriteria "ab_tran.tran_status = 3 AND NOT EXISTS (select 1 from stldoc_details sd join stldoc_header sh on sh.document_num = sd.document_num where sd.event_num = ab_tran_event.event_num and sh.doc_type = $DocTypeInt)"
    
    if ($Events -ne $null){
        try 
        {
            New-EndurDocument -Definition $DocDef -Status "Draft" -EventIds $Events -ErrorAction Stop
            New-TestResult -Passed $true -Message "Document created for query $BOQuery and Doc Def $DocDef"
        }
        catch
        {
            $ErrorMessage = $_.Exception.Message
            Break
        }
        finally
        {
            Write-Host "Done"
        }
    }
    else{
        Write-Host "No events for BO Query: $BOQuery and BODocDef: $DocDef and BO Doc Type: $DocType"
        Write-Host "SKIPPING"
    }
    
}

# Mainline
try {
    Initialise
    Main    
}

finally {
    if ($RunMode -eq "Auto") {Stop-EndurSession}
    Write-Host "`nDone $ThisTest"
}