# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  
# |  $Global:ThisTest='MarketDataOnly'
# |  $Global:MDFile='Common\Data\market_data_day2.csv'
# |  $Global:VolsFile='Common\Data\volatility_day2.csv'
# |  
# |  
# |  
# |  
# |  
# +-----------------------------------------------------------------------------------------------------------------------------------------------

# To assign variables and make their scope global; 
Function Initialise {
    
    cls
    # Runtime initialisation
    $Global:script     ="$TestAssetBaseDir\Common\Powershell\EndurUtils.ps1"
    . $script                # Loads functions into current scope
    ImportRunTimeModules

    EndurConn $ThisTest       # External function call to establish either a Manual (local) or Auto (batch) Endur session
}

# Mainline: To import market data and volatilities into Endur from CSV files
try {
    Initialise
    $Global:MDFile      = "$TestAssetBaseDir\$MDFile"
    $Global:VolsFile    = "$TestAssetBaseDir\$VolsFile"
    Import-MarketData -MarketDataCsvFile $MDFile -Verbose
    Invoke-EndurScript -ScriptName "TESTUTIL_Load_Volatilities" -Parameters @{"file"=$VolsFile} -Verbose
}

finally {
    if ($RunMode -eq "Auto") {Stop-EndurSession}
    Write-Host "`nDone $ThisTest"
}
