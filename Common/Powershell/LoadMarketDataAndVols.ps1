# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  
# |  $Global:ThisTest='MarketData'
# |  $Global:MDFile='Common\Data\market_data.csv'
# |  $Global:VolsFile='Common\Data\volatility.csv'
# |  $Global:BatchSIMDef='\Regression\DealLifeCycle\SQL\CreateBatchSIMDef.sql'
# |  
# |  
# |  
# |  
# +-----------------------------------------------------------------------------------------------------------------------------------------------

# To assign variables and make their scope global; 
Function Initialise {
    
    cls
    # Runtime initialisation
    $Global:script     ="$TestAssetBaseDir\Common\Powershell\EndurUtils.ps1"
    . $script                # Loads functions into current scope
    ImportRunTimeModules

    EndurConn $ThisTest       # External function call to establish either a Manual (local) or Auto (batch) Endur session    
}

# To import market data into Endur from CSV file
Function Main {

    if ($MDFile -ne $null)
    {
        $MDfile = Join-Path $TestAssetBaseDir $MDFile
        Import-MarketData -MarketDataCsvFile $MDFile -Verbose
        New-TestResult -Passed $True -Message "Market data loaded successfully"
    }
    if ($VolsFile -ne $null)
    {
        $VolsFile = Join-Path $TestAssetBaseDir $VolsFile
        Invoke-EndurScript -ScriptName "TESTUTIL_Load_Volatilities" -Parameters @{"file"=$VolsFile} -Verbose
        New-TestResult -Passed $True -Message "Volatility data loaded successfully"
    }
        
}

# Mainline
try {
    Initialise
    Main
}

finally {
    if ($RunMode -eq "Auto") {Stop-EndurSession}
    Write-Host "`nDone $ThisTest"
}
