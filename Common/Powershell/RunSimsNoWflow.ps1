# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  
# |  $Global:ThisTest='RunSims'
# |  $Global:batch_name='EOD_SIM_GAS_GMT1'
# |  $Global:holiday_schedule='EOD_UK'
# |  $Global:run_close='true'
# |  $Global:market_price='true'
# |  $Global:fail_on_msg='false'
# |  
# |  
# +-----------------------------------------------------------------------------------------------------------------------------------------------

# To assign variables and make their scope global; 
Function Initialise {
    
    cls
    # Runtime initialisation
    $Global:script     ="$TestAssetBaseDir\Common\Powershell\EndurUtils.ps1"
    . $script                # Loads functions into current scope
    ImportRunTimeModules
        
    EndurConn $ThisTest      # External function call to establish either a Manual (local) or Auto (batch) Endur session    
}

# To run the simulations
Function Main {

# override
    if (Test-Path variable:\batch_name){    
        $obj = @{"workflow"=1;"batch_name"=$batch_name;"holiday_schedule"=$holiday_schedule;"run_close"=$run_close;"currency"="USD";"market_price"=$market_price;"fail_on_msg"=$fail_on_msg}
    }
# default
    else {     
        $obj = @{"workflow"=1;"batch_name"="TESTBATCH_SIM_DEF";"holiday_schedule"="EOD_UK";"run_close"="true";"currency"="USD";"market_price"="true";"fail_on_msg"="true"}
        $batch_name = "TESTBATCH_SIM_DEF"
    }    
    # Create PS Object from 
    $inp = New-Object –TypeName PSObject –Prop $obj

    Invoke-EndurScript -Argt $inp -ScriptName "GP_STD_Batch_Simulation" -ServiceName "FEE SimRun" -ServiceMethod "RunProcess"   
    New-TestResult -Passed $True -Message "Sim Execution for $batch_name succeeeded" -Comment "Sim Execution for $batch_name succeeeded"
}

# Mainline
try {
    Initialise
    Main
}

finally {
    if ($RunMode -eq "Auto") {Stop-EndurSession}
    Write-Host "`nDone $ThisTest"
}
