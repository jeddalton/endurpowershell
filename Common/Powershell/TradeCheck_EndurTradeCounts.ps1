﻿# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  
# |  $Global:ThisTest='TradeCheck_EndurTradeCounts'
# |  $Global:EndurSQL='\Regression\EMIR\SQL\trade_GROUPBY_COUNT.sql'
# |  $Global:ERFile='\Regression\EMIR\ExpectedResults\ER_EndurTradeCounts.xlsx'
# |  $Global:Sheetname='Day1_Validate'
# |  $Global:ExcludeKeys=''
# |  $Global:CompKeys='toolsetID|instrumentID'
# |  
# |  
# +-----------------------------------------------------------------------------------------------------------------------------------------------

# To assign variables and make their scope global; 
Function Initialise {
    
    cls
    # Runtime initialisation
    $Global:script     ="$TestAssetBaseDir\Common\Powershell\EndurUtils.ps1"
    . $script                # Loads functions into current scope
    ImportRunTimeModules
    import-module DataCompareModule
}

# Join Endur and Pacman extracts and compare with expected result sheet
Function Main {
    
    # Get counts from Endur database
    $sql = [System.IO.File]::ReadAllText($TestAssetBaseDir + $EndurSQL)
    $EndurDeals = @(Invoke-Sqlcmd -ServerInstance $EndurServerName -Database $EndurDatabaseName -Query $sql -QueryTimeout 200 -ErrorAction Stop)
    if ($EndurDeals.count -eq 0){
        Write-Host "No deals returned from Endur."
        New-TestResult -Passed $false -Message "No deals returned from Endur" -Comment "using ""$EndurSQL"""
    }
    else{        
        $ERFilename     =Join-Path $TestAssetBaseDir $ERFile
        # Define the keys and exclusions for the upcoming data compare 
        $exclude        =$ExcludeKeys.split("|")
        $keys           =$CompKeys.split("|")
   
        Write-Host "`nExclude keys   : " $exclude
        Write-Host   "Comparison keys: " $keys
    
        if (Test-Path $ERFilename){
            Write-Host "Path to master is $ERFilename; Sheetname is $Sheetname"
            $ERPFile = Get-ExcelObject -Path $ERFilename -WorksheetName $Sheetname
            $result = Compare-DataObject -Left $EndurDeals -Right $ERPFile -Keys $keys -ExcludeFields $exclude -NullValuesEqual -MergeColumns
            $result | Convert-CompareObjectToTestResult 
        }
        else{        
            Write-Host "No master document exist in $ERFilename"
            New-TestResult -Passed $false -Message "No master document exists" -Comment "for $ERFilename"
        }
        $Global:Pass = "TRUE"
    }
}

# Mainline
try {
    Initialise
    Main
}

finally {
    Write-Host "`nDone $ThisTest"
}
