﻿# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  
# |  $Global:ThisTest='DealLifeCycle_v3'
# |  $Global:OSSFile='Data\SetOSS.csv'
# |  
# |  
# |  
# |  
# |  
# |  
# +-----------------------------------------------------------------------------------------------------------------------------------------------

# To assign variables and make their scope global; 
Function Initialise {

    cls
    # Runtime initialisation
    $Global:script     ="$TestAssetBaseDir\Common\Powershell\EndurUtils.ps1"
    . $script                # Loads functions into current scope
    ImportRunTimeModules

    EndurConn $ThisTest      # External function call to establish either a Manual (local) or Auto (batch) Endur session
}

Function Main {
    SetOpsServices
    New-TestResult -Passed $true -Message "OpsServices Set" -Comment "from $OSSPath"
}

Function SetOpsServices {
    Write-Host "`nSet Ops Services"
    $Global:OSSPath=Join-Path $TestAssetBaseDir $OSSFile
    Write-Host "`nOSS file path is: $OSSPath"
    SetOSS $OSSPath
}

# Mainline
try {
    Initialise
    Main
}

finally {
    if ($RunMode -eq "Auto") {Stop-EndurSession}
    Write-Host "`nDone " $OSSPath$OSSFile
}
