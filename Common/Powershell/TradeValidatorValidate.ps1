# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  RECORD What the test is doing / points of interest
# +-----------------------------------------------------------------------------------------------------------------------------------------------
# | This ps1 script is used to invoke tradeValidator
# |
# | $Global:TranNum = 123456												- Transaction number that will be validated
# | $Global:TranReference = 'FX_TRADE_01'									- Reference of the trade that will be validated
# |	Only One of the $TranNum or $TranReference must be delivered, If $TranNum is delivered then $TranReference is ignored
# |
# | Amendment variables will be applied to all trades processed - Optional values
# |		
# |	$GLobal:AmendUTI					- Free text 
# |	$GLobal:AmendStrategy				- USER_Strategy.strategy
# |	$GLobal:AmendInternalBusinessUnit	- Party.short_name
# |	$GLobal:AmendInternalContact		- Personnel.short_name
# |	$GLobal:AmendInternalPortfolio		- Portfolio.Short_name
# |	$GLobal:AmendSpread					- Yes/No
# |	$GLobal:AmendEfp					- Yes/No
# |	$GLobal:AmendSleeve					- Yes/No
# |	$GLobal:SetVersion					- integer - will update the version within the message
# |	$GLobal:ExpectedSuccess				- Yes/No (Default Yes)
# +-----------------------------------------------------------------------------------------------------------------------------------------------

# To perform initialisation for the test
Function Initialise {  

    Write-Host "In initialise"
    $Global:script = "$TestAssetBaseDir\Common\Powershell\EndurUtils.ps1"
    . $script                # Loads functions into current scope
    #$Global:script = "$TestAssetBaseDir\Common\Powershell\GeneralUtils.ps1"
    #. $script                # Loads functions into current scope
    ImportRunTimeModules  
    
    # Make Endur connection   
    EndurConn $ThisTest      # External function call to establish an Endur session
    Write-Host TradeValidatorEnvURL
    # Retrieve URL of TradeValidator Service for actual Environment $Global:TradeValidatorEnvURL
    $Global:EnvironmentUrl = ((Get-Variable -Name TradeValidatorEnvURL).value)
    Write-Host "Connecting to service: " $EnvironmentUrl
    if ($EnvironmentUrl -eq $null) {RaiseException "Unable to retrieve URL for trade validator service. Check Environment Variables 'TradeValidatorEnvURL' or specify the variable in the Suite"}


	 # TradeValidator Service User for actual Environment $Global:TradeValidatorUser
    $Global:EnvironmentUrl = ((Get-Variable -Name TradeValidatorEnvURL).value)
	$Global:TVUser = ((Get-Variable -Name ServiceUser).value)
    Write-Host "Using trade validator User: " $TVUser
    if ($TVUser -eq $null) {RaiseException "Unable to retrieve Trade validator User. Check Environment Variables 'TradeValidatorUser' or specify the variable in the Suite"}
}

# Main Processing
Function Main {
	Write-Host "Retrieveing transactions for reference" $TranReference
	# If no tranNum delivered as input parameter, query for all trades with reference delivred
	if (-not($Global:ExpectedSuccess)){
		$Global:ExpectedSuccess = 'YES'
	}else{
		$Global:ExpectedSuccess = $Global:ExpectedSuccess.toUpper()
	}
	if (($TranNum.length -eq 0) -and ($TranReference.length -eq 0)){

		#If No tranNum and no Reference delivered, fail test
		Write-Host "TestFailed: TranNumber or TranReference must be delivered"
		New-TestResult -Passed $False -Message "Retrieveing Transactions failed" -Comment "TranNumber or TranReference must be delivered"
	}else{
		if ($TranNum.length -eq 0){			
			$Trans = Get-EndurQueryResult -Fields @{"Transaction.Ref"=$TranReference;"Transaction.Tran Status"="New"}
			Write-Host "Found " $Trans.count " results in Endur Database (" $Trans ")"
		}else{
			Write-Host "Using TranNum : " $TranNum
			$Trans = $TranNum
		}
		# At least one transaction retrieved
		if ($Trans.count -ge 1){
			Write-Host "Processing " $Trans.count " trade(s)"

			# Build SQL using all used tran_numbers
			$SQL = buildSQL
			# Actual status will be used for further check
			Write-Host "Query for actual (before processing) status of the trades"
#			Write-Host $SQL
			$statusBefore = @(Invoke-EndurSqlCmd -Query $SQL)
			Write-Host "Validating transactions"
			if (-not($SetVersion)){$SetVersion = 0}
			if (-not($SetTranNum)){$SetTranNum = 0}
			if (-not($SetDealNum)){$SetDealNum = 0}
			if ($AmendUTI){Write-Host 'Amending UTI to' $AmendUTI}
			if ($AmendStrategy){Write-Host 'Amending Strategy to' $AmendStrategy}
			if ($AmendInternalBusinessUnit){Write-Host 'Amending Internal business unit to' $AmendInternalBusinessUnit}
			if ($AmendInternalContact){Write-Host 'Amending Internal Contact to' $AmendInternalContact}
			if ($AmendInternalPortfolio){Write-Host 'Amending Internal Portfolio to' $AmendInternalPortfolio}
			if ($AmendSpread){Write-Host 'Amending Spread to' $AmendSpread}
			if ($AmendEfp){Write-Host 'Amending EFP to' $AmendEfp}
			if ($AmendSleeve){Write-Host 'Amending Sleeve to' $AmendSleeve}
			if ($IntercompanyBU){
				$Global:ValidateResult = ($Trans | Set-TradeValidatorValidate  -EnvironmentUrl $EnvironmentUrl -User $TVUser -AmendUTI $AmendUTI -AmendStrategy $AmendStrategy -AmendInternalBusinessUnit $AmendInternalBusinessUnit -AmendInternalContact $AmendInternalContact -AmendInternalPortfolio $AmendInternalPortfolio -AmendSpread $AmendSpread -AmendEfp $AmendEfp -AmendSleeve $AmendSleeve <# -SetVersion $SetVersion  -SetDealNum $SetDealNum -SetTranNum $SetTranNum -IntercompanyBU $IntercompanyBU #>)
			}else{
				$Global:ValidateResult = ($Trans | Set-TradeValidatorValidate  -EnvironmentUrl $EnvironmentUrl -User $TVUser -AmendUTI $AmendUTI -AmendStrategy $AmendStrategy -AmendInternalBusinessUnit $AmendInternalBusinessUnit -AmendInternalContact $AmendInternalContact -AmendInternalPortfolio $AmendInternalPortfolio -AmendSpread $AmendSpread -AmendEfp $AmendEfp -AmendSleeve $AmendSleeve <#-SetVersion $SetVersion  -SetDealNum $SetDealNum -SetTranNum $SetTranNum #>)
			}
			$expectedStatus = ($ExpectedSuccess -eq 'YES')
			if ($ValidateResult.Success[0] -ne $expectedStatus){
				$message = "Processing result not as expected. Validation is expected to end up with Success status: " + $expectedStatus + ", but ended with status: " + $ValidateResult.Success[0]
				New-TestResult -Passed $False -Message $message -Comment "Unexpected validation result"
			}else{
				$message = "Processing result as expected. Validation is expected to end up with Success status: " + $expectedStatus + ", ended with status: " + $ValidateResult.Success[0]
				New-TestResult -Passed $true -Message $message -Comment "Validation result as expected"
			}
			# Post processing status
			Write-Host "Query for actual (after processing) status of the trades"
			$statusAfter = @(Invoke-EndurSqlCmd -Query $SQL)

			CheckResults -statusAfter $statusAfter -statusBefore $statusBefore
		}else{
			#If No tranNum and no Reference delivered, fail test
			Write-Host "TestFailed : No deal to validate found"
			New-TestResult -Passed $false -Message "Test failed" -Comment "No deal to validate found"
		}
	}
}

Function CheckResults {
	param($statusAfter, $statusBefore)
	$expectedChanges = ($ExpectedSuccess -eq 'YES')
    Write-Host "Checking results of processing"
		#Indicator of fail
    	$bFailed = 0
		# Results must have same rowCount
		If ($statusBefore.length -eq $statusAfter.length){
			# Expected Status for all transactions is 3 - Validated
			if ($expectedChanges){
				$AmendStatus = "Validated"
			}
			if ($ExpectedInternalPortfolio){
				$AmendInternalPortfolio = $ExpectedInternalPortfolio
			}
			for ($iRow=0; $iRow -lt $statusAfter.length; $iRow++){
				# gather all 'columns' of the result set
				$theMembers = ($statusAfter | Get-Member -MemberType Property).Name
				# If Version of trade did not change, make test failed
				#If version was set to different number than original deal version, the deal was not touched, therefore the version is expected to be the same
				if (($SetVersion -eq $statusBefore[$iRow].Version) -or ($setVersion -eq 0)){
					if (($statusAfter[$iRow].Version -gt $statusBefore[$iRow].Version))
						# if new trade version is higher than the original one, and there's no sepcific version expected by Suite, asign new version as expected
						{$AmendVersion = $statusAfter[$iRow].Version} 
<#					else 
						{
							$AmendVersion = "Version not shifted"
							New-TestResult -Passed $false -Message ($statusAfter[$iRow].reference + " - Version not shifted") -Comment ("Test for: " + $statusAfter[$iRow].reference)
						}#>
				}else{
					$AmendVersion = $statusBefore[$iRow].Version
				}
				# Check all values retrieved by the query
                $failedDisplayed = $false
				foreach ($member in $theMembers){
					# Check if the value is supposed to be amended, Status ($AmendStatus) and Version ($AmendVersion) are set within this PowerShell to enable dynamic validation
					# Assign the expected Value to $expectedValue
					if ($expectedChanges){
						$amendVarName = "Amend" + $member
						$amendProperty = Get-Variable -Name $amendVarName
						if ($amendProperty.Name){
							$expectedValue = $amendProperty.value
						}else{
							$expectedValue = $statusBefore[$iRow].$member
						}
					}else{
						$expectedValue = $statusBefore[$iRow].$member
					}
					# This line is for debugging purposes only, displays complete check progress
#					Write-Host "Checking results - Comparison - " $member " -> Before : " $statusBefore[$iRow].$member "; After : " $statusAfter[$iRow].$member " <=> " $expectedValue

					# If after-processing value does not match expected value, make test failed.
					$afterValue = $statusAfter[$iRow] | select-object -ExpandProperty $member
#					Write-Host 'Comparing:' $member ', expected value = ' $expectedValue ', actual value: ' $afterValue
					if ($afterValue -ne $expectedValue){
						if ($failedDisplayed -eq $false){
							$failedDisplayed = $true
							Write-Host 'Test failed for:' $statusAfter[$iRow].reference
						}
						$message =  "Values do not match for: $member, Expected value is : $expectedValue , actual value is $afterValue"
						New-TestResult -Passed $false -Message $message -Comment ("Test for: " + $statusAfter[$iRow].reference)
						$bFailed++;
					}
				}
			}
		}else{$bFailed++}

		if ($bFailed -ge 1){
			New-TestResult -Passed $false -Comment "Test failed" -Message "Before/After results do not match as expected"
		} else{
			if ($expectedChanges){
				$sMessage = "Trades validated "
				if (($AmendUTI) -or ($AmendStrategy) -or ($AmendInternalBusinessUnit) -or ($AmendInternalContact) -or ($AmendInternalPortfolio) -or ($AmendSpread) -or ($AmendEfp) -or ($AmendSleeve)){
					$sMessage += "and amended "
				}
				$sMessage += "successfully"
				New-TestResult -Passed $true -Comment "Test passed" -Message $sMessage
			}else{
				New-TestResult -Passed $true -Comment "Test passed" -Message "Trade not validated, values not changed"
			}
		}
    #
}

Function buildSQL{
	$SQL = "select 
	at.deal_tracking_num as DealNum, 
    at.reference,
	at.tran_num as TranNum, 
	ts.name as Status, 
	p.short_name as InternalBusinessUnit, 
	at.version_number as Version,
	pers.name as InternalContact,
	pfolio.name as InternalPortfolio, 
	stg.value as Strategy, 
	uti.value as UTI,
	case when efp.value = 'Yes' then 'Yes' else 'No' end as EFP,
	case when sle.value = 'Sleeve' then 'Yes' else 'No' end as Sleeve,
	case when spr.value = 'Yes' then 'Yes' else 'No' end as Spread
	from ab_Tran at
	left join ab_tran_info stg on stg.tran_num = at.tran_num and stg.type_id in (select type_id from tran_info_types where type_name = 'Strategy')
	left join ab_tran_info uti on uti.tran_num = at.tran_num and uti.type_id in (select type_id from tran_info_types where type_name = 'UTI')
	left join ab_tran_info efp on efp.tran_num = at.tran_num and efp.type_id in (select type_id from tran_info_types where type_name = 'EFP')
	left join ab_tran_info sle on sle.tran_num = at.tran_num and sle.type_id in (select type_id from tran_info_types where type_name = 'Sleeve')
	left join ab_tran_info spr on spr.tran_num = at.tran_num and spr.type_id in (select type_id from tran_info_types where type_name = 'Spread')
	left join trans_status ts on ts.trans_status_id = at.tran_status
	left join portfolio pfolio on at.internal_portfolio = pfolio.id_number
	left join party p on at.internal_bunit = p.party_id
	left join personnel pers on pers.id_number = at.internal_contact
	where at.tran_num in ("
	for ($i = 0; $i -lt $trans.count; $i++){
		$SQL += $trans[$i]
		if ($i -lt ($trans.count - 1)){
			$SQL += ","
		}
	}
	$SQL += ") order by at.tran_num"
	return $SQL
}

# Mainline
try {
    Initialise
    Main    
}

finally {
    # Do any clear down here as appropriate
    # When an Endur connection has been made, then $RunMode will be set
    if ($RunMode -eq "Auto") {Stop-EndurSession}
    Write-Host "`nDone $ThisTest"
}
