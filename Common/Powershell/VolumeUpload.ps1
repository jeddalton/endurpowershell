# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  
# |  $Global:ThisTest='VolumeUpload'
# |  $Global:VolFilename='Tests\FeeFlow\Data\Volume\volume.xlsx'
# |  $Global:VolSheet='16-12-2013'
# |  
# |  
# |  
# |  
# |  
# +-----------------------------------------------------------------------------------------------------------------------------------------------

# To assign variables and make their scope global; 
Function Initialise {
    
    cls
    # Runtime initialisation
    $Global:script     ="$TestAssetBaseDir\Common\Powershell\EndurUtils.ps1"
    . $script                # Loads functions into current scope
    ImportRunTimeModules

    EndurConn $ThisTest       # External function call to establish either a Manual (local) or Auto (batch) Endur session
}

# To import volumes into Endur from CSV file
Function Main {
    Write-Host "Uploading volume from file: $TestAssetBaseDir\$VolFilename"
    Write-Host "Sheet name is $VolSheet"

    Invoke-EndurScript -ScriptName "GGTP.Volume.GasVolumeUpload" -Parameters @{"filename"=($TestAssetBaseDir+"\"+$VolFilename);"sheet"=$VolSheet} -Verbose -ErrorAction Stop
    New-TestResult -Passed $True -Message "Volumes uploaded successfully"
}

# Mainline
try {
    Initialise
    Main
}

catch
{
    $ErrorMessage = $_.Exception.Message
    $FailedItem = $_.Exception.ItemName
    New-TestResult -Passed $false -Message $ErrorMessage   
    Break
}

finally {
    if ($RunMode -eq "Auto") {Stop-EndurSession}
    Write-Host "`nDone $ThisTest"
}
