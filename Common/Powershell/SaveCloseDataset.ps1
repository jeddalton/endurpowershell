# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  
# |  $Global:ThisTest='SaveCloseDataset'
# |  
# |  
# |  
# |  
# |  
# |  
# |  
# +-----------------------------------------------------------------------------------------------------------------------------------------------

# To assign variables and make their scope global; 
Function Initialise {
    cls
    # Runtime initialisation
    $Global:script     ="$TestAssetBaseDir\Common\Powershell\EndurUtils.ps1"
    . $script                # Loads functions into current scope
    $Global:script     ="$TestAssetBaseDir\Common\Powershell\GeneralUtils.ps1"
    . $script                # Loads functions into current scope
    ImportRunTimeModules

    EndurConn $ThisTest      # External function call to establish either a Manual (local) or Auto (batch) Endur session
    AssignEndurVariables     # External function call to set up variables read from USER_AppConfig e.g. Reporting DB connection string
}

Function Main {
# To run Endur workflow
    Write-Host "`nStart Workflow TEST Save MD"
    Invoke-EndurWorkflow -WorkflowName "TEST Save MD" -TimeoutInSeconds 600 -PollingFrequency 20 -Verbose

# To ETL from Endur to Reporting DB
    Write-Host "`nStart ETL Copy Month End"
    $request = @()
    $request += New-Object Object | Add-Member NoteProperty PackageName ($SSISPackageFolder + "\EOD Copy Month End") -PassThru | Add-Member NoteProperty QueueName "EOD.CopyMonthEnd" -PassThru | Add-Member NoteProperty JobKey "EOD Copy Month End" -PassThru
    RunSSISPackageV2 $SSISRunnerURL $request $SSISSqlServer $Environment -1 @{"Endur"="$EndurDBConnString";"Reporting"="$ReportingDBConnString"} $null
}

# Mainline
try {
    Initialise
    Main
}

finally {
    if ($RunMode -eq "Auto") {Stop-EndurSession}
    Write-Host "`nDone $ThisTest"
}
