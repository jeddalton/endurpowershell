# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  
# |  $Global:ThisTest='TradeCancel'
# |  $Global:TradeCancelFile='Tests\CumulativeValuation\Data\20131031_DealCancellations.csv'
# |  
# |  
# |  
# |  
# |  
# +-----------------------------------------------------------------------------------------------------------------------------------------------

# To assign variables and make their scope global; 
Function Initialise {
    
    cls
    # Runtime initialisation
    $Global:script     ="$TestAssetBaseDir\Common\Powershell\EndurUtils.ps1"
    . $script                # Loads functions into current scope
    ImportRunTimeModules

    EndurConn $ThisTest      # External function call to establish either a Manual (local) or Auto (batch) Endur session

    $Global:TradeCancelFile = "$TestAssetBaseDir\$TradeCancelFile"
}

# To import trade Cancelments into Endur from CSV file
Function Main {
    CancelDeals -Filepath $TradeCancelFile -Verbose
    New-TestResult -Passed $True -Message "Cancelled all deals in" -Comment "$TradeCancelFile"
}

# Mainline
try {
    Initialise
    Main
}

finally {
    if ($RunMode -eq "Auto") {Stop-EndurSession}
    Write-Host "`nDone $ThisTest"
}
