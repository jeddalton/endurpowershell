# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  RECORD What the test is doing / points of interest
# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  
# |  $Global:ThisTest='ThisTestName'
# |  
# |  
# |  
# |  
# |  
# |  
# +-----------------------------------------------------------------------------------------------------------------------------------------------

# To assign variables and make their scope global; 
Function Initialise {

    cls
    # Runtime initialisation
    $Global:script     ="$TestAssetBaseDir\Common\Powershell\GeneralUtils.ps1"
    . $script                # Loads functions into current scope
    ImportRunTimeModules     # Imports various cmdlets, registers SQL snapins, etc.           
}

# Put the main test processing into this function
Function Main {
    Write-Host "Doing processing"

    $sqlFile = Join-Path $TestAssetBaseDir $sqlFile
    Write-Host "`nSQL file is: $sqlFile"

    $DatabaseServer = ((Get-Variable -Name $DbServerName).value)
    $Databasename   = ((Get-Variable -Name $DatabaseName).value)

    $ExcelWb = Join-Path $TestAssetBaseDir $ExcelWb
    Write-Host "`nExpected results file is: $ExcelWb"

    $Master = Get-ExcelObject -Path $ExcelWb -WorksheetName $ExcelWs

    Check-DbVsExcel -ServerName $DatabaseServer -DbName $DatabaseName -Master $Master -sqlfile $sqlFile -ExcludeKeys $ExcludeKeys -CompKeys $CompKeys -SwapVar $subst    
}

# Mainline
try {
    Initialise
    Main
}

finally {    
    Write-Host "`nDone"
}