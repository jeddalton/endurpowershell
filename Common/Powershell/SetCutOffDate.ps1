# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  
# |  $Global:SetCutOffDateSql='\Regression\DealLifeCycle\SQL\SetCutOffDate.sql'
# |  
# |  
# |  
# |  
# |  
# +-----------------------------------------------------------------------------------------------------------------------------------------------

# To assign variables and make their scope global; 
Function Initialise {

    cls
    # Runtime initialisation
    $Global:script     ="$TestAssetBaseDir\Common\Powershell\EndurUtils.ps1"
    . $script                # Loads functions into current scope
    ImportRunTimeModules
}

# To drop trades from view that have delivered on or before $CutOffDate
Function SetCutOffDate {
    Write-Host "`nDrop trades from view that have delivered on or before $CutOffDate"
    $SetCutOffDatePath=Join-Path $TestAssetBaseDir $SetCutOffDateSql
    Invoke-Sqlcmd -ServerInstance $ReportingServerName -Database $ReportingDatabaseName -InputFile $SetCutOffDatePath -Verbose -ErrorAction Stop
}

Function Main {
    SetCutOffDate
}

# Mainline
try {
    Initialise
    Main
}

finally {
    Write-Host "`nDone SetCutOffDate"
}
