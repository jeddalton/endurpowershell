# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  
# | $Global:DealRef='EMIR_D1_OIL_FXO1'
# |  
# |  
# |  
# |  
# |  
# |  
# |  
# +-----------------------------------------------------------------------------------------------------------------------------------------------

# To assign variables and make their scope global; 
Function Initialise {

    cls
    # Runtime initialisation
    $Global:script = "$TestAssetBaseDir\Common\Powershell\EndurUtils.ps1"
    . $script                # Loads functions into current scope
    $Global:script = "$TestAssetBaseDir\Common\Powershell\GeneralUtils.ps1"
    . $script                # Loads functions into current scope
    ImportRunTimeModules
        
    # Script variables
    EndurConn $DealRef       # External function call to establish either a Manual (local) or Auto (batch) Endur session
}

# To exercise an option by validated deal reference
Function Exercise {
    Write-Host "`nOptionExercise: $DealRef"
    $trans = Get-EndurQueryResult -Fields @{"Transaction.Tran Type"="Trading"; "Transaction.Tran Status"="Validated"; "Transaction.Asset Type"="Trading";
                                            "Transaction.Ref"="$DealRef";
    } 
    Get-EndurEvent -TransactionId $trans -EventTypes "Exercise" | ForEach-Object { $_.EventNum } | Invoke-EndurOptionExercise -Verbose
}

# Mainline
try {
    Initialise               # Assign variables
    Exercise                 # Exercise $DealRef trades
}

finally {
    if ($RunMode -eq "Auto") {Stop-EndurSession}
    $Error[0].InnerException.StackTrace
    Write-Host "`nOptionExercise: $DealRef - Done"
}
