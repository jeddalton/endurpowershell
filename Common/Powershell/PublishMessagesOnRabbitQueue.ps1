﻿# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  Purpose: Publish the xml messages on Rabbit Exchange/queue.
# |  Respective environmet configuration script(example - FT7.ps1) will need to be set as startup script in test suite runner.
# +-----------------------------------------------------------------------------------------------------------------------------------------------

Function Initialise {    
 
   #Running the Rabbit Variables script(Rabbit Variableslike Exchange, Server etc)

	 $RabbitVariables				 = Join-Path $TestAssetBaseDir "Common\EnvVariables\RabbitVariables.ps1"
	.$RabbitVariables
 
    $Global:Xmlfilelocation        = Join-Path $TestAssetBaseDir "Regression\SuperTigerEndurTests\Data\XMLMessagesToEndur"
    
    
  #Displaying Rabbit Exchange/port details etc
    Write-Host "Using RabbitHost:" $RabbitString " Exchange: " $RabbitExchangeSTSTimeSeriesIncoming  " Queue: " $RabbitQueueSTSTimeSeriesIncoming
    Write-Host $RabbitExchangeSTSIncomingTimeSeries
 
}

Function Main {

     # Publishing each file message to Rabbit exchange/queue
       # if(Test-Path $Xmlfilelocation\*.xml)
         if((Test-Path $Xmlfilelocation\*.xml) -eq $false) 
         {
         Write-Host "Files does not exist in the respective folder"
         }
       
       foreach ($file in get-ChildItem $Xmlfilelocation\*.xml) {
        Write-Host "Publishing Message" $file.Name 
       $result = Publish-RabbitMQMessage -ExchangeName $RabbitExchangeSTSTimeSeriesIncoming -Message $file -HostName $RabbitHost -Password $RabbitPwd -Port $RabbitComPort  -UserName $RabbitUser
        Write-Host "Message got Published successfully" ($file.Name)   
    

        }
        
      Start-Sleep -s 45
   
        }




try {
    Initialise
    Main    
}
finally {
    # Do any clear down here as appropriate
    Get-Process -name PowerShellTf | Stop-Process
    Write-Host "Done $ThisTest"
}