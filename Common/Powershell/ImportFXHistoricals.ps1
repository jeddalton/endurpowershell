# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  RECORD What the test is doing / points of interest
# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  
# |  $Global:ThisTest='ThisTestName'
# |  
# |  
# |  
# |  
# |  
# |  
# +-----------------------------------------------------------------------------------------------------------------------------------------------

# To assign variables and make their scope global; 
Function Initialise {

    cls
    # Runtime initialisation
    $Global:script     ="$TestAssetBaseDir\Common\Powershell\EndurUtils.ps1"
    . $script                # Loads functions into current scope
    ImportRunTimeModules     # Imports various cmdlets, registers SQL snapins, etc.
	
    
    # Make Endur connection    
    EndurConn $ThisTest      # External function call to establish either a Manual (local) or Auto (batch) Endur session            
}

# Put the main test processing into this function
Function Main {    
    Write-Host "Importing FX Historicals from spreadsheet"
		
	
    $fxRates = Get-ExcelObject -Path (Join-Path $TestAssetBaseDir $ExcelWb) -WorksheetName $ExcelSheet
    
    
    Import-EndurHistoricalFxRates -Input $fxRates 

    
    New-TestResult -Passed $true -Message "Passed" -Comment "FX Historical rates imported successfully"

}


# Mainline
try {
    Initialise
    Main      
}

finally {
    if ($RunMode -eq "Auto") {Stop-EndurSession}
    Write-Host "`nDone $ThisTest"
}
