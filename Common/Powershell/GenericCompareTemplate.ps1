# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  This template is to be used for doing any kind of Endur data comparison
# |  It relies upon a suite file that sets variables as below
# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  
# |  <Variable Name="ThisTest">BackOffice</Variable>			
# |  <Variable Name="ERFile">\Tests\BackOffice\ExpectedResults\Tax\ER_TaxAssignment.xlsx</Variable>
# |  <Variable Name="ERSheet">TaxAssignment</Variable>
# |  <Variable Name="CompSQL">\Tests\BackOffice\SQL\GetTaxAssignments.sql</Variable>
# |  <Variable Name="CompKeys">Reference</Variable>
# |  <Variable Name="ExcludeKeys">TranNum|DealNum</Variable>			
# |  
# +-----------------------------------------------------------------------------------------------------------------------------------------------

# To assign variables and make their scope global; 
Function Initialise {

    cls
    # Runtime initialisation
    $Global:script     ="$TestAssetBaseDir\Common\Powershell\Scripts\EndurUtils.ps1"
    . $script                # Loads functions into current scope
    ImportRunTimeModules     # Imports various cmdlets, registers SQL snapins, etc.
    
    # Make Endur connection    
    EndurConn $ThisTest      # External function call to establish either a Manual (local) or Auto (batch) Endur session            
}

# Put the main test processing into this function
Function Main {
    Write-Host "Doing processing"
    # Main processing here
    #
    #
    #
    
}

# To check results match with expected result sheet - Manual: Display comparison results; Auto: make comparison results available for subsequent catch
Function CheckResults {

    # Set path to expected results sheet
    $ERFilename =Join-Path $TestAssetBaseDir $ERFile
    Write-Host "`n0020 - Expected results file is: $ERFilename"
    
    $sqlfile    =Join-Path $TestAssetBaseDir $CompSQL
    Write-Host "`n0025 - SQL file is: $sqlfile"
    $sql        =[System.IO.File]::ReadAllText($sqlfile)
    
    $exclude    =$ExcludeKeys.split("|")
    $keys       =$CompKeys.split("|")
    Write-Host "`n0030 - Comparison keys: " $CompKeys
    Write-Host "`n0030 - Exclude keys: " $ExcludeKeys
  
    
    Write-Host "`n0030 - Extract and compare with Master"
    if ($RunMode -eq "Manual") {
        $result = @(Compare-Data -LeftConnectionString $EndurConnectionString -RightConnectionString $ERFilename -LeftQuery $sql -RightQuery $ERSheet -Excludes $exclude -EmptyStringIsNull -Keys $keys -Pseudo "Description" -CommandTimeout 200)
        $result | Convert-CompareResultToTestResult -ColumnSeparator "," -Comment { $args[0].Right.Description } | Out-GridView
    }
    else {
        $result = @(Compare-Data -LeftConnectionString $EndurConnectionString -RightConnectionString $ERFilename -LeftQuery $sql -RightQuery $ERSheet -Excludes $exclude -EmptyStringIsNull -Keys $keys -Pseudo "Description" -CommandTimeout 200)
        $result | Convert-CompareResultToTestResult -ColumnSeparator "," -Comment { $args[0].Right.Description }
    }
    
    $Global:Pass = "TRUE"
}


# Mainline
try {
    Initialise
    Main    
    CheckResults
}

finally {
    if ($RunMode -eq "Auto") {Stop-EndurSession}
    Write-Host "`nDone $ThisTest"
}
