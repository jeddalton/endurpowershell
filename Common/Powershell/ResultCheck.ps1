# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  
# |  $Global:ThisTest='DealLifeCycle'
# |  $Global:Date1='20131030'
# |  $Global:TPRef='MiniLoad'
# |  $Global:IntBU='GAS_UK'
# |  $Global:InsType='COMM-PHYS'
# |  
# |  
# |  
# +-----------------------------------------------------------------------------------------------------------------------------------------------

# To assign variables and make their scope global; 
Function Initialise {

    cls
    # Runtime initialisation
    $Global:script     ="$TestAssetBaseDir\Common\Powershell\EndurUtils.ps1"
    . $script                # Loads functions into current scope
    ImportRunTimeModules

    # Script variables
    $Global:ERFilename =("$TestAssetBaseDir\Regression\$ThisTest"+"\ExpectedResults\ER_$ThisTest"+"_$Date1"+".xlsx")
    $Global:exclude =@("Deal Number","Tran Number","Fee Cash Trade Deal Number")
    $Global:keys       =@("reference","Scenario","Deal Side number","Profile Sequence Number","Ins Source ID","Portfolio Name","Cash flow Type","Broker Fee Type","Start Date","Row Status")

    AssignEndurVariables     # External function call to set up variables read from USER_AppConfig e.g. Reporting DB connection string
}

# To check results match with expected result sheet - Manual: Display comparison results; Auto: make comparison results available for subsequent catch
Function Main {
    Write-Host "`n0090 - Extract and compare with Master"
    $sql = ReturnEODSQL ("$TestAssetBaseDir\Regression\$ThisTest"+"\SQL\vwFactPnLDetailErp.sql") $Date1 ($TPRef+"%") $IntBU $InsType
    $result = @(Compare-Data -LeftConnectionString $ReportingDBConnStringShort -LeftQuery $sql -RightConnectionString $ERFilename -RightQuery $Date1 -RightFilter @{"intBusinessUnit"=$IntBU; "instrumentType"=$InsType; "tpref"=$TPRef} -Excludes $exclude -EmptyStringIsNull -Keys $keys -Pseudo "Description")
    
    if ($RunMode -eq "Manual") {
        $result | Convert-CompareResultToTestResult -ColumnSeparator "," -Comment { $args[0].Right.Description } | Out-GridView
    }
    else {
         $scriptBlock = {
            Param
            (
              $compareResult
            )
            ("Keys: " + [String]::Join(",", $compareResult.KeyValues))
        }
        $result | Convert-CompareResultToTestResult -ColumnSeparator "," -Comment $scriptBlock
    }
    $Global:Pass = "TRUE"
}

# Mainline
try {
    Initialise
    Main
}

finally {
    Write-Host "`nDone $ThisTest"
}
