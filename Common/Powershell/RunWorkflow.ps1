# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  
# |  $Global:ThisTest='RunWorkflow'
# |  $Global:WorkflowName='TEST 500 Batch Simulation ETL'
# |  $Global:Async='N'
# |  
# |  
# |  
# |  
# |  
# +-----------------------------------------------------------------------------------------------------------------------------------------------

# To assign variables and make their scope global; 
Function Initialise {
    
    cls
    # Runtime initialisation
    $Global:script     ="$TestAssetBaseDir\Common\Powershell\EndurUtils.ps1"
    . $script                # Loads functions into current scope
    ImportRunTimeModules
        
    EndurConn $ThisTest      # External function call to establish either a Manual (local) or Auto (batch) Endur session    
}

# To run the batch workflow
Function Main {

    if ($Async -eq 'Y') {
        Write-Host "`nProcessing $WorkflowName Async"
        Invoke-EndurWorkflow -Workflowname  $WorkflowName -Verbose -Async
        New-TestResult -Passed $True -Message "Workflow $Workflowname running" -Comment ""
    }
    else {
        Write-Host "`nProcessing $WorkflowName"
        if ($TimeOut -eq $null)  {$TimeOut = 600}
        if ($PollFreq -eq $null) {$PollFreq = 20}
        Invoke-EndurWorkflow -Workflowname  $WorkflowName -TimeoutInSeconds $TimeOut -PollingFrequency $PollFreq -Verbose
        New-TestResult -Passed $True -Message "Workflow $Workflowname succeeeded" -Comment "Workflow $Workflowname succeeeded"
    }
}

# Mainline
try {
    Initialise
    Main
}

finally {
    if ($RunMode -eq "Auto") {Stop-EndurSession}
    Write-Host "Done $ThisTest"
}
