# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  
# |  $Global:ThisTest='RunETL'
# |  $Global:Date1='30-Oct-2013'
# |  
# |  
# |  
# |  
# |  
# |  
# +-----------------------------------------------------------------------------------------------------------------------------------------------

# To assign variables and make their scope global;
Function Initialise {
    cls
    # Runtime initialisation
   $Global:script     ="$TestAssetBaseDir\Common\Powershell\GeneralUtils.ps1"
    . $script 
    $Global:script     ="$TestAssetBaseDir\Common\Powershell\EndurUtils.ps1"
   . $script                # Loads functions into current scope
                   # Loads functions into current scope
    ImportRunTimeModules    
    AssignEndurVariables     # External function call to set up variables read from USER_AppConfig e.g. Reporting DB connection string
    $Global:EodDate = [DateTime]$Date1
    Write-Host $SSISRunnerURL
    Write-Host $SSISSqlServer
    Write-Host $SSISPackageFolder
    Write-Host $EndurDBConnString
    Write-Host $ReportingDBConnString
}

Function Main {



# To Run Rebuild Statistics task against Reporting DB
    $request = @()
    $request += New-Object Object | Add-Member NoteProperty PackageName "Update Statistics" -PassThru | Add-Member NoteProperty QueueName "EOD.ETL.GAS_UK"       -PassThru | Add-Member NoteProperty JobKey "Rebuild Statistics"       -PassThru
    RunSSISPackageV3 -url $SSISRunnerURL -SSISPackageProject $SSISPackageProject -request $request -sqlServer $SSISSqlServer -environment $Environment -groupkey -1 -connectionstrings @{"Endur"="$EndurDBConnString";"Reporting"="$ReportingDBConnString"} -variables @{"EodDate"=$EodDate}
    
# To ETL from Endur to Reporting DB
    Write-Host "`nStart ETL RefData"
    $request = @()
    $request += New-Object Object | Add-Member NoteProperty PackageName ("EOD User-Security")       -PassThru | Add-Member NoteProperty QueueName "EOD.RefData"          -PassThru | Add-Member NoteProperty JobKey "EOD User-Security"       -PassThru
    $request += New-Object Object | Add-Member NoteProperty PackageName ("EOD Reference Data")      -PassThru | Add-Member NoteProperty QueueName "EOD.RefData"          -PassThru | Add-Member NoteProperty JobKey "EOD Reference Data"      -PassThru
    $request += New-Object Object | Add-Member NoteProperty PackageName ("EOD Prices and Curves")   -PassThru | Add-Member NoteProperty QueueName "EOD.RefData"          -PassThru | Add-Member NoteProperty JobKey "EOD Prices and Curves"   -PassThru
    RunSSISPackageV3 -url $SSISRunnerURL -SSISPackageProject $SSISPackageProject -request $request -sqlServer $SSISSqlServer -environment $Environment -groupkey -1 -connectionstrings @{"Endur"="$EndurDBConnString";"Reporting"="$ReportingDBConnString"} -variables @{"EodDate"=$EodDate}
    
    Write-Host "`nStart ETL Dimensions"
    $request = @()
    $request += New-Object Object | Add-Member NoteProperty PackageName ("DimPortfolio Import")     -PassThru | Add-Member NoteProperty QueueName "EOD.DimPortfolio"     -PassThru | Add-Member NoteProperty JobKey "DimPortfolio Import"     -PassThru
    $request += New-Object Object | Add-Member NoteProperty PackageName ("DimParty Import")         -PassThru | Add-Member NoteProperty QueueName "EOD.DimParty"         -PassThru | Add-Member NoteProperty JobKey "DimParty Import"         -PassThru
    $request += New-Object Object | Add-Member NoteProperty PackageName ("DimSimulation Import")    -PassThru | Add-Member NoteProperty QueueName "EOD.DimSimulation"    -PassThru | Add-Member NoteProperty JobKey "DimSimulation Import"    -PassThru
    $request += New-Object Object | Add-Member NoteProperty PackageName ("DimDealInfo Import")      -PassThru | Add-Member NoteProperty QueueName "EOD.DimDealInfo"      -PassThru | Add-Member NoteProperty JobKey "DimDealInfo Import"      -PassThru
    $request += New-Object Object | Add-Member NoteProperty PackageName ("DimCurrency Import")      -PassThru | Add-Member NoteProperty QueueName "EOD.DimCurrency"      -PassThru | Add-Member NoteProperty JobKey "DimCurrency Import"      -PassThru
    $request += New-Object Object | Add-Member NoteProperty PackageName ("DimCashflow Import")      -PassThru | Add-Member NoteProperty QueueName "EOD.DimCashflow"      -PassThru | Add-Member NoteProperty JobKey "DimCashflow Import"      -PassThru
    $request += New-Object Object | Add-Member NoteProperty PackageName ("DimInstrument Import")    -PassThru | Add-Member NoteProperty QueueName "EOD.DimInstrument"    -PassThru | Add-Member NoteProperty JobKey "DimInstrument Import"    -PassThru
    $request += New-Object Object | Add-Member NoteProperty PackageName ("Holiday Calendar Import") -PassThru | Add-Member NoteProperty QueueName "EOD.Holiday Calendar" -PassThru | Add-Member NoteProperty JobKey "Holiday Calendar Import" -PassThru
    #$request += New-Object Object | Add-Member NoteProperty PackageName ("Import EOD")              -PassThru | Add-Member NoteProperty QueueName "EOD.Ref.SetDate"      -PassThru | Add-Member  Business Unit "ALL"        -PassThru | Add-Member  Legal Entity "GMT CH - LE"        -PassThru| Add-Member  JobKey "EOD ETL Set EOD CH" -PassThru
    #$request += New-Object Object | Add-Member NoteProperty PackageName ("Import EOD")              -PassThru | Add-Member NoteProperty QueueName "EOD.Ref.SetDate"      -PassThru | Add-Member NoteProperty Business Unit "ALL"        -PassThru | Add-Member NoteProperty Legal Entity "GMT SINGAPORE - LE"        -PassThru| Add-Member NoteProperty JobKey "EOD ETL Set EOD SG" -PassThru

   # $request += New-Object Object | Add-Member NoteProperty PackageName ("Import EOD")              -PassThru | Add-Member NoteProperty QueueName "EOD ETL Set EOD US"        -PassThru | Add-Member NoteProperty JobKey "EOD ETL Set EOD US" -PassThru
   # $request += New-Object Object | Add-Member NoteProperty PackageName ("Import EOD")              -PassThru | Add-Member NoteProperty QueueName "EOD ETL Set EOD GGLNG"        -PassThru | Add-Member NoteProperty JobKey "EOD ETL Set EOD GGLNG" -PassThru
   # $request += New-Object Object | Add-Member NoteProperty PackageName ("Import EOD")              -PassThru | Add-Member NoteProperty QueueName "EOD ETL Set EOD CH"        -PassThru | Add-Member NoteProperty JobKey "EOD ETL Set EOD CH" -PassThru
    RunSSISPackageV3 -url $SSISRunnerURL -SSISPackageProject $SSISPackageProject -request $request -sqlServer $SSISSqlServer -environment $Environment -groupkey -1 -connectionstrings @{"Endur"="$EndurDBConnString";"Reporting"="$ReportingDBConnString"} -variables @{"EodDate"=$EodDate}


    
    Write-Host "`nStart Import EOD - one per Legal Entity"
    $request = @()
    $request += New-Object Object | Add-Member NoteProperty PackageName ("Import EOD")              -PassThru | Add-Member NoteProperty QueueName "EOD.ImportEOD"        -PassThru | Add-Member NoteProperty JobKey "Import EOD"              -PassThru
    RunSSISPackageV3 -url $SSISRunnerURL -SSISPackageProject $SSISPackageProject -request $request -sqlServer $SSISSqlServer -environment $Environment -groupkey -1 -connectionstrings @{"Endur"="$EndurDBConnString";"Reporting"="$ReportingDBConnString"} -variables @{"internalLegalEntity"="GMT LTD - LE";"internalBusinessUnit"="All";"EodDate"=$EodDate}
    



    Write-Host "`nStart Import EOD - 'GMT SINGAPORE - LE'"
    $request = @()
    $request += New-Object Object | Add-Member NoteProperty PackageName ("Import EOD")              -PassThru | Add-Member NoteProperty QueueName "EOD.ImportEOD.SG"        -PassThru | Add-Member NoteProperty JobKey "Import EOD"              -PassThru
    RunSSISPackageV3 -url $SSISRunnerURL -SSISPackageProject $SSISPackageProject -request $request -sqlServer $SSISSqlServer -environment $Environment -groupkey -1 -connectionstrings @{"Endur"="$EndurDBConnString";"Reporting"="$ReportingDBConnString"} -variables @{"internalLegalEntity"="GMT SINGAPORE - LE";"internalBusinessUnit"="All";"EodDate"=$EodDate}
    
    Write-Host "`nStart ETL GAS_UK"
    $request = @()
    $request += New-Object Object | Add-Member NoteProperty PackageName ("EOD Deals")               -PassThru | Add-Member NoteProperty QueueName "EOD.ETL.GAS_UK"       -PassThru | Add-Member NoteProperty JobKey "EOD Deals"               -PassThru
    $request += New-Object Object | Add-Member NoteProperty PackageName ("EOD Simulations")         -PassThru | Add-Member NoteProperty QueueName "EOD.ETL.GAS_UK"       -PassThru | Add-Member NoteProperty JobKey "EOD Simulations"         -PassThru
    RunSSISPackageV3 -url $SSISRunnerURL -SSISPackageProject $SSISPackageProject -request $request -sqlServer $SSISSqlServer -environment $Environment -groupkey -1 -connectionstrings @{"Endur"="$EndurDBConnString";"Reporting"="$ReportingDBConnString"} -variables @{"internalLegalEntity"="GMT LTD - LE";"internalBusinessUnit"="GAS_UK";"EodDate"=$EodDate}
    
    $request = @()
    $request += New-Object Object | Add-Member NoteProperty PackageName ("FactPnLDetail Import")    -PassThru | Add-Member NoteProperty QueueName "EOD.ETL.GAS_UK"       -PassThru | Add-Member NoteProperty JobKey "FactPnLDetail Import"    -PassThru
    RunSSISPackageV3 -url $SSISRunnerURL -SSISPackageProject $SSISPackageProject -request $request -sqlServer $SSISSqlServer -environment $Environment -groupkey -1 -connectionstrings @{"Endur"="$EndurDBConnString";"Reporting"="$ReportingDBConnString"} -variables @{"internalLegalEntity"="GMT LTD - LE";"internalBusinessUnit"="GAS_UK";"EodDate"=$EodDate}
    $request = @()
    $request += New-Object Object | Add-Member NoteProperty PackageName ("FactPnLDetail Daily")    -PassThru | Add-Member NoteProperty QueueName "EOD.ETL.GAS_UK"       -PassThru | Add-Member NoteProperty JobKey "FactPnLDetail Daily"    -PassThru
    RunSSISPackageV3 -url $SSISRunnerURL -SSISPackageProject $SSISPackageProject -request $request -sqlServer $SSISSqlServer -environment $Environment -groupkey -1 -connectionstrings @{"Endur"="$EndurDBConnString";"Reporting"="$ReportingDBConnString"} -variables @{"internalLegalEntity"="GMT LTD - LE";"internalBusinessUnit"="GAS_UK";"EodDate"=$EodDate}
    
    Write-Host "`nStart ETL FX_UK"
    $request = @()
    $request += New-Object Object | Add-Member NoteProperty PackageName ("EOD Deals")               -PassThru | Add-Member NoteProperty QueueName "EOD.ETL.FX_UK"       -PassThru | Add-Member NoteProperty JobKey "EOD Deals"               -PassThru
    $request += New-Object Object | Add-Member NoteProperty PackageName ("EOD Simulations")         -PassThru | Add-Member NoteProperty QueueName "EOD.ETL.FX_UK"       -PassThru | Add-Member NoteProperty JobKey "EOD Simulations"         -PassThru
    RunSSISPackageV3 -url $SSISRunnerURL -SSISPackageProject $SSISPackageProject -request $request -sqlServer $SSISSqlServer -environment $Environment -groupkey -1 -connectionstrings @{"Endur"="$EndurDBConnString";"Reporting"="$ReportingDBConnString"} -variables @{"internalLegalEntity"="GMT LTD - LE";"internalBusinessUnit"="FX_UK";"EodDate"=$EodDate}
    $request = @()
    $request += New-Object Object | Add-Member NoteProperty PackageName ("FactPnLDetail Import")    -PassThru | Add-Member NoteProperty QueueName "EOD.ETL.FX_UK"       -PassThru | Add-Member NoteProperty JobKey "FactPnLDetail Import"    -PassThru
    RunSSISPackageV3 -url $SSISRunnerURL -SSISPackageProject $SSISPackageProject -request $request -sqlServer $SSISSqlServer -environment $Environment -groupkey -1 -connectionstrings @{"Endur"="$EndurDBConnString";"Reporting"="$ReportingDBConnString"} -variables @{"internalLegalEntity"="GMT LTD - LE";"internalBusinessUnit"="FX_UK";"EodDate"=$EodDate}
    $request = @()
    $request += New-Object Object | Add-Member NoteProperty PackageName ("FactPnLDetail Daily")    -PassThru | Add-Member NoteProperty QueueName "EOD.ETL.FX_UK"       -PassThru | Add-Member NoteProperty JobKey "FactPnLDetail Daily"    -PassThru
    RunSSISPackageV3 -url $SSISRunnerURL -SSISPackageProject $SSISPackageProject -request $request -sqlServer $SSISSqlServer -environment $Environment -groupkey -1 -connectionstrings @{"Endur"="$EndurDBConnString";"Reporting"="$ReportingDBConnString"} -variables @{"internalLegalEntity"="GMT LTD - LE";"internalBusinessUnit"="FX_UK";"EodDate"=$EodDate}

    Write-Host "`nStart ETL OIL_UK"
    $request = @()
    $request += New-Object Object | Add-Member NoteProperty PackageName ("EOD Deals")               -PassThru | Add-Member NoteProperty QueueName "EOD.ETL.OIL_UK"       -PassThru | Add-Member NoteProperty JobKey "EOD Deals"               -PassThru
    $request += New-Object Object | Add-Member NoteProperty PackageName ("EOD Simulations")         -PassThru | Add-Member NoteProperty QueueName "EOD.ETL.OIL_UK"       -PassThru | Add-Member NoteProperty JobKey "EOD Simulations"         -PassThru
    RunSSISPackageV3 -url $SSISRunnerURL -SSISPackageProject $SSISPackageProject -request $request -sqlServer $SSISSqlServer -environment $Environment -groupkey -1 -connectionstrings @{"Endur"="$EndurDBConnString";"Reporting"="$ReportingDBConnString"} -variables @{"internalLegalEntity"="GMT LTD - LE";"internalBusinessUnit"="OIL_UK";"EodDate"=$EodDate}
    $request = @()
    $request += New-Object Object | Add-Member NoteProperty PackageName ("FactPnLDetail Import")    -PassThru | Add-Member NoteProperty QueueName "EOD.ETL.OIL_UK"       -PassThru | Add-Member NoteProperty JobKey "FactPnLDetail Import"    -PassThru
    RunSSISPackageV3 -url $SSISRunnerURL -SSISPackageProject $SSISPackageProject -request $request -sqlServer $SSISSqlServer -environment $Environment -groupkey -1 -connectionstrings @{"Endur"="$EndurDBConnString";"Reporting"="$ReportingDBConnString"} -variables @{"internalLegalEntity"="GMT LTD - LE";"internalBusinessUnit"="OIL_UK";"EodDate"=$EodDate}
    $request = @()
    $request += New-Object Object | Add-Member NoteProperty PackageName ("FactPnLDetail Daily")    -PassThru | Add-Member NoteProperty QueueName "EOD.ETL.OIL_UK"       -PassThru | Add-Member NoteProperty JobKey "FactPnLDetail Daily"    -PassThru
    RunSSISPackageV3 -url $SSISRunnerURL -SSISPackageProject $SSISPackageProject -request $request -sqlServer $SSISSqlServer -environment $Environment -groupkey -1 -connectionstrings @{"Endur"="$EndurDBConnString";"Reporting"="$ReportingDBConnString"} -variables @{"internalLegalEntity"="GMT LTD - LE";"internalBusinessUnit"="OIL_UK";"EodDate"=$EodDate}
    
    Write-Host "`nStart ETL COAL_UK"
    $request = @()
    $request += New-Object Object | Add-Member NoteProperty PackageName ("EOD Deals")               -PassThru | Add-Member NoteProperty QueueName "EOD.ETL.COAL_UK"       -PassThru | Add-Member NoteProperty JobKey "EOD Deals"               -PassThru
    $request += New-Object Object | Add-Member NoteProperty PackageName ("EOD Simulations")         -PassThru | Add-Member NoteProperty QueueName "EOD.ETL.COAL_UK"       -PassThru | Add-Member NoteProperty JobKey "EOD Simulations"         -PassThru
    RunSSISPackageV3 -url $SSISRunnerURL -SSISPackageProject $SSISPackageProject -request $request -sqlServer $SSISSqlServer -environment $Environment -groupkey -1 -connectionstrings @{"Endur"="$EndurDBConnString";"Reporting"="$ReportingDBConnString"} -variables @{"internalLegalEntity"="GMT LTD - LE";"internalBusinessUnit"="COAL_UK";"EodDate"=$EodDate}
    $request = @()
    $request += New-Object Object | Add-Member NoteProperty PackageName ("FactPnLDetail Import")    -PassThru | Add-Member NoteProperty QueueName "EOD.ETL.COAL_UK"       -PassThru | Add-Member NoteProperty JobKey "FactPnLDetail Import"    -PassThru
    RunSSISPackageV3 -url $SSISRunnerURL -SSISPackageProject $SSISPackageProject -request $request -sqlServer $SSISSqlServer -environment $Environment -groupkey -1 -connectionstrings @{"Endur"="$EndurDBConnString";"Reporting"="$ReportingDBConnString"} -variables @{"internalLegalEntity"="GMT LTD - LE";"internalBusinessUnit"="COAL_UK";"EodDate"=$EodDate}
    $request = @()
    $request += New-Object Object | Add-Member NoteProperty PackageName ("FactPnLDetail Daily")    -PassThru | Add-Member NoteProperty QueueName "EOD.ETL.COAL_UK"       -PassThru | Add-Member NoteProperty JobKey "FactPnLDetail Daily"    -PassThru
    RunSSISPackageV3 -url $SSISRunnerURL -SSISPackageProject $SSISPackageProject -request $request -sqlServer $SSISSqlServer -environment $Environment -groupkey -1 -connectionstrings @{"Endur"="$EndurDBConnString";"Reporting"="$ReportingDBConnString"} -variables @{"internalLegalEntity"="GMT LTD - LE";"internalBusinessUnit"="COAL_UK";"EodDate"=$EodDate}

    Write-Host "`nStart ETL POWER_UK"
    $request = @()
    $request += New-Object Object | Add-Member NoteProperty PackageName ("EOD Deals")               -PassThru | Add-Member NoteProperty QueueName "EOD.ETL.POWER_UK"       -PassThru | Add-Member NoteProperty JobKey "EOD Deals"               -PassThru
    $request += New-Object Object | Add-Member NoteProperty PackageName ("EOD Simulations")         -PassThru | Add-Member NoteProperty QueueName "EOD.ETL.POWER_UK"       -PassThru | Add-Member NoteProperty JobKey "EOD Simulations"         -PassThru
    RunSSISPackageV3 -url $SSISRunnerURL -SSISPackageProject $SSISPackageProject -request $request -sqlServer $SSISSqlServer -environment $Environment -groupkey -1 -connectionstrings @{"Endur"="$EndurDBConnString";"Reporting"="$ReportingDBConnString"} -variables @{"internalLegalEntity"="GMT LTD - LE";"internalBusinessUnit"="POWER_UK";"EodDate"=$EodDate}
    $request = @()
    $request += New-Object Object | Add-Member NoteProperty PackageName ("FactPnLDetail Import")    -PassThru | Add-Member NoteProperty QueueName "EOD.ETL.POWER_UK"       -PassThru | Add-Member NoteProperty JobKey "FactPnLDetail Import"    -PassThru
    RunSSISPackageV3 -url $SSISRunnerURL -SSISPackageProject $SSISPackageProject -request $request -sqlServer $SSISSqlServer -environment $Environment -groupkey -1 -connectionstrings @{"Endur"="$EndurDBConnString";"Reporting"="$ReportingDBConnString"} -variables @{"internalLegalEntity"="GMT LTD - LE";"internalBusinessUnit"="POWER_UK";"EodDate"=$EodDate}
    $request = @()
    $request += New-Object Object | Add-Member NoteProperty PackageName ("FactPnLDetail Daily")    -PassThru | Add-Member NoteProperty QueueName "EOD.ETL.POWER_UK"       -PassThru | Add-Member NoteProperty JobKey "FactPnLDetail Daily"    -PassThru
    RunSSISPackageV3 -url $SSISRunnerURL -SSISPackageProject $SSISPackageProject -request $request -sqlServer $SSISSqlServer -environment $Environment -groupkey -1 -connectionstrings @{"Endur"="$EndurDBConnString";"Reporting"="$ReportingDBConnString"} -variables @{"internalLegalEntity"="GMT LTD - LE";"internalBusinessUnit"="POWER_UK";"EodDate"=$EodDate}


    Write-Host "`nStart ETL OIL_SG"
    $request = @()
    $request += New-Object Object | Add-Member NoteProperty PackageName ("EOD Deals")               -PassThru | Add-Member NoteProperty QueueName "EOD.ETL.OIL_SG"       -PassThru | Add-Member NoteProperty JobKey "EOD Deals"               -PassThru
    $request += New-Object Object | Add-Member NoteProperty PackageName ("EOD Simulations")         -PassThru | Add-Member NoteProperty QueueName "EOD.ETL.OIL_SG"       -PassThru | Add-Member NoteProperty JobKey "EOD Simulations"         -PassThru
    RunSSISPackageV3 -url $SSISRunnerURL -SSISPackageProject $SSISPackageProject -request $request -sqlServer $SSISSqlServer -environment $Environment -groupkey -1 -connectionstrings @{"Endur"="$EndurDBConnString";"Reporting"="$ReportingDBConnString"} -variables @{"internalLegalEntity"="GMT SINGAPORE - LE";"internalBusinessUnit"="OIL_SG";"EodDate"=$EodDate}
    $request = @()
    $request += New-Object Object | Add-Member NoteProperty PackageName ("FactPnLDetail Import")    -PassThru | Add-Member NoteProperty QueueName "EOD.ETL.OIL_SG"       -PassThru | Add-Member NoteProperty JobKey "FactPnLDetail Import"    -PassThru
    RunSSISPackageV3 -url $SSISRunnerURL -SSISPackageProject $SSISPackageProject -request $request -sqlServer $SSISSqlServer -environment $Environment -groupkey -1 -connectionstrings @{"Endur"="$EndurDBConnString";"Reporting"="$ReportingDBConnString"} -variables @{"internalLegalEntity"="GMT SINGAPORE - LE";"internalBusinessUnit"="OIL_SG";"EodDate"=$EodDate}
    $request = @()
    $request += New-Object Object | Add-Member NoteProperty PackageName ("FactPnLDetail Daily")    -PassThru | Add-Member NoteProperty QueueName "EOD.ETL.OIL_SG"       -PassThru | Add-Member NoteProperty JobKey "FactPnLDetail Daily"    -PassThru
    RunSSISPackageV3 -url $SSISRunnerURL -SSISPackageProject $SSISPackageProject -request $request -sqlServer $SSISSqlServer -environment $Environment -groupkey -1 -connectionstrings @{"Endur"="$EndurDBConnString";"Reporting"="$ReportingDBConnString"} -variables @{"internalLegalEntity"="GMT SINGAPORE - LE";"internalBusinessUnit"="OIL_SG";"EodDate"=$EodDate}



    Write-Host "`nStart ETL Materialise FactPnLDetail - all Legal Entities"
    $request = @()
    $request += New-Object Object | Add-Member NoteProperty PackageName ("EOD Materialise FactPnLDetail") -PassThru | Add-Member NoteProperty QueueName "EOD.MatFactPnLDetail" -PassThru | Add-Member NoteProperty JobKey "Mat FactPnLDetail"   -PassThru
    RunSSISPackageV3 -url $SSISRunnerURL -SSISPackageProject $SSISPackageProject -request $request -sqlServer $SSISSqlServer -environment $Environment -groupkey -1 -connectionstrings @{"Endur"="$EndurDBConnString";"Reporting"="$ReportingDBConnString"} -variables @{"EodDate"=$EodDate}


}

# Mainline
try {
    Initialise
    Main
}

finally {
    Write-Host "`nDone $ThisTest"
}
