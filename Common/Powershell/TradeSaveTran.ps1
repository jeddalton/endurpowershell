# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  
# |  $Global:ThisTest='TradeSaveTran'
# |  $Global:TradeSaveTranFile='Regression\EMIR\Data\day4_TempUTIUpdates.csv'
# |  
# |  
# |  
# |  
# +-----------------------------------------------------------------------------------------------------------------------------------------------

# To assign variables and make their scope global; 
Function Initialise {
    
    cls
    # Runtime initialisation
    $Global:script     ="$TestAssetBaseDir\Common\Powershell\EndurUtils.ps1"
    . $script                # Loads functions into current scope
    ImportRunTimeModules
    EndurConn $ThisTest      # External function call to establish either a Manual (local) or Auto (batch) Endur session
}

# To import tran info amendments into Endur from CSV file
Function Main {

    $Global:TradeSaveTranPath = $TestAssetBaseDir + "\" + $TradeSaveTranFile 
    SaveTran -Filepath $TradeSaveTranPath  -Verbose
}

# Mainline
try {
    Initialise
    Main
}

finally {
    if ($RunMode -eq "Auto") {Stop-EndurSession}
    Write-Host "`nDone $ThisTest"
}
