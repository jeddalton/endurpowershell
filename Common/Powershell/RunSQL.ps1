# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  RECORD What the test is doing / points of interest
# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  
# |  $Global:ThisTest='ThisTestName'
# |  
# |  
# |  
# |  
# |  
# |  
# +-----------------------------------------------------------------------------------------------------------------------------------------------

# To assign variables and make their scope global; 
Function Initialise {

    cls
    # Runtime initialisation
    $Global:script     ="$TestAssetBaseDir\Common\Powershell\EndurUtils.ps1"
    . $script                # Loads functions into current scope
    ImportRunTimeModules     # Imports various cmdlets, registers SQL snapins, etc.           
}

# Put the main test processing into this function
Function Main {
    Write-Host "Doing processing"
    $InputFile = Join-Path $TestAssetBaseDir $sqlFile

    Write-Host "SQL File is: $InputFile"

    $DatabaseServer = ((Get-Variable -Name $DbServerName).value)
    $Databasename   = ((Get-Variable -Name $DatabaseName).value)

    Write-Host "Database server is $DatabaseServer"
    Write-Host "Database name is $Databasename"
    Write-Host "Running SQL in file: $InputFile"
    
    try {
        Invoke-Sqlcmd -ServerInstance $DatabaseServer -Database $Databasename -InputFile $InputFile -QueryTimeout $LongTimeout -ErrorAction Stop
        New-TestResult -Passed $true -Comment "Script run successfully $InputFile" -Message "Script run successfully $InputFile"
    }
    catch {
        $ErrorMessage = $_.Exception.Message
        New-TestResult -Passed $false -Comment "Exception raised $ErrorMessage" -Message "Script run failure $InputFile"
    }    
    
}

# Mainline
try {
    Initialise
    Main
}

finally {    
    Write-Host "`nDone"
}