# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  RECORD What the test is doing / points of interest
# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  
# |  $Global:ThisTest='ThisTestName'
# |  
# |  
# |  
# |  
# |  
# |  
# +-----------------------------------------------------------------------------------------------------------------------------------------------

# To assign variables and make their scope global; 
Function Initialise {

    cls
    # Runtime initialisation
    $Global:script     ="$TestAssetBaseDir\Common\Powershell\EndurUtils.ps1"
    . $script                # Loads functions into current scope
    ImportRunTimeModules     # Imports various cmdlets, registers SQL snapins, etc.
	
    
    # Make Endur connection    
    EndurConn $ThisTest      # External function call to establish either a Manual (local) or Auto (batch) Endur session            
}

# Put the main test processing into this function
Function Main {    
    Write-Host "Importing Historical FX Rates from spreadsheet"
	
	#$fxRates = Get-ExcelObject -Path (Join-Path $TestAssetBaseDir $ExcelWb) -WorksheetName $ExcelSheet

	#Import-EndurHistoricalFxRates -Input $fxRates 

	#New-TestResult -Passed $true -Message "Passed" -Comment "Historical FX rates imported successfully"
	
	
	
	
    $data = Get-ExcelObject -Path (Join-Path $TestAssetBaseDir $ExcelWb) -WorksheetName $ExcelSheet
    $sql = Join-Path $TestAssetBaseDir $sqlFile
    
    foreach ($row in $data) {
       $vars = @("fx_rate_date='" + $row.fx_rate_date + "'";"currency_id=" + $row.currency_id;"reference_currency_id=" + $row.reference_currency_id;"fx_rate_bid=" + $row.fx_rate_bid;"fx_rate_mid=" + $row.fx_rate_offer;"fx_rate_offer=" + $row.fx_rate_offer;"data_set_type=" + $row.data_set_type;"currency_convention=" + $row.currency_convention;"personnel_id=" + $row.personnel_id)
       Invoke-Sqlcmd -Database $EndurDatabaseName -ServerInstance $EndurServerName -InputFile $sql -Variable $vars -AbortOnError
    }
    
    New-TestResult -Passed $true -Message "Passed" -Comment "Historical FX rates imported successfully"

}


# Mainline
try {
    Initialise
    Main      
}

finally {
    if ($RunMode -eq "Auto") {Stop-EndurSession}
    Write-Host "`nDone $ThisTest"
}
