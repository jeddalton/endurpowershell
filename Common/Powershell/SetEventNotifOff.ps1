﻿# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  
# |  $Global:ThisTest='DealLifeCycle_v3'
# |  $Global:EventNotifSql='SQL\SetEventNotifOff.sql'
# |  
# |  
# |  
# |  
# |  
# |  
# +-----------------------------------------------------------------------------------------------------------------------------------------------

# To assign variables and make their scope global; 
Function Initialise {

    cls
    # Runtime initialisation
    $Global:script     ="$TestAssetBaseDir\Common\Powershell\EndurUtils.ps1"
    . $script                # Loads functions into current scope
    ImportRunTimeModules

    EndurConn $ThisTest      # External function call to establish either a Manual (local) or Auto (batch) Endur session
}

Function Main {
    SetEventNotif
    New-TestResult -Passed $true -Message "EventNotifications Set" -Comment "from $EventNotifPath"
}

Function SetEventNotif {
    Write-Host "`nTurn Endur Event Notifications Off"
    $Global:EventNotifPath=Join-Path $TestAssetBaseDir $EventNotifSql

    Write-Host "DatabaseName is:" $EndurSession.DatabaseName
    #Invoke-Sqlcmd -ServerInstance $EndurSession.ServerName -Database $EndurSession.DatabaseName -InputFile $EventNotifPath -ErrorAction Stop
    Invoke-Sqlcmd -ServerInstance $EndurServerName -Database $EndurDatabaseName -InputFile $EventNotifPath -ErrorAction Stop
}

# Mainline
try {
    Initialise
    Main
}

finally {
    if ($RunMode -eq "Auto") {Stop-EndurSession}
    Write-Host "`nDone" $EventNotifPath
}
