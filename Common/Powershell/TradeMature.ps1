# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  
# |  $Global:ThisTest='TradeMature'
# |  $Global:TradeRefLike='CumVal_%_S4%'
# |  
# |  
# |  
# |  
# |  
# +-----------------------------------------------------------------------------------------------------------------------------------------------

# To assign variables and make their scope global; 
Function Initialise {
    
    cls
    # Runtime initialisation
    $Global:script     ="$TestAssetBaseDir\Common\Powershell\EndurUtils.ps1"
    . $script                # Loads functions into current scope
    ImportRunTimeModules

    EndurConn $ThisTest      # External function call to establish either a Manual (local) or Auto (batch) Endur session

    $Global:sql = 
    "SELECT at.deal_tracking_num,at.tran_num,at.version_number,at.tran_status,t.name as toolset " +
    "FROM ab_tran at " +
    "INNER JOIN toolsets t ON t.id_number=at.toolset " +
    "WHERE 1=1 " +
    "AND at.tran_status=3 " +
    "AND at.reference LIKE '$TradeRefLike'"
}

# To import trade amendments into Endur from CSV file
Function Main {
    Invoke-EndurScript -ScriptName "TESTUTIL_Trade_Mature" -Parameters @{"sql"=$sql} -Verbose
}

# Mainline
try {
    Initialise
    Main
}

finally {
    if ($RunMode -eq "Auto") {Stop-EndurSession}
    Write-Host "`nDone $ThisTest"
}
