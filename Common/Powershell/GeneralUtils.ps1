Function Global:SQLReplace {
    param ($SQLString, $ReplaceString)
    
    $data=$ReplaceString.split(":")
        
    $Find = $data[0]
    $Replace = $data[1]
    
    $SQLString = $SQLString.Replace($Find,$Replace)
    
    return $SQLString
}

Function Global:PublishRabbitData {
    
    param($RabbitString, $RabbitExchange, $RabbitQueue, $HighAvailabilityQ, $MessageDir)
    
    Write-Host($RabbitString)
    Write-Host($RabbitExchange)
    Write-Host($RabbitQueue)
    Write-Host($HighAvailabilityQ)
    Write-Host($MessageDir)

    Invoke-RabbitMessageQueuePublisher -RabbitString $RabbitString -Exchange $rabbitExchange -QueueName $RabbitQueue -MessageDir $MessageDir -HighAvailabilityQ $HighAvailabilityQ

}

Function Global:ImportRunTimeModules{

    # Check if we're running within the Test Framework
    if ($ExecutionContext.Host.Name -eq "PowerShellTf") {$Global:RunMode ="Auto"}  
    else {$Global:RunMode ="Manual"}

    RegisterSQLSnapIns
    
    # Check if required modules are imported and if not import them
	# Get correct version of EndurModule
	<#
	$cfg = get-content $EndurConfigFile
	$lookup = "SET OLF_BIN=E:\OpenLink\Endur_V14"
	$version = 0
	foreach($cf in $cfg){
		if($cf.StartsWith($lookup)){
			 [string]$version = $cf.Substring($cf.Length - 8,4)
		}
	}
	if($version -eq '1109'){
		$version = '2'
	}
	$EndurModule = 'EndurModule.' + $version
	#>

	$EndurModule = 'EndurModule.2'
	if (CheckModuleIsImported $EndurModule) {
		Write-Host 'GeneralUtils: Using EndurModule:' $EndurModule
		Import-Module $EndurModule
	}
    if (CheckModuleIsImported TestModule)   {Import-Module TestModule}
    if (CheckModuleIsImported RabbitModule) {Import-Module RabbitModule}
    if (CheckModuleIsImported SSISModule)   {Import-Module SSISModule}
    
    #Set standard global test variables
    $Global:Pass       ="FALSE"
    $Global:results    =New-Object System.Collections.ArrayList
}

Function Global:CheckModuleIsImported {
    param($ModuleName)
    if ((Get-Module -Name $ModuleName -ErrorAction SilentlyContinue) -ne $null )
    {
        return $false
    }
    else
    {
        return $true
    }
}        

Function Global:RegisterSQLSnapIns {
    Import-Module SqlPs
}

Function Global:RunSSISPackage {
    param($url, [string[]]$packageNames, $sqlServer, $environment, $queueName, $jobKey, [hashtable]$connectionStrings, [hashtable]$variables)

    if (CheckModuleIsImported SsisRunnerModule)  {Import-Module SsisRunnerModule}
    if (CheckModuleIsImported TestModule)        {Import-Module TestModule}
        
    $result = $packageNames | Start-SsisRunnerJob -Url  $url `
           -PackageSource "SqlServer"  `
           -SqlServer $sqlServer `
           -Environment $environment `
           -QueueName $queueName `
           -JobKey $jobKey `
           -GroupKey "-1" `
           -ConnectionStrings $connectionStrings `
           -Variables $variables

    Write-Host "Request IDs"
    $result | Select-Object -Property "RequestId" | ft

    Write-Host "Waiting for jobs to complete..."
    @($result | Wait-SsisRunnerJob -Url $url -Interval 20 -Verbose) | fl

    Write-Host "Job Status"
    $SSISResult = $result | Get-SsisRunnerJobStatus -Url $url

    foreach ($Result in $SSISResult) {
    $TestResult = Test-AreEqual -Actual $Result.Status -Expected "Success" -Comment ("Result of running " + $Result.PackageName) -Verbose
    $TestResult
    }
}

Function Global:RunSSISPackageV2 {
    param($url, [PSObject[]]$request, $sqlServer, $environment, $groupKey, [hashtable]$connectionStrings, [hashtable]$variables)
    
    if (CheckModuleIsImported SsisRunnerModule)  {Import-Module SsisRunnerModule}
    if (CheckModuleIsImported TestModule)        {Import-Module TestModule}
    
    $result = $request | Start-SsisRunnerJob -Url $url -Environment $environment -PackageSource "SqlServer" -SqlServer $sqlServer -ConnectionStrings $connectionStrings -Variables $variables -GroupKey $groupKey

    Write-Host "Request IDs"
    $result | Out-String

    Write-Host "Waiting for jobs to complete..."
    @($result | Wait-SsisRunnerJob -Url $url -Interval 20 -Verbose) | Out-String

    Write-Host "Job Status"
    $SSISResult = $result | Get-SsisRunnerJobStatus -Url $url

    foreach ($Result in $SSISResult) {
    $TestResult = Test-AreEqual -Actual $Result.Status -Expected "Success" -Comment ("Result of running " + $Result.PackageName) -Verbose
    $TestResult
    }
}

Function Global:RunSSISPackageV3 {
    param($url, $SSISPackageProject, [PSObject[]]$request, $sqlServer, $environment, $groupKey, [hashtable]$connectionStrings, [hashtable]$variables)
    
    if (CheckModuleIsImported SsisRunnerModule)  {Import-Module SsisRunnerModule}
    if (CheckModuleIsImported TestModule)        {Import-Module TestModule}
    Write-Host 'Url:' $url
    Write-Host 'environment:' $environment
    Write-Host 'SSISPackageProject:' $SSISPackageProject
    Write-Host 'PackageFolder:' $environment
    Write-Host 'sqlServer:' $sqlServer
    Write-Host 'connectionStrings:' $connectionStrings
    Write-Host 'variables:' $variables
    Write-Host 'groupKey:' $groupKey
    
    $result = $request | Start-SsisRunnerJob -Url $url -Environment $environment -PackageProject $SSISPackageProject -PackageFolder $environment -SqlServer $sqlServer -ConnectionStrings $connectionStrings -Variables $variables -GroupKey $groupKey 

    Write-Host "Request IDs"
    $result | Out-String

    Write-Host "Waiting for jobs to complete..."
    @($result | Wait-SsisRunnerJob -Url $url -Interval 20 -Verbose) | Out-String

    Write-Host "Job Status"
    $SSISResult = $result | Get-SsisRunnerJobStatus -Url $url

    foreach ($Result in $SSISResult) {
    $TestResult = Test-AreEqual -Actual $Result.Status -Expected "Success" -Comment ("Result of running " + $Result.PackageName) -Verbose
    $TestResult
    }
}

Function Global:Check-DbVsExcel {
    param($ServerName, $DbName, $Master, $sqlfile, $ExcludeKeys, $CompKeys,[array]$SwapVar)

    Write-Host $ServerName
    Write-Host $DbName


    if ($SwapVar -eq $null)
    {
        $SwapVar = @()
    }

    # Create hash table of arguments
    
    # Set path to expected results sheet    
    Write-Host "`nExpected results file is: $ExcelWb worksheet is: $ExcelWs"
    Write-Host "`nSQL File for comparison is: $sqlfile"    
    
    # Get database results for comparison
    # if we've got a substituion variable
    
    $sql        =[System.IO.File]::ReadAllText($sqlfile)

    write-host $sql
    write-host $SwapVar

    # Run the SQL
    $Res = @(Invoke-Sqlcmd -ServerInstance $ServerName -Database $DbName -Query $sql -Variable $SwapVar -QueryTimeout 200)

    
    # Define the keys and exclusions for the upcoming data compare 
    if ($ExcludeKeys -ne $null)
    {
        $exclude    = $ExcludeKeys.split("|")
    }
    else
    {
        $exclude = @()
    }

    $keys       = $CompKeys.split("|")   
   
    Write-Host "`nExclude keys: " $ExcludeKeys
    Write-Host "`nComparison keys: " $CompKeys
  
    # Do the actual data comparison
    Write-Host "`nExtract and compare with Master"
    if ($RunMode -eq "Manual") {

        $result = Compare-DataObject -Left $Res -Right $Master -ExcludeFields $exclude -NullValuesEqual -Keys $keys -MergeColumns -Epsilon 0.001
        $result | Convert-CompareObjectToTestResult | Out-GridView
    }
    else {
        $result = Compare-DataObject -Left $Res -Right $Master -ExcludeFields $exclude -NullValuesEqual -Keys $keys -MergeColumns -Epsilon 0.001 -Verbose   
        $result | Convert-CompareObjectToTestResult
        #if ($Res.count -eq $Master.count)
        #{
        #    New-TestResult -Passed $true -Message ("Total records for: " + $ExcelWB + " - " + $ExcelWs + " is: " + $Res.count)
        #}
        #else
        #{
        #    New-TestResult -Passed $false -Message ("Total records for: " + $ExcelWB + " - " + $ExcelWs + " is: " + $Res.count)
        #}
        Write-Host "Total records is:" $res.Count
        #New-TestResult -Passed $true -Message "Total records for $ExcelWS is: ($Res | measure-object -Property Count)"
    }
    
    $Global:Pass = "TRUE"
}