# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  
# |  
# |  $Global:updateEndurSql='\Regression\OSS\SetConfirmationMethod\SQL\UpdateConfirmationMethod.sql'
# |  $Global:verifyEndurSql='\Regression\OSS\SetConfirmationMethod\SQL\VerifyConfirmationMethod.sql'
# |  $Global:ExpectedRowCount=1
# |  
# |  
# |  
# +-----------------------------------------------------------------------------------------------------------------------------------------------

# To assign variables and make their scope global; 
Function Initialise {

    cls
    # Runtime initialisation
    $Global:script     ="$TestAssetBaseDir\Common\Powershell\EndurUtils.ps1"
    . $script                # Loads functions into current scope
    ImportRunTimeModules

    # Make Endur connection    
    EndurConn $updateEndurSql      # External function call to establish either a Manual (local) or Auto (batch) Endur session            
}

# 
Function Main {
    Write-Host "`nPokeEndur SQL:  " (Join-Path $TestAssetBaseDir $updateEndurSql)
    Invoke-Sqlcmd -ServerInstance $EndurSession.ServerName -Database $EndurSession.DatabaseName -InputFile (Join-Path $TestAssetBaseDir $updateEndurSql) -Verbose -ErrorAction Stop
}

Function CheckResults {
    Write-Host "Verify SQL:   " (Join-Path $TestAssetBaseDir $verifyEndurSql)
    Write-Host "Expected Rows=" $ExpectedRowCount 
    $res = Invoke-Sqlcmd -ServerInstance $EndurSession.ServerName -Database $EndurSession.DatabaseName -InputFile (Join-Path $TestAssetBaseDir $verifyEndurSql) -Verbose -ErrorAction Stop
    Write-Host "Actual Rows=" $res.Num 
    if ($res.Num -eq $ExpectedRowCount) {
        New-TestResult -Passed $True -Message "$updateEndurSql succeeded" -Comment "$ExpectedRowCount Update(s) set"
        }
    else{
    $AcualRowCount=$res.Num
    New-TestResult -Passed $False -Message "$updateEndurSql failed: Expected=$ExpectedRowCount vs. Acual=$AcualRowCount" -Comment "Update(s) FAILED"
    }          
}


# Mainline
try {
    Initialise
    Main
    CheckResults
}

finally {
    if ($RunMode -eq "Auto") {Stop-EndurSession}
    Write-Host "Done $updateEndurSql"
}
