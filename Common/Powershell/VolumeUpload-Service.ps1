﻿<#
Purpose: Sets Trade volume for different Deal Volume types using tradeVolume service
Parameters:
$Global:DealNum = 2187051 
$Global:VolumeFile = "C:\GGTP\Test Assets\Common\VolumeAmendFile.txt"

Volume File Format
FromDate,ToDate,TimeZone,DealVolumeType,VolumeType,Quantity
 #Column					#possible values								#description
 - 'FromDate' and 'ToDate'	- format:'yyyy.mm.dd HH:MM:SS'					- Volume in this range will be uploaded
 - 'TimeZone'				- 'UTC','Local','GetCompleteTrade'				- Each volume on the trade is booked against UTC and local trade time. This will specfy what time zone to use.
																			- GetCompleteTrade will return all volumes of the trade. dates will be ignored
 - 'DealVolumeType'			- 'Trading-BKR','Unscheduled','Forecasted','BAV'- The Type of volume on the trade
 - 'VolumeType'				- 'Hourly','Daily','PerTradeSetup'				- Will set on daily or hourly level, or by the level specified on the trade
 - 'Quantity'				- double number									- Quantity to be set for every entry in specified time range for specified DealVolumeType			
#>
function init{
	$Global:proceed = $true
	if ($RunMode -ne 'Debug'){
		EndurConn $ThisTest	
	}	
}

function Main{
    $volumes = Import-Csv -Path $VolumeFile
    checkStop -checkObject $volumes -description "No volumes to amend. Please check the file $VolumeFile"
    if ($proceed){
        foreach ($volume in $volumes){
            $loadedVolumes = Get-EndurTradeVolume -DealNumber $DealNum -VolumeType $volume.DealVolumeType -TimeZone $volume.TimeZone -StartDateTime $volume.FromDate -EndDateTime $volume.ToDate -EnvironmentUrl $TradeVolumeEnvURL -User $ServiceUser 
            ForEach ($entry in $loadedVolumes){
                if ($entry.VolumeTypeName -eq $volume.VolumeType){
                    Write-Host "Setting Quantity" $volume.Quantity "on trade" $DealNum "in period" $volume.FromDate $volume.ToDate
                    $entry.Quantity = $volume.Quantity
                }
            }
            $result = Set-EndurTradeVolume -VolumeEntries $loadedVolumes -VolumeType $volume.DealVolumeType -EnvironmentUrl $TradeVolumeEnvURL -User $ServiceUser
            checkStop -checkObject $result.Success -comment "Trade Upload for $volume failed" -description "Upload Failed"
            if ($proceed){
                $updatedVolumes = Get-EndurTradeVolume -DealNumber $DealNum -VolumeType $volume.DealVolumeType -TimeZone $volume.TimeZone -StartDateTime $volume.FromDate -EndDateTime $volume.ToDate -EnvironmentUrl $TradeVolumeEnvURL -User $ServiceUser
                 $CheckPassed = $true
                ForEach ($entry in $updatedVolumes){
                    if (($entry.VolumeTypeName -eq $volume.VolumeType) -and ($entry.Quantity -ne $volume.Quantity)){
                       $CheckPassed = $false
                    }
                }
                if ($CheckPassed){
                    $message = "VolumeUpload for $volume successful."
                    Write-Host $message
                    New-TestResult -Passed $true -Message $message
                }else{
                    $message = "Upload for $volume Failed."
                    New-TestResult -Passed $false -Message $message
                }
            }
        }
    }
}

function checkStop{
	param($checkObject, $comment, $description, $raiseTestResult = $true, $raiseTestResultPassed = $true)
	if (-not($checkObject)){
		$Global:proceed = $false
		$message = "$comment : $description"
		Write-Host $message
		if ($raiseTestResult){
			New-TestResult -Passed $false -Message $message -Comment $comment
		}
	}
}

# MAIN BLOCK
if ($RunMode -ne 'Debug'){		# in case of debug mode do not run anything automatically
	Write-Host "`nStart $ThisTest"
	try{
		init
		Main
	}catch{
			$ErrorMessage = $_.Exception.Message
			Write-Host 'Exception caught'
			Write-Host $ErrorMessage
			Write-Host $_.Exception.StackTrace
			New-TestResult -Passed $false -Message ("Exception Caught: " + $ErrorMessage) -Comment "Exception caught"
	}finally {
		if ($RunMode -eq "Auto") {Stop-EndurSession}
		Write-Host "`nDone $ThisTest"
	}
}else{
	Write-Host 'VolumeUpload-Service Script loaded'
}