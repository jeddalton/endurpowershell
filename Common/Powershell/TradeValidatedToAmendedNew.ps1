# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  
# |  $Global:DealRef='EMIR_GU_OIL_OTCSwaps'
# |  $Global:TranStatus='AmendedNew'
# |  
# |  
# |  
# |  
# |  
# |  
# +-----------------------------------------------------------------------------------------------------------------------------------------------

# To assign variables and make their scope global; 
Function Initialise {

    cls
    # Runtime initialisation
    $Global:script = "$TestAssetBaseDir\Common\Powershell\EndurUtils.ps1"
    . $script                # Loads functions into current scope
    $Global:script = "$TestAssetBaseDir\Common\Powershell\GeneralUtils.ps1"
    . $script                # Loads functions into current scope
    ImportRunTimeModules
        
    # Script variables
    EndurConn $DealRef       # External function call to establish either a Manual (local) or Auto (batch) Endur session
}

# To move test's trades to 'Validated' status (if necessary)
Function AmendedNew {
    $vars = @("DealRef=$DealRef")
    $trans = @(Invoke-Sqlcmd -ServerInstance $EndurSession.ServerName -Database $EndurSession.DatabaseName -Variable $vars -InputFile (Join-Path $TestAssetBaseDir Common\SQL\Endur\GetValidatedTransactions.sql)  -ErrorAction Stop) | Select-Object -ExpandProperty tran_num
    

    # See if we need to move deals to a status other than Validated
    if (-not($Global:TranStatus)){
		$Global:TranStatus = 'AmendedNew'
	}
	
	Write-Host "`nTradeValidatedToAmendedNew: $DealRef - Moving deals at status 'Validated' to '$TranStatus'"
	$trans | Set-EndurTransactionStatus -status $TranStatus
	New-TestResult -Passed $true -Comment "Deals with deal ref: $DealRef" -Message "Moved to Status: $TranStatus"
}

# Mainline
try {
    Initialise               # Assign variables
    AmendedNew                 # Move $DealRef trades to Validated status
}

finally {
    if ($RunMode -eq "Auto") {Stop-EndurSession}
    $Error[0].InnerException.StackTrace
    Write-Host "`nTradeValidate: $DealRef - Done"
}
