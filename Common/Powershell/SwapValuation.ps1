﻿Invoke-EndurTask -TaskName "EOD Save Market Data"

# Prices
$SimResultsFile = Invoke-SimRun -UseClose $true -RevalType "EOD" -DealRefsFile "C:\GGTP\Main\Test Assets\Regression\SwapDealRefs.txt" -SimName "EOD_SIM_GAS_GMT_GBP_JD" -ScenarioName "Discounted" -ResultType "PNL Detail" -OutputXlsxFilePath "C:\temp"