# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  
# |  $Global:DealRef='BackOffice_GAS_PhysIdxPr-Smpl'
# |  $Global:TradePack='\Regression\BackOffice\Data\Trades\TP_BackOffice_GAS.xlsm'
# |  $Global:TradeSheet='2.PhysIdxPrice-Simple'
# |  $Global:Strike='41|42'
# |  
# |  
# |  
# |  
# +-----------------------------------------------------------------------------------------------------------------------------------------------

# To assign variables and make their scope global; 
Function Initialise {

    cls
    # Runtime initialisation
    $Global:script = "$TestAssetBaseDir\Common\Powershell\EndurUtils.ps1"
    . $script                # Loads functions into current scope
    $Global:script = "$TestAssetBaseDir\Common\Powershell\GeneralUtils.ps1"
    . $script                # Loads functions into current scope
    ImportRunTimeModules
        
    # Script variables
    EndurConn $DealRef       # External function call to establish either a Manual (local) or Auto (batch) Endur session
}

# To purge test's trades
Function ClearDown {
    # Purge trades
    Write-Host "`nTradeLoad: $DealRef - Custom purge"
    $vars = @("DealRef=$DealRef")

    $trans = @(Invoke-Sqlcmd -ServerInstance $EndurSession.ServerName -Database $EndurSession.DatabaseName -Variable $vars -InputFile (Join-Path $TestAssetBaseDir Common\SQL\Endur\GetTransactionsToPurge.sql)  -QueryTimeout 360 -ErrorAction Stop) | Select-Object -ExpandProperty tran_num
    Write-Host "`nQueryTimeout 360"
    $trans | Clear-EndurTransaction
    
    #Get-EndurQueryResult -Fields @{"Transaction.Tran Type"="Trading"; "Transaction.Tran Status"="!Template"; "Transaction.Asset Type"="Trading","FO-Profile";
    #                               "Transaction.Ref"="$DealRef";
    #} | Clear-EndurTransaction

    # See if we need to purge holding instruments
    if (Test-Path variable:\Strike){
            $strike_array = $Strike.split("|")
            Write-Host "`nTradeLoad: $DealRef - Purge Holding Instrument with Strike=$Strike"
            # delete holding instruments
            Get-EndurQueryResult -Fields @{"Transaction.Tran Type"="Holding"; "Transaction.Tran Status"="!Template";
                                           "Transaction.Ref"="$strike_array"; "Transaction.Ins Type"="ENGY-EXCH-OPT"; "Transaction.Trade Date"="0d";
            } | Clear-EndurTransaction
            # purge holding instruments
            Get-EndurQueryResult -Fields @{"Transaction.Tran Type"="Holding"; "Transaction.Tran Status"="!Template";
                                           "Transaction.Ref"="$strike_array"; "Transaction.Ins Type"="ENGY-EXCH-OPT"; "Transaction.Trade Date"="0d";
            } | Clear-EndurTransaction
    }
}

# To load test's trades from workbook and sheetname(s)
Function LoadTrades {
    # Path to spreadsheet containing deals
    $TradesXLS  = Join-Path $TestAssetBaseDir $TradePack
    Write-Host "`nTradeLoad: $DealRef - Spreadsheet name is: $TradesXLS"
    
    #First sheetname
    Write-Host "`nTradeLoad: $DealRef - Load deals in SheetName $TradeSheet"
    #Import-EndurTransaction -ExcelFileName $TradesXLS -SheetName $TradeSheet
    #$result = Import-EndurTransactionFromExcel -ExcelFileName $TradesXLS -SheetName $TradeSheet
    $result = Import-EndurTransaction -ExcelFileName $TradesXLS -SheetName $TradeSheet
    
    # See if we need to load an additional sheet(s)
    if (Test-Path variable:\TradeSheet2){
    Write-Host "`nTradeLoad: $DealRef - Load deals in SheetName2 $TradeSheet2"
    #Import-EndurTransaction -ExcelFileName $TradesXLS -SheetName $TradeSheet2
    #Import-EndurTransactionFromExcel -ExcelFileName $TradesXLS -SheetName $TradeSheet2
    Import-EndurTransaction -ExcelFileName $TradesXLS -SheetName $TradeSheet2
    }
    if (Test-Path variable:\TradeSheet3){
    Write-Host "`nTradeLoad: $DealRef - Load deals in SheetName3 $TradeSheet3"
    #Import-EndurTransaction -ExcelFileName $TradesXLS -SheetName $TradeSheet3
    #Import-EndurTransactionFromExcel -ExcelFileName $TradesXLS -SheetName $TradeSheet3
    Import-EndurTransaction -ExcelFileName $TradesXLS -SheetName $TradeSheet3
    }
    New-TestResult -Passed $true -Comment "Deals loaded for reference $DealRef" -Message "Deals loaded for reference $DealRef"
}

# Mainline
try {
    Initialise               # Assign variables
    ClearDown                # Purge   $DealRef trades
    LoadTrades               # Load    $DealRef trades
}


finally {
    if ($RunMode -eq "Auto") {Stop-EndurSession}
    if ($Error.count -Ge 2  )
    {
    Write-Host "`nTradeLoad: $DealRef - Failed "    
    $error | foreach-object -process { 
    write-host  "$_.InnerException.stackTrace"
    New-TestResult -Passed $False -Message "Error captured From Test Suite Runner nTradeLoad: $DealRef" -Comment "$_.InnerException.stackTrace"
    }
    }  
    else {Write-Host "`nTradeLoad: $DealRef - Done "}    
}
