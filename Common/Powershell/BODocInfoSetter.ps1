# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  
# |  $Global:ThisTest='Set DocInfo OTC COAL SHADOW INVOICES'
# |  $Global:DocQuery='\Regression\BackOffice\SQL\GetInvoiceDocFromDealRef.sql'
# |  $Global:DocDef='Fin Shadow Invoice'
# |  $Global:DocInfoFile='\Regression\BackOffice\Data\Invoices\OTC COAL SHADOW INVOICES_DocInfo.csv'
# |  
# |  
# |  
# |  
# +-----------------------------------------------------------------------------------------------------------------------------------------------

# To assign variables and make their scope global; 
Function Initialise {

    cls
    # Runtime initialisation
    $Global:script     ="$TestAssetBaseDir\Common\Powershell\EndurUtils.ps1"
    . $script                # Loads functions into current scope
    ImportRunTimeModules     # Imports various cmdlets, registers SQL snapins, etc.
    
    # Make Endur connection    
    EndurConn $ThisTest      # External function call to establish either a Manual (local) or Auto (batch) Endur session            
}

# Put the main test processing into this function
Function Main {
    Write-Host "File for document processing is:" ($TestAssetBaseDir + $DocInfoFile)
    
    $sql = [System.IO.File]::ReadAllText($TestAssetbaseDir+$DocQuery)
    Import-Csv -Path ($TestAssetBaseDir+$DocInfoFile) | foreach-object{
    
        $DealRef=$_.DealRef
        $DocInfoType=$_.DocInfoType
        $DocInfoValue=$_.DocInfoValue
        
        Write-Host ("Processing document for deal reference: " + ($DealRef))
        Write-Host ("DocInfoType: " + ($DocInfoType))
        Write-Host ("DocInfoValue: " + ($DocInfoValue))
                
        $vars = @("DealRef=$DealRef";"DocDef=$DocDef")
        
        $res = Invoke-Sqlcmd -ServerInstance $EndurServerName -Database $EndurDatabaseName -Query $sql -Variable $vars
       
        # No document retrieved
        if ($res.count -eq 0){
            Write-Host "No document num for deal reference: $DealRef"
            New-TestResult -Passed $false -Message "No document exists deal reference: $DealRef" -Comment "No document exists deal reference: $DealRef"
        }

        else{        
            # Loop the documents returned
            foreach ($row in $res){
                Write-Host "Document num for deal reference: $DealRef is: " $row.document_num
                Write-Host "Document version number is: " $row.DocVersion
                
                # Process DocInfo
                Set-DocInfo -DocNum $row.document_num -DocInfoType $DocInfoType -DocInfoValue $DocInfoValue 
            }
        }
    }
}

# Mainline
try {
    Initialise
    Main    
}

finally {
    if ($RunMode -eq "Auto") {Stop-EndurSession}
    Write-Host "`nDone $ThisTest"
}
