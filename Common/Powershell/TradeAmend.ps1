# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  
# |  $Global:ThisTest='TradeAmend'
# |  $Global:TradeAmendFile='Tests\CumulativeValuation\Data\20131031_DealUpdates.csv'
# |  
# |  
# |  
# |  
# |  
# +-----------------------------------------------------------------------------------------------------------------------------------------------

# To assign variables and make their scope global; 
Function Initialise {
    
    cls
    # Runtime initialisation
    $Global:script     ="$TestAssetBaseDir\Common\Powershell\EndurUtils.ps1"
    . $script                # Loads functions into current scope
    ImportRunTimeModules

    EndurConn $ThisTest      # External function call to establish either a Manual (local) or Auto (batch) Endur session

    $Global:TradeAmendFile = "$TestAssetBaseDir\$TradeAmendFile"
}

# To import trade amendments into Endur from CSV file
Function Main {
    UpdateDeals -Filepath $TradeAmendFile -Verbose
}

# Mainline
try {
    Initialise
    Main
}

finally {
    if ($RunMode -eq "Auto") {Stop-EndurSession}
    Write-Host "`nDone $ThisTest"
}
