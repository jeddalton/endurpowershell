# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  NB: If Manual then run EnvVariables first.
# |  $Global:ThisTest='StartStopWinService'
# |  $Global:ServiceName="ZemaToEndurService"
# |  $Global:ServerName="EndurAppServer2"
# |  $Global:StartStop="Start"
# |  
# |  
# |  
# +-----------------------------------------------------------------------------------------------------------------------------------------------
# To assign variables and make their scope global; 
Function Initialise {

    cls
    # Runtime initialisation
    $Global:script     ="$TestAssetBaseDir\Common\Powershell\EndurUtils.ps1"
    . $script                # Loads functions into current scope
    ImportRunTimeModules     # Imports various cmdlets, registers SQL snapins, etc.
             
}

# Put the main test processing into this function
Function Main {
    cls
    # Main processing here
    Write-Host $StartStop $ServiceName " WinService"
    
	$ComputerName = ((Get-Variable -Name $ServerName).value)
    Write-Host $ComputerName
    if ($ComputerName -eq $null) {RaiseException "Unable to retrieve server name"}
        
    $service = Get-Service -ComputerName $ComputerName -Name $ServiceName
    Write-Host $service
    if ($service -eq $null) {RaiseException "Unable to retrieve service details"}
    
    switch ($StartStop.toUpper())
    {
       "RESTART"{
       if ($service.Status -eq "Stopped") {Write-Host "Starting service $ServiceName";Start-Service -InputObject $service}
       else {
               Write-Host "Restarting service $ServiceName"
               Restart-Service -InputObject $service       
            }
       }
       "START"{
       if ($service.CanStop -eq $False) {
            Write-Host "Starting service $ServiceName"
            Start-Service -InputObject $service
            $service.WaitForStatus("Running")
            Write-Host $service.Status
            if ($service.Status -eq "Running"){
                New-TestResult -Passed $true -Message "Started $ServiceName correctly" -Comment "Pass"
            }
            else
            {
                New-TestResult -Passed $false -Message "Failed to start $ServiceName correctly" -Comment "Fail"
            }
                        
       }       
       else {RaiseException "Not starting service $ServiceName because not in a startable state"}
       }
       "STOP" {
       if ($service.CanStop -eq $True) {Write-Host "Stopping service $ServiceName";Stop-Service -InputObject $service}
       else {RaiseException "Not stopping service $ServiceName because not in a stoppable state"}
       } 
       default {RaiseException "Invalid Start/Stop action: $StartStop selected"}
    }
    
}

# Mainline
try {
    Initialise
    Main    
}

finally {
    if ($RunMode -eq "Auto"){Write-Host "`nDone $ThisTest"}
}
