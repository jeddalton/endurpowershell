#
# *** FUNCTIONS ONLY IN THIS FILE!!! ***
#
# To connect to Endur - Manual: Endur session exists (run this script locally); Auto: creates Endur session (run from .bat file)
#
Function Global:RaiseException {
    param($Message)
    throw Write-Host $Message -foregroundcolor red -backgroundcolor yellow    
}

Function Global:ImportRunTimeModules{

    # Check if we're running within the Test Framework
    if ($ExecutionContext.Host.Name -eq "PowerShellTf") {$Global:RunMode ="Auto"}  
    else {$Global:RunMode ="Manual"}

    Write-Host "In Import Run Time modules"

    RegisterSQLSnapIns

    # Check if required modules are imported and if not import them
	# Get correct version of EndurModule
	<#
	$cfg = get-content $EndurConfigFile
	$lookup = "SET OLF_BIN=E:\OpenLink\Endur_V14"
	$version = 0
	foreach($cf in $cfg){
        Write-Host $cf
		if($cf.StartsWith($lookup)){
			 [string]$version = $cf.Substring($cf.Length - 8,4)
		}
	}
	if($version -eq '1109'){
		$version = '2'
	}
	$EndurModule = 'EndurModule.' + $version
    #>

	$EndurModule = 'EndurModule.2'
	if (CheckModuleIsImported $EndurModule) {
		Write-Host 'EndurUtils: Using EndurModule:' $EndurModule
		Import-Module $EndurModule
	}
    if (CheckModuleIsImported TestModule)   { Import-Module TestModule}
    if (CheckModuleIsImported DataCompareModule)   { Import-Module DataCompareModule}
    if (CheckModuleIsImported RabbitModule) { Import-Module RabbitModule}
    if (CheckModuleIsImported SSISModule)   { Import-Module SSISModule}
	if (CheckModuleIsImported SQLPS)   { Import-Module SQLPS}
    
    #Set standard global test variables
    $Global:Pass       ="FALSE"
    $Global:results    =New-Object System.Collections.ArrayList
}

Function DeclareEndurSessionVariables ()
{
        $Global:EndurSessionDisposer = $null
        $Global:EndurBinaryFolder = $null
        $Global:EndurSessionId = $null
        $Global:EndurSession = $null
        $Global:EndurServerName = $null
        $Global:EndurDatabaseName = $null
        $Global:EndurConnectionString = $null
}

Function Global:EndurConn ($Test) {

    # Check if we're running within the Test Framework
    if ($ExecutionContext.Host.Name -eq "PowerShellTf") {$Global:RunMode ="Auto"}    
            
    # Running manual but Endur session is already registered
    if ($RunMode -eq "Manual" -and $EndurSessionId -ne $null) {              
        Write-Host "`nEndur session already registered`nStart $Test ..."
    }
    
    # Running manual but Endur session is unregistered
    if ($RunMode -eq "Manual" -and $EndurSessionId -eq $null) {      
        Register-EndurSession | Out-Null
        Write-Host "`nStart $Test ..."
    }
    
    # Start and register a new Endur session because we're running within Powershell Test Framework i.e. from .bat file
    if ($RunMode -eq "Auto") {
        $userName = "test" + ("{0:D2}" -f [int]$TestScriptId)

        Write-Host "Using username: $userName"
        write-host $EndurDatabaseName        
        
        New-EndurInProcessSession -ConfigFilePath $EndurConfigFile -User $UserName -Password password01 | Register-EndurSession
        #New-EndurSession -ConfigFilePath $EndurConfigFile -User $UserName -Password password01 | Register-EndurSession
    }
}

Function Global:CheckModuleIsImported {
    param($ModuleName)
    # Write-Host "Check for module $ModuleName"
    if ((Get-Module -Name $ModuleName -ErrorAction SilentlyContinue) -ne $null )
    {   
        # Write-Host "Found"    
        return $false
    }
    else
    {        
        return $true
    }
}        

Function Global:RegisterSQLSnapIns {
    Import-Module SqlPs -WarningAction SilentlyContinue -DisableNameChecking
}
        
Function Global:RunETL {
    param ($path, $server, [hashtable]$connStrings,[hashtable]$variables)
    
    # We need the SSIS Module to run ETL
    if (CheckModuleIsImported SSISModule)   {Import-Module SSISModule}
    
    $ETLRes = Invoke-SsisPackage `
    -Path $path `
    -Server $server `
    -ConnectionStrings $connStrings `
    -Variables $variables `
    -Verbose
    
    Write-Host "Done: " $path
    Write-Host "Variables used were: " ($variables | Out-String)
    
#    if ($ETLRes.Result  -eq "Failure")
#    {
#        throw "ETL failure: $path"
#    }
}

Function Global:GetEndurAppConfig {
    param ($Name, $ConfigName)
    # write-host $EndurServername
    # write-host $EndurDatabaseName
    $query = "SELECT value FROM USER_AppConfig WHERE name='" + "$Name" + "' AND ConfigName='" + "$ConfigName" + "'"
    $result = Invoke-Sqlcmd -ServerInstance $EndurServername -Database $EndurDatabaseName -Query $query -Verbose -ErrorAction Stop
    return $result.value
}

Function Global:ReturnEODSQL {
    param ($SQLFile, $EODDate, $DealRefString, $IntBUString, $InsTypeString)
    $SQLString = [System.IO.File]::ReadAllText($SQLFile)
    $SQLString = $SQLString.Replace("EODDATE",$EODDate)
    $SQLString = $SQLString.Replace("DEALREFSTRING",$DealRefString)
    $SQLString = $SQLString.Replace("INTBUSTRING",$IntBUString)    
    $SQLString = $SQLString.Replace("INSTYPESTRING",$InsTypeString)
    
    return $SQLString
}

Function Global:AssignEndurVariables {

    # Register SQL snapins
    #RegisterSQLSnapIns

    # Assign variables that we will need in automated tests for Endur / Reporting DB
    $Global:ReportingDBConnString        =     GetEndurAppConfig "SSISReportingConnectionString" "GGTP.Default"
    $Global:EndurDBConnString            =     GetEndurAppConfig "SSISEndurConnectionString" "GGTP.Default"
    $Global:SSISPackageFolder            =     GetEndurAppConfig "SSISPackageFolder" "GGTP.Default"
    $Global:SSISPackageProject           =     GetEndurAppConfig "SSISPackageProject" "GGTP.Default"
    $Global:SSISSqlServer                =     GetEndurAppConfig "SSISSqlServer" "GGTP.Default"
    $Global:SSISRunnerURL                =     GetEndurAppConfig "SSISRunnerUrl" "GGTP.Default"    
    $Global:PnLDailyCubeConnString       =     GetEndurAppConfig "SSAS.ErpCube" "EOD SSAS PnLDaily GMT v4"
    $Global:PnLDeliveryDayCubeConnString =     GetEndurAppConfig "SSAS.ErpCube" "EOD SSAS PnLDeliveryDay GMT 11"
    $Global:PnLExplainedCubeConnString   =     GetEndurAppConfig "SSAS.ErpCube" "EOD SSAS PnLExplained GMT 11"
    $Global:PnLExposureCubeConnString    =     GetEndurAppConfig "SSAS.ErpCube" "EOD SSAS PnLExposure GMT 11"
    $Global:XMLADirectory                =     GetEndurAppConfig "Directory" "GGTP.Default"
    

    $data=$ReportingDBConnString.split(";") 
    $Global:ReportingDBConnStringShort=$data[0]+";"+$data[1]+";"+$data[3] # Short string required for comparisons
    $data=$EndurDBConnString.split(";") 
    $Global:EndurDBConnStringShort=$data[0]+";"+$data[1]+";"+$data[3] # Short string required for comparisons
}

Function Global:PurgeSims {   
    $query = "DELETE sim_blob WHERE sim_run_id IN
            (SELECT sim_run_id FROM sim_header WHERE run_time = (SELECT business_date FROM system_dates WHERE trading_location_id=0))"
    $result = Invoke-Sqlcmd -ServerInstance $EndurServername -Database $EndurDatabaseName -Query $query -Verbose -ErrorAction Stop
    $query = "DELETE sim_header WHERE run_time = (SELECT business_date FROM system_dates WHERE trading_location_id=0)"
    $result = Invoke-Sqlcmd -ServerInstance $EndurServername -Database $EndurDatabaseName -Query $query -Verbose -ErrorAction Stop
}

Function Global:SaveTran {
    param ($Filepath)
    Write-Host "Save Tran on deals in file: $FilePath"    
    
    Import-Csv -Path $Filepath | foreach-object {
        
        Write-Host ("Processing: " + ($_.Reference))
        Write-Host ("Comment: " + ($_.Comment))
        Write-Host ("Tran Info: " + ($_.FieldName))
        Write-Host ("Side: " + ($_.Side))
        Write-Host ("Value: " + ($_.Value))
        
        $trans=Get-EndurTransaction -Reference $_.Reference 
        $trans | Set-EndurFieldValue -Field $_.FieldName -Side $_.Side -Value $_.Value
        $trans.SaveInfoFields($false,$false)
        $trans.Dispose()

        $Status="Validated"
        CheckDealUpdate $_.Reference $Status $_.FieldName $_.Side $_.Value
    }
}

Function Global:UpdateDeals {
    param ($Filepath)
       
    Write-Host "Updating transactions from file: $FilePath"    
    
    Import-Csv -Path $Filepath | foreach-object {
        
        Write-Host ("Processing: " + ($_.Reference))
        if ($_.ToStatus -eq [DBNull]::Value){
            $Status="Validated"
        }
        else {
            $Status=$_.ToStatus
        }
        Write-Host ("Setting status to: " + ($Status))
        Write-Host ("Comment: " + ($_.Comment))
        Write-Host ("Side: " + ($_.Side))
        Write-Host ("Value: " + ($_.Value))
        
        # See if a 'FromStatus' has been passed and if so get correct transaction
        if ($_.FromStatus -ne [DBNull]::Value) {
            Write-Host ("From status: " + ($_.FromStatus))
            $trans=Get-EndurTransaction -Reference $_.Reference -Status $_.FromStatus            
        }
        else {
            $trans=Get-EndurTransaction -Reference $_.Reference 
        }
        
        # See if we are updating any transaction fields and if so update the transaction before booking to 'New' status
        # otherwise just book to the new status
        if ($_.FieldName -ne [DBNull]::Value){               
            $trans | Set-EndurFieldValue -Field $_.FieldName -Side $_.Side -Value $_.Value -PassThru | Set-EndurTransactionStatus -Status $Status
            $trans.Dispose()
            if ($_.Expected -ne $null -and $_.Expected -ne [DBNull]::Value ){ 
            CheckDealUpdate $_.Reference $Status $_.FieldName $_.Side $_.Expected
            }
            else {
            CheckDealUpdate $_.Reference $Status $_.FieldName $_.Side $_.Value
            }
        }
        else {           
            $trans | Set-EndurTransactionStatus -Status $Status
        }
                
        
                
    }
}

Function Global:CheckDealUpdate {
    param ($reference,$status,$fieldname,$side,$value)
    
    Write-Host "Checking deal update"
    
    $trans=Get-EndurTransaction -Reference $reference -Status $status
    $setValue = $trans | Get-EndurFieldValue -Field $fieldname -Side $side
    
    if ($setValue -ne $value){
        New-TestResult -Passed $false -Message "Deal $reference failed to amend. Expected value is $value actual value is $setValue"
    }
    else {
        New-TestResult -Passed $true -Message "Deal $reference updated.  Expected result for $fieldname, side $side = $value"
    }
    $trans.Dispose()
}     
    

Function Global:CancelDeals {
    param ($Filepath)
    
    Import-Csv -Path $Filepath | foreach-object {
    
        Write-Host ("Processing: " + ($_.Reference))

        $tran=Get-EndurTransaction -Reference $_.Reference 

        if ($tran.TransactionStatus -ne "Validated")
        {
            Write-Host "Transaction is in incorrect state will not be cancelled"
        }
        
        else
        {
            $tran | Set-EndurTransactionStatus -Status $_.Status
        }
    }
}

Function Global:SetOSS {
    param ($Filepath)
    
    Import-Csv -Path $Filepath | foreach-object {
    
        Write-Host ("Setting OSS: " + ($_.OSS) + " Action: " + ($_.Action) + " Comment: " + ($_.Comment))        

        if ($_.OSS -ne $null){
        Set-OpService -ServiceName $_.OSS -Action $_.Action
        }        
    }
}    


Function Global:RunAsWflow {
    param ($Filepath)
    
    Import-Csv -Path $Filepath | foreach-object {
    
        Write-Host ("Retrieving parameters from Param Script: " + ($_.Param))
        ## retrieves parameters as table
        $params = Invoke-EndurScript -ScriptName $_.Param -ServiceName "FEE SimRun" -ServiceMethod "RunProcess"

        ## Runs main script with parameters
        Write-Host ("Running main Script: " + ($_.Main))
        $returnt = Invoke-EndurScript -ScriptName $_.Main -Argt $params -ServiceName "FEE SimRun" -ServiceMethod "RunProcess"

        ## Check results of running task
        if ($returnt.Status -eq 1){
        New-TestResult -Passed $true -Comment "Task $_.Main complete with status success" -Message "Task $_.Main complete with status success"
        }
        else{
        New-TestResult -Passed $false -Comment "Task $_.Main failed" -Message "Task $_.Main failed"
        }
    }        
}    