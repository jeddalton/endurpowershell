﻿# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  
# |  $Global:ThisTest='Check OTC GAS CAPACITY OUTBOUND INV'
# |  $Global:DiffOrOutput='Diff'
# |  $Global:OutputXML='\Tests\BackOffice\ExpectedResults\Invoices'
# |  $Global:DocQuery='\Tests\BackOffice\SQL\GetInvoiceDocFromDealRef.sql'
# |  $Global:DocDef='OTC Gas Capacity Outbound Inv'
# |  $Global:DealRefs='\Tests\BackOffice\Data\Invoices\OTC GAS CAPACITY OUTBOUND INV_DealRefs.txt'
# |  $Global:xmlFilterPath='\Tests\BackOffice\Data\Invoices\OTC GAS CAPACITY OUTBOUND INV_filter.txt' (Diff only)
# |  
# +-----------------------------------------------------------------------------------------------------------------------------------------------

Function Initialise {

    cls
    # Runtime initialisation
    $Global:script = "$TestAssetBaseDir\Common\Powershell\EndurUtils.ps1"
    . $script                # Loads functions into current scope
    ImportRunTimeModules      
    
     # Script variables
    EndurConn $ThisTest      # External function call to establish either a Manual (local) or Auto (batch) Endur session
}

Function WriteXML {
    param ($tran_num, $reference)
    
    $xmlPath = Join-Path ((Join-path $TestAssetBaseDir  $OutputXML)) "$reference.xml"
    $xmlobj = Convert-EndurTransactionToXml -TransactionId $tran_num

    $xmlobj.Save($xmlPath)
    Write-Host "Saved file is $xmlPath"
}

Function DiffXML {
    param ($TranNum, $Ref)
    
    $cmppath = Join-Path ($TestAssetBaseDir + $ERDir) "$Ref.xml"
    
    if (Test-Path $cmppath){
        Write-Host "Path to master .xml is $cmppath"
        $RefObject = Get-Item $cmppath
        $xmlFilterArray = get-content (Join-Path $TestAssetBaseDir $xmlFilterPath)
        #$xmlFilterArray = $xmlFilterArray + @("cfDealNum","cfTranNum","cfInsNum","cfEventNum","cfTaxEventNum")
        $CompareObject= Convert-EndurTransactionToXml -TransactionId $TranNum
        Compare-Xml -Left $RefObject -Right $CompareObject -Options IgnoreWhiteSpace -IgnoreMoves -Comment "Test for document containing deal reference $Ref" -Filter $xmlFilterArray
    }
    else{        
        Write-Host "No master document for $cmppath"
        New-TestResult -Passed $false -Message "No master document exists for $cmppath" -Comment "No master document exists for $cmppath"
    }
}

# Main program
try {
    Initialise
    #$refs = Get-Content -Path ($TestAssetBaseDir + $DealRefs)
    $sql = [System.IO.File]::ReadAllText((Join-Path $TestAssetBaseDir $SQL))
    $vars = @("DealRef=$DealRef")
    
    # Retrieve the transactions for each deal ref string
    $res = Invoke-Sqlcmd -ServerInstance $EndurServerName -Database $EndurDatabaseName -Query $sql -Variable $vars

    # No transactions retrieved
    if ($res.count -eq 0){
        Write-Host "No transactions retrieved for reference: $DealRef"
        New-TestResult -Passed $false -Message "No document exists deal reference: $ref" -Comment "No document exists deal reference: $ref"       
    }

    else{
    # Loop the deal references
        foreach ($row in $res){

        if ($DiffOrOutput -eq "Diff") { DiffXML $row.tran_num $row.reference }
        if ($DiffOrOutput -eq "Output") { WriteXML $row.tran_num $row.reference }
        }             
    }
}

finally {
    if ($RunMode -eq "Auto") {Stop-EndurSession}
    Write-Host "`nDone $ThisTest"
}