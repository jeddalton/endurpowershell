# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  RECORD What the test is doing / points of interest
# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  
# |  $Global:ThisTest='ThisTestName'
# |  
# |  
# |  
# |  
# |  
# |  
# +-----------------------------------------------------------------------------------------------------------------------------------------------

# To assign variables and make their scope global; 
Function Initialise {

    cls
    # Runtime initialisation
    $Global:script     ="$TestAssetBaseDir\Common\Powershell\EndurUtils.ps1"
    . $script                # Loads functions into current scope
    ImportRunTimeModules     # Imports various cmdlets, registers SQL snapins, etc.           
}

# Put the main test processing into this function
Function Main {
    Write-Host "Doing processing"
    $InputFile = JOin-Path $TestAssetBaseDir $SqlFile
    
    $DatabaseServer = ((Get-Variable -Name $DbServerName).value)
    $Databasename   = ((Get-Variable -Name $DatabaseName).value)
	


    $result = Invoke-Sqlcmd -ServerInstance $DatabaseServer -Database $Databasename -InputFile $InputFile -AbortOnError
    write-host $result
    write-host $result.Column1
    $actualCount = $result.Column1

    if ($actualCount -eq $rowcount)
    {
        New-TestResult -Passed $true -Comment "Ran script $InputFile expected count: $rowCount actual count: $actualCount" -Message "Pass"
    }
    else
    {
        New-TestResult -Passed $false -Comment "Ran script $InputFile expected count: $rowCount actual count: $actualCount" -Message "Fail"
    }
}

# Mainline
try {
    Initialise
    Main
}

finally {    
    Write-Host "`nDone"
}