# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  
# |  $Global:ThisTest='Send OTC COAL OUTBOUND INVOICES'
# |  $Global:DocQuery='\Regression\BackOffice\SQL\GetInvoiceDocFromDealRef.sql'
# |  $Global:DocDef='Fin Invoice'
# |  $Global:ProcessFile='\Regression\BackOffice\Data\Invoices\OTC COAL OUTBOUND INVOICES_InvoiceProcess.csv'
# |  
# |  
# |  
# |  
# +-----------------------------------------------------------------------------------------------------------------------------------------------

# To assign variables and make their scope global; 
Function Initialise {

    cls
    # Runtime initialisation
    #$Global:TestAssetBasedir = "C:\GGTP\Test Assets"
    $Global:script     ="$TestAssetBaseDir\Common\Powershell\EndurUtils.ps1"
    . $script                # Loads functions into current scope
    ImportRunTimeModules     # Imports various cmdlets, registers SQL snapins, etc.
    
    <#
    $Global:ThisTest='Process FX C-P SWIFT CONFIRMATIONS'
    $Global:DocQuery='\Common\SQL\Endur\GetConfirmDocFromDealRef.sql'
    $Global:DocDef='FX SWIFT Confirmations'
    $Global:ProcessFile='\Regression\STSSimplex\Data\Confirmations\FX C-P SWIFT CONFIRMATIONS_ProcessToSent.csv'
    #>



    # Make Endur connection    
    EndurConn $ThisTest      # External function call to establish either a Manual (local) or Auto (batch) Endur session    
     
}

# Put the main test processing into this function
Function Main {
    Write-Host "File for document processing is:" ($TestAssetBaseDir + $ProcessFile)
    
    $sql = [System.IO.File]::ReadAllText($TestAssetbaseDir+$DocQuery)
    Import-Csv -Path ($TestAssetBaseDir+$ProcessFile) | foreach-object{
    
        $DealRef=$_.DealRef
        $Comment=$_.Comment
        $ProcessTo=$_.ProcessTo
        
        Write-Host ("Processing document for deal reference: " + ($DealRef))
        Write-Host ("To status: " + ($ProcessTo))
        Write-Host ("Comment: " + ($Comment))
                
        $vars = @("DealRef=$DealRef";"DocDef=$DocDef")
        
        $res = Invoke-Sqlcmd -ServerInstance $EndurServerName -Database $EndurDatabaseName -Query $sql -Variable $vars
       
        # No document retrieved
        if ($res.count -eq 0){
            Write-Host "No document num for deal reference: $DealRef"
            New-TestResult -Passed $false -Message "No document exists deal reference: $DealRef" -Comment "DealRef: $DealRef"
        }

        else{        
            # Loop the documents returned
            foreach ($row in $res){
                Write-Host "Document num for deal reference: $DealRef is: " $row.document_num
                Write-Host "Document version number is: " $row.DocVersion
                
                # Process document
                Set-EndurDocumentStatus -DocumentNumber $row.document_num -Version $row.DocVersion -Status $ProcessTo
                
                #Warning: Could show false positive message
                New-TestResult -Passed $true -Message "Deal processed to: $ProcessTo" -Comment "DealRef: $DealRef"
                
            }
        }
    }
}

# Mainline
try {
    Initialise
    Main    
}

finally {
    if ($RunMode -eq "Auto") {Stop-EndurSession}
    Write-Host "`nDone $ThisTest"
}
