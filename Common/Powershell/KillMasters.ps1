﻿Stop-Process -processname olisten* -Confirm

Stop-Process -processname master* -Confirm

Get-Process | Where-Object {$_.name -like "master*" -or $_.name -like "olisten*" -and $_.StartTime -gt ((Get-Date).AddMinutes(-30))} | Stop-Process

Get-Process | Where-Object {$_.name -like "master*" -or $_.name -like "PowershellTf.exe" } | Stop-Process