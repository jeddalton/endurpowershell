# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  RECORD What the test is doing / points of interest
# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  
# |  This is a template .ps1 script to be used when creating new .ps1 scripts
# |  
# |  
# |  
# |  
# +-----------------------------------------------------------------------------------------------------------------------------------------------

# To perform initialisation for the test
Function Initialise {    
    # Below are listed the kinds of things that may be required
    #
    # Make Endur connection (only required if you need an Endur session)    
    EndurConn $ThisTest      # External function call to establish an Endur session
    #
    # Data
    # Remove trades that must be absent at the start of this test
    # Ensure that any static data required is set up
    # 
    # Produce test results even for environment setup steps
    New-TestResult -Passed $True -Message "Test passed" -Comment "Data setup completed correctly"
}

# Put the main test processing into this function
Function Main {
    Write-Host "Doing main processing"
    # Main processing here
    #
    CheckResults
    #
    
}

Function CheckResults {
    Write-Host "Checking results of processing"
    # Result checking here
    #
    New-TestResult -Passed $True -Message "Test passed" -Comment "Test case 12: Realised PnL is non-zero for trade in delivery"
    #
    
}


# Mainline
try {
    Initialise
    Main    
    CheckResults
}

finally {
    # Do any clear down here as appropriate
    # When an Endur connection has been made, then $RunMode will be set
    if ($RunMode -eq "Auto") {Stop-EndurSession}
    Write-Host "`nDone $ThisTest"
}
