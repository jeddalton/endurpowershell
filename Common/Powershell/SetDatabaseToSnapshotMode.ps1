# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  RECORD What the test is doing / points of interest
# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  
# |  $Global:ThisTest='ThisTestName'
# |  
# |  
# |  
# |  
# |  
# |  
# +-----------------------------------------------------------------------------------------------------------------------------------------------

# To assign variables and make their scope global; 
Function Initialise {

    cls
    # Runtime initialisation
    $Global:script     ="$TestAssetBaseDir\Common\Powershell\EndurUtils.ps1"
    . $script                # Loads functions into current scope
    ImportRunTimeModules     # Imports various cmdlets, registers SQL snapins, etc.
    
    # Make Endur connection    
    #EndurConn $ThisTest      # External function call to establish either a Manual (local) or Auto (batch) Endur session            
}

# Put the main test processing into this function
Function Main {
    Write-Host "Setting database to snapshot isoaltion mode"
    # Main processing here

    $vars = @("DatabaseName=$EndurDatabaseName")

    Invoke-Sqlcmd -Database $EndurDatabaseName -ServerInstance $EndurServerName -InputFile "C:\GGTP\Test AssetsV14\Common\SQL\Endur\DatabaseToSnapshotIsolationMode.sql" -Variable $vars -QueryTimeout $LongTimeout -ErrorAction Stop 
    New-TestResult -Passed $true -Message "Database successfully changed to snapshot mode." -Comment "Database successfully changed to snapshot mode."
    #
    
}


# Mainline
try {
    Initialise
    Main    
}

finally {
    Write-Host "`nDone $ThisTest"
}
