# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  
# |  $Global:ThisTest='RunV4Cubes'
# |  $Global:Date1='10-Jan-2014'
# |  
# |  
# |  
# |  
# |  
# |  
# +-----------------------------------------------------------------------------------------------------------------------------------------------

# To assign variables and make their scope global;
Function Initialise {
    cls
    # Runtime initialisation
    $Global:script     ="$TestAssetBaseDir\Common\Powershell\Scripts\EndurUtils.ps1"
    . $script                # Loads functions into current scope
    $Global:script     ="$TestAssetBaseDir\Common\Powershell\Scripts\GeneralUtils.ps1"
    . $script                # Loads functions into current scope
    ImportRunTimeModules    
    AssignEndurVariables     # External function call to set up variables read from USER_AppConfig e.g. Reporting DB connection string
}

Function Main {

    Write-Host "`nStart 770 - GMT EOD SSAS Processing - PnLDailyCube"
    $EodDate = [DateTime]$Date1
    $ConnectionStrings = @{"Endur"="$EndurDBConnString";"Reporting"="$ReportingDBConnString";"SSAS.ErpCube"="$PnLDailyCubeConnString"}
    $Variables = @{"internalLegalEntity"="GMT LTD - LE";"internalBusinessUnit"="All";"EodDate"=$EodDate;"Directory"=$XMLADirectory;"filterByLE"="False";"XmlaFileName"="PnLDaily_1111"}
    $request = @()
    $request += New-Object Object | Add-Member NoteProperty PackageName ($SSISPackageFolder + "\DynamicEndurPnLGeneric")  -PassThru | Add-Member NoteProperty QueueName "EOD.SSAS.Processing"  -PassThru | Add-Member NoteProperty JobKey "EOD SSAS PnLDaily GMT v4" -PassThru

    if (CheckModuleIsImported SsisRunnerModule)  {Import-Module SsisRunnerModule}
    if (CheckModuleIsImported TestModule)        {Import-Module TestModule}
    $request
    $result = $request | Start-SsisRunnerJob -Url $SSISRunnerURL -SqlServer $SSISSqlServer -Environment $Environment -GroupKey -1 -PackageSource "SqlServer" -ConnectionStrings $ConnectionStrings -Variables $Variables

    Write-Host "Request IDs"
    $result | Out-String

    Write-Host "Waiting for jobs to complete..."
    @($result | Wait-SsisRunnerJob -Url $SSISRunnerURL -Interval 20 -Verbose) | Out-String

    Write-Host "Job Status"
    $SSISResult = $result | Get-SsisRunnerJobStatus -Url $SSISRunnerURL

    foreach ($Result in $SSISResult) {
    $TestResult = Test-AreEqual -Actual $Result.Status -Expected "Success" -Comment ("Result of running " + $Result.PackageName) -Verbose
    $TestResult
    }

    #RunSSISPackageV2 $SSISRunnerURL $request $SSISSqlServer $Environment -1 $ConnectionStrings $Variables
    
    #Write-Host "`nStart 770 - GMT EOD SSAS Processing - PnLExplainedCube"
    #$request = @()
    #$request += New-Object Object | Add-Member NoteProperty PackageName ($SSISPackageFolder + "\DynamicEndurPnLGeneric")  -PassThru | Add-Member NoteProperty QueueName "EOD.SSAS.Processing"  -PassThru | Add-Member NoteProperty JobKey "EOD SSAS PnLExplained GMT 11" -PassThru
    #RunSSISPackageV2 $SSISRunnerURL $request $SSISSqlServer $Environment -1 @{"Endur"="$EndurDBConnString";"Reporting"="$ReportingDBConnString";"SSAS.ErpCube"="$PnLDailyCubeConnString"} @{"internalLegalEntity"="GMT LTD - LE";"internalBusinessUnit"="All";"EodDate"=$EodDate;"Directory"=$XMLADirectory;"filterByLE"="False";"XmlaFileName"="PnLExplained_111"}

    #Write-Host "`nStart 770 - GMT EOD SSAS Processing - PnLExposureCube"
    #$request = @()
    #$request += New-Object Object | Add-Member NoteProperty PackageName ($SSISPackageFolder + "\DynamicEndurPnLGeneric")  -PassThru | Add-Member NoteProperty QueueName "EOD.SSAS.Processing"  -PassThru | Add-Member NoteProperty JobKey "EOD SSAS PnLExposure GMT 11" -PassThru
    #RunSSISPackageV2 $SSISRunnerURL $request $SSISSqlServer $Environment -1 @{"Endur"="$EndurDBConnString";"Reporting"="$ReportingDBConnString";"SSAS.ErpCube"="$PnLDailyCubeConnString"} @{"internalLegalEntity"="GMT LTD - LE";"internalBusinessUnit"="All";"EodDate"=$EodDate;"Directory"=$XMLADirectory;"filterByLE"="False";"XmlaFileName"="PnLExposure_111"}

    #Write-Host "`nStart 770 - GMT EOD SSAS Processing - PnLDeliveryDayCube"
    #$request = @()
    #$request += New-Object Object | Add-Member NoteProperty PackageName ($SSISPackageFolder + "\DynamicEndurPnLGeneric")  -PassThru | Add-Member NoteProperty QueueName "EOD.SSAS.Processing"  -PassThru | Add-Member NoteProperty JobKey "EOD SSAS PnLDeliveryDay GMT 11" -PassThru
    #RunSSISPackageV2 $SSISRunnerURL $request $SSISSqlServer $Environment -1 @{"Endur"="$EndurDBConnString";"Reporting"="$ReportingDBConnString";"SSAS.ErpCube"="$PnLDailyCubeConnString"} @{"internalLegalEntity"="GMT LTD - LE";"internalBusinessUnit"="All";"EodDate"=$EodDate;"Directory"=$XMLADirectory;"filterByLE"="False";"XmlaFileName"="PnLDeliveryDay_111"}
}

# Mainline
try {
    Initialise
    Main
}

finally {
    Write-Host "`nDone $ThisTest"
}
