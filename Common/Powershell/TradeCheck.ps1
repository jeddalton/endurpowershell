# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  
# |  $Global:DealRef='BackOffice_GAS_PhysIdxPr-Smpl'
# |  $Global:ERFile='\Regression\BackOffice\ExpectedResults\ER_BackOffice_GAS.xlsx'
# |  $Global:SQLCompare='\Common\SQL\TradeExtract\GAS_PhysIdxPr-Smpl.sql'
# |  $Global:ExcludeKeys='xinput_date|xdeal_num|xins_num|xtran_num|xlast_updated_by|xlast_update'
# |  $Global:CompKeys='Reference'
# |  
# |  
# |  
# +-----------------------------------------------------------------------------------------------------------------------------------------------

# To assign variables and make their scope global; 
Function Initialise {

    cls
    # Runtime initialisation
    Write-Host "In initialise"
    $Global:script = "$TestAssetBaseDir\Common\Powershell\EndurUtils.ps1"
    . $script                # Loads functions into current scope
    #$Global:script = "$TestAssetBaseDir\Common\Powershell\GeneralUtils.ps1"
    #. $script                # Loads functions into current scope
    ImportRunTimeModules

    get-command -module testmodule
        
    # Script variables
    #EndurConn $DealRef       # External function call to establish either a Manual (local) or Auto (batch) Endur session
}

# To check results match with expected result sheet - Manual: Display comparison results; Auto: make comparison results available for subsequent catch
Function CheckResults {

    # Set path to expected results sheet
    $ERFilename = Join-Path $TestAssetBaseDir $ERFile
    Write-Host "`nTradeLoad: $DealRef - Expected results file is: $ERFilename"
    
    # Set path to SQL file used in comparison
    $sqlfile    = Join-Path $TestAssetBaseDir $SQLCompare
    Write-Host "`nTradeLoad: $DealRef - SQL file is: $sqlfile"
    $sql        =[System.IO.File]::ReadAllText($sqlfile)
    #Write-Host $sql
    
    # Define string substitution in trade extract SQL i.e. the deal reference
    $vars = @("DealRef=$DealRef")
    
    # Get trade extract query results from Endur database 
    $EndurRes = @(Invoke-Sqlcmd -ServerInstance $EndurServerName -Database $EndurDatabaseName -Query $sql -Variable $vars -QueryTimeout 200)
    
    # Define the keys and exclusions for the upcoming data compare 
    $exclude    = $ExcludeKeys.split("|")
    $keys       = $CompKeys.split("|")
   
    Write-Host "`nTradeLoad: $DealRef - Exclude keys: " $ExcludeKeys
    Write-Host "`nTradeLoad: $DealRef - Comparison keys: " $CompKeys
  
    # Do the actual data comparison
    Write-Host "`nTradeLoad: $DealRef - Extract and compare with Master"
    if ($RunMode -eq "Manual") {
        $result = @(Compare-Data -LeftObject $EndurRes -RightConnectionString $ERFilename -RightQuery $DealRef -Excludes $exclude -EmptyStringIsNull -Keys $keys -Pseudo "Description" -CommandTimeout 200)
        $result | Convert-CompareResultToTestResult -ColumnSeparator "," -Comment { $args[0].Right.Description } | Out-GridView
    }
    else {
        $result = @(Compare-Data -LeftObject $EndurRes -RightConnectionString $ERFilename -RightQuery $DealRef -Excludes $exclude -EmptyStringIsNull -Keys $keys -Pseudo "Description" -CommandTimeout 200)
        $result | Convert-CompareResultToTestResult -ColumnSeparator "," -Comment { $args[0].Right.Description }
    }
    
    $Global:Pass = "TRUE"
}

# Mainline
try {
    Initialise               # Assign variables
    CheckResults             # Compare $DealRef trades with master sheet
}

finally {
    # if ($RunMode -eq "Auto") {Stop-EndurSession}
    # $Error[0].InnerException.StackTrace
    Write-Host "`nTradeLoad: $DealRef - Done"
}
