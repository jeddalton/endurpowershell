﻿# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  
# |  $Global:Date1='30-Oct-2013'
# |  
# |  
# |  
# |  
# |  
# |  
# |  
# +-----------------------------------------------------------------------------------------------------------------------------------------------

# To assign variables and make their scope global; 
Function Initialise {

    cls
    # Runtime initialisation
    $Global:script     ="$TestAssetBaseDir\Common\Powershell\EndurUtils.ps1"
    . $script                # Loads functions into current scope
    ImportRunTimeModules

    EndurConn $ThisTest      # External function call to establish either a Manual (local) or Auto (batch) Endur session
}

Function Main {
    SetEndurDate
}

Function SetEndurDate {
    Write-Host "`nSet Endur Date to be $Date1"
    Set-EndurDate $Date1 -Verbose
    New-TestResult -Passed $true -Message "Date rolled to $Date1" -Comment "Date rolled to $Date1"
}

# Mainline
try {
    Initialise
    Main
}

finally {
    if ($RunMode -eq "Auto") {Stop-EndurSession}
    Write-Host "`nDone $ThisTest Date Roll"
}
