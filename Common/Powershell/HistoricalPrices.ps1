# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  
# |  $Global:ThisTest='HistoricalPrices'
# |  $Global:HPFile='Regression\PACMAN\Data\historical_prices.csv'
# |  
# |  
# |  
# |  
# |  
# |  
# +-----------------------------------------------------------------------------------------------------------------------------------------------

# To assign variables and make their scope global; 
Function Initialise {
    
    cls
    # Runtime initialisation
    $Global:script     ="$TestAssetBaseDir\Common\Powershell\EndurUtils.ps1"
    . $script                # Loads functions into current scope
    ImportRunTimeModules

    EndurConn $ThisTest       # External function call to establish either a Manual (local) or Auto (batch) Endur session

}

# To import historical prices into Endur from CSV file
Function Main {
    $Global:HPFilePath = "$TestAssetBaseDir\$HPFile"
    Import-HistoricPrices -HistoricPricesCsvFile $HPFilePath -Verbose
    New-TestResult -Passed $true -Message "Historical Prices Loaded" -Comment "from $HPFilePath"
}

# Mainline
try {
    Initialise
    Main
}

finally {
    if ($RunMode -eq "Auto") {Stop-EndurSession}
    Write-Host "`nDone $ThisTest"
}
