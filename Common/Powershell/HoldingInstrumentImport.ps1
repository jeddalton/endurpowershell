﻿# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  
# |  $Global:HoldingInstruments='\Regression\BackOffice\Data\HoldingInstruments\Oil and Coal Holding Instruments.xlsx'
# |  $Global:Date1='01-Sep-2013'
# |  $Global:TestAssetBaseDir='C:\Ggtp\Test Assets'
# |  
# |  
# |  
# |  
# |  
# |  
# +-----------------------------------------------------------------------------------------------------------------------------------------------

# To assign variables and make their scope global; 
Function Initialise {
    
    cls
    # Runtime initialisation
    $Global:script     ="$TestAssetBaseDir\Common\Powershell\EndurUtils.ps1"
    . $script                # Loads functions into current scope
    ImportRunTimeModules
        
    EndurConn $ThisTest      # External function call to establish either a Manual (local) or Auto (batch) Endur session    
}

# To run the holding instrument import
Function Main {

    # Purge: Read list of Contract Codes and corresponding Instrument Type from SheetName="Header"
    $HeaderRows = Get-ExcelObject -Path (Join-Path $TestAssetBaseDir $HoldingInstruments) -WorksheetName "Header"
    foreach ($HeaderRow in $HeaderRows) {
        $InsType = $HeaderRow.InsType
        $ContractCode = $HeaderRow.ContractCode
        $EndDate = $HeaderRow.EndDate
        Write-Host "`nPurging $InsType - $ContractCode"

        # Purge Existing Holding Instruments
        $vars = @("InsType=" + $HeaderRow.InsType;"ContractCode=" + $HeaderRow.ContractCode;"EndDate=" + $EndDate)
        $trans = @(Invoke-Sqlcmd -ServerInstance $EndurServerName -Database $EndurDatabaseName -Variable $vars -InputFile (Join-Path $TestAssetBaseDir Common\SQL\Endur\GetHoldingInstrumentsToPurge.sql) -ErrorAction Stop) | Select-Object -ExpandProperty tran_num
        Write-Host "Holding Instrument Count to Purge is " $trans.count
        $trans | Clear-EndurTransaction
    }

    # Confirm Zero: Read same list of Contract Codes and corresponding Instrument Type from SheetName="Header".
    $HeaderRows = Get-ExcelObject -Path (Join-Path $TestAssetBaseDir $HoldingInstruments) -WorksheetName "Header"
    foreach ($HeaderRow in $HeaderRows) {
        $InsType = $HeaderRow.InsType
        $ContractCode = $HeaderRow.ContractCode
        $EndDate = $HeaderRow.EndDate
        Write-Host "`nZero Count $InsType - $ContractCode"

        # Count Remaining Holding Instruments
        $vars = @("InsType=" + $HeaderRow.InsType;"ContractCode=" + $HeaderRow.ContractCode;"EndDate=" + $EndDate)
        $trans = @(Invoke-Sqlcmd -ServerInstance $EndurServerName -Database $EndurDatabaseName -Variable $vars -InputFile (Join-Path $TestAssetBaseDir Common\SQL\Endur\GetHoldingInstrumentsToPurge.sql) -ErrorAction Stop) | Select-Object -ExpandProperty tran_num
        if ($trans.count -ne 0) {
            New-TestResult -Passed $False -Message "Holding Instruments failed to purge correctly. Records Remaining =" -Comment $trans.count
            Break
        }
    }

    # Import: Read list of Instrument Types and corresponding worksheet names in the Import Spreadsheet
    $HeaderRows = Get-ExcelObject -Path (Join-Path $TestAssetBaseDir $HoldingInstruments) -WorksheetName "Header"
    foreach ($HeaderRow in $HeaderRows) {
        $InsType = $HeaderRow.InsType
        $ContractCode = $HeaderRow.ContractCode
        $EndDate = $HeaderRow.EndDate

        if ($InsType -ne $prevInsType) {
            Write-Host "`nImporting $InsType - $ContractCode"
            $params = @{"filename"=(Join-Path $TestAssetBaseDir $HoldingInstruments);"sheet"=$InsType;"from_date"=[DateTime]$Date1;"eod_mode"=0;"preview_mode"=0}
            
            Write-Host "Holding Ins: " + $HoldingInstruments
            Write-Host "Ins Type: " + $InsType
            Write-Host "From Date: " + $Date1            

            $res = Invoke-EndurScript -ScriptName "GGTP.Config.InstrumentImport" -Parameters $params
        }
        else {
            Write-Host "      ... $InsType - $ContractCode"
        }
        $prevInsType = $InsType
    }

    # Confirm Import: Read same list of Contract Codes and corresponding Instrument Type from SheetName="Header".
    $HeaderRows = Get-ExcelObject -Path (Join-Path $TestAssetBaseDir $HoldingInstruments) -WorksheetName "Header"
    foreach ($HeaderRow in $HeaderRows) {
        $InsType = $HeaderRow.InsType
        $ContractCode = $HeaderRow.ContractCode
        $EndDate = $HeaderRow.EndDate
        Write-Host "`nGt Zero Count $InsType - $ContractCode"

        # Count Imported Holding Instruments
        $vars = @("InsType=" + $HeaderRow.InsType;"ContractCode=" + $HeaderRow.ContractCode;"EndDate=" + $EndDate)
        $trans = @(Invoke-Sqlcmd -ServerInstance $EndurServerName -Database $EndurDatabaseName -Variable $vars -InputFile (Join-Path $TestAssetBaseDir Common\SQL\Endur\GetHoldingInstrumentsToPurge.sql) -ErrorAction Stop) | Select-Object -ExpandProperty tran_num
        if ($trans.count -gt 0) {
            New-TestResult -Passed $True -Message "$ContractCode Holding Instruments imported correctly. Records loaded =" -Comment $trans.count
        }
        else{
            New-TestResult -Passed $False -Message "$ContractCode Holding Instruments NOT imported correctly" -Comment "Holding Instruments NOT imported correctly"
        }
    }
}

# Mainline
try {
    Initialise
    Main
}

finally {
    if ($RunMode -eq "Auto") {Stop-EndurSession}
    Write-Host "`nDone $ThisTest"
}
