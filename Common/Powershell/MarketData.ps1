# +-----------------------------------------------------------------------------------------------------------------------------------------------
# |  
# |  $Global:ThisTest='MarketData'
# |  $Global:MDFile='Common\Data\market_data.csv'
# |  $Global:VolsFile='Common\Data\volatility.csv'
# |  $Global:BatchSIMDef='\Regression\DealLifeCycle\SQL\CreateBatchSIMDef.sql'
# |  
# |  
# |  
# |  
# +-----------------------------------------------------------------------------------------------------------------------------------------------

# To assign variables and make their scope global; 
Function Initialise {
    
    cls
    # Runtime initialisation
    $Global:script     ="$TestAssetBaseDir\Common\Powershell\EndurUtils.ps1"
    . $script                # Loads functions into current scope
    ImportRunTimeModules

    EndurConn $ThisTest       # External function call to establish either a Manual (local) or Auto (batch) Endur session

    $Global:MDFile      = "$TestAssetBaseDir\$MDFile"
    $Global:VolsFile    = "$TestAssetBaseDir\$VolsFile"
    $Global:BatchSIMDef = "$TestAssetBaseDir\$BatchSIMDef"
    
    # Turn off this result as this fails
    Invoke-Sqlcmd -ServerInstance $EndurSession.ServerName -Database $EndurSession.DatabaseName -Query "UPDATE pfolio_result_detail SET active_result=0 WHERE result_enumeration='USER_RESULT_FINANCIAL_EXPOSURE_EXPIRY'" -ErrorAction Stop
	
    #Make sure Test batch sim def is set up
    Invoke-Sqlcmd -ServerInstance $EndurSession.ServerName -Database $EndurSession.DatabaseName -InputFile $BatchSIMDef -ErrorAction Stop
}

# To import market data into Endur from CSV file
Function Main {
    Import-MarketData -MarketDataCsvFile $MDFile -Verbose
    Invoke-EndurScript -ScriptName "TESTUTIL_Load_Volatilities" -Parameters @{"file"=$VolsFile} -Verbose
}

# Mainline
try {
    Initialise
    Main
}

finally {
    if ($RunMode -eq "Auto") {Stop-EndurSession}
    Write-Host "`nDone $ThisTest"
}
