﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Management.Automation;
using System.Reflection;
using System.ServiceModel;
using Aspose.Cells;
using Gazprom.MT.PowerShell.Endur.Code;
using Gazprom.MT.PowerShell.Endur.DataLoaderService;
using Gazprom.MT.PowerShell.Endur.DataLoaderService.Enums;
using Gazprom.MT.PowerShell.Endur.DataLoaderService.Profiles;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsData.Import, "Deals")]
    public class ImportDealsCmdlet : EndurCmdletBase
    {
        private const int DefaultPortNumberForDealLoader = 9000;
        private WorksheetCollection dealSheets;

        [Alias("File")]
        [Parameter(Mandatory = true, Position = 0, HelpMessage = "Deals xls file")]
        public String ExcelFile { get; set; }

        [Alias("Sheet")]
        [Parameter(Mandatory = false, Position = 1)]
        public String SheetName { get; set; }

        [Alias("Port")]
        [Parameter(Mandatory = false, Position = 2)]
        public int PortNumber { get; set; }

        public ImportDealsCmdlet()
        {
            var license = new License();
            license.SetLicense("Aspose.Cells.lic");
        }

        protected override void ProcessRecord()
        {
            try
            {
                var fullPath = Path.GetFullPath(ExcelFile);
                if (File.Exists(fullPath))
                {
                    var sheetList = new Hashtable();

                    using (var fs = File.Open(ExcelFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {

                        var workbook = new Workbook(fs);
                        dealSheets = workbook.Worksheets;

                        foreach (var worksheet in dealSheets.Cast<Worksheet>().Where(worksheet => !sheetList.ContainsKey(worksheet.Name)))
                        {
                            sheetList.Add(worksheet.Name, worksheet.Name);
                        }

                        if (!string.IsNullOrEmpty(SheetName) && !SheetName.Equals("*"))
                        {
                            ProcessSheet(GetWorkSheet(ref dealSheets, SheetName));
                        }
                        else
                        {
                            foreach (var sheetKey in sheetList.Keys)
                            {
                                try
                                {
                                    ProcessSheet(GetWorkSheet(ref dealSheets, (string)sheetList[sheetKey]));
                                }
                                catch (Exception ex)
                                {
                                    this.Session.Debug.PrintLine(ex.Message);
                                    WriteWarning(ex.Message);
                                }
                            }
                        }
                    }
                }
                else { throw new Exception(String.Format("Specified Excel file '{0}' File not found.", ExcelFile)); }
            }
            catch (Exception e)
            {
                WriteError(new ErrorRecord(e, "Import-Deals", ErrorCategory.NotSpecified, null));
            }
        }


        private Worksheet GetWorkSheet(ref WorksheetCollection sheets, string sheetName)
        {
            var worksheet = sheets[sheetName];
            if (worksheet == null) throw new Exception(String.Format("Worksheet '{0}' does not exist in specified excel file: {1}", sheetName, ExcelFile));
            worksheet.Cells.DeleteBlankRows();
            worksheet.Cells.DeleteBlankColumns();
            return worksheet;
        }

        private static DataTable ConvertSheetToDataTable(Worksheet worksheet)
        {
            try
            {
                return worksheet.Cells.ExportDataTable(0, 0, worksheet.Cells.MaxDataRow + 1, worksheet.Cells.MaxDataColumn + 1, true, true);
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("Unable to process worksheet '{0}', due to: {1}", worksheet.Name, ex.Message));
            }
        }

        private void ProcessSheet(Worksheet sheet)
        {
            if (sheet == null) return;

            var dealsTable = ConvertSheetToDataTable(sheet);
            if (dealsTable.Rows.Count == 0) throw new Exception(String.Format("'{0}' worksheet does not contain any deals to import.", sheet.Name));

            if (dealsTable.Columns.Contains("Toolset")
                && dealsTable.Columns.Contains("InstrumentType"))
            {
                LoadProductsFromDataTable(dealsTable);
            }
            else if (dealsTable.Columns.Contains("ProfileType"))
            {
                throw new ApplicationException("Profile cannot be saved on their own but should be saved as part of the product in deal booking service");
            }

            WriteObject(String.Format("Deal(s) processed from worksheet: '{0}'.", sheet.Name));
        }

        private void LoadProductsFromDataTable(DataTable dataTable)
        {
            var toolset = (string)dataTable.Rows[0]["Toolset"];
            var instrumentType = (string)dataTable.Rows[0]["InstrumentType"];
            var cashflowType = string.Empty;
            if (dataTable.Columns.Contains("CashflowType"))
            {
                cashflowType = (string)dataTable.Rows[0]["CashflowType"];
            }

            var dealBookingService = GetInstanceOfDealBookingService();
            var productTypeName = dealBookingService.GetProductTypeByDefinition(toolset, instrumentType, cashflowType);
            var productType = Assembly.GetExecutingAssembly().GetType(string.Format("{0}.{1}", typeof(ProductBase).Namespace,
                                                                      productTypeName));
            var productList = DataTableMapper.GetMappedList(productType, dataTable, LoadProductProfiles).ToList();

            var productResultList = new List<ProductResult>();
            productList.ForEach(p => productResultList.AddRange(SaveProduct(dealBookingService, p as ProductBase)));
            foreach (var result in productResultList)
            {
                if (String.IsNullOrEmpty(result.ErrorMessage))
                {
                    WriteObject(String.Format("Deal with reference '{0}' imported successfully.", result.OriginalProduct.Reference));
                }
                else
                {
                    WriteWarning(result.ErrorMessage);
                }
            }
        }

        private IEnumerable LoadProductProfiles(DataRow row, PropertyInfo propertyInfo, string tableName)
        {
            Type profileType = propertyInfo.PropertyType;

            if (propertyInfo.PropertyType.IsGenericType)
            {
                Type[] genericTypeList = propertyInfo.PropertyType.GetGenericArguments();
                profileType = genericTypeList[0];
            }
            if (propertyInfo.PropertyType.IsArray)
            {
                profileType = propertyInfo.PropertyType.GetElementType();
            }

            DataTable relatedTable = ConvertSheetToDataTable(GetWorkSheet(ref dealSheets, tableName));
            var tableView = relatedTable.DefaultView;
            tableView.RowFilter = string.Format("Reference='{0}'", row["reference"]);
            tableView.Sort = "StartDate ASC";
            IEnumerable<object> profiles = DataTableMapper.GetMappedList(profileType, tableView.ToTable(), null);
            if (profileType.Equals(typeof(FloatProfile)))
            {
                return profiles.Cast<FloatProfile>().ToArray();
            }
            if (profileType.Equals(typeof(FixedProfile)))
            {
                return profiles.Cast<FixedProfile>().ToArray();
            }
            return profiles.ToArray();
        }

        private static IEnumerable<ProductResult> SaveProduct(IDealBookingService dealBookingService, ProductBase p)
        {

            var productResultList = new List<ProductResult>();
            var productResult = new ProductResult { OriginalProduct = p };
            try
            {
                var savedDealnumber = 0;
                savedDealnumber = dealBookingService.SaveProduct(p);
                p.DealNumber = savedDealnumber;
            }
            catch (Exception ex)
            {
                productResult.ErrorMessage = ex.Message;
            }
            try
            {
                productResult.SavedProducts.AddRange(
                    dealBookingService.GetProductsByReference(p.Reference).Where(
                        pr => pr.TranStatus != TranStatusEnum.Cancelled).
                        OrderByDescending(pr => pr.ExternalRef));
            }
            catch (Exception exception)
            {
                productResult.ErrorMessage = String.Format("{0}/tProduct Retrival Failed {1}", productResult.ErrorMessage, exception.ToString());
            }
            productResultList.Add(productResult);
            return productResultList;
        }

        private DealBookingServiceClient GetInstanceOfDealBookingService()
        {
            var binding = new BasicHttpBinding(BasicHttpSecurityMode.TransportCredentialOnly) { MessageEncoding = WSMessageEncoding.Mtom };
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Ntlm;
            binding.Security.Transport.ProxyCredentialType = HttpProxyCredentialType.Ntlm;
            return new DealBookingServiceClient(binding, new EndpointAddress(String.Format(@"http://{0}:{1}/DealLoader", AppServerName,PortNumber==0?DefaultPortNumberForDealLoader:PortNumber)));
        }

        private string AppServerName
        {
            get
            {
                /*NOTE: It is assumed that 32bit service name will be Appserver02*/
                var sql = string.Format("select workstation_name from service_mgr where service_name='Appserver02'");
                var table = Session.IOFactory.RunSQL(sql);
                return table.Rows.Select((row => row.Cells.GetItem(0).String.Trim())).SingleOrDefault();
            }
        }

    }
}
