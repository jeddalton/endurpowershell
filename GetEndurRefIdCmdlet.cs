﻿namespace Gazprom.MT.PowerShell.Endur
{
    using System;
    using System.Linq;
    using System.Management.Automation;

    using Olf.Openrisk.Staticdata;

    [Cmdlet(VerbsCommon.Get, "EndurRefId", DefaultParameterSetName = "Convert")]
    public class GetEndurRefIdCmdlet : EndurCmdletBase
    {
        [Parameter(Mandatory = true, ValueFromPipeline = true, ValueFromPipelineByPropertyName = true, Position = 0, ParameterSetName = "Convert")]
        public string[] Name { get; set; }

        [Parameter(Mandatory = true, ValueFromPipelineByPropertyName = true, Position = 1, ParameterSetName = "Convert")]
        public string ReferenceTable { get; set; }

        [Parameter(Mandatory = true, ParameterSetName = "List")]
        public SwitchParameter ListTables { get; set; }

        protected override void ProcessRecord()
        {
            if (this.ParameterSetName == "Convert")
            {
                EnumReferenceTable referenceTable;
                if (!Enum.TryParse(this.ReferenceTable, true, out referenceTable))
                {
                    throw new InvalidOperationException(
                        string.Format(
                            "Cannot convert {0} to type EnumReferenceTable. Use -ListTables to get valid values.",
                            this.ReferenceTable));
                }

                foreach (var name in this.Name)
                {
                    this.WriteObject(this.Session.StaticDataFactory.GetId(referenceTable, name));
                }
            }
            else
            {
                this.WriteObject(Enum.GetNames(typeof(EnumReferenceTable)).OrderBy(x => x), true);
            }
        }
    }
}