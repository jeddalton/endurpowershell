using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Management.Automation;
using System.Threading;
using Gazprom.MT.PowerShell.Endur.Code;
using Olf.Openrisk.Table;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsLifecycle.Invoke, "EndurScript", DefaultParameterSetName = "ParameterSet")]
    public class InvokeEndurScriptCmdlet : EndurCmdletBase
    {
        private static readonly Dictionary<Type, EnumColType> TypeToColTypeMap = new Dictionary<Type, EnumColType>
            {
                { typeof(String), EnumColType.String },
                { typeof(Int32), EnumColType.Int },
                { typeof(Double), EnumColType.Double },
                { typeof(DateTime), EnumColType.DateTime}
            };

        [Parameter(Mandatory = true, Position = 0, HelpMessage = "Name of the Endur script to execute")]
        public String ScriptName { get; set; }

        [Parameter(ParameterSetName = "ParameterSet")]
        public Hashtable Parameters { get; set; }

        [Parameter(ParameterSetName = "ArgtSet")]
        public PSObject[] Argt { get; set; }

        [Parameter]
        public String ServiceName { get; set; }

        [Parameter]
        public String ServiceMethod { get; set; }

        protected override void ProcessRecord()
        {

            Table argt = null;
            if (this.ParameterSetName == "ArgtSet")
            {
                argt = this.Session.TableFactory.CreateTable();
                EndurPowershellUtil.PSObjectsToTable(argt, this.Argt, null);
            }

            using (var table = argt ?? this.Session.TableFactory.CreateTable())
            {
                if (this.Parameters != null)
                {
                    foreach (var columnName in this.Parameters.Keys)
                    {
                        table.AddColumn(columnName.ToString(), GetColType(Parameters[columnName].GetType()));
                    }

                    var row = table.AddRow();
                    var index = 0;
                    foreach (var columnName in this.Parameters.Keys)
                    {
                        SetCellValue(row, index++, this.Parameters[columnName]);
                    }
                }

                if (ServiceName == null)
                {
                    using (var returnt = this.Session.ControlFactory.RunScript(this.ScriptName, table))
                    {
                        if (returnt == null || returnt.Rows.Count == 0)
                        {
                            return;
                        }

                        WriteObject(EndurPowershellUtil.TableToPSObjects(returnt).ToList(), true);
                    }
                }
                else
                {
                    var methodParam = CreateMethodParamTable();
                    methodParam.SetTable("method_params", 0, table);
                    methodParam.SetString("script_name", 0, ScriptName);
                    var service = Session.ControlFactory.GetService(ServiceName);
                    if (!service.IsRunning)
                    {
                        service.Start();
                        Thread.Sleep(3000);
                    }

                    var serviceMethod = service.GetMethod(ServiceMethod);
                    using (var returntTbl = serviceMethod.Execute(methodParam))
                    {
                        if (returntTbl == null || returntTbl.Rows.Count == 0)
                        {
                            return;
                        }
                        var returnt = returntTbl.GetTable("returnt", 0);
                        WriteObject(EndurPowershellUtil.TableToPSObjects((returnt == null || (returnt.RowCount == 0 && returnt.ColumnCount == 0)) ? 
                                                                                returntTbl.GetTable("argt", 0).GetTable("method_params", 0) : 
                                                                                returnt).ToList(), true);
                    }
                }
            }
        }

        private static EnumColType GetColType(Type type)
        {
            return TypeToColTypeMap.ContainsKey(type) ? TypeToColTypeMap[type] : EnumColType.String;
        }

        private static void SetCellValue(TableRow row, int cellIndex, object value)
        {
            if (value == null)
            {
                return;
            }

            TableCell cell = row.Cells.GetItem(cellIndex);

            switch (cell.Type)
            {
                case EnumColType.Date:
                case EnumColType.DateTime:
                    cell.Date = Convert.ToDateTime(value);
                    break;

                case EnumColType.Double:
                    cell.Double = Convert.ToDouble(value);
                    break;

                case EnumColType.Int:
                    cell.Int = Convert.ToInt32(value);
                    break;

                case EnumColType.String:
                    cell.String = value.ToString();
                    break;

                default:
                    throw new InvalidOperationException(String.Format("Cannot store value '{0}' of type '{1}' for table cell type '{2}'", value, value.GetType(), cell.Type));
            }
        }

        private Table CreateMethodParamTable()
        {
            var table = Session.TableFactory.CreateTable("method_params");
            table.AddColumn("method_params", EnumColType.Table);
            table.AddColumn("mktd_cache", EnumColType.Int);
            table.AddColumn("script_id", EnumColType.Int);
            table.AddColumn("script_name", EnumColType.String);
            table.AddRow();
            return table;
        }
    }
}
