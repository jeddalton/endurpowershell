using System.Management.Automation;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsCommon.Get, "EndurSession")]
    public class GetEndurSessionCmdlet : EndurCmdletBase
    {
        protected override void ProcessRecord()
        {
            WriteObject(this.Session);
        }
    }
}