using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Management.Automation;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsCommon.New, "EndurWorkflow")]
    public class NewEndurWorkflowCmdlet : EndurCmdletBase
    {
        [Parameter(Mandatory = true, Position = 0)]
        public string Name { get; set; }

        [Parameter(Position = 1)]
        public string ServiceManager { get; set; }

        protected override void ProcessRecord()
        {
            var workflowId = Session.GetNewWorkflowJobId();
            var sequenceNumber = GetNextWorkflowSequenceNumber();
            var serviceManagerId = GetServiceManagerId(ServiceManager);

            var parameters = new Dictionary<string, string>
            {
                {"job_id", $"{workflowId}"},
                {"wflow_id", $"{workflowId}"},
                {"type", "0"},
                {"sub_id", "0"},
                {"name", $"'{Name}'"},
                {"sequence", "0"},
                {"personnel_type", "0"},
                {"run_site", $"{serviceManagerId}"},
                {"max_run", "0"},
                {"exc_type", "0"},
                {"exc_id", "0"},
                {"what_next", "0"},
                {"owner", "0"},
                {"scope", "0"},
                {"schedule_0", "0"},
                {"schedule_1", "0"},
                {"schedule_2", "1993523515"},
                {"schedule_3", "3180349"},
                {"schedule_4", "184886104"},
                {"schedule_5", "184885248"},
                {"schedule_6", "184886104"},
                {"schedule_7", "3183615"},
                {"subscription", "' '"},
                {"wflow_sequence", $"{sequenceNumber}"},
                {"pin_flag", "0"},
                {"service_type", "1"},
                {"service_group_type", "0"},
                {"job_run_site", "-2"},
                {"grid_scheduler", "0"},
                {"rerun_counter", "0"},
                {"script_log", "1"},
                {"user_id", $"{Session.User.Id}"},
                {"wflow_mgr_cat_id", "0"}
            };

            var parametersString = string.Join(",\n", parameters.Select(kvp => $"@{kvp.Key}={kvp.Value}"));

            using (Session.IOFactory.RunSQL($"EXEC insert_job_cfg\n{parametersString}"))
            {
            }

            Session.SendJobCfgChanged();

            WriteObject(workflowId);
        }

        private int GetServiceManagerId(string serviceManagerName)
        {
            if (string.IsNullOrEmpty(serviceManagerName)) return 0;

            var serviceManagerSql = 
                "SELECT  \r\n" + 
                "	id AS 'Id'\r\n" + 
                $"FROM service_mgr WHERE service_name='{serviceManagerName}'\r\n";

            using (var result = Session.IOFactory.RunSQL(serviceManagerSql))
            {
                if (result.RowCount == 0)
                {
                    throw new IOException($"Cannot find service manager called {serviceManagerName}");
                }

                return result.GetInt(0, 0);
            }
        }

        private int GetNextWorkflowSequenceNumber()
        {
            const string sql = "SELECT MAX(wflow_sequence) AS 'sequence_num' \r\n" +
                               "FROM job_cfg j \r\n" +
                               "WHERE j.type=0 \r\n" +
                               "AND j.service_type=1 \r\n";

            using (var resultTable = Session.IOFactory.RunSQL(sql))
            {
                return resultTable.GetInt(0, 0) + 1;
            }
        }
    }
}