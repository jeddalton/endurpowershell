﻿using System;
using System.Linq;

namespace Gazprom.MT.PowerShell.Endur
{
    using System.Management.Automation;

    using Gazprom.MT.PowerShell.Endur.Code;

    using Olf.NetToolkit;
    using Olf.NetToolkit.Enums;

    /// <summary>
    /// A Cmdlet to import FX historic prices from a CSV file
    /// </summary>
    [Cmdlet(VerbsCommon.Set, "EndurHistoricalPrices")]
    public class SetEndurHistoricPricesCmdlet : EndurCmdletBase
    {
        [Parameter(Mandatory = true, ValueFromPipeline = true, Position = 0)]
        public PSObject Input { get; set; }

        private Table _table;

        protected override void BeginProcessing()
        {
            _table = GetHistoricPriceTable();
        }

        protected override void ProcessRecord()
        {
            try
            {
                var properties = this.Input.Properties.Where(x => x.IsGettable && this._table.GetColNum(x.Name) > 0).ToArray();
                if (properties.Length < 8)
                {
                    throw new PSInvalidOperationException("Date object does not have enough properties");
                }

                var row = _table.AddRow();
                Console.WriteLine("Processing index: {0} for date: {1}", properties.Where(x => x.Name.Equals("index_id")), properties.Where(x => x.Name.Equals("date")));
                foreach (var column in properties)
                {
                    EndurRowUtil.SetNtkTableValue(_table, _table.GetColNum(column.Name), row, column.Value);
                }
            }
            catch (Exception e)
            {
                WriteError(new ErrorRecord(e, "Import-HistoricPrices", ErrorCategory.NotSpecified, null));
            }
        }

        protected override void EndProcessing()
        {
            var errorTable = Table.TableNew();
            try
            {
                ConvertColsToIds(_table);
                
                Console.WriteLine("saving indexes");
                var retVal = Index.TableImportHistoricalPrices(this._table, errorTable);
                if (retVal != (int)OLF_RETURN_CODE.OLF_RETURN_SUCCEED)
                {
                    throw new Exception(
                        "Failed to import historical prices for some unkown reason - check the error logs");
                }
                base.EndProcessing();
            }
            catch (Exception e)
            {
                WriteError(new ErrorRecord(e, "Import-HistoricPrices", ErrorCategory.NotSpecified, null));
                throw;
            }
            finally
            {
                this._table.Destroy();
                errorTable.Destroy();
            }
        }

        #region privatemethods
        private static Table GetHistoricPriceTable()
        {
            var table = Table.TableNew();
            table.AddCols("S(index_id) I(date) I(start_date) I(end_date) I(yield_basis) S(ref_source) I(index_location) F(price)");
            return table;
        }

        private static void ConvertColsToIds(Table table)
        {
            table.ConvertStringCol((int)HIST_PRICE_TABLE_ENUM.HP_TABLE_COL_INDEX_ID, (int)COL_TYPE_ENUM.COL_INT, (int)SHM_USR_TABLES_ENUM.INDEX_TABLE);

            table.ConvertStringCol((int)HIST_PRICE_TABLE_ENUM.HP_TABLE_COL_REF_SOURCE, (int)COL_TYPE_ENUM.COL_INT, (int)SHM_USR_TABLES_ENUM.REF_SOURCE_TABLE);

           // table.ConvertStringCol((int)HIST_PRICE_TABLE_ENUM.HP_TABLE_COL_INDEX_LOCATION, (int)COL_TYPE_ENUM.COL_INT, (int)SHM_USR_TABLES_ENUM.INDEX_LOCATION_TABLE);
        }       

        #endregion
    }
}

