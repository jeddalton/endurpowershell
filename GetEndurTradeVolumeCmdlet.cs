﻿namespace Gazprom.MT.PowerShell.Endur
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Management.Automation;

    using Olf.Openrisk.Table;

    using TradeVolume.ServiceContracts;

    public enum VolumeTypeOptions
    {
        PerTradeSetup,
        Hourly,
        Daily
    }

    public enum VolumeTypeTimeZones
    {
        GetCompleteTrade,
        UTC,
        Local
    }

    [Cmdlet(VerbsCommon.Get, "EndurTradeVolume")]
    public class GetEndurTradeVolumeCmdlet : EndurCmdletBase
    {
        
        [Parameter(Mandatory = false, ParameterSetName = "DealNumber", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true, Position = 0)]
        public Int32 DealNumber { get; set; }

        [Alias("Url")]
        [Parameter(Mandatory = true,  Position = 1)]
        public string EnvironmentUrl { get; set; }

        [Alias("U")]
        [Parameter(Mandatory = true, Position = 2)]
        public string User { get; set; }
        
        [Parameter(Mandatory = false, Position = 3)]
        public VolumeTypeOptions VolumeType {get; set;}

        [Parameter(Mandatory = false, Position = 4)]
        public VolumeTypeTimeZones TimeZone {get; set;}

        [Parameter(Mandatory = false, Position = 5)]
        public DateTime StartDateTime { get; set; }

        [Parameter(Mandatory = false, Position = 6)]
        public DateTime EndDateTime { get; set; }

        protected override void ProcessRecord()
        {
            var client = new TradeVolumeClient(new Uri(EnvironmentUrl), User);
            IList<VolumeEntry> volumes = null;
            try
            {
                DealVolumeType dvt = DealVolumeType.NotAllowed;
                switch (this.VolumeType){
                    case VolumeTypeOptions.PerTradeSetup: dvt = DealVolumeType.NotAllowed; break;
                    case VolumeTypeOptions.Hourly: dvt = DealVolumeType.Hourly; break;
                    case VolumeTypeOptions.Daily: dvt = DealVolumeType.Daily; break;
                }
                if (this.TimeZone == VolumeTypeTimeZones.GetCompleteTrade)
                {
                    volumes = client.GetTradeVolumesByVolumeType(this.DealNumber, dvt);
                }
                else
                {
                    if (this.TimeZone == VolumeTypeTimeZones.UTC)
                    {
                        volumes = client.GetTradeVolumesByDateRangeUtcByVolumeType(this.DealNumber, StartDateTime, EndDateTime, dvt);
                    }
                    else
                    {
                        volumes = client.GetTradeVolumesByDateRangeDealLocaleByVolumeType(this.DealNumber, StartDateTime, EndDateTime, dvt);
                    }
                }
             }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            WriteObject(volumes);
        }
    }
}