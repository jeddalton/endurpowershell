﻿using Olf.Openrisk.Trading;

namespace Gazprom.MT.PowerShell.Endur
{
    using System;
    using System.Management.Automation;

    using Olf.NetToolkit;

    [Cmdlet(VerbsCommon.Set, "EndurUnderlyingTranOnComFutCmdlet")]
    public class SetEndurUnderlyingTranOnComFutCmdlet : EndurCmdletBase
    {
        [Parameter(Mandatory = true)]
        public Transaction Tran { get; set; }

        [Parameter(Mandatory = true)]
        public Transaction Underlying { get; set; }

        [Parameter(Mandatory = false, ParameterSetName = "Authorise")]
        public SwitchParameter Authorise { get; set; }

        protected override void ProcessRecord()
        {
            var retVal = Tran.SetUnderlyingTranOnComOptFut(Underlying);
            if (retVal == 0)
            {
                throw new Exception(
                    string.Format(
                        "failed to set underlying tran [{0}] onto tran [{1}]",
                        this.Underlying.GetTranNum(),
                        this.Tran.GetTranNum()));
            }

            //if (this.Authorise.IsPresent)
            //{
            //    Tran.AuthorizeSharedIns();
            //}
        }
    }
}
