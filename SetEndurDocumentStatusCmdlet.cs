using System;
using System.Management.Automation;
using Gazprom.MT.PowerShell.Endur.Code;
using Olf.Openrisk.BackOffice;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsCommon.Set, "EndurDocumentStatus")]
    public class SetEndurDocumentStatusCmdlet : EndurCmdletBase
    {
        [Parameter(Mandatory = true, Position = 0, ValueFromPipelineByPropertyName = true)]
        public Int32 DocumentNumber { get; set; }

        [Parameter(Mandatory = true, Position = 1, ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        public String Status { get; set; }

        [Parameter(ValueFromPipelineByPropertyName = true)]
        public Int32 Version { get; set; }

        protected override void ProcessRecord()
        {
            using (var dc = new DisposableCollection())
            {
                var document = this.Version != -1 ?
                    dc.Add(this.Session.BackOfficeFactory.RetrieveDocument(this.DocumentNumber, this.Version)) :
                    dc.Add(this.Session.BackOfficeFactory.RetrieveDocument(this.DocumentNumber));

                var documentStatus = dc.Add(this.Session.StaticDataFactory.GetReferenceObject<DocumentStatus>(this.Status));

                document.Process(documentStatus, true);
            }
        }
    }
}