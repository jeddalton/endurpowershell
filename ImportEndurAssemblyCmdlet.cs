using System;
using System.IO;
using System.Linq;
using System.Management.Automation;
using System.Reactive.Disposables;
using System.Text;
using Olf.Openrisk.IO;
using File = System.IO.File;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsData.Import, "EndurAssembly")]
    public class ImportEndurAssemblyCmdlet : EndurCmdletBase
    {
        [Parameter(Mandatory = true, ParameterSetName = "FileSet", ValueFromPipeline = true)]
        public object AssemblyFile { get; set; }

        protected override void ProcessRecord()
        {
            try
            {
                var assemblyFile = GetAssemblyFileFromParameter(AssemblyFile);

                ImportIntoEndur(assemblyFile);

                WriteObject(assemblyFile);
            }
            catch (Exception e)
            {
                WriteError(new ErrorRecord(e, e.HResult.ToString(), ErrorCategory.NotSpecified, e.Data));
                throw;
            }
        }

        private void ImportIntoEndur(FileInfo assemblyFile)
        {
            // from https://ocwikiext.openlink.com/opencomponents_user_guide/articles/i/o/_/IO_(Common).html
            // file, directive, extra info
            // directive 35 = Add OpenComponents .NET Assembly

            var iof = Session.IOFactory;
            var importRecord = string.Format("{0},35,{1}", assemblyFile.Name, Path.GetFileNameWithoutExtension(assemblyFile.Name));
            var importFileName = Path.Combine(assemblyFile.DirectoryName, assemblyFile.Name + ".import");
            using (Disposable.Create(() => File.Delete(importFileName)))
            {
                File.WriteAllText(importFileName, importRecord, Encoding.ASCII);
                iof.ImportScripts(importFileName, EnumScriptType.User);
            }
        }

        private FileInfo GetAssemblyFileFromParameter(object assemblyFilePsParameter)
        {
            var baseObject = UnwrapAssemblyFileValue(assemblyFilePsParameter);
            var assemblyFile = CreateFileInfoFrom(baseObject);

            WriteVerbose(string.Format("importing {0}", assemblyFile.FullName));

            if (!assemblyFile.Exists)
            {
                throw new FileNotFoundException("Specified Assembly file not found", assemblyFile.FullName);
            }
            return assemblyFile;
        }

        private static FileInfo CreateFileInfoFrom(object baseObject)
        {
            FileInfo fileInfo;
            if (baseObject is FileInfo)
            {
                fileInfo = baseObject as FileInfo;
            }
            else
            {
                fileInfo = new FileInfo(baseObject.ToString());
            }
            return fileInfo;
        }

        private static object UnwrapAssemblyFileValue(object assemblyFilePsParameter)
        {
            var powershellObject = assemblyFilePsParameter as PSObject;
            var baseObject = powershellObject != null ? powershellObject.BaseObject : assemblyFilePsParameter;

            if (baseObject is PSCustomObject)
            {
                baseObject = ((PSObject) assemblyFilePsParameter).Properties.First().Value;
            }
            return baseObject;
        }
    }
}