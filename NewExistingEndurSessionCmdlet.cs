﻿using System.Management.Automation;
using Gazprom.MT.PowerShell.Endur.Code;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsCommon.New, "ExistingEndurSession")]
    public class NewExistingEndurSessionCmdlet : EndurCmdletBase
    {
        [Parameter(Position = 0)]
        public string ConfigFilePath { get; set; }

        [Parameter(Mandatory = false, Position = 1)]
        public string User { get; set; }

        [Parameter(Mandatory = false, Position = 2)]
        public string Password { get; set; }

        protected override void ProcessRecord()
        {
            var implementationStrategy = new EndurAttachToExistingSessionFactory();
            var session = CreateSession(implementationStrategy, implementationStrategy.GetOlfBinDirectory());
            WriteObject(session.Session);
        }
    }
}