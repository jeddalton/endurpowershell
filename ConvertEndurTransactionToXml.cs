﻿namespace Gazprom.MT.PowerShell.Endur
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Management.Automation;
    using System.Xml;
    using System.Xml.Linq;

    using Gazprom.MT.PowerShell.Endur.Code;

    using Olf.Openrisk.Staticdata;
    using Olf.Openrisk.Table;
    using Olf.Openrisk.Trading;

    using Field = Olf.Openrisk.Trading.Field;

    [Cmdlet(VerbsData.Convert, "EndurTransactionToXml")]
    public class ConvertEndurTransactionToXml : EndurCmdletBase
    {
        private const string TransactionSet = "Transaction";

        private const string TransactionIdSet = "TransactionId";

        [Parameter(Mandatory = true, ValueFromPipeline = true, Position = 0, ParameterSetName = TransactionSet)]
        public Transaction[] Transaction { get; set; }

        [Parameter(Mandatory = true, ValueFromPipeline = true, Position = 0, ParameterSetName = TransactionIdSet)]
        public int[] TransactionId { get; set; }

        protected override void ProcessRecord()
        {
            using (var dc = new DisposableCollection())
            {
                var transactions = Enumerable.Empty<Transaction>();

                switch (this.ParameterSetName)
                {
                    case TransactionSet:
                        transactions = this.Transaction;
                        break;

                    case TransactionIdSet:
                        transactions =
                            this.TransactionId.Select(
                                // ReSharper disable once AccessToDisposedClosure
                                id => dc.Add(this.Session.TradingFactory.RetrieveTransactionById(id)));
                        break;
                }

                this.WriteObject(transactions.Select(this.ConvertToXml), true);
            }
        }

        private static string GetXmlName(string text)
        {
            return string.Concat(text.Trim().SelectMany(FilterChar));
        }

        private static string GetFieldName(Field field)
        {
            return GetXmlName(field.Name);
        }

        private static IEnumerable<char> FilterChar(char c, int index)
        {
            if (char.IsDigit(c) && index == 0)
            {
                return "_" + c;
            }

            if (char.IsLetterOrDigit(c) || c == '_')
            {
                return c.ToString(CultureInfo.InvariantCulture);
            }

            return char.IsWhiteSpace(c) ? "_" : string.Empty;
        }

        private XmlNode ConvertToXml(Transaction transaction)
        {
            var tradeElement = this.CreateTradeElement(transaction);

            using (var reader = tradeElement.CreateReader())
            {
                var doc = new XmlDocument();
                doc.Load(reader);

                return doc;
            }
        }

        private XElement CreateTradeElement(Transaction transaction)
        {
            using (var dc = new DisposableCollection())
            {
                var tradeElement = new XElement("trade", new XAttribute("tran_num", transaction.TransactionId));

                transaction.Fields.Select(dc.Add).Select(this.CreateFieldElement).ForEach(tradeElement.Add);

                try
                {
                    if (transaction.Toolset != EnumToolset.Fx)
                    {
                        using (var pricingDetailsTable = transaction.PricingDetails.AsTable())
                        {
                            var pricingDetails = new XElement("PricingDetails");
                            tradeElement.Add(pricingDetails);
                            pricingDetails.Add(this.CreateTableElement(pricingDetailsTable));
                        }
                    }
                }
// ReSharper disable EmptyGeneralCatchClause
                catch
// ReSharper restore EmptyGeneralCatchClause
                {
                }

                var legsElement = new XElement("legs");
                tradeElement.Add(legsElement);

                transaction.Legs.Select(dc.Add).Select(this.CreateLegElement).ForEach(tradeElement.Add);

                return tradeElement;
            }
        }

        private XElement CreateLegElement(Leg leg)
        {
            var element = new XElement("leg", new XAttribute("number", leg.LegNumber), new XAttribute("label", leg.LegLabel));

            // add legs
            leg.Fields.Select(f => this.CreateFieldElement(f)).ForEach(element.Add);

            // add cashflows
            var cflows = new XElement("cashflows");
            element.Add(cflows);
            leg.Cashflows.Select(this.CreateCashFlowElement).ForEach(cflows.Add);

            // add fees
            var fees = new XElement("fees");
            element.Add(fees);
            leg.Fees.Select(this.CreateFeeElement).ForEach(fees.Add);

            // add formulas
            var formulas = new XElement("formulas");
            element.Add(formulas);
            leg.Formulas.Select(this.CreateFormulaElement).ForEach(formulas.Add);

            // add commodity prices
            var commPrices = new XElement("commodity_prices");
            element.Add(commPrices);
            leg.CommodityPrices.Select(this.CreateCommodityPriceElement).ForEach(commPrices.Add);

            // add keep whole defs
            var keepWholeDefinitions = new XElement("keep_whole_definitions");
            element.Add(keepWholeDefinitions);
            leg.KeepWholeDefinitions.Select(this.CreateKeepWholeDefinitionElement).ForEach(keepWholeDefinitions.Add);

            // logical cashflows??
            var logicalCashflows = new XElement("logical_cashflows");
            element.Add(logicalCashflows);
            leg.LogicalCashflows.Select(this.CreateLogicalCashFlowElement).ForEach(logicalCashflows.Add);

            // options
            var options = new XElement("options");
            element.Add(options);
            leg.Options.Select(this.CreateOptionElement).ForEach(options.Add);

            // profiles
            var profiles = new XElement("profiles");
            element.Add(profiles);
            leg.Profiles.Select(this.CreateProfileElement).ForEach(profiles.Add);

            // reset definitions
            var resetDefinitions = new XElement("reset_definitions");
            element.Add(resetDefinitions);
            resetDefinitions.Add(this.CreateResetDefinitionElement(leg.ResetDefinition));

            // resets
            var resets = new XElement("resets");
            element.Add(resets);
            leg.Resets.Select(this.CreateResetElement).ForEach(resets.Add);

            // schedule details
            var scheduleDetails = new XElement("schedule_details");
            element.Add(scheduleDetails);
            leg.ScheduleDetails.Select(this.CreateScheduleDetailElement).ForEach(scheduleDetails.Add);

            return element;
        }

        private XElement CreateCashFlowElement(Cashflow cflow)
        {
            var element = new XElement("cashflow", new XAttribute("number", cflow.CashflowNumber));

            cflow.Fields.Select(f => new XElement(GetXmlName(f.Name), this.CreateFieldElement(f)))
            .ForEach(element.Add);

            return element;
        }

        private XElement CreateFeeElement(Fee fee)
        {
            var element = new XElement("fee", new XAttribute("number", fee.FeeNumber));

            fee.Fields.Select(f => new XElement(GetXmlName(f.Name), this.CreateFieldElement(f)))
            .ForEach(element.Add);

            return element;
        }

        private XElement CreateFormulaElement(Formula formula)
        {
            var element = new XElement("formula", new XAttribute("number", formula.FormulaNumber));

            formula.Fields.Select(f => new XElement(GetXmlName(f.Name), this.CreateFieldElement(f)))
            .ForEach(element.Add);

            return element;
        }

        // commodity prices
        private XElement CreateCommodityPriceElement(CommodityPrice commPrice)
        {
            var element = new XElement("commodity_price", new XAttribute("number", commPrice.CommodityPriceNumber));

            commPrice.Fields.Select(f => new XElement(GetXmlName(f.Name), this.CreateFieldElement(f)))
            .ForEach(element.Add);

            return element;
        }

        // Keep whole
        private XElement CreateKeepWholeDefinitionElement(KeepWholeDefinition keepWholeDef)
        {
            var element = new XElement("keep_whole_definition", new XAttribute("number", keepWholeDef.KeepWholeDefinitionNumber));

            keepWholeDef.Fields.Select(f => new XElement(GetXmlName(f.Name), this.CreateFieldElement(f)))
            .ForEach(element.Add);

            return element;
        }

        // Logical cashflows, no idea how they differ from cashflows
        private XElement CreateLogicalCashFlowElement(LogicalCashflow logicalCashflow)
        {
            var element = new XElement("logical_cashflow", new XAttribute("number", logicalCashflow.CashflowNumber));

            logicalCashflow.Fields.Select(f => new XElement(GetXmlName(f.Name), this.CreateFieldElement(f)))
                .ForEach(element.Add);

            return element;
        }

        private XElement CreateOptionElement(Option option)
        {
            var element = new XElement("option", new XAttribute("number", option.OptionNumber));

            option.Fields.Select(f => new XElement(GetXmlName(f.Name), this.CreateFieldElement(f))).ForEach(element.Add);

            return element;
        }

        private XElement CreateProfileElement(Profile profile)
        {
            var element = new XElement("profile", new XAttribute("number", profile.ProfileNumber));

            profile.Fields.Select(f => new XElement(GetXmlName(f.Name), this.CreateFieldElement(f)))
            .ForEach(element.Add);

            // profile details
            var profileDetails = new XElement("profile_details");
            element.Add(profileDetails);
            profile.ProfileDetails.Select(this.CreateProfileDetailsElement).ForEach(profileDetails.Add);

            return element;
        }

        private XElement CreateProfileDetailsElement(ProfileDetail profileDetail)
        {
            var element = new XElement("profile_detail", new XAttribute("number", profileDetail.ProfileDetailNumber));

            profileDetail.Fields.Select(f => new XElement(GetXmlName(f.Name), this.CreateFieldElement(f)))
            .ForEach(element.Add);

            return element;
        }

        private XElement CreateResetDefinitionElement(ResetDefinition resetDefinition)
        {
            var element = new XElement("reset_definition");

            resetDefinition.Fields.Select(f => new XElement(GetXmlName(f.Name), this.CreateFieldElement(f)))
            .ForEach(element.Add);

            return element;
        }

        // resets -- seems to produce nothing
        private XElement CreateResetElement(Reset reset)
        {
            var element = new XElement("reset");

            reset.Fields.Select(f => new XElement(GetXmlName(f.Name), this.CreateFieldElement(f)))
            .ForEach(element.Add);

            return element;
        }

        private XElement CreateScheduleDetailElement(ScheduleDetail scheduleDetail)
        {
            var element = new XElement("schedule_detail");

            scheduleDetail.Fields.Select(f => new XElement(GetXmlName(f.Name), this.CreateFieldElement(f)))
            .ForEach(element.Add);

            return element;
        }

        private XElement CreateFieldElement(Field field)
        {
            object value;

            if (field.DataType == EnumFieldType.Table)
            {
                try
                {
                    using (var table = field.GetValueAsTable())
                    {
                        value = this.CreateTableElement(table);
                    }
                }
                catch (Exception)
                {
                    value = new XText(field.GetValueAsString().Trim());
                }

            }
            else
            {
                switch (field.DataType)
                {
                    case EnumFieldType.Date:
                        try
                        {
                            value = field.GetValueAsDate();
                        }
                        catch (Exception)
                        {
                            value = field.GetValueAsString().Trim();
                        }
                        break;

                    case EnumFieldType.DateTime:
                        try
                        {
                            value = field.GetValueAsDateTime();
                        }
                        catch (Exception)
                        {
                            value = field.GetValueAsString().Trim();
                        }
                        break;

                    case EnumFieldType.Time:
                        value = field.GetValueAsTime();
                        break;

                    case EnumFieldType.Int:
                    case EnumFieldType.UnsignedInt:
                        value = field.DisplayString.Trim();
                        break;

                    case EnumFieldType.Boolean:
                        value = field.GetValueAsBoolean();
                        break;

                    case EnumFieldType.Double:
                        value = field.GetValueAsDouble();
                        break;

                    case EnumFieldType.Long:
                        value = field.GetValueAsLong();
                        break;

                    default:
                        value = field.GetValueAsString().Trim();
                        break;
                }
            }

            return new XElement(
                GetFieldName(field),
                new XAttribute("id", field.Id),
                new XAttribute("name", field.Name),
                value);
        }

        private XElement CreateTableElement(ConstTable table)
        {
            if (table == null)
            {
                return null;
            }

            var tableElement = new XElement("table");

            var columns = Enumerable.Range(0, table.ColumnCount)
            .Select(i => new EndurColumn { Name = table.GetColumnName(i), Type = table.GetColumnType(i).ToString() })
            .ToArray();

            Enumerable.Range(0, table.RowCount)
            .ForEach(rowIndex =>
            {
                var rowElement = new XElement("row", new XAttribute("RowId", rowIndex));
                tableElement.Add(rowElement);

                Enumerable.Range(0, table.ColumnCount).ForEach(colIndex =>
                {
                    var colElement = new XElement(GetXmlName(columns[colIndex].Name));

                    switch (columns[colIndex].Type)
                    {
                        case "Table":
                            {
                                using (var childTable = table.GetTable(colIndex, rowIndex))
                                {
                                    colElement.Add(this.CreateTableElement(childTable));
                                }
                            }

                            break;

                        case "Int":
                        case "Int64":
                        case "Uint64":
                            colElement.Add(table.GetDisplayString(colIndex, rowIndex).Trim());
                            break;

                        case "Date":
                        case "DateTime":
                            colElement.Add(table.GetDate(colIndex, rowIndex));
                            break;

                        case "Double":
                            colElement.Add(table.GetDouble(colIndex, rowIndex));
                            break;

                        default:
                            var value = table.GetDisplayString(colIndex, rowIndex).Trim();
                            colElement.Add(value);
                            break;
                    }

                    rowElement.Add(colElement);
                });
            });

            return tableElement;
        }

        internal class EndurColumn
        {
            public string Name { get; set; }

            public string Type { get; set; }
        }
    }
}