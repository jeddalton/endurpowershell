﻿using System;
using System.IO;
using System.Linq;
using System.Management.Automation;
using Aspose.Cells;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsCommon.Get, "ExcelWorksheet")]
    public class GetExcelWorksheetCmdlet : PSCmdlet
    {
        [Parameter(Mandatory = true, ValueFromPipeline = true, ParameterSetName = "FileSet")]
        public String Filename { get; set; }

        [Parameter(Mandatory = true, ValueFromPipeline = true, ParameterSetName = "FileInfoSet")]
        public FileInfo FileInfo { get; set; }

        public GetExcelWorksheetCmdlet()
        {
            // Initialise Aspose
            var license = new License();
            license.SetLicense("Aspose.Cells.lic");

        }

        protected override void ProcessRecord()
        {
            string filename = this.ParameterSetName == "FileSet" ? this.Filename : this.FileInfo.FullName;

            if (!File.Exists(filename))
            {
                throw new InvalidOperationException(String.Format("Specified Excel file '{0}' does not exist", filename));
            }

            using (var stream = File.Open(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                var workbook = new Workbook(stream);

                WriteObject(workbook.Worksheets.Cast<Worksheet>().Select(s => s.Name), true);
            }

        }
    }
}