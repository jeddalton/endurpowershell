using System.Management.Automation;
using Gazprom.MT.PowerShell.Endur.Code;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsCommon.New, "EndurInProcessSession")]
    public class NewEndurSessionInProcessCmdlet : EndurCmdletBase
    {
        [Parameter(Mandatory = true, Position = 0)]
        public string ConfigFilePath { get; set; }

        [Parameter(Mandatory = true, Position = 1)]
        public string User { get; set; }

        [Parameter(Mandatory = true, Position = 2)]
        public string Password { get; set; }

        protected override void ProcessRecord()
        {
            var endurConfigFile = new EndurConfigFile(ConfigFilePath);
            var implementationStrategy = new EndurInProcessSessionFactory(User, Password, endurConfigFile);
            var session = CreateSession(implementationStrategy, endurConfigFile.GetOlfBinDirectory());
            WriteObject(session.Session);
        }
    }
}