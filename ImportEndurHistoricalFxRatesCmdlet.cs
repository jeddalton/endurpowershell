﻿using System;

namespace Gazprom.MT.PowerShell.Endur
{
    using System.Management.Automation;

    using Gazprom.MT.PowerShell.Endur.Code;

    using Olf.NetToolkit;
    using Olf.NetToolkit.Enums;

    /// <summary>
    /// A Cmdlet to import FX historic prices from a CSV file
    /// </summary>
    [Cmdlet(VerbsData.Import, "EndurHistoricalFxRates")]
    public class ImportEndurHistoricalFxRatesCmdlet : EndurCmdletBase
    {
        [Parameter(Mandatory = true, ValueFromPipeline = true)]
        public PSObject[] Input { get; set; }
        
        protected override void ProcessRecord()
        {
            try
            {
                using (var table = GetHistoricFxRatesTable())
                {
                    foreach (var rate in Input)
                    {
                        var row = table.AddRow();
                        foreach (var column in rate.Properties)
                        {
                            EndurRowUtil.SetNtkTableValue(table, table.GetColNum(column.Name), row, column.Value);
                        }
                    }
                    
                    ConvertColsToIds(table);
                    
                    var retVal = Index.SaveHistoricalFxRates(table);
                    if (retVal != (int)OLF_RETURN_CODE.OLF_RETURN_SUCCEED)
                    {
                        throw new Exception(
                            "Failed to save historical FX rates for some unkown reason - check the error logs");
                    }
                }
            }
            catch (Exception e)
            {
                WriteError(new ErrorRecord(e, "Import-HistoricPrices", ErrorCategory.NotSpecified, null));
            }
        }

        #region Private Functions
        private static Table GetHistoricFxRatesTable()
        {
            var table = Table.TableNew();
            table.AddCols("I(fx_rate_date) S(currency_id) S(reference_currency_id) F(fx_rate_bid) F(fx_rate_mid) F(fx_rate_offer) S(data_set_type) S(currency_convention)");
            return table;
        }

        private static void ConvertColsToIds(Table table)
        {
            table.ConvertStringCol((int)HIST_FX_PRICE_TABLE_ENUM.IMP_COL_FX_CURRENCY, (int)COL_TYPE_ENUM.COL_INT, (int)SHM_USR_TABLES_ENUM.CURRENCY_TABLE);

            table.ConvertStringCol((int)HIST_FX_PRICE_TABLE_ENUM.IMP_COL_REFERENCE_CURRENCY, (int)COL_TYPE_ENUM.COL_INT, (int)SHM_USR_TABLES_ENUM.CURRENCY_TABLE);

            table.ConvertStringCol((int)HIST_FX_PRICE_TABLE_ENUM.IMP_COL_FX_DATA_SET, (int)COL_TYPE_ENUM.COL_INT, (int)SHM_USR_TABLES_ENUM.IDX_MARKET_DATA_TYPE_TABLE);

            table.ConvertStringCol((int)HIST_FX_PRICE_TABLE_ENUM.IMP_COL_FX_CURRENCY_CONV, (int)COL_TYPE_ENUM.COL_INT, (int)SHM_USR_TABLES_ENUM.IDX_CURRENCY_CONVENTION_TABLE);
        }
        #endregion
    }
}
