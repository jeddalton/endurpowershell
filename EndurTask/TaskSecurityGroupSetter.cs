﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management.Automation;
using Olf.Openrisk.Application;
using Olf.Openrisk.IO;
using Olf.Openrisk.Staticdata;

namespace Gazprom.MT.PowerShell.Endur.EndurTask
{
    public class TaskSecurityGroupSetter
    {
        private readonly Session session;

        public TaskSecurityGroupSetter(Session session)
        {
            this.session = session;
        }

        public void Set(string taskName, IEnumerable<string> securityGroupNames)
        {
            if (!session.ControlFactory.IsValidTaskDefinition(taskName))
                throw new MethodException(string.Format("Endur Task named '{0}' is not valid.", taskName));

            var administrator = (Person)session.StaticDataFactory.GetReferenceObject(EnumReferenceObject.Person, "Administrator");
            try
            {
                session.Debug.FlushDirectoryCache();
                using (var endurTask = session.ControlFactory.RetrieveTaskDefinition(taskName))
                {
                    if (endurTask == null)
                    {
                        throw new MethodException(string.Format("Could not retrieve task named '{0}' from Endur", taskName));
                    }

                    var node = endurTask.Node;
                    if (node.Owner.Name != "Administrator")
                        node.Owner = administrator;

                    node.Group = (SecurityGroup)session.StaticDataFactory.GetReferenceObject(EnumReferenceObject.SecurityGroup, "Administration");

                    node.GroupPermissions = (int)(EnumFilePermissions.Executable + (int)EnumFilePermissions.Readable);
                    node.PublicPermissions = 0; //Remove all permissions

                    SecurityGroup[] securityGroups = GetSecurityGroups(securityGroupNames);
                    node.Groups = securityGroups;
                }
            }
            catch (Exception ex)
            {
                throw new MethodException(string.Format("Failed to assign security to the task named '{0}': {1}", taskName, ex.Message));
            }
        }

        private SecurityGroup[] GetSecurityGroups(IEnumerable<string> securityGroupNames)
        {
            return (from securityGroupName in securityGroupNames
                    where !string.IsNullOrEmpty(securityGroupName.Trim()) 
                    select (SecurityGroup)session.StaticDataFactory.GetReferenceObject(EnumReferenceObject.SecurityGroup, securityGroupName.Trim()))
                    .ToArray();
        }

    }
}