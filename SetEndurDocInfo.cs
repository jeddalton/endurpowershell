﻿using System;
using System.IO;
using System.Linq;
using System.Management.Automation;
using Gazprom.MT.PowerShell.Endur.Code;
using Olf.Openrisk.Application;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsCommon.Set, "DocInfo")]
    public class SetDocInfoCmdlet : EndurCmdletBase
    {
        [Parameter(Mandatory = true, Position = 0)]
        public int DocNum { get; set; }

        [Parameter(Mandatory = true, Position = 1)]
        public String DocInfoType { get; set; }

        [Parameter(Mandatory = true, Position = 2)]
        public String DocInfoValue { get; set; }

        protected override void ProcessRecord()
        {
            //var uniqueStldocHdrHistId = this.Session.GetNewWorkflowJobId("UNIQUE_STLDOC_HDR_HIST_ID"); 
            //var docInfoType = GetDocInfoTypeId(DocInfoType);
            //const int personnelId = 20075; // deploy user

            using (var result = this.Session.ControlFactory.RunScriptText(string.Format("void main() {{STLDOC_SaveInfoValue({0}, \"{1}\", \"{2}\");}}", DocNum, DocInfoType, DocInfoValue)))
            {
                //this.WriteObject(EndurPowershellUtil.TableToPSObjects(result).ToList());
            }
            //Session.BeginTransaction();

            //var sql =
            //    String.Format(
            //        "EXEC stldoc_save_info_value  {0} ,  {1} , {2} , '{3}' , {4}",
            //        DocNum, 
            //        uniqueStldocHdrHistId,
            //        docInfoType,
            //        DocInfoValue,
            //        personnelId);

            //using (this.Session.IOFactory.RunSQL(sql))
            //{
            //}

            //Session.CommitTransaction();

            this.Session.RefreshCache();

            //this.WriteObject(uniqueStldocHdrHistId);
        }

        //private Int32 GetDocInfoTypeId(String docInfoType)
        //{
        //    if (String.IsNullOrEmpty(docInfoType)) return 0;

        //    var docInfoTypesSql = String.Format(
        //       "SELECT  \r\n" +
        //       "	type_id AS 'Id'\r\n" +
        //       "FROM stldoc_info_types WHERE type_name='{0}'\r\n", docInfoType);

        //    using (var result = this.Session.IOFactory.RunSQL(docInfoTypesSql))
        //    {
        //        if (result.RowCount == 0)
        //        {
        //            throw new IOException(String.Format("Cannot find Doc Info Type called {0}", docInfoType));
        //        }

        //        return result.GetInt(0, 0);
        //    }
        //}
    }
}

