using System.Management.Automation;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsCommon.Get, "EndurWorkflow")]
    public class GetEndurWorkflowCmdlet : EndurCmdletBase
    {
        [Parameter(Mandatory = false, Position = 0, ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        public string Name { get; set; }

        protected override void ProcessRecord()
        {
            var sql = @"
SELECT wflow_id, name
FROM job_cfg j
WHERE j.service_type=1
AND j.type=0";

            if (!string.IsNullOrEmpty(this.Name))
            {
                sql += string.Format(" AND j.name='{0}'", this.Name);
            }

            using (var table = this.Session.IOFactory.RunSQL(sql))
            {
                for (var row = 0; row < table.RowCount; row++)
                {
                    this.WriteObject(new { Id = table.GetInt(0, row), Name = table.GetString(1, row) });
                }
            }
        }
    }
}