﻿using System;
using System.Management.Automation;
using Olf.Openrisk.Staticdata;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsCommon.Get, "EndurStaticDataId")]
    public class GetEndurStaticDataIdCmdlet : EndurCmdletBase
    {
        [Parameter(Mandatory = true, Position = 0)]
        public String Type { get; set; }

        [Parameter(Mandatory = true, Position = 1, ValueFromPipeline = true)]
        public String Value { get; set; }

        protected override void ProcessRecord()
        {
            var referenceTable = (EnumReferenceTable)Enum.Parse(typeof(EnumReferenceTable), this.Type, true);

            this.WriteObject(this.Session.StaticDataFactory.GetId(referenceTable, this.Value));
        }
    }
}