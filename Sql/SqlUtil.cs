﻿namespace Gazprom.MT.PowerShell.Endur.Sql
{
    using System;
    using System.IO;
    using System.Reflection;

    public static class SqlUtil
    {
        public static string GetSql(string filename)
        {
            using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(typeof(SqlUtil), filename))
            {
                if (stream != null)
                {
                    using (var reader = new StreamReader(stream))
                    {
                        return reader.ReadToEnd();
                    }
                }
            }

            throw new InvalidOperationException(string.Format("Could not reader assembly manifest stream for {0}", filename));
        }
    }
}