﻿CREATE TRIGGER TRG_ab_tran_history
ON ab_tran_history
AFTER INSERT
AS
BEGIN
	DECLARE @locationId int;
	DECLARE @tradingDate datetime;

	SET @locationId = (SELECT tl.trading_location_id FROM personnel p INNER JOIN personnel_trading_location tl ON tl.personnel_id=p.id_number WHERE p.name=USER_NAME() AND tl.default_flag=1);

	IF (@locationId IS NULL)
	BEGIN
		SET @locationId = 0;
	END

	SET @tradingDate = (SELECT d.business_date FROM system_dates d WHERE d.trading_location_id=@locationId)

	UPDATE ab_tran_history
	SET row_creation= @tradingDate + CAST(i.row_creation AS float)-FLOOR(CAST(i.row_creation AS float))
	FROM ab_tran_history ath
		INNER JOIN inserted i ON i.tran_num=ath.tran_num AND i.version_number=ath.version_number
END;
GO

CREATE TRIGGER TRG_ab_tran_info_history
ON ab_tran_info_history
AFTER INSERT
AS
BEGIN
	DECLARE @locationId int;
	DECLARE @tradingDate datetime;

	SET @locationId = (SELECT tl.trading_location_id FROM personnel p INNER JOIN personnel_trading_location tl ON tl.personnel_id=p.id_number WHERE p.name=USER_NAME() AND tl.default_flag=1);

	IF (@locationId IS NULL)
	BEGIN
		SET @locationId = 0;
	END

	SET @tradingDate = (SELECT d.business_date FROM system_dates d WHERE d.trading_location_id=@locationId)

	UPDATE ab_tran_info_history
	SET last_update= @tradingDate + CAST(i.last_update AS float)-FLOOR(CAST(i.last_update AS float))
	FROM ab_tran_info_history tih
		INNER JOIN inserted i ON i.tran_num=tih.tran_num AND i.type_id=tih.type_id AND i.last_update=tih.last_update
END;