﻿IF EXISTS
(
	SELECT *
	FROM sys.triggers trg
	WHERE trg.name = '{0}'
)
BEGIN
	DROP TRIGGER {0};
END;
