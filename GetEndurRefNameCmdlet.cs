﻿namespace Gazprom.MT.PowerShell.Endur
{
    using System;
    using System.Linq;
    using System.Management.Automation;

    using Olf.Openrisk.Staticdata;

    [Cmdlet(VerbsCommon.Get, "EndurRefName", DefaultParameterSetName = "Convert")]
    public class GetEndurRefNameCmdlet : EndurCmdletBase
    {
        [Parameter(Mandatory = true, ValueFromPipeline = true, ValueFromPipelineByPropertyName = true, Position = 0, ParameterSetName = "Convert")]
        public int[] Id { get; set; }

        [Parameter(Mandatory = true, ValueFromPipelineByPropertyName = true, Position = 1, ParameterSetName = "Convert")]
        public string ReferenceTable { get; set; }

        [Parameter(Mandatory = true, ParameterSetName = "List")]
        public SwitchParameter ListTables { get; set; }

        protected override void ProcessRecord()
        {
            if (this.ParameterSetName == "Convert")
            {
                EnumReferenceTable referenceTable;
                if (!Enum.TryParse(this.ReferenceTable, true, out referenceTable))
                {
                    throw new InvalidOperationException(
                        string.Format(
                            "Cannot convert {0} to type EnumReferenceTable. Use -ListTables to get valid values.",
                            this.ReferenceTable));
                }

                foreach (var id in this.Id)
                {
                    this.WriteObject(this.Session.StaticDataFactory.GetName(referenceTable, id));
                }
            }
            else
            {
                this.WriteObject(Enum.GetNames(typeof(EnumReferenceTable)).OrderBy(x => x), true);
            }
        }
    }
}