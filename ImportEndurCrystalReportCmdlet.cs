﻿using System;
using System.IO;
using System.Linq;
using System.Management.Automation;
using Gazprom.MT.PowerShell.Endur.Code;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsData.Import, "EndurCrystalReport")]
    public class ImportEndurCrystalReportCmdlet : EndurCmdletBase
    {
        [Parameter(Mandatory = true, Position = 0, ValueFromPipeline = true)]
        public Object File { get; set; }

        [Parameter]
        public String DirNodePath { get; set; }

        public ImportEndurCrystalReportCmdlet()
        {
            this.DirNodePath = @"/Crystal/Site";
        }

        protected override void ProcessRecord()
        {
            Object baseObject;
            if (this.File is PSObject)
            {
                baseObject = ((PSObject)this.File).BaseObject;
            }
            else
            {
                baseObject = this.File;
            }

            if (baseObject is PSCustomObject)
            {
                baseObject = ((PSObject)this.File).Properties.First().Value;
            }

            FileInfo fileInfo;
            if (baseObject is FileInfo)
            {
                fileInfo = baseObject as FileInfo;
            }
            else
            {
                fileInfo = new FileInfo(baseObject.ToString());
            }

            var result = Externals.oCRYSTAL_ImportTemplateFileToDB(fileInfo.FullName, this.DirNodePath);

            WriteObject(result);
        }
    }
}