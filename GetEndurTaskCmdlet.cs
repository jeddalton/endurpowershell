﻿using System.Management.Automation;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsCommon.Get, "EndurTask")]
    public class GetEndurTaskCmdlet: EndurCmdletBase
    {
        [Parameter(Mandatory = true, Position = 0)]
        public string Name { get; set; }

        protected override void ProcessRecord()
        {
            if (!Session.ControlFactory.IsValidTaskDefinition(Name))
                throw new MethodException(string.Format("Endur Task named '{0}' is not valid.", Name));

            var task = Session.ControlFactory.RetrieveTaskDefinition(Name);
            WriteObject(task);
        }
    }
}