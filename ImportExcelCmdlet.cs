﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Management.Automation;
using System.Linq;
using Gazprom.MT.PowerShell.Endur.Excel;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsData.Import, "Excel")]
    public class ImportExcelCmdlet : PSCmdlet
    {
        [Alias("File")]
        [Parameter(Mandatory = true, Position = 0, HelpMessage = "Excel file's Full Path")]
        public String ExcelFile { get; set; }

        [Alias("Sheet")]
        [Parameter(Mandatory = false, Position = 1)]
        public String SheetName { get; set; }

        protected override void ProcessRecord()
        {
            var dataTable = new ExcelFileManager(ExcelFile).Read(SheetName);
            WriteObject(TableToPSObjects(dataTable), true);
        }

        public static IEnumerable<PSObject> TableToPSObjects(DataTable table)
        {
            if (table == null) return null;

            var objectList = new List<PSObject>();

            foreach (var tableRow in table.Rows.Cast<DataRow>())
            {
                var o = new PSObject();

                for (var i = 0; i < table.Columns.Count; i++)
                {
                    var propertyName = table.Columns[i].ColumnName;
                    var propertyValue = tableRow[i];

                    var property = new PSNoteProperty(propertyName, propertyValue);
                    o.Properties.Add(property);
                }
                objectList.Add(o);
            }

            return objectList;
        }
 
    }
}