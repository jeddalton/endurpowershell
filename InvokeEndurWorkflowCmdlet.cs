﻿namespace Gazprom.MT.PowerShell.Endur
{
    using System;
    using System.Management.Automation;
    using System.Threading;

    using Olf.Openrisk.Table;

    [Cmdlet(VerbsLifecycle.Invoke, "EndurWorkflow")]
    public class InvokeEndurWorkflowCmdlet : EndurCmdletBase
    {
        [Parameter(Mandatory = true, Position = 0, ValueFromPipeline = true)]
        public string WorkflowName { get; set; }

        [Parameter(Position = 1)]
        public Int32 Sequence { get; set; }

        [Parameter(Position = 2)]
        public Int32 TimeoutInSeconds { get; set; }

        [Parameter(Position = 3)]
        public Int32 PollingFrequency { get; set; }

        [Parameter]
        public SwitchParameter Wait { get; set; }

        [Parameter]
        public SwitchParameter Async { get; set; }

        protected override void ProcessRecord()
        {
            var startDateTime = this.Session.ServerTime;
            var result = this.Session.StartWorkflow(this.WorkflowName, this.Sequence);

            CheckIfWorkflowStartedSuccessfully(result);

            if (!this.Async.IsPresent)
            {
                WaitToFinish(startDateTime);
            }
            else
            {
                this.WriteObject(result);
            }
        }

        private void WaitToFinish(DateTime startDateTime)
        {
            Thread.Sleep(1000);
            while (true)
            {
                if (DateTime.Now.Subtract(startDateTime).Seconds > TimeoutInSeconds)
                {
                    throw new ApplicationException("Workflow timed out before completion - check Services Manager.");
                }

                WriteMessage("Polling workflow status...");
                var table = GetStatus(startDateTime);

                if (table.RowCount != 0)
                {
                    var status = table.GetCell(0, 0).Int;

                    if (status != 6)
                    {
                        throw new ApplicationException("Workflow failed - check Services Manager for details.");
                    }

                    this.WriteMessage("Workflow finished successfully.");
                    break;
                }

                Thread.Sleep(PollingFrequency * 1000);
            }
        }

        private void WriteMessage(string msg)
        {
            this.Session.Debug.PrintLine(msg);
            this.WriteVerbose(msg);
        }

        private void CheckIfWorkflowStartedSuccessfully(int result)
        {
            switch (result)
            {
                case 1:
                    WriteMessage("Workflow started.");
                    break;
                case -1:
                    WriteMessage("Workflow failed to start: already running.");
                    break;
                case -2:
                    WriteMessage("Workflow failed to start: runsite not running..");
                    break;
                case -3:
                    WriteMessage("Workflow failed to start: security check not running.");
                    break;
                case 0:
                    WriteMessage("Workflow failed to start: reason unknown - check for errors above.");
                    break;
                default:
                    WriteMessage("Workflow failed to start: reason unknown.");
                    break;
            }

            if (result != 1)
            {
                throw new ApplicationException(string.Format("Failed to start workflow {0}", WorkflowName));
            }
        }

        private Table GetStatus(DateTime startDateTime)
        {
            var sql = string.Format(
@"
SELECT wfh.status
FROM wflow_history wfh, job_cfg jc 
WHERE wfh.end_time >= '{0}'
AND jc.name = '{1}' 
AND wfh.job_cfg_id = jc.job_id", 
                               startDateTime.ToString("yyyy-MM-dd HH:mm:ss"),
                               WorkflowName);

            return this.Session.IOFactory.RunSQL(sql);
        }
    }
}