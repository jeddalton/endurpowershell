﻿namespace Gazprom.MT.PowerShell.Endur
{
    using System;
    using System.Linq;
    using System.Management.Automation;

    using Gazprom.MT.PowerShell.Endur.Code;

    using Olf.Openrisk.Calendar;

    [Cmdlet(VerbsDiagnostic.Resolve, "EndurDateExpression")]
    public class ResolveEndurDateExpressionCmdlet : EndurCmdletBase
    {
        private DisposableCollection _disposableCollection;
        private SymbolicDate _symbolicDate;

        [Parameter(Mandatory = true, Position = 0)]
        public String Expresion { get; set; }

        [Parameter(Mandatory = true, ValueFromPipeline = true, ValueFromPipelineByPropertyName = true, Position = 1)]
        public DateTime Date { get; set; }

        [Parameter]
        public String[] HolidaySchedules { get; set; }

        protected override void BeginProcessing()
        {
            this._disposableCollection = new DisposableCollection();
            this._symbolicDate = this._disposableCollection.Add(this.Session.CalendarFactory.CreateSymbolicDate(this.Expresion));
            if (this.HolidaySchedules == null)
            {
                return;
            }

            var holidaySchedules = this._disposableCollection.Add(this.Session.CalendarFactory.CreateHolidaySchedules());
            foreach (
                var holidaySchedule in
                    this.HolidaySchedules.Where(x => !String.IsNullOrEmpty(x))
                        .Select(
                            holidayScheduleName =>
                            this._disposableCollection.Add(
                                this.Session.CalendarFactory.GetHolidaySchedule(holidayScheduleName))))
            {
                holidaySchedules.AddSchedule(holidaySchedule);
            }

            this._symbolicDate.HolidaySchedules = holidaySchedules;
        }

        protected override void EndProcessing()
        {
            this._disposableCollection.Dispose();
        }

        protected override void ProcessRecord()
        {
            var evaluatedDate = this._symbolicDate.Evaluate(this.Date, true);

            this.WriteObject(evaluatedDate);
        }
    }
}