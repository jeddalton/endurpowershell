﻿namespace Gazprom.MT.PowerShell.Endur
{
    using System.Management.Automation;

    [Cmdlet(VerbsData.Update, "EndurIndex")]
    public class UpdateEndurIndexCmdlet : EndurCmdletBase
    {
        [Parameter(Mandatory = true, Position = 0, ValueFromPipeline = true)]
        public string Name { get; set; }

        protected override void ProcessRecord()
        {
            this.RefreshIndex(this.Name);
            this.WriteVerbose(string.Format("Index {0} refreshed", this.Name));
        }

        private void RefreshIndex(string curveName)
        {
            var avsText = "void main() { TablePtr t = TABLE_New(); TABLE_AddCol(returnt, \"retval\", COL_INT); TABLE_AddRow(returnt); TABLE_LoadFromDbWithSQL( t, \"DISTINCT index_id, index_name\", \"idx_def\", \"db_status = 1 and index_name='" +
                                curveName + "'\"); TABLE_SetInt(returnt, 1, 1, INDEX_RefreshDbList( t, 0 )); TABLE_Destroy( t );}";
            this.Session.ControlFactory.RunScriptText(avsText).Dispose();
        }
    }
}