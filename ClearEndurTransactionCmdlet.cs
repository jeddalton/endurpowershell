using System;
using System.Management.Automation;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsCommon.Clear, "EndurTransaction")]
    public class ClearEndurTransactionCmdlet : EndurCmdletBase
    {
        [Parameter(Mandatory = true, Position = 0, ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        public Int32 TransactionId { get; set; }

        protected override void ProcessRecord()
        {
            var sql = String.Format("EXEC [dbo].[purge_transaction] {0}, 0, 0,1; ", this.TransactionId);

            try
            {
                this.Session.BeginTransaction();
                this.Session.IOFactory.RunSQL(sql).Dispose();
                this.Session.CommitTransaction();
            }
            catch
            {
                this.Session.RollbackTransaction();
                throw;
            }
        }
    }
}