using System;
using System.Collections;
using System.Collections.Generic;
using System.Management.Automation;
using Olf.Openrisk.Control;
using Olf.Openrisk.Table;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsLifecycle.Invoke, "EndurTask")]
    public class InvokeEndurTaskCmdlet : EndurCmdletBase
    {
        [Parameter(Mandatory = true, Position = 0, HelpMessage = "Name of the Endur task to execute")]
        public String TaskName { get; set; }

        [Parameter]
        public Hashtable Parameters { get; set; }

        protected override void ProcessRecord()
        {
            using (var launchable = this.Session.ControlFactory.RetrieveLaunchable(EnumLaunchableType.RunTaskByName))
            {
                using (var args = launchable.CreateArguments())
                {
                    args.GetItem(0).SetValue(this.TaskName);
                    launchable.Launch(args);
                }
            }
        }

    }
}