﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Management.Automation;
using System.Text;
using System.Text.RegularExpressions;
using Aspose.Cells;
using Gazprom.MT.PowerShell.Endur.Code;
using Olf.Openrisk.Simulation;
using Olf.Openrisk.Table;
using Olf.Openrisk.Trading;
using Olf.Openrisk.IO;
using Olf.Openrisk.Staticdata;

namespace Gazprom.MT.PowerShell.Endur
{
    /// <summary>
    /// A Cmdlet to run simulations to value specified transactions.
    /// </summary>
    [Cmdlet(VerbsLifecycle.Invoke, "SimRun")]
    public class InvokeSimRunCmdlet : EndurCmdletBase
    {
        private const string ExcelSheetName = "SimResults";

        #region Public Properties / Expected Parameters for Cmdlet
        /// <summary>Gets or sets the flat file which contains deal references.</summary>
        /// <value>The deal refs file.</value>
        [Alias("File")]
        [Parameter(Mandatory = true, Position = 0, HelpMessage = "Deal references flat file")]
        [ValidateNotNullOrEmpty]
        public string DealRefsFile { get; set; }

        /// <summary>Gets or sets the name of the eod sim to run</summary>
        /// <value>The name of the sim.</value>
        [Alias("Sim")]
        [Parameter(Mandatory = true, Position = 1, HelpMessage = "Name of EOD simulation to run")]
        [ValidateNotNullOrEmpty]
        public string SimName { get; set; }

        /// <summary>Gets or sets the name of the scenario.</summary>
        /// <value>The name of the scenario.</value>
        [Alias("Scenario")]
        [Parameter(Mandatory = true, Position = 2, HelpMessage = "Name of scenario to run")]
        [ValidateNotNullOrEmpty]
        public string ScenarioName { get; set; }

        /// <summary>Gets or sets the type of the result from the specified scenario.</summary>
        /// <value>The type of the result.</value>
        [Alias("Result")]
        [Parameter(Mandatory = true, Position = 3, HelpMessage = "Name of result type to be extracted")]
        [ValidateNotNullOrEmpty]
        public string ResultType { get; set; }

        /// <summary>Gets or sets a value indicating whether [output XLSX file].</summary>
        /// <value><c>true</c> if [output XLSX file]; otherwise, <c>false</c>.</value>
        [Alias("XlsPath")]
        [Parameter(Mandatory = false, Position = 4, HelpMessage = "Path where result of sim are to be saved in xlsx file")]
        public string OutputXlsxFilePath { get; set; }

        /// <summary>Gets or sets the name of the eod sim to run</summary>
        /// <value>The name of the sim.</value>
        [Alias("Reval Type")]
        [Parameter(Position = 5, HelpMessage = "Reval Type (IntraDay, EOD, etc.)")]
        public string RevalType { get; set; }

        /// <summary>Gets or sets the name of the eod sim to run</summary>
        /// <value>The name of the sim.</value>
        [Alias("Use Close")]
        [Parameter(Position = 6, HelpMessage = "Use Closing Prices")]
        public bool UseClose { get; set; }

        #endregion

        #region Private Properties
        private Transactions _transactions;
        private int _resultTypeId;
        private Hashtable _transactionNumRefLookup;
        #endregion

        public InvokeSimRunCmdlet()
        {
            var license = new License();
            license.SetLicense("Aspose.Cells.lic");
        }

        #region Overridden methods
        protected override void ProcessRecord()
        {
            // Uncomment if we need to debug
            //System.Diagnostics.Debugger.Launch();
            try
            {
                _transactions = this.Session.TradingFactory.CreateTransactions();
                _transactionNumRefLookup = new Hashtable();

                var commaSeparatedTranNums = GetCommaSeparatedTranNums(RetrieveTransactionRefsFromFile());

                using (Query query1 = Session.IOFactory.CreateQuery(EnumQueryType.Transaction))
                {
                    var page = query1.AddPage();
                    page.AdditionalCriteria = string.Format(@"ab_tran.tran_num in ({0})", commaSeparatedTranNums);

                    var simulation = RetrieveSimulation();
                    
                    // Set the REVAL Type = EOD
                    if (RevalType == "EOD")
                    simulation.SetRevalType(EnumSimulationRunType.Eod);
                    
                    if (UseClose == true)
                    simulation.UseClosingPrices = true;

                    var simResults = simulation.Run(query1);
                    var scenarioResults = simResults.GetScenarioResults(this.ScenarioName);

                    if (_resultTypeId > 0)
                    {
                        var resultsForType = scenarioResults.GetResultsForType(_resultTypeId);
                        var resultAsTable = resultsForType.GetColumnValuesAsTable("result")[0];
                        if (!string.IsNullOrEmpty(OutputXlsxFilePath)) { WriteObject(ExportToXlsFile(resultAsTable, OutputXlsxFilePath)); }
                        else { WriteObject(PrepareTableForOutput(resultAsTable), true); }
                    }
                }
            }
            catch (Exception e)
            {
                WriteError(new ErrorRecord(e, "Invoke-SimRun", ErrorCategory.NotSpecified, null));
            }
        }

        #endregion

        #region Private Functions
        private string RetrieveTransactionRefsFromFile()
        {
            var deals = new StringBuilder();
            var fullPath = Path.GetFullPath(DealRefsFile);
            if (System.IO.File.Exists(fullPath))
            {
                using (var sr = new StreamReader(fullPath))
                {
                    while (sr.Peek() > 0)
                    {
                        var line = sr.ReadLine();
                        if (line != null)
                        {   
                            deals.Append("'"+ line +"',");
                        }
                    }
                    if (deals.Length > 0) deals.Remove(deals.Length - 1, 1);
                }
            }
            else
            {
                throw new Exception(DealRefsFile + " file does not exist or check the path.");
            }
            return deals.ToString();
        }

        private string GetCommaSeparatedTranNums(string tranRefs)
        {
            var sql = String.Format(
                @"SELECT at.deal_tracking_num as deal_num, max(at.TRAN_NUM) as tran_num, at.reference
                FROM ab_tran at 
                WHERE at.reference in ({0}) 
                group by at.deal_tracking_num, at.reference
                order by 1 desc", tranRefs);

            using (var table = this.Session.IOFactory.RunSQL(sql))
            {
                IList<string> tranNums = new List<string>();
                foreach (var row in table.Rows)
                {
                    var tranNum = row.GetCell(1).Int;
                    tranNums.Add(tranNum.ToString());
                    var reference = row.GetCell(2).String;
                    if (!_transactionNumRefLookup.ContainsKey(tranNum)) _transactionNumRefLookup.Add(tranNum, reference);
                }

                return string.Join(",", tranNums.ToArray<string>());
            }
        }

        private Simulation RetrieveSimulation()
        {
            const string script = "{INDEX_RefreshDb(0);}";
            Session.ControlFactory.RunScriptText(script);
            
            var sim = Session.SimulationFactory.RetrieveSimulation(this.SimName);
            if (sim == null){ throw new Exception("No Sim found with name: " + this.SimName); }

            if (sim.Scenarios.Count < 1)
            {
                var message = string.Format("No or more than one scenario attached with sim def '{0}", sim.Name);
                throw new Exception(message);
            }

            var scenario = sim.GetScenario(this.ScenarioName);

            var resultTypeExists = false;
            foreach (var rt in scenario.ResultTypes.Where(rt => rt.Name == this.ResultType))
            {
                resultTypeExists = true;
                _resultTypeId = rt.Id;
            }

            if (!resultTypeExists) { throw new Exception("Result type: " + this.ResultType + " not found for scenario: " + this.ScenarioName); }

            return sim;
        }

        private IEnumerable<PSObject> PrepareTableForOutput(Table table)
        {
            System.Diagnostics.Debugger.Launch();

            if (table == null) return null;                  
                        
            var sdf = this.Session.StaticDataFactory;

            var objectList = new List<PSObject>();

            foreach (var tableRow in table.Rows)
            {
                var o = new PSObject();

                var dealNum = EndurRowMapperUtil.GetCellValue(tableRow.GetCell("deal_num"));
                if (_transactionNumRefLookup.ContainsKey(dealNum))
                {
                    o.Properties.Add(new PSNoteProperty("trade_ref", _transactionNumRefLookup[dealNum]));
                }

                for (var i = 0; i < table.ColumnCount; i++)
                {
                    var propertyName = table.Columns.GetItem(i).Name;
                    var propertyValue = EndurRowMapperUtil.GetCellValue(tableRow.Cells.GetItem(i));

                    if (Regex.IsMatch(propertyName, "_date"))
                    {
                        if ((int)propertyValue > 0) propertyValue = Session.CalendarFactory.GetDate((int)propertyValue);
                    }                    

                    if (propertyName.Equals("index"))
                    {
                        propertyValue = sdf.GetName(EnumReferenceTable.Index,(int)propertyValue);                        
                    }

                    var property = new PSNoteProperty(propertyName, propertyValue);
                    o.Properties.Add(property);
                    
                }
                objectList.Add(o);
            }

            return objectList;
        }

        private string ExportToXlsFile(Table table, string outputXlsxFilePath)
        {
            if (table == null) return string.Empty;
            var dataTable = TableToDataTable(table);

            //creaeting new workbook and a sheet
            var workbook = new Workbook();
            var sheet = workbook.Worksheets[0];

            sheet.Name = ExcelSheetName;
            //data import
            sheet.Cells.ImportDataTable(dataTable, true, "A1");
            
            //adding style
            var colCount = dataTable.Columns.Count;
            for (int i = 0; i < colCount; i++)
            {
                if (Regex.IsMatch(sheet.Cells[0, i].Value.ToString(), "_date"))
                {
                    var column = sheet.Cells.Columns[i];
                    var colStyle = column.Style;
                    colStyle.Custom = "dd/mm/yyyy";
                    column.ApplyStyle(colStyle, new StyleFlag(){All = true});
                }
            }

            if (outputXlsxFilePath.Substring(outputXlsxFilePath.Length - 1, 1) == @"\")
                outputXlsxFilePath = outputXlsxFilePath.Remove(outputXlsxFilePath.Length - 1, 1);
            
            var dir = System.IO.Directory.Exists(outputXlsxFilePath) ? outputXlsxFilePath : Path.GetDirectoryName(this.DealRefsFile);
            var fileName = string.Format(@"{0}\{1:yyyyMMddHHmmss}", dir, DateTime.Now);

            workbook.Save(fileName + ".xlsx", SaveFormat.Xlsx);
            return fileName + ".xlsx";
        }

        private DataTable TableToDataTable(Table table)
        {
            var dataTable = PrepareDataTable(table, ExcelSheetName);

            var sdf = this.Session.StaticDataFactory;

            foreach (var tableRow in table.Rows)
            {
                var row = dataTable.NewRow();

                var dealNum = EndurRowMapperUtil.GetCellValue(tableRow.GetCell("deal_num"));
                if (_transactionNumRefLookup.ContainsKey(dealNum))
                {
                    row["trade_ref"] = _transactionNumRefLookup[dealNum];
                }

                for (var i = 0; i < table.ColumnCount; i++)
                {
                    var columnName = table.Columns.GetItem(i).Name;
                    var columnValue = EndurRowMapperUtil.GetCellValue(tableRow.Cells.GetItem(i));

                    if (Regex.IsMatch(columnName, "_date") && (int) columnValue > 0)
                    {
                         row[columnName] = Session.CalendarFactory.GetDate((int) columnValue);
                    }
                    else if (columnName.Equals("index") && (int)columnValue > 0)
                    {
                        row[columnName] = sdf.GetName(EnumReferenceTable.Index, (int)columnValue);
                    }
                    else
                    {
                        row[columnName] = columnValue;
                    }
                }
                dataTable.Rows.Add(row);
            }
            return dataTable;
        }

        private static DataTable PrepareDataTable(Table table, string tableName)
        {
            var dt = new DataTable(tableName);

            // Add in Trade Ref because we add this data in
            dt.Columns.Add(new DataColumn("trade_ref", typeof(String)));
            
            foreach (var pi in table.Columns)
            {
                DataColumn column;
                // Convert 'date' columns to DateTime because we susbstitute the Julian dates later
                if (Regex.IsMatch(pi.Name, "_date"))
                {
                column = new DataColumn(pi.Name) {DataType = typeof(DateTime)};
                }
                // Convert 'index' column to String because we put that data back in later
                else if (pi.Name.Equals("index"))
                {
                column = new DataColumn(pi.Name) {DataType = typeof(String)};
                }
                // Otherwise set the column type = the Endur column type
                else
                {                
                column = new DataColumn(pi.Name) {DataType = EndurRowMapperUtil.GetCellType(pi.Cells.GetItem(0))};
                }
                
                dt.Columns.Add(column);
            }

            return dt;
        }
        
        #endregion
    }
}
