﻿namespace Gazprom.MT.PowerShell.Endur
{
    using System.Management.Automation;

    [Cmdlet(VerbsCommon.Get, "EndurCurrentUser")]
    public class GetEndurCurrentUser : EndurCmdletBase
    {
        protected override void ProcessRecord()
        {
            this.WriteObject(this.Session.User);
        }
    }
}