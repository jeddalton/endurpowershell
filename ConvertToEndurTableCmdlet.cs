﻿using System;
using System.Collections.Generic;
using System.Management.Automation;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsData.ConvertTo, "EndurTable")]
    public class ConvertToEndurTableCmdlet : EndurCmdletBase
    {
        [Parameter(Mandatory = true, ValueFromPipeline = true, Position = 0)]
        public PSObject Object { get; set; }

        [Parameter(Position = 1)]
        public String[] Properties { get; set; }

        private readonly List<PSObject> _objects = new List<PSObject>();

        protected override void ProcessRecord()
        {
            this._objects.Add(this.Object);
        }

        protected override void EndProcessing()
        {
            var table = this.Session.TableFactory.CreateTable();
            Code.EndurPowershellUtil.PSObjectsToTable(table, this._objects, this.Properties);

            WriteObject(table);
        }
    }
}