﻿using System;
using System.Collections.Generic;
using System.Management.Automation;
using Gazprom.MT.PowerShell.Endur.Code;
using Olf.Openrisk.Staticdata;
using Olf.Openrisk.Table;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsCommon.New, "EndurDocument")]
    public class NewEndurDocumentCmdlet : EndurCmdletBase
    {
        private List<long> _eventIds;

        [Parameter(Mandatory = true, Position = 0)]
        public String Definition { get; set; }

        [Parameter(Mandatory = true, Position = 1, ValueFromPipeline = true)]
        public long[] EventIds { get; set; }

        [Parameter(Position = 2)]
        public String Status { get; set; }

        public NewEndurDocumentCmdlet()
        {
            this.Status = "Draft";
        }

        protected override void BeginProcessing()
        {
            this._eventIds = new List<long>();
        }

        protected override void ProcessRecord()
        {
            this._eventIds.AddRange(EventIds);
        }

        protected override void EndProcessing()
        {
            using (var dc = new DisposableCollection())
            {
                var stldocDefinitionId = this.Session.StaticDataFactory.GetId(EnumReferenceTable.StldocDefinitions, this.Definition);
                var documentStatusId = this.Session.StaticDataFactory.GetId(EnumReferenceTable.StldocDocumentStatus, this.Status);

                var args = dc.Add(this.Session.TableFactory.CreateTable());
                args.AddColumn("stldoc_def_id", EnumColType.Int);
                args.AddColumn("events", EnumColType.Table);
                dc.Add(args.AddRow());

                var events = dc.Add(this.Session.TableFactory.CreateTable());
                events.AddColumn("event_num", EnumColType.Long);
                events.AddColumn("next_doc_status", EnumColType.Int);

                args.SetTable("events", 0, events);

                args.SetInt("stldoc_def_id", 0, stldocDefinitionId);
                events.AddRows(this._eventIds.Count);
                events.SetColumnValues(0, this._eventIds.ToArray());
                events.SetColumnValues(1, documentStatusId);

                dc.Add(this.Session.ControlFactory.RunScript("StldocProcessDocs", args));
            }

            this.WriteObject(this._eventIds, true);
        }
    }
}