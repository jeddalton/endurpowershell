﻿namespace Gazprom.MT.PowerShell.Endur
{
    using System;
    using System.Management.Automation;

    using Gazprom.MT.PowerShell.Endur.Code;

    using Olf.Openrisk.Market;
    using Olf.Openrisk.Table;

    /// <summary>
    /// A Cmdlet to zero forward curve data
    /// </summary>
    [Cmdlet(VerbsCommon.Set, "ZeroEndurMarketData")]
    public class SetZeroEndurMarketDataCmdlet : EndurCmdletBase
    {
        [Parameter(Mandatory = true, Position = 0, HelpMessage = "Curve Name")]
        [ValidateNotNullOrEmpty]
        public String CurveName { get; set; }

        protected override void ProcessRecord()
        {
            using (var dc = new DisposableCollection())
            {
                var forwardCurve =
                    dc.Add((ForwardCurve)this.Session.Market.GetIndex(CurveName));

                forwardCurve.LoadUniversal();
                var gridPoints = forwardCurve.GridPoints;

                foreach (var gPoint in gridPoints)
                {
                    gPoint.GetField(EnumGptField.EffInput).SetValue(0.0);
                    gPoint.GetField(EnumGptField.UserInput).SetValue(0.0);
                    gPoint.GetField(EnumGptField.Close).SetValue(0.0);
                }
                forwardCurve.SaveUniversal();                
            }

        }
    }
}
