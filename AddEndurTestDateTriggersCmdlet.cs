namespace Gazprom.MT.PowerShell.Endur
{
    using System.Data.SqlClient;
    using System.Management.Automation;

    [Cmdlet(VerbsCommon.Add, "EndurTestDateTriggers")]
    public class AddEndurTestDateTriggersCmdlet : EndurTestDateTriggersCmdletBase
    {
        [Parameter]
        public SwitchParameter Force { get; set; }

        protected override void ProcessRecord()
        {
            var statements = GetTriggerStatements();

            foreach (var statement in statements)
            {
                var triggerName = GetTriggerName(statement);
                if (string.IsNullOrEmpty(triggerName))
                {
                    continue;
                }

                this.WriteVerbose(string.Format("Applying trigger {0}", triggerName));

                var triggerExists = this.GetTriggerExists(triggerName);

                if (triggerExists && !this.Force.IsPresent)
                {
                    this.WriteVerbose("Skipping trigger as it already exists");
                    continue;
                }

                if (triggerExists)
                {
                    this.WriteVerbose("Dropping trigger as it already exists");
                    this.DropTrigger(triggerName);
                }

                this.WriteVerbose(string.Format("Creating trigger {0}", triggerName));
                this.CreateTrigger(statement);
            }
        }

        private void CreateTrigger(string statement)
        {
            using (var connection = new SqlConnection(this.EndurConnectionString))
            {
                connection.Open();

                using (var command = new SqlCommand(statement, connection))
                {
                    command.ExecuteNonQuery();
                }
            }
        }
    }
}