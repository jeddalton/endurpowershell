﻿using System;
using Olf.Openrisk.Application;

namespace Gazprom.MT.PowerShell.Endur
{
    public static class SessionExtensions
    {
        public static void BeginTransaction(this Session session)
        {
            var ssqlPtr = session.GetSsqlPtr();
            Externals.syb_begin_tran_priv(ssqlPtr);
        }

        public static void CommitTransaction(this Session session)
        {
            var ssqlPtr = session.GetSsqlPtr();
            Externals.syb_commit_tran_priv(ssqlPtr);
        }

        public static void RollbackTransaction(this Session session)
        {
            var ssqlPtr = session.GetSsqlPtr();
            Externals.syb_rollback_tran_priv(ssqlPtr);
        }

        public static IntPtr GetSsqlPtr(this Session session)
        {
            return Externals.get_ssql(0);
        }

        public static long GetNewWorkflowJobId(this Session session)
        {
            return session.GetUniqueNumber("UNIQUE_WFLOW_JOB_ID");
        }

        private static long GetUniqueNumber(this Session session, string description)
        {
            var ssqlPtr = session.GetSsqlPtr();
            return Externals.get_unique_num(ssqlPtr, description);
        }

        public static int StartWorkflow(this Session session, string workflowName, int sequence)
        {
            return Externals.WFLOW_StartWorkflow(workflowName, sequence);
        }

        public static void SendJobCfgChanged(this Session session)
        {
            Externals.send_job_cfg_changed_nfy(1);
        }

        public static void RefreshCache(this Session session)
        {
            const string avs = "{UTIL_RefreshCache(\"SESSION\", \"ENTIRE_CACHE\");}";

            using (session.ControlFactory.RunScriptText(avs))
            {
            }
        }
    }
}