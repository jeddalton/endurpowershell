using System.Management.Automation;

namespace Gazprom.MT.PowerShell.Endur
{
    [Cmdlet(VerbsCommon.Get, "ObjectType")]
    public class GetObjectTypeCmdlet : Cmdlet
    {
        [Parameter(Mandatory = true, Position = 1, ValueFromPipeline = true)]
        public PSObject InputObject { get; set; }

        protected override void ProcessRecord()
        {
            WriteObject(InputObject.BaseObject.GetType().ToString());
        }
    }
}